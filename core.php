<?php
/*if (substr($_SERVER['HTTP_HOST'],0,3) != 'www') {
   header('HTTP/1.1 301 Moved Permanently');
   header('Location: http://www.'.$_SERVER['HTTP_HOST']
	 .$_SERVER['REQUEST_URI']);
}*/

/**
 * @Author: Dusten Smith
 * @Description: Core Library
 */
if (!isset($_SESSION))
{
    session_start();
}

/**
 * Define Directory Structure
 */
define('ROOT_DIR',		$_SERVER['DOCUMENT_ROOT'].'/');
define('DIRCORE',		ROOT_DIR.'core/');
define('DIRMODULES',	DIRCORE.'modules/');	// Project Documents
define('DIRCONFIG',     ROOT_DIR.'config/');		        // Core Configuration
define('DIRIMAGES',		DIRCORE.'images/');		// Core Images
define('DIRTHEMES',     ROOT_DIR.'themes/');

/**
 * Load Configuration
 */
Core_Configuration::getRegistry(DIRCONFIG."config.ini.php");

/**
 * Load Class Objects
 */
// Autoload
function __autoload($className){
   $className = strtolower($className);
   if (!class_exists($className)){
      $classfile = DIRMODULES.'class.'.$className.'.php';
      if (is_file($classfile)){
	 require_once( $classfile );
      }
   }
}


/**
 * Setup Site
 */
require_once( DIRCONFIG."configuration.php"); 		// Global Settings
//if($_SERVER['PHP_SELF'] != '/admin/login/login.php')
//{
//   require_once("nav3.php");
//}

?>
