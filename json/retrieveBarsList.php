<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jstevens
 * Date: 1/25/13
 * Time: 10:13 PM
 * To change this template use File | Settings | File Templates.
 */



require_once '../scripts/app_config.php';



$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);

if (mysqli_connect_errno()) {
    createErrorAndExit('error', mysqli_connect_error(), $mysqli);
}

if (isset($_POST['maxLat'])
        && isset($_POST['minLat'])
        && isset($_POST['maxLng'])
        && isset($_POST['minLng'])
        && isset($_POST['lat'])
        && isset($_POST['lng'])) {

    $maxLat = $_POST['maxLat'];
    $minLat = $_POST['minLat'];
    $maxLng = $_POST['maxLng'];
    $minLng = $_POST['minLng'];
    $lat = $_POST['lat'];
    $lng = $_POST['lng'];
}
else {
    createErrorAndExit('error', "Post vars were bad or incomplete.", $mysqli);
}


$number_of_seconds_before_feed_is_stale = 500;

$sql = "
	    SELECT
	    barID as id,
	    barName as name,
	    barLat as lat,
	    barLon as lng
	    FROM bars
	    WHERE barActive = '1'
	    and barID != 55411
	    AND barLat > $minLat
        AND barLat < $maxLat
        And barLon > $minLng
        AND barLon < $maxLng
	    ";


$data = array();

if ($result = $mysqli->query($sql)) {
    
    $data['success'] = true;
    $results = array();
    $id=0;
    
    $liveFeedsSQL = "SELECT barID from liveFeeds l where l.barID = ? AND (l.timestamp > (UNIX_TIMESTAMP() - $number_of_seconds_before_feed_is_stale))";
    if (!($feedStmt = $mysqli->prepare($liveFeedsSQL))){
        createErrorAndExit('feed_prepare_error', $mysqli->error, $mysqli);
    }

    if (!$feedStmt->bind_param('i', $id)) {
        createErrorAndExit('feed_bind_error', $feedStmt->error, $mysqli);
    }

    $virtualTourSQL = "SELECT barID from barVirtualTours where barID = ?";
    if (!($virtualTourStmt = $mysqli->prepare($virtualTourSQL))) {
        createErrorAndExit('vt_prepare_error', $mysqli->error, $mysqli);
    }

    if (!$virtualTourStmt->bind_param('i', $id)) {
        createErrorAndExit('vt_bind_error', $virtualTourStmt->error, $mysqli);
    }

    $i = 0;
    while ($row = $result->fetch_assoc()) {
        $i++;
        $row['test_id'] = (int)$row['id'];
        $id = (int) $row['id'];

        if (!$feedStmt->execute()) {
            createErrorAndExit('feed_execute_error', $feedStmt->error, $mysqli);
        }
        $feedStmt->store_result();
        $row['hasLiveFeed'] = $feedStmt->num_rows;
        $feedStmt->free_result();

        if (!$virtualTourStmt->execute()) {
            createErrorAndExit('vt_execute_error', $virtualTourStmt->error, $mysqli);
        }
        $virtualTourStmt->store_result();
        $row['hasVirtualTour'] = $virtualTourStmt->num_rows;
        $virtualTourStmt->free_result();
        array_push($results, $row);
    }

    $data['results'] = $results;
    $data['count'] = $i;

    $feedStmt->close();
    $virtualTourStmt->close();
    $result->close();
}
else {
    $data['success'] = false;
    $data['results'] = $mysqli->errno;
    $data['post'] = array('lat'=>$lat, 'lng'=>$lng, 'maxLat'=>$maxLat, 'minLat'=>$minLat, 'maxLng'=>$maxLng, 'minLng'=>$minLng);
}

echo json_encode($data);

$mysqli->close();

function createErrorAndExit($error_name, $error_string, $mysqli) {
    $data['success'] = false;
    $data[$error_name] = $error_string;
    $data['results'] = array();
    echo json_encode($data);
    mysqli_close($mysqli);
    exit();
}
