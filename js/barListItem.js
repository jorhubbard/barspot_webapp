/**
 * Created with JetBrains PhpStorm.
 * User: jstevens
 * Date: 2/2/13
 * Time: 3:09 PM
 * To change this template use File | Settings | File Templates.
 */


var BarListItem = function BarListItem(data) {
    if (data) {
        if (data.id)
            this.id = data.id;
        if (data.name)
            this.name = data.name;
        if (data.lat)
            this.lat = data.lat;
        if (data.lng)
            this.lng = data.lng;

        this.hasLiveFeed = Boolean(data.hasLiveFeed);
        this.hasVirtualTour = Boolean(data.hasVirtualTour);

        if (data.googleId)
            this.googleId = data.googleId;
    }
};

BarListItem.prototype.id = 0;
BarListItem.prototype.name = "";
BarListItem.prototype.lat = 0;
BarListItem.prototype.lng = 0;
BarListItem.prototype.hasLiveFeed = false;
BarListItem.prototype.hasVirtualTour = false;
BarListItem.prototype.googleId = 0;

BarListItem.prototype.getListHtml = function() {
    var html = "<div class='barListItem'>\n";

    if (this.hasLiveFeed) {
        html += "    <img src='/video/clients/" + this.id + "/frameDrop.jpg'/>\n";
    }
    else if (this.hasVirtualTour) {
        html += "    <img src='/video/clients/" + this.id + "/demoFrameDrop.jpg'/>\n";
    }

    html += "    <p>" + this.name + "</p>\n" +
        "    <a href='/review.php?id=" + this.id + "'>VIEW BAR</a>\n" +
        "</div>\n";

    return html;
};
