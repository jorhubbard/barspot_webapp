/**
 * Created with JetBrains PhpStorm.
 * User: jstevens
 * Date: 1/7/13
 * Time: 2:14 PM
 * To change this template use File | Settings | File Templates.
 */

// Map Object
var map;
var places;

// Markers/Icon properties
var icon;
var clickShape;
var infoWindow;



function mapInitialize(lat, lng) {
    console.log("mapInitialize");

    icon = new google.maps.MarkerImage();
    icon.anchor = google.maps.Point(11, 38);
    icon.origin = google.maps.Point(0, 0);
    icon.size = new google.maps.Size(22, 40);
    icon.url = "../images/hbsPin.gif";

    clickShape = new Object();
    clickShape.type = "rect";
    clickShape.coord = [0, 15, 22, 29];
    
    infoWindow = new google.maps.InfoWindow();

    var mapOptions = {
        center: new google.maps.LatLng(lat, lng),
        zoom: 14,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };


    map = new google.maps.Map(document.getElementById("map"), mapOptions);
    google.maps.event.addListener(map, 'bounds_changed', mapBoundsChanged_handler);

    getBarsList(map.getBounds());
}

function mapBoundsChanged_handler() {
    console.log('mapBoundsChanged');

    getBarsList(map.getBounds());
}

function getBarsList(bounds) {
    console.log('getBarsList');
    console.log(bounds);
    
    if (bounds) {
        if (!bounds.isEmpty()) {
            console.log("not empty");
            var center = bounds.getCenter();
            var northEast = bounds.getNorthEast();
            var southWest = bounds.getSouthWest();
            console.log(center);
            console.log(northEast);
            console.log(southWest);
            
            $.ajax({
                type: "POST",
                dataType: "json",
                data: {
                    lat: center.lat(),
                    lng: center.lng(),
                    maxLat: northEast.lat(),
                    maxLng: northEast.lng(),
                    minLat: southWest.lat(),
                    minLng: southWest.lng()
                },
                url: 'json/retrieveBarsList.php',
                success: retrieveBars_success,
                error: function() {
                    console.log('error');
                }
            });
        }
    }
    
    //getGooglePlacesBars(bounds);
}

function retrieveBars_success(data, status) {
    console.log(status);
    console.log(data);

    setMarkersForBarData(data.results);
}

/**
 * Until Google places is less experimental or at least gives more results,
 * this code will wait
 */
function getGooglePlacesBars(bounds) {
    var request = {
        bounds: bounds,
        types: ['bar'],
        query: "bar"
    };

    console.log(request);
    places = new google.maps.places.PlacesService(map);
    places.textSearch(request, barSearch_resultsHandler);
}


function barSearch_resultsHandler(results, status) {
    console.log("barSearch_resultsHandler");
    if (status == google.maps.places.PlacesServiceStatus.OK) {
        console.log(results);
    }
    else {
        console.log(status);
    }
}

var markers = {};
var listItems = {};

function setMarkersForBarData(bars) {
    console.log('setMarkersForBarData');
    var i = 0, i_limit = bars.length, bar;
    for (i; i < i_limit; ++i)
    {
        bar = bars[i];
        console.log(bar);
        if (markers[bar.id] == undefined) {
            console.log(markers);
            markers[bar.id] = createBarspotMarkerFor(bar);
        }
        else { console.log('already marked');}

        if (listItems[bar.id] == undefined) {
            listItems[bar.id] = createListItemFor(bar);
        }
        else { console.log('already in list');}
    }
}


function createBarspotMarkerFor(bar) {
    console.log('createBarspotMarkerFor');
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(bar.lat, bar.lng),
        map: map,
        icon: icon,
        shape: clickShape,
        title: bar.name
    });

    marker.barId = bar.id;

    google.maps.event.addListener(marker, 'click', function() {
        
        infoWindow.setContent(getInfoWindowContentFor(bar));
        infoWindow.open(map, marker);
    });

    return marker;
}

function getInfoWindowContentFor(bar) {
    var content = "<div id='infoWindowContent'>" +
        "<p>" + bar.name + "</p>" +
        "</div>";
    return content;
}

function createListItemFor(bar) {
    console.log('createListItemFor');
    var marker = markers[bar.id.toString()];
    var listItem = new BarListItem(bar);
    var listItemElement = $(listItem.getListHtml());
    listItemElement.click(function() {
        map.setCenter(marker.getPosition());
        infoWindow.setContent(getInfoWindowContentFor(bar));
        infoWindow.open(map, marker);
        $(".selected").removeClass("selected");
        listItemElement.addClass("selected");
    });
    $('#barList').append(listItemElement);
    return listItem;
}