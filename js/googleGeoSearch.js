/**
 * Created with JetBrains PhpStorm.
 * User: jstevens
 * Date: 1/19/13
 * Time: 3:16 PM
 * To change this template use File | Settings | File Templates.
 */

var geocoder;


function translateGeoLocation(zip, city, state) {
    console.log("translateGeoLocation");
    if (zip || city || state) {
        searchForGeoLocation(zip, city, state);
    }
    else if (navigator && navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(geolocation_successHandler, geolocation_errorHandler);
    }
    else {
        alert("whoops, no values to search");
    }
}

function searchForGeoLocation(zip, city, state) {
    console.log("searchForGeoLocation");
    var request = {};
    request.address = "";
    var address = [];
    if (city != null) { address.push(city); }
    if (state != null) { address.push(state); }
    if (zip != null && zip > 0) { address.push(zip); }
    request.address += address.pop();
    while (address.length > 0) {
        request.address += "+" + address.pop();
    }
    console.log(request.address);

    geocoder = new google.maps.Geocoder();
    geocoder.geocode(request, geocode_successHandler);
}

function geocode_successHandler(result, status) {
    console.log("geocode_successHandler-" + status);
    if (status == google.maps.GeocoderStatus.OK) {
        console.log(result);
        var lat = result[0].geometry.location.lat();
        var lng = result[0].geometry.location.lng();
        console.log(lat + ", " + lng);
        mapInitialize(lat, lng);
    }
    else if (status == google.maps.GeocoderStatus.ZERO_RESULTS
        || status == google.maps.GeocoderStatus.INVALID_REQUEST) {
        displayError("Unable to find what you're looking for, please try to adjust your search criteria.");
    }
    else if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT
        || status == google.maps.GeocoderStatus.REQUEST_DENIED) {
        // TODO: Big time error, needs immediate attention
    }
    else {
        console.log("else");
    }
}

function geolocation_successHandler(position) {
    mapInitialize(position.coords.latitude, position.coords.longitude);
}

function geolocation_errorHandler(error) {
    if (error.code == 1) {
        displayError('The user denied the request for location information.');
    }
    else if (error.code == 2) {
        displayError('Your location information is unavailable.')
    }
    else if (error.code == 3) {
        displayError('The request to get your location timed out.')
    }
    else {
        displayError('An unknown error occurred while requesting your location.')
    }
}

function displayError(message) {
    $("#message").append('<p>' + message + '</p>');
}