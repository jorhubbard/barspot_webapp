# get property vars
. ./script_properties.sh

echo $BS_WEB_ROOT

# Copy project to development environment
rsync -e ssh --filter='merge .rsync_filter_dev' --delete-after -av $BS_WEB_ROOT root@app01.hotbarspot.com:/var/dev