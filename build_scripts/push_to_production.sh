# get property vars
. ./script_properties.sh

echo $BS_WEB_ROOT

echo "Do you really want to push to Production?"

select yn in "Yes" "No"; do
    case $yn in
        Yes ) rsync -e ssh --filter='merge .rsync_filter_production' --delete-after -av $BS_WEB_ROOT root@app01.hotbarspot.com:/var/www; break;;
        No ) exit;;
    esac
done
