<?php
include '../../html/header.html.php';
?>
<div align="right"><img src="../../images/barspotshuffle_banner.png" alt="barspot shuffle" title="barspot shuffle"  /></div>

<div id="colOne" style="position:relative; z-index:100; top:0px">
<img src="../../images/PartyBusPhotos.jpg" />

<p style="padding-right:20px;"><font style="font-size: 15px; color: #A8A8A8; font-style: normal; font-family: Arial, Helvetica, sans-serif;"><strong>Go from bar to bar on the coolest party bus in Columbus. Marshall's Party Bus will make its way on a cicuite to 5 bars. Hop on and ride in style to the next bar. While in transit grab a drink, or dance to the music and light&nbsp;show.</strong></font></p>

<br/><br/>
<p><font style="font-size: 25px; color: #A31D21; font-style:normal; font-family: 'Arial Black', Gadget, sans-serif;"><strong>Marshall's Party Bus</strong></font><br />
<font style="font-size: 16px; color: #A8A8A8; font-style:normal; font-family: 'Arial Black', Gadget, sans-serif;"><strong>Check out the Ride!</strong></font></p>
<iframe style="padding-left:20px;" width="480" height="360" src="http://www.youtube.com/embed/fm-j9s7EmjY" frameborder="0" allowfullscreen></iframe>


<br/><br/><br/><br/>
<p><font style="font-size: 25px; color: #A31D21; font-style:normal; font-family: 'Arial Black', Gadget, sans-serif;"><strong>Bus Tour Map</strong></font><br />
<font style="font-size: 16px; color: #A8A8A8; font-style:normal; font-family: 'Arial Black', Gadget, sans-serif;"><strong>Route Updates Tweeted Live @barspot_TV</strong></font></p>
<p style="padding-left:45px;">
<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps/ms?t=m&amp;msa=0&amp;msid=200054922527703316008.0004d6808efa8a5b2397b&amp;source=embed&amp;ie=UTF8&amp;ll=39.986558,-83.025227&amp;spn=0.028179,0.05064&amp;output=embed"></iframe><br /><small>View <a href="https://maps.google.com/maps/ms?t=m&amp;msa=0&amp;msid=200054922527703316008.0004d6808efa8a5b2397b&amp;source=embed&amp;ie=UTF8&amp;ll=39.986558,-83.025227&amp;spn=0.028179,0.05064" style="color:#0000FF;text-align:left">Barspot Shuffle 4-12-13</a> in a larger map</small></p>

<p style="width:525px;"><font style="font-size: 10px; color: #A8A8A8; font-style: normal; font-family: Arial, Helvetica, sans-serif;">You must present your I.D. and barspot shuffle ticket at time of boarding. The Marshall's party bus is first come first serve. Riders may ride multiple stops only if there is room and no new riders want to get on. Everyone will unload at each stop. If riders don't want to wait for the Marshall's party bus, they may take the Buckeye Vodka express line which will run in the opposit direction. Locations of the party bus and Buckeye Vodka bus will be live tweeted @barspot_TV.</font></p>
</div> 

<div id="colTwo" style="position:relative; background-color:#F0F0F0; margin-right:25px; z-index:100; top:0px">
<p style="padding-left:20px;"><font style="font-size: 23px; color: #A31D21; font-style: italic; font-family: 'Arial Black', Gadget, sans-serif;"><strong>5 BAR BUS TOUR $20</strong></font><br />
<font style="font-size: 22px; color: #A8A8A8; font-style:normal; font-family: 'Arial Black', Gadget, sans-serif;"><strong>4/12/13 | 7pm-Midnight</strong></font></p>

<p style="padding-top:15px; padding-left:20px"><font style="font-size: 18px; color: #A8A8A8; font-style:normal; font-family: 'Arial Black', Gadget, sans-serif;"><strong>Tour Stops:</strong></font><br />
<font style="font-size: 16px; color: #A8A8A8; font-style: normal; font-family: Arial, Helvetica, sans-serif;"><strong>
<ul>
<li>Marshall’s</li>
<li>Bryne’s Pub</li>
<li>King Avenue 5</li>
<li>The Shrunken Head</li>
<li>Zeno’s</li></ul> </strong></font></p> 
  
<p style="padding-left:20px;"><font style="font-size: 18px; color: #A8A8A8; font-style:normal; font-family: 'Arial Black', Gadget, sans-serif;"><strong>After Party at Zeno's</strong></font><br />
<font style="font-size: 16px; color: #A8A8A8; font-style: normal; font-family: Arial, Helvetica, sans-serif;"><strong>Midnight-2am</strong></font></p>

<p style="padding-left:20px;"><font style="font-size: 18px; color: #A8A8A8; font-style:normal; font-family: 'Arial Black', Gadget, sans-serif;"><strong>Free Buckeye Vodka Shot</strong></font><br />
<font style="font-size: 16px; color: #A8A8A8; font-style: normal; font-family: Arial, Helvetica, sans-serif;"><strong>Redeem on the Party Bus</strong></font></p>

<p style="padding-top:15px; padding-left:20px"><font style="font-size: 25px; color: #A31D21; font-style: italic; font-family: 'Arial Black', Gadget, sans-serif;"><strong>Get Your Tickets</strong></font><br />
<font style="font-size: 14px; color: #A8A8A8; font-style:normal; font-family: 'Arial Black', Gadget, sans-serif;"><strong>Limited Tickets Available</strong></font></p>

<div style="padding-left:20px;"><img src="../../images/shuffletickets.png" style="margin-right:20px;" /><a style="vertical-align:top; margin-top:15px;" class="wepay-widget-button wepay-green" id="wepay_widget_anchor_5121b1f7ea0c7" href="https://www.wepay.com/events/108476">Buy Now</a><script type="text/javascript">var WePay = WePay || {};WePay.load_widgets = WePay.load_widgets || function() { };WePay.widgets = WePay.widgets || [];WePay.widgets.push( {object_id: 108476,widget_type: "event",anchor_id: "wepay_widget_anchor_5121b1f7ea0c7",widget_options: {button_text: "Buy Now"}});if (!WePay.script) {WePay.script = document.createElement('script');WePay.script.type = 'text/javascript';WePay.script.async = true;WePay.script.src = 'https://static.wepay.com/min/js/widgets.v2.js';var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(WePay.script, s);} else if (WePay.load_widgets) {WePay.load_widgets();}</script></div>
<p style="padding-left:20px;">
<font style="font-size: 14px; color: #A8A8A8; font-style:normal; font-family: 'Arial', Gadget, sans-serif;">Or purchase at participating bars.</font></p>
</div>


<div id="colTwo" style="position:relative; background-color:#F0F0F0; margin-right:25px; z-index:100; top:20px">
<p style="padding-left:20px;"><font style="font-size: 25px; color: #A31D21; font-style: italic; font-family: 'Arial Black', Gadget, sans-serif;"><strong>Shuffle FAQ:</strong></font></p>
<p style="padding-left:20px;"><font style="font-size: 14px; color: #A8A8A8; font-style:normal; font-family: 'Arial Black', Gadget, sans-serif;"><strong>How does the shuffle work?</strong></font><br/>
<font style="font-size: 12px; color: #A8A8A8; font-style:normal; font-family: 'Arial', Gadget, sans-serif;">Patrons will take Marshall's party bus from bar to&nbsp;bar.</font></p>

<p style="padding-left:20px;"><font style="font-size: 14px; color: #A8A8A8; font-style:normal; font-family: 'Arial Black', Gadget, sans-serif;"><strong>How many people does the bus&nbsp;hold?</strong></font><br/>
<font style="font-size: 12px; color: #A8A8A8; font-style:normal; font-family: 'Arial', Gadget, sans-serif;">The bus holds between 25-30&nbsp;people.</font></p>

<p style="padding-left:20px;"><font style="font-size: 14px; color: #A8A8A8; font-style:normal; font-family: 'Arial Black', Gadget, sans-serif;"><strong>What if more than 30 want to ride the&nbsp;bus?</strong></font><br/>
<font style="font-size: 12px; color: #A8A8A8; font-style:normal; font-family: 'Arial', Gadget, sans-serif;">The bus is first come, first serve. We will unload and load the bus at each stop.  Patrons wanting to ride to more than one stop at a time will be subject to being&nbsp;bumped.</font></p>

<p style="padding-left:20px;"><font style="font-size: 14px; color: #A8A8A8; font-style:normal; font-family: 'Arial Black', Gadget, sans-serif;"><strong>We just missed the bus what do we&nbsp;do?</strong></font><br/>
<font style="font-size: 12px; color: #A8A8A8; font-style:normal; font-family: 'Arial', Gadget, sans-serif;">Buckeye Vodka will be running an express line in the opposite direction. You can jump on and hit another bar without much&nbsp;delay.</font></p>


<p style="padding-left:20px;"><font style="font-size: 14px; color: #A8A8A8; font-style:normal; font-family: 'Arial Black', Gadget, sans-serif;"><strong>How do I know where the buses will&nbsp;be?</strong></font><br/>
<font style="font-size: 12px; color: #A8A8A8; font-style:normal; font-family: 'Arial', Gadget, sans-serif;">We will post tour schedules that we will try our best to stick to.  For live updates check&nbsp;@Barspot_TV.</font></p>


<p style="padding-left:20px;"><font style="font-size: 14px; color: #A8A8A8; font-style:normal; font-family: 'Arial Black', Gadget, sans-serif;"><strong>Where do I start my shuffle? </strong></font><br/>
<font style="font-size: 12px; color: #A8A8A8; font-style:normal; font-family: 'Arial', Gadget, sans-serif;">Your shuffle begins where you buy your ticket, if you buy one online, we will tell you where to&nbsp;start.</font></p>
<p>&nbsp;</p>
</div>


<?php include '../../html/footer.html.php'; ?>
