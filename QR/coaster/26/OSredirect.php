<?php

ob_start();
echo $_SERVER['HTTP_USER_AGENT'];
$iphone = strpos($_SERVER['HTTP_USER_AGENT'],"iPhone");
$android = strpos($_SERVER['HTTP_USER_AGENT'],"Android");
$palmpre = strpos($_SERVER['HTTP_USER_AGENT'],"webOS");
$berry = strpos($_SERVER['HTTP_USER_AGENT'],"BlackBerry");
$ipod = strpos($_SERVER['HTTP_USER_AGENT'],"iPod");

//get current page url

function curPageURL()
{
$pageURL = 'http';
if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
$pageURL .= "://";
if ($_SERVER["SERVER_PORT"] != "80")
{
$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
} else
{
$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
}
return $pageURL;
}

$url = curPageURL(); // Gives the function a temporary variable
$mobileapple = str_replace('http://www.hotbarspot.com/QR/coaster/26/OSredirect.php', 'http://itunes.apple.com/us/app/hotbarspot/id381980269?mt=8', $url); // Allows the returned url from curPageURL to be replaced with the redirect URL for apple devices 
$mobiledroid = str_replace('http://www.hotbarspot.com/QR/coaster/26/OSredirect.php', 'https://play.google.com/store/apps/details?id=com.horn.android.hbs&feature=search_result#?t=W251bGwsMSwxLDEsImNvbS5ob3JuLmFuZHJvaWQuaGJzIl0.', $url); // Allows the returned url from curPageURL to be replaced with the redirect URL for droid devices 

// phone OS redirects

if ($iphone || $ipod == true)
{
header('Location:' . $mobileapple);
//OR
echo "<script>window.location='" . $mobileapple . "'</script>";
} else
{
ob_end_clean();
}


if ($android || $palmpre || $berry == true)
{
header('Location:' . $mobiledroid);
//OR
echo "<script>window.location='" . $mobiledroid . "'</script>";
} else
{
ob_end_clean();
}

?>
