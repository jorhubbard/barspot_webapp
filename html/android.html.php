<?php include 'header.html.php'; ?>

<div id="colOne">
    <div id="androidapp">
        <h1>Application Information</h1>
        <p>
            We are currently working on our android application.  As we add new features we will republish the app to our site here.  Currently the application will show you a list of all of the bars that are currently streaming and allow you to view their live video feed.  Features soon to come include:<br />
        <ul>
            <li>Directions to bars</li>
            <li>Automatically order based on distance from your location</li>
            <li>Display of how far you are from the bars you are viewing</li>
        </ul>

        <br />
        <br />
        Some known issues so far:
        <ul>
            <li>Bar list does not refresh when you go back from viewing a bar profile page, currently you have to exit the application</li>
            <li>The codecs the video's are using are not supported by all phones.  If you get an invalid video type, that is why.  Please look for updates that will soon address this.</li>
        </ul>
        <br />
        <br />
        If you would like to report any issues or request features, please send an email to <a href="mailto:support@hotbarspot.com">support@hotbarspot.com</a>
        </p>
    </div><!-- end localBars-->
    <br>
</div><!-- end col1 -->

<div id="colTwo">
    <div id="barInfoFront">
        <h1>Download our BETA Android App</h1>
        <div id="barInfoFrontWrap">
            <p>
                You will need to allow "Unknown sources" to install this BETA application.  To allow unknown sources, go to settings, applications, and checkmark the Unknown sources box. <br />
            <center><a href='/downloads/hotbarspot.apk'><img src='/images/downloadnow.jpg' /></a>

            </p>
        </div><!-- bar info front wrap -->

        <!-- add bar to profile -->
        <br style="clear: both";>
        <!-- visit bars website -->
        <div id="visitWebsite">
        </div><!-- end visit website -->
        <br style="clear: both";>
    </div><!-- end barInfo -->
</div><!-- end col2 -->

<?
include 'footer.html.php';
?>
