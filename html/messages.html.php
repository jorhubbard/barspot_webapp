<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jstevens
 * Date: 2/3/13
 * Time: 8:05 PM
 * To change this template use File | Settings | File Templates.
 */


function displayMessage($message, $messageType) {
    echo "<div class='{$messageType}'>\n";
    echo "    <p>{$message}</p>\n";
    echo "</div>\n";
}

echo "<div id='messages'>\n";

if (!is_null($success_message) && strlen($success_message) > 0) {
    displayMessage($success_message, SUCCESS_MESSAGE);
}

if (!is_null($error_message) && strlen($error_message) > 0) {
    displayMessage($error_message, ERROR_MESSAGE);
}

echo "</div>\n\n";