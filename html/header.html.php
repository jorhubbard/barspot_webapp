<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Welcome to Barspot.tv</title>
    <meta name="Keywords" content="" />
    <meta name="Description" content="" />
    <link href="/css/Template.css" rel="stylesheet" type="text/css" />
    <link href="/css/carousel.css" rel="stylesheet" type="text/css" />
    <link href="/css/screen.css" rel="stylesheet" type="text/css" media="screen" />

    <?php
    if (isset($javascript)) {
        print $javascript;
    }
    ?>
</head>
<body <?php if(isset($bodyParams) ){print $bodyParams; }?>>

<!-- Google Analytics tracking code -->
<?php include_once "analyticstracking.html.php" ?>

<div id="wrapper">
    <div id="header">
        <form action='/index.php' method='post'>
            <div id="searchhint"><img src="/images/SearchHint.png" alt="Look for Bars, Citys or Zipcodes" /></div>
            <div id="search"><input type="text" value="Zip Code" size="20" name="search" alt="search" style="width: 160px; color: rgb(128, 128, 128); font-style: normal;" onfocus="if(this.value=='Zip Code')this.value=''"></div>
            <div id="searchbutton"><input type="image" src="/images/findbutton.png" alt="Submit" value="FIND!" style="font-family:Arial, Helvetica, sans-serif; font-weight:bold;" /></div>
            <div id="leftnavside"><img src="/images/LeftNavSide.png" /></div>
            <div id="rightnavside"><img src="/images/RightNavSide.png" /></div>
        </form>
    </div>
    <?php
    include 'nav.html.php';
    ?>
    <div id="content">
        <div id="SecondaryNav"></div>
