<?php

include 'header.html.php';

if ((isset($success_message) && $success_message != NULL)
    || (isset($error_message) && $error_message != NULL)) {
    include 'messages.html.php';
}

echo $content;

include 'footer.html.php';