<?PHP
/* Config */
	require_once "../../core.php";

   
include '../connect.php';

ob_start();
/* Login */
	if(!$login->is_logged_in()){ HTTP::redirect_to_url("login.php"); 	}


if(isset($_POST['newstatus']))
{
   $date = date( 'Y-m-d H:i:s');
   $sql = "INSERT INTO status_tbl VALUES('','$_SESSION[userid]','$date','".quote_smart($_POST['newstatus'])."');";
   //print("sql = $sql");
   $result = mysql_query($sql); //or die("Invalid query: " . mysql_error());

}

$searchResults = "";
if(isset($_POST['searchTerms']))
{
   if($_POST['searchType'] == '1') //bands
   {
      $sql = "
      SELECT userId
      FROM user_tbl
      WHERE userTypeId = '".quote_smart($_POST['searchType'])."'
      AND 
      {
	 (username LIKE '%".quote_smart($_POST['searchTerms'])."%')
	 OR
	 (userEmail LIKE '%".quote_smart($_POST['searchTerms'])."%')
      )
      ORDER BY username
      ;
      ";
   }

   if($_POST['searchType'] == '4') //people
   {
      $sql = "
      SELECT u.userId as userId,
      CONCAT(userFirst, ' ' , userLast) AS name,
      userEmail

      FROM user_tbl AS u
      LEFT JOIN user_info_tbl AS ui ON u.userId = ui.userId
      WHERE #userTypeId = '".quote_smart($_POST['searchType'])."'
      #AND 
      (
	 (username LIKE '%".quote_smart($_POST['searchTerms'])."%')
	 OR
	 (userEmail LIKE '%".quote_smart($_POST['searchTerms'])."%')
	 OR
	 (CONCAT(userFirst, ' ' , userLast) LIKE '%".quote_smart($_POST['searchTerms'])."%')
      )
      ORDER BY username
      ;
      ";
      //print("sql = $sql<br />");
      $result = mysql_query($sql);
      
      if($row = mysql_fetch_array($result))
      {
	 do
	 {
	    $searchResults .="<a href='/user.php?id=$row[userId]'><font color='#FFFFFF'>".stripslashes($row['name'])."</font></a><br /><hr />";
	 
	 }while($row = mysql_fetch_array($result));
      }
   }

   if($_POST['searchType'] == '2') //bars
   {
      $sql = "
      SELECT 
      barID, 
      barName,
      barAddress,
      barZip,
      barPhone,
      barWebsite
      FROM bars 
      LEFT JOIN zipcode ON barZip = zipcode
      WHERE barName LIKE '%".quote_smart($_POST['searchTerms'])."%'
      AND barActive = '1'
      ORDER BY zipCodeState ASC, zipCodeCity ASC;
      ";
   
      $result = mysql_query($sql);

      if($row = mysql_fetch_array($result))
      {
	 do
	 {
	    $searchResults .="<a href='/review.php?id=$row[barID]'><font color='#FFFFFF'>$row[barName]</font></a> - $row[barAddress] - $row[barPhone]<br />$row[barWebsite]<hr />";
	 
	 }while($row = mysql_fetch_array($result));
      }
   }


}

if(!empty($_GET['removeBar'])) //remove subscribed bar
{
   $sql = "SELECT userID, userSubscribeID FROM userSubscribe WHERE userSubscribeBarID = '".quote_smart($_GET[removeBar])."' AND userID = '$_SESSION[userid]' LIMIT 1;";
//   print("sql = <pre>$sql</pre>");
   $result = mysql_query($sql);
   $row = mysql_fetch_array($result);
   if($row['userID'] == $_SESSION['userid'])
   {
      //$_SESSION[removeBarError] = "ready to remove bar";
      $sql = "DELETE FROM `userSubscribe` WHERE `userSubscribeID` = '$row[userSubscribeID]' LIMIT 1;";
      //print("sql = <pre>$sql</pre>");
      $result = mysql_query($sql)or die("Invalid query: " . mysql_error());;

      if($result)
      {
	 $_SESSION['removeBarError'] = "Your bar has been removed.";
      }
      else
      {
	 $_SESSION['removeBarError'] = "There was a problem removing the bar you selected.  Please try again.";
      }
   }
   else
   {
      $_SESSION['removeBarError'] = "The bar you are trying to remove was not found to be registered to your account.<br />";
   }
}
else
{
   $_SESSION['removeBarError'] = '';
}

function friendSuggest($friendIDs,$myBarIDs)
{
    $string = "";
   if(!empty($friendIDs) && !empty($myBarIDs) )
   {
      $friendIDs = rtrim(implode($friendIDs,','),',');
      $sql = "
      SELECT 
      userSubscribeID, 
      userSubscribeBarID, 
      us.userID, 
      userProfilePicImage,
      username,
      count(us.userID) AS count 
      FROM userSubscribe AS us
      LEFT JOIN user_tbl AS u ON us.userID = u.userId
      LEFT JOIN userProfilePic ON us.userId = userProfileUserID
      WHERE userSubscribeBarID IN($myBarIDs) 
      AND us.userID != '$_SESSION[userid]'
      AND us.userID NOT IN ($friendIDs)
      GROUP BY us.userID
      ORDER BY count DESC";

      //print("sql = <pre>$sql</pre>");
      $result = mysql_query($sql);

      if($row = mysql_fetch_array($result) )
      {
	 do
	 {
	    if(!empty($row['userProfilePicImage']))
	    {
	       $profileImage = "/images/uploaded/iphone/$row[userID]/$row[userProfilePicImage]";
	    }
	    else
	    {
	       $profileImage = '/themes/default/images/iphonenopic.jpg';
	    }
	    $string .="<div class='trueFriends'><a href='/user.php?id=$row[userID]'>$row[username]<br /><img src='$profileImage' border='0' alt='user picture' /></a></div>"; 
	 }while($row = mysql_fetch_array($result) );
      }
   }
   else
   {
      $string = "Before we can suggest some friends, you have to join your favorite bars and start adding friends.";
   }
   
   return $string;

}
function drawUserBackend($id,$searchResults)
{
   $sql = "SELECT statusValue, username, userFirst, userLast FROM user_tbl 
   LEFT JOIN user_info_tbl ON user_tbl.userId = user_info_tbl.userId 
   LEFT JOIN status_tbl ON user_tbl.userId = statusUserID
   WHERE user_tbl.userId = '$id'
   ORDER BY statusID DESC;
   ";

   $result = mysql_query($sql)or die("Invalid query: " . mysql_error());

   $row = mysql_fetch_array($result);
    $string = "";
if($_SESSION['usertype'] == 2) //bars
{
   $string .= drawBarBackend($_SESSION['userid'],$searchResults);
}
   $myBars = myBars($id);
   $friendIDs = getFriendsInfo();
   $friendIDs = $friendIDs[0];
   $friendSuggest = friendSuggest($friendIDs, $myBars[1]);
   $string .="
   <div> <!-- balance columns -->
      <div id='userLeft'>
      <div id='welcometopage'>
      <img src='/themes/default/images/welcometoyourpage.png' alt='Welcome to your page' /><br />
      </div> <!-- end welcome to page -->
      

      <div id='myBars'>
      <img src='/themes/default/images/mybars.png' alt='My Bars' />
      ".$myBars[0]."
      </div> <!-- end my bars -->
      
      <div id='friendSuggest'>
	 <img src='/themes/default/images/barbuddies.png' alt='Suggested Friends' />
	 <div id='friendSuggestInner'>
	 ".$friendSuggest."
	 </div> <!-- end friendSuggestInner -->
      </div> <!-- end friendSuggest -->

      
      


      <!-- end backendSearch -->



      </div> <!-- end userLeft -->


      <div id='userRight'>
      <div id='userTopInfo'>

	 username: <font size='5' color='#FF0000'><strong>$row[username]</strong></font><br />
	 name: ".stripslashes($row['userFirst'])." ".stripslashes($row['userLast'])." <br />
	 Age: <br />
	 Status: $row[statusValue]<br />
	 <span style='padding-right:40px;'><a href=\"user_edit_profile.php\"><font color='#FF0000'>edit profile</font></a></span>
	 <span style='padding-right:40px;'><a href='javascript: void(0)' onclick=\"showStatus()\"><font color='#FF0000'>change status</font></a></span>
	 <a href='/admin/napkins/?ref=cp'><font color='#FF0000'>napkins</font></a>
	 <br />
	 <span style='padding-right:20px;'><a href='friendrequests.php'><font color='#FF0000'>Friend Requests ".getNumberFriendRequests()."</font></a></span>
	 <span style='padding-right:60px;'><a href=\"login.php?logout=1\"><font color='#FF0000'>logout</font></a></span>
	 <span style='padding-right:40px;'><a href=\"user_edit_pass.php\"><font color='#FF0000'>change password</font></a></span>
	 <br />
	 <span style='padding-right:40px;'><a href='/admin/gallery/imageupload/uploadimage.php'><font color='#FF0000'>photo gallery</font></a></span>
	 <br />
	 <div id='changeStatus'></div>	 <!-- end change status -->
      </div> <!-- end user top info -->
      
      
      <div id='friendList'>
      <img src='/themes/default/images/friendslist.png' alt='Friends List' />
	 <div id='friendListInner'>
	 ".showFriends()."
	 </div> <!-- end friend list inner -->
      </div> <!-- end friend list -->
      
      ";

      //<div id='currentEvents'>
      //<img src='/themes/default/images/currentevents.png' alt='Current Events' />
//	 <div id='currentEventsInner'>
	 
//	 </div> <!-- end current events inner -->
//      </div> <!-- end current events -->
   $string .="

      
<!--      
      <div id='userChat'>
      <font color='#FFFFFF' size='+2'><strong> Chat Coming Soon</strong></font>
      </div> 
-->
      <!-- end user Chat -->

      
<!--
      <div id='searchResults'>
      <div id='searchBody'><h1><font color='#FFFFFF'>Search Results</font></h1>
      <font color='#FFFFFF'>$searchResults</font></div>
      </div> 
-->
      <!-- end search results -->

";
/*if(is_array($_SESSION['groupList']) && in_array('1',$_SESSION['groupList']) )
{
   $string .="<a href='manage_bars.php'>Bar info</a>";
}*/
$string .="


      </div> <!-- end userRight -->
      <div style=\"clear:both;\"></div>

   </div> <!-- balance columns -->
      ";

   return $string;

}
function getNumberFriendRequests()
{
   $sql = "SELECT COUNT(receivingID) as numFriends FROM user_friends 
   WHERE receivingID = '$_SESSION[userid]' 
   AND response = '2'
   GROUP BY receivingID 
   LIMIT 1;";
   $result = mysql_query($sql);
   $row = mysql_fetch_array($result);
   if(empty($row['numFriends']) )
   {  
      $numFriends = 0;
   }
   else
   {
      $numFriends = $row['numFriends'];
   }
   return "($numFriends)";

}
function getFriendsInfo()
{
   $sql = "
      SELECT userId, username, userProfilePicImage
      FROM user_friends 
      LEFT JOIN user_tbl ON requestingID = userId
      LEFT JOIN userProfilePic ON userId = userProfileUserID
      WHERE 
      requestingID != '$_SESSION[userid]'
      AND receivingID = '$_SESSION[userid]'
      AND response = '1'
      
      UNION
      (
	 SELECT userId, username, userProfilePicImage
	 FROM user_friends 
	 LEFT JOIN user_tbl ON receivingID = userId
	 LEFT JOIN userProfilePic ON userId = userProfileUserID
	 WHERE 
	 requestingID = '$_SESSION[userid]'
	 AND receivingID != '$_SESSION[userid]'
	 AND response = '1'

      )
      
      ORDER BY username DESC

   ";
   //print("sql = $sql<br />");
   $result=mysql_query($sql);
   $x = 0;
    $ids = array();
    $uns = array();
    $pp = array();
   if($row = mysql_fetch_array($result))
   {
      do
      {
	 $ids[$x] = $row['userId'];
	 $uns[$x] = $row['username'];
	 if(!empty($row['userProfilePicImage']))
	 {
	    $profileImage = "/images/uploaded/iphone/$row[userId]/$row[userProfilePicImage]";
	 }
	 else
	 {
	    $profileImage = '/themes/default/images/iphonenopic.jpg';
	 }
	 $pp[$x] = $profileImage;
	 $x++;
      }while($row = mysql_fetch_array($result));
   }
   $var = array($ids,$uns,$pp); //packages for return
   return $var;
}
function showFriends()
{
   $var = getFriendsInfo();
   $ids = $var[0];
   $uns = $var[1];
   $pp  = $var[2];
    $string = "";
   for($x=0;$x<count($ids);$x++)
   {
      $string .= "<div class='trueFriends'><a href='/user.php?id=$ids[$x]'>$uns[$x]<br /><img src='$pp[$x]' border='0' alt='user picture' /></a></div>";
   }

   return $string;
}
function myBars($id)
{
   $sql = "
   SELECT barID, barName
   FROM `userSubscribe`
   LEFT JOIN bars ON userSubscribeBarID = bars.barID
   WHERE userID = '$id'
   ";

   $result = mysql_query($sql);
   $string = "";
    $myBars = "";
   if($row = mysql_fetch_array($result))
   {
      $string .="
      <div id='myBarsContent'>
      $_SESSION[removeBarError]<br/>
      <table width='350'>\n";
      do
      {
	 $string .="
	 <tr>
	    <td><img src='/themes/default/images/toplevel.png' /> $row[barName]</td>
	    <td><a href='/review.php?id=$row[barID]'>View Bar</a></td>
	    <td><a href='controlpanel.php?removeBar=$row[barID]'>Remove</a></td>
	 </tr>
	 ";
	 $myBars .= $row['barID'].',';
      }while($row = mysql_fetch_array($result));
      $myBars = rtrim($myBars,',');
      $string .="</table>\n
      </div> <!-- end my bars content -->";
   }
   else
   {
      $string .="You are currently not subscribed to any bars or bands.";
   }
   $stringBack[0] = $string;
   $stringBack[1] = $myBars;
   return $stringBack;
}
function drawBarList($id)
{
   $sql = "SELECT user_bars_tbl.barID as barID, barName, barPhone
   FROM user_bars_tbl LEFT JOIN bars on user_bars_tbl.barId = bars.barID WHERE userId = '".quote_smart($id)."';";
   $result = mysql_query($sql);

   if($row = mysql_fetch_array($result) )
   {
      $string = "<table cellpadding='10'>"; 
      do{
	 $string .= "
	 <tr>
	    <td><img src='/themes/default/images/toplevel.png' /> <a href='/review.php?id=$row[barID]'><abbr title='View Bar Profile Page'>$row[barName] $row[barPhone]</abbr></a></td>
	    <td>
	   <div id='stuff' syle='display: block; position: absolute; text-align: left;'> 
	    <a href='/admin/barinfo/?id=$row[barID]' style='float: left; margin:0;'>
	    <abbr title='Edit Bar info'><img border='0' align='left' alt='Edit Bar Info' src='/images/aesthetica/process.png' /></abbr>
	    </a>
	    <form action='/admin/menu/menuCategories.php' name='menuCategories$row[barID]' method='POST' style='float: left; margin:0;'>
	       <input type='hidden' name='userID' value='$id' />
	       <input type='hidden' name='barID' value='$row[barID]' />
	       <abbr title='Edit Menu'><input class='but' type='image' name='Menu' alt='Edit Menu' src='/images/aesthetica/application_edit.png' /></abbr>
	    </form>
	    <form action='/admin/specials/manageSpecials.php' name='specials$row[barID]' method='POST' style='float: left;'>
	       <input type='hidden' name='userID' value='$id' />
	       <input type='hidden' name='barID' value='$row[barID]' />
	       <abbr title='Edit Specials'><input class='but' type='image' name='submit' src='/images/aesthetica/note_edit.png' alt='Edit Specials' /></abbr>
	    </form>
	    <a href='/admin/gallery/imageupload/uploadimage.php?barID=$row[barID]' style='float: left; margin:0;' border='1'><abbr title='Edit Photo Gallery'><img  border='0' align='left' alt='Photo Gallery' src='/images/aesthetica/image_multi.png' style='float: left;'/></abbr></a>
	    <!-- <td><a href='/admin/metaInfo/?id=$row[barID]'>Keywords</a></td> -->
	    <a href='/admin/stats/?id=$row[barID]' style='float: left; margin:0;'>
	    <abbr title='View Site Statistics'><img border='1' align='left' alt='Statistics' src='/images/aesthetica/chart.png' /></abbr>
	    </a>
	    <a href='/admin/calendar/events_manage.php?barID=$row[barID]' style='float: left;'>
	    <abbr title='Manage Events'>Events</abbr>
	    </a>
	    <a href='/admin/schedule/manageSchedule.php?barID=$row[barID]' style='fload: left;'>
	    <abbr title='Record Schedule'>Record Schedule</abbr>
	    </a></div>
	    </td>
	 </tr>
	 ";
      }while($row = mysql_fetch_array($result) );
      $string .="</table>";
   }
   else
   {
      $string = "You currently do not have any bars under your control. Search for bars and click the claim this bar link for any bars that you own or manage.";
   }

   return $string;

}
function drawBarBackend($id,$searchResults)
{
   $string = "
   <div id='myPageMenu'>
   



   <h2>Manage My Bars</h2>
   ".drawBarList($id)."
   <br /><br/>

   <form action='/admin/menu/menu.php' name='menuItems' method='POST'>
   <input type='hidden' name='userID' value='$id' />
   </form>
  
   <span style='padding-right:60px;'><a href=\"/admin/barinfo/addbar.php\"><font color='#FF0000'>Add my bar</font></a></span>
<!--
   <span style='padding-right:60px;'><a href=\"login.php?logout=1\"><font color='#FF0000'>logout</font></a></span>
   <span style='padding-right:40px;'><a href=\"user_edit_pass.php\"><font color='#FF0000'>change password</font></a></span>
-->
   </div>
      <!-- end backendSearch -->

<!-- 
      <div id='searchResults'>
      <h1><font color='#FFFFFF'>Search Results</font></h1>
      <font color='#FFFFFF'>$searchResults</font>
      </div> 
      -->
      <!-- end search results -->
   ";

   return $string;

}
/* Content */

//if($_SESSION['usertype'] == 4) //users
//{
   print drawUserBackend($_SESSION['userid'],$searchResults);
//}
//elseif($_SESSION['usertype'] == 2) //bars
//{
//   print drawBarBackend($_SESSION[userid],$searchResults);
//   print drawUserBackend($_SESSION[userid],$searchResults);
//}

$cnt = ob_get_contents();
ob_clean();
/* Save Content */
        $doc->DOMChangeTemplate("content",$cnt);
        $doc->WriteHTML();

?>
