<?PHP
/* Config */
	require_once("../../core.php");

/* Get Registry */
$mysql = Preferences::getInstance('database')->get('connection');
$usertable = Preferences::getInstance('login')->get('SqlUserTable');

ob_start();

/* Login */
	if(!$login->is_logged_in()){ HTTP::redirect_to_url("login.php"); }

/* Process Form Submittion */
    $msg = "";
	if (isset($_POST['submit'])){
  
        $result = $mysql->query("SELECT * 
                                   FROM `{$usertable}` 
                                  WHERE `userId`='{$_SESSION['userid']}'");
        $mr = $result->fetch_array();
        // Validate Old Password
        if (crypt($_POST['oldpass'],$mr['userpassword'])==$mr['userpassword']){
          // Validate New and ReEntered Password
          if ($_POST['newpass'] == $_POST['valpass']){
            // Update Password
            $newpass = crypt($_POST['newpass']);
            $mysql->query("UPDATE `{$usertable}` 
                              SET `userpassword`='{$newpass}'
                            WHERE `userId`={$_SESSION['userid']}");
            $msg = "<b>Password has been changed successfully</b><br /><br />";
          }else{
            $msg = "<span class=\"error\">Change Password Failed: New Password and the Re-entered password do not match.</span><br /><br />";
          }
        }else{
          $msg = "<span class=\"error\">Change Password Failed: Old password is not valid</span><br /><br />";
        }
	}	


/* Content */
	// DISPLAY FORM
	echo "<span class=\"pageheader\"><a href=\"controlpanel.php\" class=\"pageheader\">Control Panel</a> 
			> Change Password</span><hr /><br />";

        echo  $msg;

	echo "<form action=\"user_edit_pass.php\" method=\"POST\">";
	echo "<table>";
		echo "<tr><td width=\"200\">Current Password:</td><td><input type=\"password\" name=\"oldpass\"/></td></tr>";
		echo "<tr><td width=\"200\"><br/></td></tr>";
		echo "<tr><td>New Password:</td><td><input type=\"password\" name=\"newpass\"/></td></tr>";
		echo "<tr><td>Re-enter New Password:</td><td><input type=\"password\" name=\"valpass\"/></td></tr>";
	echo "<tr><td colspan=\"2\" align=\"center\"><br /><input type=\"submit\" name=\"submit\" value=\"Change Password\"/></td></tr>";
	echo "</table>";
	echo "</form>";

$cnt = ob_get_contents();
ob_clean();

/* Save Content */
        $doc->DOMChangeTemplate("content",$cnt);
        $doc->WriteHTML();

?>
