<?PHP
/* Config */
	require_once("../../core.php");

ob_start();

/* Content */
	echo "<h2>Thank you for signing up! Please check your email for a verification message to enable your account.</h2><br />";
	echo "<a href=\"login.php\">Continue to Login</a><hr />";

$cnt = ob_get_contents();
ob_clean();
/* Save Content */	
  	$doc->DOMChangeTemplate("content",$cnt);
	$doc->WriteHTML();
?>
