<?PHP
/* Config */
	require_once("../../core.php");

/* Registry */
$mysql = Preferences::getInstance('database')->get('connection');
$usertable = Preferences::getInstance('login')->get('SqlUserTable');

ob_start();	

/* Database */
$profile = $mysql->query("SELECT * FROM `{$usertable}` AS U
                                   JOIN `user_info_tbl` AS I ON U.userId = I.userId
						   WHERE U.userId = {$_SESSION['userid']}");

echo $mysql->error;
/* Content */
echo "<h2>My Profile</h2><hr/>";
echo "<span class=\"pageheader\"><a href=\"controlpanel.php\" class=\"pageheader\">Control Panel</a>
            > My Profile</span><br />";

echo "<br/><a href=\"user_edit_profile.php\">Edit Profile</a><br/>";

if ($profile){
	$mr = $profile->fetch_array();

		echo "<table>";
		echo "<tr><td width=\"150\">Username:</td><td>{$mr['username']}</td></tr>";
		echo "<tr><td>First Name:</td><td>{$mr['userFirst']}</td></tr>";
		echo "<tr><td>Last Name:</td><td>{$mr['userLast']}</td></tr>";
		echo "</table>";
		echo "<table>";
        echo "<th colspan=\"2\">Contact Information</th>";
		echo "<tr><td width=\"150\">E-Mail:</td><td>{$mr['userEmail']}</td></tr>";
		echo "<tr><td>Date of Birth:</td><td>{$mr['userDOB']}</td></tr>";
      	echo "</table>";

/*		echo "<table>";
                echo "<th colspan=\"2\">Address Information</th>";
		echo "<tr><td width=\"150\">Street 1:</td><td><input type=\"text\" name=\"addr1\" value=\"{$street1}\"/></td></tr>";
		echo "<tr><td>Street 2:</td><td><input type=\"text\" name=\"addr2\" value=\"{$street2}\"/></td></tr>";
		echo "<tr><td>City:</td><td><input type=\"text\" name=\"city\" value=\"{$city}\"/></td></tr>";
		echo "<tr><td>State:</td><td><input type=\"text\" name=\"state\" value=\"{$state}\" size=\"2\"/></td></tr>";
		echo "<tr><td>Zip:</td><td><input type=\"text\" name=\"zip\" value=\"{$zip}\"/></td></tr>";
		echo "</table>";
*/
} else {
 echo "Unable to load profile";
}

$cnt = ob_get_contents();
ob_clean();
/* Save Content */
        $doc->DOMChangeTemplate("content",$cnt);
        $doc->WriteHTML();

