<?PHP
/* Config */
	require_once("../../core.php");
include '../connect.php';
include '../functions.php';
/* Retrieve Registry */
$mysql = Preferences::getInstance('database')->get('connection');
$usertable = Preferences::getInstance('login')->get('SqlUserTable');

ob_start();

$msg = "";
$username = "";
$firstname = "";
$lastname = "";
$email = "";
$phone_area = "";
$phone_exchange = "";
$phone_number = "";
$dob = "";

/* Database */
	// Add
if (isset($_POST['submit'])){
   $err = FALSE;
   if (isset($_POST['username']) && $_POST['username'] != ""){
      // Validate The user is not already taken.
      $r = $mysql->query("SELECT * FROM `{$usertable}` WHERE `username` = '{$_POST['username']}'");
      if ($r->num_rows > 0 ){
	 $err = TRUE;
	 $msg .= "Username Already Taken<br/>";
      }
   } else {
      $err = TRUE;
      $msg .= "Please Enter a Username<br />";
   }
   if(empty($_POST['userType']) || $_POST['userType'] == '0')
   {
      $err = TRUE;
      $msg .="You must select the type of user you are.  If you are unsure of what type of user you should be, select Bar Hopper<br />";
   }
   if (isset($_POST['password']) && ($_POST['password'] == $_POST['c_password'])){
      $pass 		= crypt($_POST['password']);
   }else{
      $err = TRUE;
      $msg .= "Passwords Do Not Match<br/>";
   }
   if (isset($_POST['email']) && $_POST['email'] != ""){
      // Validate The user is not already taken.
      $r = $mysql->query("SELECT * FROM `{$usertable}` WHERE `username` = '{$_POST['email']}'");
      if ($r->num_rows > 0 ){
	 $err = TRUE;
	 $msg .= "Email Already Taken<br/>";
      }
   } else {
      $err = TRUE;
      $msg .= "Please Enter an Email<br />";
   }
   if (isset($_POST['firstname']) && empty($_POST['firstname']) )
   {
      $err = TRUE;
      $msg .="Please enter your first name<br />";
   }
   if (isset($_POST['lastname']) && empty($_POST['lastname']))
   {
      $err = TRUE;
      $msg .="Please enter your last name<br />";
   }
   if (isset($_POST['dob']) && empty($_POST['dob']))
   {
      $err = TRUE;
      $msg .="Please enter your date of birth<br />";
   }
   if (!isset($_POST['tos']) || $_POST['tos'] != 'yes' )
   {
      $err = TRUE;
      $msg .="Please Agree to the Terms of Service<br />";
   }
   $username 	= $mysql->real_escape_string(strip_tags($_POST['username']));
   $firstname	= $mysql->real_escape_string(strip_tags($_POST['firstname']));
   $lastname	= $mysql->real_escape_string(strip_tags($_POST['lastname']));
   $phone_area	= $mysql->real_escape_string(strip_tags($_POST['phone_area']));
   $phone_exchange = $mysql->real_escape_string(strip_tags($_POST['phone_exchange']));
   $phone_number	= $mysql->real_escape_string(strip_tags($_POST['phone_number']));
   $email 		= $mysql->real_escape_string(strip_tags($_POST['email']));
   $dob		= $mysql->real_escape_string(strip_tags($_POST['dob']));
   $street1	= $mysql->real_escape_string(strip_tags($_POST['addr1']));
   $street2	= $mysql->real_escape_string(strip_tags($_POST['addr2']));
   $city		= $mysql->real_escape_string(strip_tags($_POST['city']));
   $state		= $mysql->real_escape_string(strip_tags($_POST['state']));
   $zip		= $mysql->real_escape_string(strip_tags($_POST['zip']));
   $phone_type	= $_POST['phone_type'];
   $userType    = $_POST['userType'];

		if ($err == FALSE){
		  $timestamp = time(void);
		  $from = $mysql->real_escape_string(strip_tags($_POST['fromWhere']));
		//send email validation
		$_SESSION[validateEmail] = $email;
		$_SESSION[validateUser]  = $username;
		echo validateSendEmail();
		// Setup User
		$r = $mysql->query("INSERT INTO `{$usertable}`
					   VALUES (null,'{$username}',
					   			  '{$pass}',
								  '{$email}',
								  '0','$userType','2','0','$from','$timestamp')");
		echo $mysql->error;
		if ($r){
			$uid = $mysql->insert_id;
			// Setup User Info
			$mysql->query("INSERT INTO `user_info_tbl` VALUES (null,'{$firstname}',
							'{$lastname}',{$uid},'{$dob}','','')");
			echo $mysql->error;

			// Setup User Phone
			$mysql->query("INSERT INTO `user_phone_tbl` VALUES (null,{$uid},
							'{$phone_area}','{$phone_exchange}','{$phone_number}','',{$phone_type})");
			echo $mysql->error;
	
			// Setup User Address
			$mysql->query("INSERT INTO `user_address_tbl` VALUES (null,1,{$uid},
							'{$street1}','{$street2}','{$city}','{$state}','{$zip}')");
			echo $mysql->error;
			
			// Setup User Group
			$mysql->query("INSERT INTO `group_tbl` VALUES (null,'{$username}', {$uid}, 4, 0)");
			echo $mysql->error;
			HTTP::redirect_to_url("signup_thankyou.php");		
		}
		}
	}
/* Lists */
$phone_type = $mysql->query("SELECT * FROM `phone_type_tbl`");

/* Content */
	// DISPLAY FORM
	echo $msg;
	echo "<div id=\"colOne\">";
    echo "<div id=\"barInfoFront\">";
        echo "<h1>Create New Account</h1>";
        echo "<div id=\"barInfoFrontWrap\">";
			echo "</div>";
			echo "</div>";
			echo "</div>";
	echo "<div id=\"colOne\"><form method=\"POST\" action=\"signup_user.php\">";
		echo "<table>";
		echo "<tr><td width=\"150\">Username:</td><td><input type=\"text\" name=\"username\" value=\"{$username}\"/><span id=\"required\">*</span></td></tr>";
		echo "<tr><td>Password:</td><td><input type=\"password\" name=\"password\" /><span id=\"required\">*</span></td></tr>";
		echo "<tr><td>Confirm Password:</td><td><input type=\"password\" name=\"c_password\" /><span id=\"required\">*</span></td></tr>";
		echo "<tr><td>What Type of user are you?:</td><td>
		<select name='userType'>";
		$userTypes = array('0' => '...Choose...', '4' => 'Bar Hopper', '2' => 'Bar Owner');
		foreach($userTypes as $key=>$type)
		{
		  if (isset($_POST['userType']) && $_POST['userType'] == $key){
			  $default = "selected=\"yes\"";
		  }else{
			  $default = "";
		  }
		  echo "<option value=\"$key\" {$default}>$type</option>\n";
	       }
	       echo "</select></td></tr>";
		echo "<tr><td width=\"150\">First Name:</td><td><input type=\"text\" name=\"firstname\" value=\"{$firstname}\" /><span id=\"required\">*</span></td></tr>";
		echo "<tr><td>Last Name:</td><td><input type=\"text\" name=\"lastname\" value=\"{$lastname}\"/><span id=\"required\">*</span></td></tr>";
		echo "<tr><td>How'd you hear about us?:</td><td>".drawFromWhere()."</td></tr>";
		echo "</table>";
		echo "<table>";
                echo "<th colspan=\"2\">Contact Information</th>";
		echo "<tr><td width=\"150\">E-Mail:</td><td><input type=\"text\" name=\"email\" value=\"{$email}\"/><span id=\"required\">*</span></td></tr>";
		echo "<tr><td>Phone:</td><td>(<input type=\"text\" name=\"phone_area\" value=\"{$phone_area}\" size=\"3\"/>)
                                             <input type=\"text\" name=\"phone_exchange\" value=\"{$phone_exchange}\" size=\"3\"/> - 
                                             <input type=\"text\" name=\"phone_number\" value=\"{$phone_number}\" size=\"4\"/>
				 <select name=\"phone_type\">";
					while($pt = $phone_type->fetch_array())
					{
						if (isset($phone_type) && $phone_type == $pt[0]){
							$default = "DEFAULT=\"DEFAULT\"";
						}else{
							$default = "";
						}
						echo "<option value=\"{$pt[0]}\" {$default}>{$pt[1]}</option>";
					}
 				echo "</select>";
                echo "</td></tr>";
		echo "<tr><td>Date of Birth:</td><td><input type=\"text\" name=\"dob\" value=\"{$dob}\"/><span id=\"required\">*</span></td></tr>";
		echo "<tr><td colspan='2'>I Agree with your Terms of Service <input type='checkbox' value='yes' name='tos' checked='yes' /> <a href='/pages/tos.php' target='_new'>View TOS</a></td></tr>";
         	echo "</table>";
//		echo "<table>";
//                echo "<th colspan=\"2\">Address Information</th>";
//		echo "<tr><td width=\"150\">Street 1:</td><td><input type=\"text\" name=\"addr1\" value=\"{$street1}\"/></td></tr>";
//		echo "<tr><td>Street 2:</td><td><input type=\"text\" name=\"addr2\" value=\"{$street2}\"/></td></tr>";
//		echo "<tr><td>City:</td><td><input type=\"text\" name=\"city\" value=\"{$city}\"/></td></tr>";
//		echo "<tr><td>State:</td><td><input type=\"text\" name=\"state\" value=\"{$state}\" size=\"2\"/></td></tr>";
//		echo "<tr><td>Zip:</td><td><input type=\"text\" name=\"zip\" value=\"{$zip}\"/></td></tr>";
//		echo "</table>";
        echo "<table>";
         echo "<tr><td colspan=\"2\" align=\"center\"><br /><input type=\"submit\" name=\"submit\" value=\"Sign Up\" /></td></tr>";
	echo "</table>";
	echo "</form></div>";

$cnt = ob_get_contents();
ob_clean();
/* Save Content */
        $doc->DOMChangeTemplate("content",$cnt);
        $doc->WriteHTML();

