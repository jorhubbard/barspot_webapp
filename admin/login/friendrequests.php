<?php

include '../connect.php';
include_once '../../core.php';

ob_start();

function friendRequests($pending='requestingID')
{ 
   //setup with variable pending so that we can see people who requested us, and eventually add people who we have requested
   $sql = "
   SELECT userFriendID, $pending, message, timestamp, username, userFirst, userLast
   FROM user_friends
   LEFT JOIN user_tbl AS u ON u.userId = $pending
   LEFT JOIN user_info_tbl AS ui ON ui.userId = $pending
   WHERE receivingID = '$_SESSION[userid]'
   AND response = '2'
   ORDER BY timestamp DESC;
   ";
   //print("sql = $sql");
   $result = mysql_query($sql);

   if($row = mysql_fetch_array($result) )
   {
      $string = "<table>";
      
      do
      {
	 $string .="
	 <tr>
	    <td><a href='/user.php?id=$row[$pending]'>User: $row[username], $row[userFirst] $row[userLast]</a></td>
	    <td>Time: $row[timestamp]</td>
	 </tr>
	 ";
	 if(!empty($row['message']))
	 {
	    $string .="
	    <tr>
	       <td colspan='2'>\"$row[message]\"</td>
	    </tr>
	    ";
	 }
	 $string .= "
	 <tr>
	    <td colspan='2'>
	       <a href='".$_SERVER['PHP_SELF']."?status=1&id=$row[userFriendID]'>Accept</a> | 
	       <a href='".$_SERVER['PHP_SELF']."?status=0&id=$row[userFriendID]'>Deny</a>
	    </td>
	 </tr>
	 <tr>
	    <td colspan='2'><hr /></td>
	 </tr>
	 ";
      }while($row = mysql_fetch_array($result) );
      $string .="</table>";
   }
   else
   {
      $string = "You do not have any pending friend requests.";
   }
   return $string;
}
if(isset($_GET['status']) && !empty($_GET['id'] ) )
{
   //checks to see if the id we are updating belongs to us
   $sql = "SELECT receivingID FROM user_friends WHERE userFriendID = '".quote_smart($_GET['id'])."' LIMIT 1;";
   $result = mysql_query($sql);
   $row = mysql_fetch_array($result);
   if(mysql_affected_rows() == 1 && $row['receivingID'] == $_SESSION['userid'])
   {
      //print("ready to update the record");
      $sql = "UPDATE user_friends SET response = '".quote_smart($_GET['status'])."' WHERE userFriendID = '".quote_smart($_GET['id'])."' LIMIT 1;";
      //print("$sql = $sql<br />");
      $result = mysql_query($sql);//or die("Invalid mysql: " . mysql_error());

      if($result && $_GET['status'] == '1')
      {
	 print("You have accepted a friend request.");
      }
      else if($result && $_GET['status'] == '0')
      {
	 print("You have denied a friend request.");
      }
      else
      {
	 print("Something went wrong when we tried to update your friend status with this user. Please try again and if the problem persists, contact support at support@hotbarspot.com");
      }

      print("<br /><br />");

   }
   else
   {
      print("Looks like we ran into a problem.  Either the friend request you are tyring to edit doesn't belong to your user, or the friend request was not found.  If the problem persists, try logging out and then back in again or contact support@hotbarspot.com");
   }

}
print friendRequests();

print(" ");
$cnt = ob_get_contents();
ob_clean();
/* Save Content */
$doc->DOMChangeTemplate("content",$cnt);
$doc->WriteHTML();
