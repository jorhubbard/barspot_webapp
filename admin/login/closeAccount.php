<?PHP
include '../connect.php';
/* Config */
	require_once("../../core.php");

ob_start();

/* Content */
if($_SESSION[userid])
{
   if($_POST[submit] == 'Yes')
   {
      //print("remove account here.");
      $sql = "UPDATE `user_tbl` SET userActive = '0' WHERE `user_tbl`.`userId` = '$_SESSION[userid]' LIMIT 1";
      $result = mysql_query($sql);
      if($result)
      {
	 print("Your account has been deleted.");
	 session_destroy();
      }
      else
	 print("There was an error while deleting your account.  Please try again.  If the problem persists, contact support@hotbarspot.com");
   }
   else
   {
      echo "<form action='/admin/login/closeAccount.php' method='post'><h2>Close Account</h2><br />";
      echo "Are you sure you want to close your account? <input type='submit' name='submit' value='Yes' /> 
      <a href='/admin/login/controlpanel.php'>No, get me out of here!</a></form>";
   }
}
else
{
   print("You must be logged in to access this page.");
}
$cnt = ob_get_contents();
ob_clean();
/* Save Content */	
  	$doc->DOMChangeTemplate("content",$cnt);
	$doc->WriteHTML();
?>
