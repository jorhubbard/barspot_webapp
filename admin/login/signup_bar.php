<?php
/* Config */
require_once("../../core.php");
include '../connect.php';
include '../functions.php';
$mysql = Preferences::getInstance('database')->get('connection');
$usertable = Preferences::getInstance('login')->get('SqlUserTable');

ob_start();	
if (isset($_POST['barID'])) { $_GET['barID'] = $_POST['barID']; }

/* Database */
	// Add
	if (isset($_POST['submit'])){
		$msg = "";
		$err = FALSE;
		if (isset($_POST['password']) && ($_POST['password'] == $_POST['c_password'])){
			$pass 		= crypt($_POST['password']);
		}else{
			$err = TRUE;
			$msg .= "Passwords Do Not Match<br/>";
		}
		if (isset($_POST['username']) && $_POST['username'] != ""){
			// Validate The user is not already taken.
			$r = $mysql->query("SELECT * FROM `{$usertable}` WHERE `username` = '{$_POST['username']}'");
			if ($r->num_rows > 0 ){
				$err = TRUE;
				$msg .= "Username Already Taken<br/>";
			}
		}else{
		   $err = TRUE;
		   $msg .= "Please Enter a Username";
		}
		if (isset($_POST['password']) && ($_POST['password'] == $_POST['c_password'])){
		   $pass           = crypt($_POST['password']);
		}else{
		   $err = TRUE;
		   $msg .= "Passwords Do Not Match<br/>";
		}
		if (isset($_POST['email']) && $_POST['email'] != ""){
		   // Validate The user is not already taken.
		   $r = $mysql->query("SELECT * FROM `{$usertable}` WHERE `username` = '{$_POST['email']}'");
		   if ($r->num_rows > 0 ){
		      $err = TRUE;
		      $msg .= "Email Already Taken<br/>";
		   }
		} else {
		   $err = TRUE;
		   $msg .= "Please Enter an Email<br />";
		}
		if (isset($_POST['firstname']) && empty($_POST['firstname']) )
		{
		   $err = TRUE;
		   $msg .="Please enter your first name<br />";
		}
		if (isset($_POST['lastname']) && empty($_POST['lastname']))
		{
		   $err = TRUE;
		   $msg .="Please enter your last name<br />";
		}
		if (isset($_POST['dob']) && empty($_POST['dob']))
		{
		   $err = TRUE;
		   $msg .="Please enter your date of birth<br />";
		}
		if (!isset($_POST['tos']) || $_POST['tos'] != 'yes' )
		{
		  $err = TRUE;
		  $msg .="Please agree to the Terms of Service<br />";
	        }

		$username 	= $mysql->real_escape_string(strip_tags($_POST['username']));
		$firstname	= $mysql->real_escape_string(strip_tags($_POST['firstname']));
		$lastname	= $mysql->real_escape_string(strip_tags($_POST['lastname']));
		$phone_area	= $mysql->real_escape_string(strip_tags($_POST['phone_area']));
		$phone_exchange = $mysql->real_escape_string(strip_tags($_POST['phone_exchange']));
		$phone_number	= $mysql->real_escape_string(strip_tags($_POST['phone_number']));
		$email 		= $mysql->real_escape_string(strip_tags($_POST['email']));
		$dob		= $mysql->real_escape_string(strip_tags($_POST['dob']));
		$street1	= $mysql->real_escape_string(strip_tags($_POST['addr1']));
		$street2	= $mysql->real_escape_string(strip_tags($_POST['addr2']));
		$city		= $mysql->real_escape_string(strip_tags($_POST['city']));
		$state		= $mysql->real_escape_string(strip_tags($_POST['state']));
		$zip		= $mysql->real_escape_string(strip_tags($_POST['zip']));
		$phone_type	= $_POST['phone_type'];
		$barName	= $mysql->real_escape_string(strip_tags($_POST['barName']));
		$barAddress	= $mysql->real_escape_string(strip_tags($_POST['barAddress']));
		$barPhone	= $mysql->real_escape_string(strip_tags($_POST['barPhone']));
		$barZip		= $mysql->real_escape_string(strip_tags($_POST['barZip']));

		if ($err == FALSE){
		  $timestamp = time(void);
                  $from = $mysql->real_escape_string(strip_tags($_POST['fromWhere']));
		   //send email validation
		   $_SESSION[validateEmail] = $email;
		   echo validateSendEmail();

		
		// Setup User
		$r = $mysql->query("INSERT INTO `{$usertable}`
					   VALUES (null,'{$username}',
					   			  '{$pass}',
								  '{$email}',
								  '0','2','2','0','$from','$timestamp')");
		echo $mysql->error;
		if ($r){
			$uid = $mysql->insert_id;
			// Setup User Info
			$mysql->query("INSERT INTO `user_info_tbl` VALUES (null,'{$firstname}',
							'{$lastname}',{$uid},'{$dob}','','')");
			echo $mysql->error;

			// Setup User Phone
			$mysql->query("INSERT INTO `user_phone_tbl` VALUES (null,{$uid},
							'{$phone_area}','{$phone_exchange}','{$phone_number}','',{$phone_type})");
			echo $mysql->error;

			// Setup User Group
			$mysql->query("INSERT INTO `group_tbl` VALUES (null,'{$username}',{$uid},6,0)");
			echo $mysql->error;
	       /*
			// Bar Address
			if (isset($_POST['barID'])){
				$barID = $_POST['barID'];				
     			   	// Update Bar   
				$mysql->query("UPDATE `bars`
						  SET `barName`='{".stripslashes($barName)."}',
						      `barAddress`='{$barAddress}',
						      `barZip` = '{$barZip}',
                                                      `barPhone` = '{$barPhone}',
                                                      `barWebsite` = '{$barWebsite}'
						WHERE `barID` = {$barID}");
				echo $mysql->error;
			}else{
     			   	// Setup Bar   
				$mysql->query("INSERT INTO `bars` VALUES (null,'{$barName}','{$barAddress}','{$barZip}',
							'{$barPhone}','{$barWebsite}',CURRENT_TIMESTAMP(),'0','0','1','0')");
				echo $mysql->error;
				$barID = $mysql->insert_id;
			}
	       */
			// Setup Bar Group
			$mysql->query("INSERT INTO `group_tbl` VALUES (null,'".stripslashes($barName)."',{$uid},3,{$barID})");
			echo $mysql->error;
			$groupID = $mysql->insert_id;

			//$mysql->query("INSERT INTO user_bars_tbl VALUES (NULL,'{$groupID}','{$barID}');");
			//echo $mysql->error;
			
			HTTP::redirect_to_url("signup_thankyou.php");		
		}
		}
	}
/* Lists */
$phone_type = $mysql->query("SELECT * FROM `phone_type_tbl`");

/* Content */
	// DISPLAY FORM
	echo "<h2>New Bar Signup</h2><hr/>";
	echo $msg;
	echo "<form method=\"POST\" action=\"signup_bar.php\">";
		echo "<table>";
		echo "<tr><td width=\"150\">Username:</td><td><input type=\"text\" name=\"username\" value=\"{$username}\"/><span id=\"required\">*</span></td></tr>";
		echo "<tr><td>Password:</td><td><input type=\"password\" name=\"password\" /><span id=\"required\">*</span></td></tr>";
		echo "<tr><td>Confirm Password:</td><td><input type=\"password\" name=\"c_password\" /><span id=\"required\">*</span></td></tr>";
		echo "<tr><td width=\"150\">First Name:</td><td><input type=\"text\" name=\"firstname\" value=\"{$firstname}\" /><span id=\"required\">*</span></td></tr>";
		echo "<tr><td>Last Name:</td><td><input type=\"text\" name=\"lastname\" value=\"{$lastname}\"/><span id=\"required\">*</span></td></tr>";
		echo "<tr><td>How'd you hear about us?:</td><td>".drawFromWhere()."</td></tr>";
		echo "</table>";
		echo "<table>";
                echo "<th colspan=\"2\">Contact Information</th>";
		echo "<tr><td width=\"150\">E-Mail:</td><td><input type=\"text\" name=\"email\" value=\"{$email}\"/><span id=\"required\">*</span></td></tr>";
		echo "<tr><td>Phone:</td><td>(<input type=\"text\" name=\"phone_area\" value=\"{$phone_area}\" size=\"3\"/>)
                                             <input type=\"text\" name=\"phone_exchange\" value=\"{$phone_exchange}\" size=\"3\"/> - 
                                             <input type=\"text\" name=\"phone_number\" value=\"{$phone_number}\" size=\"4\"/>
				 <select name=\"phone_type\">";
					while($pt = $phone_type->fetch_array())
					{
						if (isset($phone_type) && $phone_type == $pt[0]){
							$default = "DEFAULT=\"DEFAULT\"";
						}else{
							$default = "";
						}
						echo "<option value=\"{$pt[0]}\" {$default}>{$pt[1]}</option>";
					}
 				echo "</select>";
                echo "</td></tr>";
		echo "<tr><td>Date of Birth:</td><td><input type=\"text\" name=\"dob\" value=\"{$dob}\"/><span id=\"required\">*</span></td></tr>";
		echo "<tr><td colspan='2'>I Agree with your Terms of Service <input type='checkbox' value='yes' name='tos' checked='yes' /> <a href='/pages/tos.php' target='_new'>View TOS</a></td></tr>";
         	echo "</table>";

if (isset($_GET['barID'])){
	$r = $mysql->query("SELECT * FROM `bars` WHERE `barID`='{$_GET['barID']}'");
	$mr = $r->fetch_array();
	
		echo "<!-- <input type=\"hidden\" name=\"barID\" value=\"{$_GET['barID']}\" />";
		echo "<table>";
                echo "<th colspan=\"2\">Bar Information</th>";
		echo "<tr><td width=\"150\">Bar Name:</td><td><input type=\"text\" name=\"barName\" value=\"{$mr['barName']}\"/></td></tr>";
		echo "<tr><td>Address:</td><td><input type=\"text\" name=\"barAddress\" value=\"{$mr['barAddress']}\"/></td></tr>";
		echo "<tr><td>Zip:</td><td><input type=\"text\" name=\"barZip\" value=\"{$mr['barZip']}\"/></td></tr>";
		echo "<tr><td>Phone:</td><td><input type=\"text\" name=\"barPhone\" value=\"{$mr['barPhone']}\" /></td></tr>";
		echo "<tr><td>website:</td><td><input type=\"text\" name=\"barWebsite\" value=\"{$mr['barWebsite']}\" /></td></tr>";
		echo "</table> -->";
}else{
		echo "<!-- <table>";
                echo "<th colspan=\"2\">Bar Information</th>";
		echo "<tr><td width=\"150\">Bar Name:</td><td><input type=\"text\" name=\"barName\" value=\"{$barName}\"/></td></tr>";
		echo "<tr><td>Address:</td><td><input type=\"text\" name=\"barAddress\" value=\"{$barAddress}\"/></td></tr>";
		echo "<tr><td>Zip:</td><td><input type=\"text\" name=\"barZip\" value=\"{$barZip}\"/></td></tr>";
		echo "<tr><td>Phone:</td><td><input type=\"text\" name=\"barPhone\" value=\"{$barPhone}\" /></td></tr>";
		echo "<tr><td>Website:</td><td><input type=\"text\" name=\"barWebsite\" value=\"{$barWebsite}\" /></td></tr>";
		echo "</table> -->";
}
        echo "<table>";
         echo "<tr><td colspan=\"2\" align=\"center\"><br /><input type=\"submit\" name=\"submit\" value=\"Sign Up\" /></td></tr>";
	echo "</table>";
	echo "</form>";

$cnt = ob_get_contents();
ob_clean();
/* Save Content */
        $doc->DOMChangeTemplate("content",$cnt);
        $doc->WriteHTML();

?>
