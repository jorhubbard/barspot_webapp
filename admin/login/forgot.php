<?PHP
/* Config */
	require_once "../../core.php";

ob_start();

/**
 * Load API's
 */
    require_once( DIRMODULES."api/recaptcha/recaptchalib.php");

/* Variables */
  $system = Preferences::getInstance('system');
  $recaptcha = Preferences::getInstance('recaptcha');
  $mysql = Preferences::getInstance('database')->get('connection');

  $showForm = TRUE;
  $formError = FALSE;
  $msg = "";
  $fromEmail = new Email_Address($system->get('admin_email'));

/* Form Events */
// Recaptcha Response
if (isset($_POST['submit'])){
	$login = Preferences::getInstance('login');
  $resp = recaptcha_check_answer ($recaptcha->get('privatekey'),
                                  $_SERVER["REMOTE_ADDR"],
                                  $_POST["recaptcha_challenge_field"],
                                  $_POST["recaptcha_response_field"]);

  if (!$resp->is_valid) {
    $msg = "The reCAPTCHA wasn't entered correctly. Go back and try it again.";
    $formError = TRUE;
  }else{
    // Get Email Address
    if (isset($_POST['email']) && $_POST['email'] != ""){
      try {
        $em  = new Email_Address($_POST['email']);
        $sql = "SELECT * FROM `{$login->get('SqlUserTable')}` WHERE `email`='{$em}'";
        $r = $mysql->query($sql);
        if ($r->num_rows > 0){
            $mr = $r->fetch_array();
            $username = $mr['username'];
            // Reset Password
            $password = substr(md5(time()),3,5);
            $pass_crypt = crypt($password);
            $mysql->query("UPDATE `{$login->get('SqlUserTable')}` SET `pass`='{$pass_crypt}' WHERE `id`='{$mr['id']}'");
            // Write Email
            $message = "<b>Mark-it Account Information</b><br />"
                     . "Your account login information is below. Please write this information down.<br /><br />"
                     . "Username: {$username} <br />"
                     . "Password: {$password} <br />";

            $email = new Email($fromEmail);
			$email->sendTo(new Email_Address($_POST['email']));
            $email->setSubject("Mark-it: Your account has been updated.");
            $email->setMessage($message);
            try {
              $email->sendMail();
              $msg = "Password Reset Successfully. You should recieve an email shortly.";
              $msg .= $message;
            }catch ( specialException $e){
              $msg = "Email failed to send: {$e}";
            }
        } else {
          $msg = "Email address, {$em}, was not found.";
          $formError = TRUE;
        }
      } catch ( specialException $e) {
        $msg = "Invalid Email Address";
        $formError = TRUE;
      }
    } else {
      $msg = "No email was provided";
      $formError = TRUE;
    }
  }

  if ($formError == TRUE){
    $showForm = TRUE;
  } else {
    $showForm = FALSE;
  }

}


/* Content */
	// Header
    echo "<h2>Forgot Password</h2><hr />";
    echo $msg;
if ($showForm){	
    // Show Form
    echo "<form method=\"POST\" action=\"forgot.php\">"; 
    
    echo "<br /><b>Step 1:</b><br/>";
    echo "Enter your email address:<br />";
    echo "<input type=\"text\" name=\"email\" />";
  
    echo "<br /><br /><b>Step 2:</b><br/>";
    echo "Enter the two words you see below:<br />";
    echo recaptcha_get_html($recaptcha->get('publickey'));

    echo "<br /><b>Step 3:</b><br/>";
    echo "Submit your request. You will receive an email at the above address with your username and new password.<br />";
    echo "<input type=\"submit\" name=\"submit\" value=\"Retrieve Login\"/>";

    echo "</form>";
} else {
    // Closing Message
    echo "<br /><a href=\"login.php\">Back to the Login</a>";
}

$cnt = ob_get_contents();
ob_clean();
/* Save Content */
        $doc->DOMChangeTemplate("content",$cnt);
        $doc->WriteHTML();


?>
