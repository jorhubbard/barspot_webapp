<?PHP
/* Config */
	require_once("../../core.php");
include '../connect.php';
/* Registry */
$mysql = Preferences::getInstance('database')->get('connection');
$usertable = Preferences::getInstance('login')->get('SqlUserTable');

ob_start();

$msg = "";
/* Database */
	// Edit This really needs to be made a command.
	if (isset($_POST['submit'])){
		$msg = "";
		$err = FALSE;

		if (isset($_POST['username']) && $_POST['username'] != ""){
		}else{
			$err = TRUE;
			$msg .= "Please Enter a Username";
		}
		$username 	= $mysql->real_escape_string(strip_tags($_POST['username']));
		$firstname	= $mysql->real_escape_string(strip_tags($_POST['firstname']));
		$lastname	= $mysql->real_escape_string(strip_tags($_POST['lastname']));
		$phone_area	= $mysql->real_escape_string(strip_tags($_POST['phone_area']));
		$phone_exchange = $mysql->real_escape_string(strip_tags($_POST['phone_exchange']));
		$phone_number	= $mysql->real_escape_string(strip_tags($_POST['phone_number']));
		$email 		= $mysql->real_escape_string(strip_tags($_POST['email']));
		$dob		= $mysql->real_escape_string(strip_tags($_POST['dob']));
		$street1	= $mysql->real_escape_string(strip_tags($_POST['addr1']));
		$street2	= $mysql->real_escape_string(strip_tags($_POST['addr2']));
		$city		= $mysql->real_escape_string(strip_tags($_POST['city']));
		$state		= $mysql->real_escape_string(strip_tags($_POST['state']));
		$zip		= $mysql->real_escape_string(strip_tags($_POST['zip']));
		$phone_type	= $mysql->real_escape_string(strip_tags($_POST['phone_type']));
		$companyEmails  = $mysql->real_escape_string(strip_tags($_POST['companyEmails']));
		$publicWallView = $mysql->real_escape_string(strip_tags($_POST['publicWallView']));
		$emailWallPosts = $mysql->real_escape_string(strip_tags($_POST['emailWallPosts']));
		$emailNapkins   = $mysql->real_escape_string(strip_tags($_POST['emailNapkins']));
		$emailFriendRequests = $mysql->real_escape_string(strip_tags($_POST['emailFriendRequests']));

		if ($err == FALSE){
		// Setup User
		     $mysql->query("UPDATE `{$usertable}`
					           SET `username` = '{$username}',
					   			   `userEmail` =  '{$email}'
						     WHERE `userId` = {$_SESSION['userid']}");
  	 	      echo $mysql->error;

			// Setup User Info
		  	  $mysql->query("Update `user_info_tbl` 
						        SET `userFirst` = '{$firstname}',	
                                    `userLast`  = '{$lastname}'
						     WHERE `userId` = {$_SESSION['userid']}");
			echo $mysql->error;
		     $sql = 
		     "UPDATE `userPreferences`
		     SET companyEmails = '$companyEmails',
		     publicWallView = '$publicWallView', 
		     emailWallPosts = '$emailWallPosts',
		     emailNapkins = '$emailNapkins',
		     emailFriendRequests = '$emailFriendRequests'
		     WHERE userID = '$_SESSION[userid]' 
		     LIMIT 1;";
		     $result = mysql_query($sql);
if(mysql_affected_rows() < 1 )
{
   $sql = "INSERT INTO userPreferences VALUES ('','$_SESSION[userid]','$companyEmails','$publicWallView','$emailWallPosts','$emailNapkins','$emailFriendRequests');";
   $result = mysql_query($sql)or die("invalid query: " . mysql_error());
}
		     //print("sql = $sql<br/>");

		     echo $mysql->error;
  			//HTTP::redirect_to_url("user_view_profile");		
		}
	}

/* Lists */
$profile = $mysql->query("SELECT * FROM `{$usertable}` AS U
                                   JOIN `user_info_tbl` AS I ON U.userId = I.userId
				   LEFT JOIN userPreferences AS up ON up.userID = I.userId
                           WHERE U.userId = {$_SESSION['userid']}");
$mr = $profile->fetch_array();
/* Content */

$statement = "checked='yes'";

$companyEmailsNo = "";
$companyEmailsYes = "";
if($mr['companyEmails'] == '0')
   $companyEmailsNo = $statement;
else
   $companyEmailsYes = $statement;

$publicWallViewNo = "";
$publicWallViewYes = "";
if($mr['publicWallView'] == '0')
   $publicWallViewNo = $statement;
else
   $publicWallViewYes = $statement;

$emailWallPostsNo = "";
$emailWallPostsYes = "";
if($mr['emailWallPosts'] == '0')
   $emailWallPostsNo = $statement;
else
   $emailWallPostsYes = $statement;

$emailNapkinsNo = "";
$emailNapkinsYes = "";
if($mr['emailNapkins'] == '0')
   $emailNapkinsNo = $statement;
else
   $emailNapkinsYes = $statement;

$emailFriendRequestsNo = "";
$emailFriendRequestsYes = "";
if($mr['emailFriendRequests'] == '0')
   $emailFriendRequestsNo = $statement;
else
   $emailFriendRequestsYes = $statement;

   // DISPLAY FORM
   echo "<h2>Edit User Profile</h2><hr/>";
   echo "<span class=\"pageheader\"><a href=\"controlpanel.php\" class=\"pageheader\">Control Panel</a>
   > My Profile</span><br />";

   echo $msg;
   echo "<form method=\"POST\" action=\"user_edit_profile.php\">";
   echo "<table>";
   echo "<tr><td width=\"150\">Username:</td><td><input type=\"text\" name=\"username\" value=\"{$mr['username']}\"/><span id=\"required\">*</span></td></tr>";
   echo "<tr><td width=\"150\">First Name:</td><td><input type=\"text\" name=\"firstname\" value=\"{$mr['userFirst']}\" /><span id=\"required\">*</span></td></tr>";
   echo "<tr><td>Last Name:</td><td><input type=\"text\" name=\"lastname\" value=\"{$mr['userLast']}\"/><span id=\"required\">*</span></td></tr>";
   echo "</table>";

   echo "<table>";
   echo "<th colspan=\"2\">Contact Information</th>";
   echo "<tr><td width=\"150\">E-Mail:</td><td><input type=\"text\" name=\"email\" value=\"{$mr['userEmail']}\"/><span id=\"required\">*</span></td></tr>";
   echo "<tr><td>Date of Birth:</td><td><input type=\"text\" name=\"dob\" value=\"{$mr['userDOB']}\"/><span id=\"required\">*</span></td></tr>";
   echo "</table>";

   echo "<table>";
   echo "<th colspan=\"2\">Site Preferences / Security Options</th>";
   echo "<tr><td width=\"200\">Receive Hotbarspot Company Emails:</td><td><input type='radio' value='1' name='companyEmails' $companyEmailsYes />Yes<br />
   <input type='radio' value='0' name='companyEmails' $companyEmailsNo />No</td></tr>";
   echo "<tr><td width=\"200\">Bathroom wall is public:</td><td><input type='radio' value='1' name='publicWallView' $publicWallViewYes />Yes<br />
   <input type='radio' value='0' name='publicWallView' $publicWallViewNo />No</td></tr>";
   echo "<tr><td>Email me my wall posts:</td><td><input type='radio' value='1' name='emailWallPosts' $emailWallPostsYes />Yes<br />
   <input type='radio' value='0' name='emailWallPosts' $emailWallPostsNo />No</td></tr>";
   echo "<tr><td>Email me my bar napkins:</td><td><input type='radio' value='1' name='emailNapkins' $emailNapkinsYes />Yes<br />
   <input type='radio' value='0' name='emailNapkins' $emailNapkinsNo/>No</td></tr>";
   echo "<tr><td>Email me my friend requests:</td><td><input type='radio' value='1' name='emailFriendRequests' $emailFriendRequestsYes />Yes<br />
   <input type='radio' value='0' name='emailFriendRequests' $emailFriendRequestsNo />No</td></tr>";
   echo "</table>";
   
   echo "<table>";
   echo "<tr><td colspan=\"2\" align=\"center\"><br /><input type=\"submit\" name=\"submit\" value=\"Update\" /></td></tr>";
   echo "</table>";
   echo "</form>";
   echo "If you would like to close your account, <a href='/admin/login/closeAccount.php'>click here</a>.  
   This action can not be undone and will make the username you have permanently unavailable.";

$cnt = ob_get_contents();
ob_clean();
/* Save Content */
        $doc->DOMChangeTemplate("content",$cnt);
        $doc->WriteHTML();

