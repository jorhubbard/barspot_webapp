<?php
include '../connect.php';
require_once("../../core.php");
if(!$login->is_logged_in()){ HTTP::redirect_to_url("../login/login.php"); 	}
if(!checkUserBar($_GET[barID])){ HTTP::redirect_to_url("../login/controlpanel.php"); 	}
if(empty($_GET[barID])){Http::redirect_to_url('../login/controlpanel.php'); }
$mysql = Preferences::getInstance('database')->get('connection');


// Delete User
	if (isset($_GET['del']) && $_GET['del']==1 && checkUserBar($_GET[barID]) ){
		$msg = "<br /><table width=\"300\">
					<tr><td colspan=\"3\"><b>Event:</b> {$_GET['text']}</td></tr>
					<tr><td colspan=\"3\"><b>Are you sure you wish to delete this event?</b></td></tr>
					<tr><td height=\"10\" bgcolor=\"#cfcfcf\" width=\"60\" align=\"center\">
						<b><a href=\"events_manage.php?del=2&eid={$_GET['eid']}&barID=$_GET[barID]\">Yes</a></b></td>
  					  	<td width=\"25\"></td>
						<td height=\"10\" bgcolor=\"#cfcfcf\" width=\"60\" align=\"center\">
						<b><a href=\"events_manage.php?barID=$_GET[barID]\">No</a></b></td>
					</tr>
				</table><br />";
	}else if (isset($_GET['del']) && $_GET['del'] == 2 && checkUserBar($_GET[barID]) ){ // Verified Data
		$mysql->query("DELETE FROM `calendar_events` WHERE `id`={$_GET['eid']}");
		$msg = "User Deleted Successfully!";
		
	}else if(!checkUserBar($_GET[barID]) ){
	    $msg = "You are trying to delete an event for a bar that is not claimed by you.  Please log in as the correct user for the bar and then delete the event.<br />";
	}


$events = $mysql->query("select * FROM `calendar_events` WHERE bar_id = '".quote_smart($_GET[barID])."' ORDER BY `event_date` DESC");
// Start Content
ob_start();
echo "<a href=\"/admin/login/controlpanel.php\">Back to Control Panel</a> / <b>Manage Events</b><hr/>";
echo "{$msg}";
echo "<b>Events</b><br/>";
echo "<a href=\"events_add.php?barID=$_GET[barID]\">Add New Event</a>";
if ($events){
	echo "<table width=\"500\">";
	echo "<tr><th style=\"width:100px; text-align:left;\">Options</th>
	          <th style=\"text-align:left;\">Event</th>
			  <th  style=\"width:150px; text-align:left;\">Date</th></tr>";
	while ($mr = $events->fetch_array()){
		$delete = "<a href=\"events_manage.php?del=1&eid={$mr['id']}&text={$mr['event_title']}&barID=$_GET[barID]\">
								<img src=\"/images/icons/delete.jpg\" border=\"0\"/>
							</a>";
		$edit = "<a href=\"events_edit.php?eid={$mr['id']}&barID={$_GET['barID']}\">
								<img src=\"/images/icons/edit.jpg\" border=\"0\"/>
							</a>";
		echo "<tr><td>{$edit} {$delete}</td><td><a href=\"events_edit.php?eid={$mr['id']}&barID={$_GET['barID']}\">{$mr['event_title']}</a></td><td>{$mr['event_date']} - {$mr['event_time']}</td></tr>";
	}
	echo "</table>";
}

$cnt = ob_get_contents();
ob_end_clean();
/* Save Content */
$doc->DOMChangeTemplate("content",$cnt);
$doc->writeHTML();
