<?php
include '../connect.php';
require_once("../../core.php");
if(!$login->is_logged_in()){ Http::redirect_to_url('login.php');	}
if(!checkUserBar($_GET[barID])){ HTTP::redirect_to_url("../login/controlpanel.php");    }
if(empty($_GET[barID])){Http::redirect_to_url('../login/controlpanel.php'); }
//ob_start();
$hdr = ob_get_contents();
ob_clean();
//$doc->DOMChangeTemplate("head",$hdr);

// Start Content
ob_start();
echo "<script language=\"JavaScript\" src=\"/themes/default/calendarpopup.js\"></script>
    <script language=\"JavaScript\" src=\"/themes/default/AnchorPosition.js\"></script>
	<script language=\"JavaScript\" src=\"/themes/default/date.js\"></script>
<script language=\"JavaScript\" src=\"/themes/default/PopupWindow.js\"></script>
<script language=\"JavaScript\">document.write(getCalendarStyles());</script>";

	if (isset($_POST['submit'])){
		try {
			$controller = new CommandController();
			 $context = $controller->getContext();
			 $context->addParm('action', 'editevent' );
			 $context->addParm('event_id', $_POST['event_id'] );
			 $context->addParm('event_date', $_POST['event_date'] );
			 $context->addParm('event_time', $_POST['event_time'] );
			 $context->addParm('event_title', $_POST['event_title'] );
			 $context->addParm('event_text', $_POST['event_text'] );
			 $context->addParm('event_user', $_POST['event_user'] );
			 $context->addParm('bar_id',$_POST['barID']);
			 $controller->process();
			 
			 Http::redirect_to_url("events_manage.php?barID={$_POST['barID']}");
		}catch(specialException $e){
			echo "Error";
		}
	}

echo "<h3>Edit Event</h3><hr />";
echo "<a href=\"events_manage.php?barID=$_GET[barID]\">Back To Manage Events</a><br/>";
if (isset($_GET['eid'])){
	$mysql = Preferences::getInstance('database')->get('connection');
	$r = $mysql->query("select * from `calendar_events` where `id`={$_GET['eid']}");
	$mr = $r->fetch_array();
echo "<form action=\"events_edit.php\" method=\"POST\">";
echo "<input type='hidden' name='barID' value='$_GET[barID]' />";
echo "<div id=\"calendarwin\" style=\"position:absolute;visibility:hidden;background-color:white;layer-background-color:white;\"></div>
<script language=\"JavaScript\" id=\"js13\">
  var calstart = new CalendarPopup('calendarwin');
  calstart.setReturnFunction(\"setISOstartDate\");
  function setISOstartDate(y,m,d) {
     document.forms[0].event_date.value=y+'-'+LZ(m)+'-'+LZ(d);
  }
</script>";
echo "<input type=\"hidden\" name=\"event_user\" value=\"1\" />";
echo "<input type=\"hidden\" name=\"event_id\" value=\"{$_GET['eid']}\" />";
echo "<b>Event Title:</b><br/><input type=\"text\" name=\"event_title\" value=\"{$mr['event_title']}\"/><br/><br/>";
echo "<b>Event Date:</b><br/><input type=\"text\" name=\"event_date\"  value=\"{$mr['event_date']}\"/> <a href=\"#\" onclick=\"calstart.select(document.forms[0].event_date,'anchor13','MM/dd/yyyy');return false;\" 
title=\"calstart.select(document.forms[0].event_date,'anchor13','MM/dd/yyyy');return false;\" 
name=\"anchor13\" ID=\"anchor13\"><img src=\"/images/icons/icon_calendar.jpg\" border=\"0\" /></a><br/><br/>";
echo "<b>Event Time:</b><br/><input type=\"text\" name=\"event_time\"  value=\"{$mr['event_time']}\"/><br/><br/>";
echo "<b>Event Text</b>";
echo "<div>
			<textarea name=\"event_text\" rows=\"15\" cols=\"40\">{$mr['event_text']}</textarea>
		</div><br/>";
echo "<input type=\"submit\" name=\"submit\" value=\"Submit\" />";
echo "</form>";
}else{
	echo "Error: No Event Id";
}
$cnt = ob_get_contents();
ob_end_clean();
/* Save Content */
$doc->DOMChangeTemplate("content",$cnt);
$doc->writeHTML();
