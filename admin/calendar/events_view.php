<?php

require_once("../../core.php");
if(!$login->is_logged_in()){ $login->redirect_to_url("login.php"); 	}

if(isset($_POST['pid'])){ $_GET['pid'] = $_POST['pid']; }

ob_start();
$mysql = Preferences::getInstance('database')->get('connection');
// Publish
if (isset($_POST['submit'])){
		try {
			$controller = new CommandController();
			 $context = $controller->getContext();
			 $context->addParm('action', 'publishpage' );
			 $context->addParm('page_id', $_POST['pid'] );
			 $context->addParm('page_revision', $_POST['publish'] );
			 $controller->process();
			 
		}catch(specialException $e){
			echo "Error: Unable to publish to the live site.";
		}
	}
	
// Start Content
$pages_main = $mysql->query("select * FROM `rb_pages` WHERE `id`='{$_GET['pid']}'");
$mn = $pages_main->fetch_array();
$pages = $mysql->query("select * FROM `rb_pages_content` WHERE `page_id`='{$_GET['pid']}'");



echo "<h3>Manage Static Content - Details</h3><hr />";
if ($pages){
	echo "<b>Page Name:</b> {$mn['page_name']}<br/>";
	
	echo "<b>Revisions:</b><br/>";
	if ($pages){
		echo "<form action=\"admin_viewpage.php\" method=\"POST\">";
		echo "<input type=\"hidden\" name=\"pid\" value=\"{$_GET['pid']}\" />";
		echo "<table width=\"500\">";
		echo "<tr><th>Options</th><th>Page Name</th><th>Last Modified</th><th>Publish</th></tr>";

		while ($mr = $pages->fetch_array()){
		$edit   = "<a href=\"admin_editpage.php?id={$mr['id']}\"><img src=\"images/icons/edit.gif\" border=\"0\"/></a>";
		if ($mn['publish_id'] == $mr['id']){
			$publish = "<input type=\"radio\" name=\"publish\" value=\"{$mr['id']}\" checked=\"checked\"/>";
		}else{
			$publish = "<input type=\"radio\" name=\"publish\" value=\"{$mr['id']}\" />";
		}

		echo "<tr><td>{$edit}</td><td>Revision {$mr['version']}</td><td>{$mr['last_modified']}</td><td>{$publish}</td></tr>";
			
		}
		echo "</table>";
		echo "<input type=\"submit\" name=\"submit\" value=\"Publish\" />";
		echo "</form>";
	}
}

$cnt = ob_get_contents();
ob_end_clean();
/* Save Content */
$doc->DOMChangeTemplate("content",$cnt);
$doc->writeHTML();
