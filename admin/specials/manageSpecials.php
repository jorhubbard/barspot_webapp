<?php
include '../connect.php';
include_once '../../core.php';
ob_start();

function drawForm($barID,$day,$special,$specialID,$flag)
{
   $dayArray = array('All','Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Weekday');
   
   $string .="<form action='manageSpecials.php' method='POST'>";
   $string .="<input type='hidden' name='barID' value='$barID' />";
   $string .="<input type='hidden' name='flag' value='$flag' />";
   $string .="<input type='hidden' name='specialID' value='$specialID' />";
   $string .="Day: <select name='day'>";
   for($x=0;$x<=8;$x++)
   {
      $string .="<option value='$x' ";
      if(!empty($day))
      {
	 if($x == $day)
	 {
	    $string .="selected='yes'";
	 }
      }
      else
      {
	 if($_POST[day] == $x)
	 {
	    $string .="selected='yes'";
	 }
      }

      $string .=">$dayArray[$x]</option>";
   }
   $string .="</select> ";
   $string .="Special: <input type='text' name='special' size='50' maxlength='160' value=\"$special\" />(Max: 160 characters)";
   $string .="<input type='submit' name='submit' value='Save' />";
   $string .="</form>";

   return $string;

}
function insertSpecial($barID,$day,$special)
{
   $sql = "INSERT INTO specials (barID, specialDay, special) VALUES ('".quote_smart($barID)."','".quote_smart($day)."','".quote_smart($special)."');";
   $result = mysql_query($sql);
   if($result)
   {
      return true;
   }
   else
   {
      return false;
   }


}
function drawSpecials($barID)
{
   $sql = "SELECT * FROM specials WHERE barID = '$barID' ORDER BY specialDay ASC";
   $result = mysql_query($sql);
   if($row = mysql_fetch_array($result))
   {
      $string .="
      <table width='70%'>
      <tr>
	 <th width='50px'>&nbsp;</th>
	 <th width='50px'>&nbsp;</th>
	 <th width='75px'>Day</th>
	 <th>Special</th>
      </tr>
      ";
      
      $dayArray = array('All','Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Weekday');
      do
      {
	 $day = $row[specialDay];
	 $string .="
	 <tr>
	    <form action='manageSpecials.php' method='POST'>
	    <td>
	       <input type='hidden' name='barID' value='$_POST[barID]' />
	       <input type='hidden' name='specialID' value='$row[specialID]' />
	       <input type='submit' name='submit' value='Edit' />
	    </td>
	    </form>
	    <form action='manageSpecials.php' method='POST'>
	    <td>
	       <input type='hidden' name='barID' value='$_POST[barID]' />
	       <input type='hidden' name='specialID' value='$row[specialID]' />
	       <input type='submit' name='submit' value='Delete' />
	    </td>
	    <td>".$dayArray[$day]."</td>
	    <td>$row[special]</td>
	    </form>
	 </tr>
	 ";
	    
      }while($row = mysql_fetch_array($result));
      $string .="</table>";
   }
   else
   {
      $string .= "You currently do not have any specials saved.";
   }
   return $string;

}
function updateSpecial($barID,$specialID,$day,$special)
{
   $sql = "UPDATE specials SET specialDay = '$day' , special = '$special' WHERE barID = '$barID' AND specialID = '$specialID' LIMIT 1;";
   //print("sql - $sql");
   $result = mysql_query($sql);
   if($result)
   {
      return true;
   }
   else
   {
      return false;
   }
   

}
function confirmDelete($barID,$specialID)
{
   $string .="Are you sure you want to delete this special? 
   <form action='manageSpecials.php' method='POST'>
   <input type='hidden' name='barID' value='$barID' />
   <input type='hidden' name='specialID' value='$specialID' />
   <input type='hidden' name='flag' value='Confirm' />
   <input type='submit' name='confirm' value='Yes' />
   |
   <input type='submit' name='confirm' value='No' />
   </form>
   ";
   return $string;
}
function deleteSpecial($specialID)
{
   $sql = "DELETE FROM specials WHERE specialID = '".quote_smart($specialID)."' LIMIT 1;";
   $result = mysql_query($sql);
   if($result)
   {
      return true;
   }
   else
   {
      return false;
   }

}


if($_SESSION[userid])
{
   if($_POST[barID])
   {

      //if(in_array(1,$_SESSION['sessgrouplist']) || ($_SESSION[sessuserid] == $_POST[barID]) )
      //if($_SESSION['admin'] <= 1)
      if ( /*in_array(1,$_SESSION['groupList']) ||*/ $_SESSION['usertype'] == 2 )
      {
	 //print("okay, should be a go from here");
	 //checks to see if we need to insert/update/delete records
	 if($_POST[flag] == 'new' && !empty($_POST[special]) ) //adds the record
	 {
	    if(insertSpecial($_POST[barID],$_POST[day],$_POST[special]) )//inserts the special  
	    {
	       print("Your item has been added.");
   
	    }
	    else
	    {
	       print("There was an error adding your item.  Please try again.  
	       If you have continued problems please contact our webmaster at 
	       <a href='mailto:webmaster@hotbarspot.com'>webmaster@hotbarspot.com</a>.");
	    }
	    $flag = 'new';
	 }
	 elseif($_POST[flag] == 'Edit' && $_POST[submit] == 'Save') //edits the record
	 {
	    if(updateSpecial($_POST[barID],$_POST[specialID],$_POST[day],$_POST[special]) )
	    {
	       print("Your item has been updated.");
   
	    }
	    else
	    {
	       print("There was an error updating your item.  Please try again.  
	       If you have continued problems please contact our webmaster at 
	       <a href='mailto:webmaster@hotbarspot.com'>webmaster@hotbarspot.com</a>.");
	    }
	    $flag = 'new';
	 }
	 elseif($_POST[submit] == 'Delete') //confirms deletion of the record
	 {
	    print confirmDelete($_POST[barID],$_POST[specialID]);
	    $sql = "SELECT * FROM specials WHERE specialID = '".quote_smart($_POST[specialID])."' LIMIT 1;";
	    //print("sql = $sql");
	    $result = mysql_query($sql);
	    if(mysql_affected_rows() == 1)
	    {
	       $row  = mysql_fetch_array($result);
	       //sets variables that will be passed to the draw form function
	       $day	   = $row[specialDay];
	       $special	   = $row[special];
	       $specialID  = $_POST[specialID];
	       $flag	   = 'Edit';

	    }
	    else
	    {
	       print("The item you are trying to delete can not be retrieved at this time.   Please try again.  
	       If you have continued problems please contact our webmaster at 
	       <a href='mailto:webmaster@hotbarspot.com'>webmaster@hotbarspot.com</a>.");
	    }				     
	    $flag = 'Edit';
	 }
	 elseif($_POST[confirm] == 'Yes' && !empty($_POST[specialID]) )
	 {
	    if(deleteSpecial($_POST[specialID]) )//inserts the special  
	    {
	       print("Your item has been deleted.");
   
	    }
	    else
	    {
	       print("There was an error deleting your item.  Please try again.  
	       If you have continued problems please contact our webmaster at 
	       <a href='mailto:webmaster@hotbarspot.com'>webmaster@hotbarspot.com</a>.");
	    }
	    $flag = 'new';
	 }
	 else
	 {
	    $flag = 'new';
	 }

	 //this pulls pertinent info if we are editing
	 if($_POST[submit] == 'Edit' && !empty($_POST[specialID]) )
	 {
	    $sql = "SELECT * FROM specials WHERE specialID = '".quote_smart($_POST[specialID])."' LIMIT 1;";
	    //print("sql = $sql");
	    $result = mysql_query($sql);
	    if(mysql_affected_rows() == 1)
	    {
	       $row  = mysql_fetch_array($result);
	       //sets variables that will be passed to the draw form function
	       $day	   = $row[specialDay];
	       $special	   = $row[special];
	       $specialID  = $_POST[specialID];
	       $flag	   = 'Edit';

	    }
	    else
	    {
	       print("The item you are trying to edit can not be retrieved at this time.   Please try again.  
	       If you have continued problems please contact our webmaster at 
	       <a href='mailto:webmaster@hotbarspot.com'>webmaster@hotbarspot.com</a>.");
	    }				     
	    
	 }
      

	 print drawForm($_POST[barID],$day,$special,$specialID,$flag);
	 print("<br /><br />");
	 print drawSpecials($_POST[barID]);



      }//ends check for proper permissions
      else
      {
	 print("You do not have sufficient privileges to edit this page for the user that was selected.");
      }
      
   }//ends check for post
   else
   {
      print("You can not link directly to this page.  
      You will have to go through the proper links to 
      navigate to this page with the correct information.");      

   }
}//ends check for sessuser (logged in)
else
{
   print("You must be logged in to view this page.");
}

$cnt = ob_get_contents();
ob_clean();

/* Save Content */
$doc->DOMChangeTemplate("content",$cnt);
$doc->WriteHTML();
