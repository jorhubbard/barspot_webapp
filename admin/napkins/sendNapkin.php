<?PHP

/*****************************************************************************
*
*  File:          /admin/napkins/sendNapkin.php
*  Author:        Jarrod B. Reuter
*  Created:       2009.08.06
*
*  Description:   Sends or Discards current message
*
*****************************************************************************/

/* Config */
include('../../core.php');
include('napkins.php');
include('transactions.php');
include '../connect.php';
include '../functions.php';
/* Connecting to Database and grabbing tables */
$db = Preferences::getInstance('database')->get('connection');
$napkins = Preferences::getInstance('napkin')->get('napkinTable');
$folders = Preferences::getInstance('napkin')->get('napkinFolders');
$toTable = Preferences::getInstance('napkin')->get('napkinToTable');

$usertable = Preferences::getInstance('login')->get('SqlUserTable');

ob_start();

if( !$login->is_logged_in()) { HTTP::redirect_to_url("../login/login.php"); }

//print ("testing page link");

switch ($_POST['submit']){
   case 'Send Napkin':
      $toList = explode(',',$_POST['to']);
      if ( isset($_POST['napkinID']) ){
         $replyID = $_POST['napkinID'];
	 $parentID = $_POST['parentID'];
      } else {
	 $replyID = 0;
	 $parentID = 0;
      }
      $subject = $_POST['subject'];
//      $message = replace_entities( $message );
      $message = $db->real_escape_string( str_replace("<br>","<br/>", $_POST['message'] ) );

      
      $sql1 = "INSERT INTO `{$napkins}` ( `napkinID`, `modifiedTimestamp`, `createdTimestamp`, `sentTimestamp`, `fromUserID`, `subject`, `message` ) VALUES ( '', NOW(), '', NOW(), '$_SESSION[userid]', '$subject', '$message' ) ";
//echo "<pre>$sql1</pre>";

      $result = $db->query($sql1);
//      echo $db->error;

      $id = mysqli_insert_id( $db );
//      echo "<pre>$id</pre>";

      if ( $replyID > 0 ){
	 $sql = "UPDATE `{$toTable}` SET `replyStatus` = '1' WHERE `napkinID` = $replyID AND `userID` = $_SESSION[userid]";
//	 $sql = "UPDATE `{$toTable}` SET `replyStatus` = '1', `parentID` = $parentID WHERE `napkinID` = $replyID AND `userID` = $_SESSION[userid]";

//echo "<pre>$sql</pre>";
	 $result = $db->query($sql);
//	 echo $db->error;
      }

      if ( $parentID > 0 ){
	 $sql = "UPDATE `{$toTable}` SET `deletedStatus` = '0' WHERE `parentID` = $parentID and `userID` = '$_SESSION[userid]'";

//	 echo "<pre>$sql</pre>";
         $result = $db->query($sql);
  //       echo $db->error;
      }
      //$id = 4321;
      if ( 0 != $id ) {

      $first = true;
      foreach ( $toList as $userName ) {
	 if ( $first ) {
	    $sql3 = "SELECT userID, username FROM `{$usertable}` WHERE username LIKE '".trim($userName)."'";
	    $first = false;
	 } else {
	    $sql3 .= " OR username LIKE '".trim($userName)."'";
	 }
      }
//      echo "<pre>$sql3</pre>";
      
      $result = $db->query($sql3);
//      echo $db->error;

      $first = true;
      while ( $row = $result->fetch_array() ){
	 if ( $first ) {
	    $sql2 = "INSERT INTO `{$toTable}` ( `toID`, `napkinID`, `parentID`, `folderTypeID`, `flagged`, `userID`, `replyStatus`, `deletedStatus`, `deletedTimestamp` ) VALUES ( '', '$id',";
//	    $sqlUpdate = "";
	    if ( $parentID > 0 ) {
	       $sql2 .= "'$parentID',";
	       $sqlUpdate = "UPDATE `{$toTable}` SET `deletedStatus` = '0', `folderTypeID` = '1' WHERE (`parentID` = $parentID AND `userID` = $row[userID])";
	    } else {
	       $sql2 .= "'$id',";
	    }
	    $sql2 .= "'1','','".$row['userID']."', '','',NULL ) ";
	     
	    $first = false;
	 } else {
	    $sql2 .= ",('','$id',";
	    if ( $parentID > 0 ) {
	       $sql2 .= "'$parentID',";
	       $sqlUpdate .= " OR (`parentID` = $parentID AND `userID` = $row[userID])";
	    } else {
	       $sql2 .= "'$id',";
	    }
	       $sql2 .= "'1','','".$row['userID']."', '','',NULL )";
	 }
//	 echo "<pre>$sql2</pre>"
//	 $result2=$db->query($sql2);
//	 echo $db->error;

/**********************************
added by cj estel 2010-01-07
checks to see if the user has their preference set to email them when 
they receive napkins and send message if they do
**********************************/

if(checkPref($row[userID],'emailNapkins') )
{
	 $to = retrieveUserInfo($row[userID]);
	 $email = $to[userEmail];
	 
$from = retrieveUserInfo($_SESSION[userid]);
	 
	 $body = "
".$from['username']." has sent you a bar napkin.

From: ".$from['userFirst']." ".$from['userLast']."

$message
";
systemEmail($email,$subject,$body);
}

/***********end cj block *********************/
      } //while loop
//      echo "<pre>$sql2</pre>";
      $result2=$db->query($sql2);
//      echo $db->error;
    
      if ( $parentID > 0 ) {
//         echo "<pre>$sqlUpdate</pre>";
         $result=$db->query($sqlUpdate);
//	 echo $db->error;
      }



      } else {
	 print ("failed to insert napkin");
      }

//      echo "<pre>$sql1</pre>";
//      echo "<pre>$sql2</pre>";
//      $insert = $db->query($sql);

//      print ("$toList<br/>$subject<br/>$message<br/>");
//      print ("Napkin Sent");
      HTTP::redirect_to_url("index.php?ref=napkinSent");
//      HTTP::redirect_to_url("index.php");
      break;
   case 'Save Now':
      print ("Saving...");
      break;
   case 'Discard':
      print ("Clear out any saved messages with this ID");
      break;
}


$cnt = ob_get_contents();
ob_clean();
/* Save Content */
$doc->DOMChangeTemplate("content",$cnt);
$doc->WriteHTML();

ob_end_flush();

?>
