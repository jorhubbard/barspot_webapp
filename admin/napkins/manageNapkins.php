<?PHP

/*****************************************************************************
*
*  File:          /admin/napkins/manageNapkins.php
*  Author:        Jarrod B. Reuter
*  Created:       2009.08.17
*
*  Description:   Form submits to this page to delete napkins, more features
*		  should come eventually (add/remove labels, etc)
*
*****************************************************************************/

   $ids = $_POST['napkinIDs'];			   // array of conversations 
						      // checked

   /* Config */
   include('../../core.php');
   include('napkins.php');
   include('transactions.php');

   /* Connecting to Database and grabbing tables */
   $db = Preferences::getInstance('database')->get('connection');
   $napkins = Preferences::getInstance('napkin')->get('napkinTable');
   $folders = Preferences::getInstance('napkin')->get('napkinFolders');
   $toTable = Preferences::getInstance('napkin')->get('napkinToTable');

   $usertable = Preferences::getInstance('login')->get('SqlUserTable');

   ob_start();

   if( !$login->is_logged_in()) { HTTP::redirect_to_url("../login/login.php"); }

   $boxID = $_POST['boxID'];

   switch ($_POST['submit']){
      case 'Delete Selected':
	 //print("Deleting message(s)");
	 print_r($ids);

	 $first = true;
	 foreach ( $ids as $val ) {
	    if ( $first ) {
	    $sql = "UPDATE `{$toTable}` SET `folderTypeID` = '3', `deletedStatus` = '1', `deletedTimestamp` = NOW() WHERE (
	                            ( parentID = $val )";
				    /*
	 $sql = "SELECT * FROM `{$napkins}` AS N
		     JOIN `{$toTable}` AS T ON N.napkinID = T.napkinID
		     JOIN `{$folders}` AS F ON T.folderTypeID = F.folderTypeID
		     JOIN `{$usertable}` AS U ON N.fromUserID = U.userID
		     WHERE (
			( T.parentID = $val )";*/
	       $first = false;
	    } else {
	       $sql .= " OR ( parentID = $val )";
	    }
	 }
	 $sql .= " AND ( userID =  $_SESSION[userid] )
		  )";
//	 echo "<pre>$sql</pre>";
	 $result=$db->query($sql);
//	 echo $db->error;
	 HTTP::redirect_to_url("index.php?box=$boxID");
	 
	 break;

      case 'Mark Unread':
	 //print("Marking as unread");
//	 print_r($ids);

         $first = true;
	 foreach ( $ids as $val ) {
	    if ( $first ) {
	       $sql = "UPDATE `{$toTable}` SET `readStatus` = '0' WHERE (
			   ( parentID = $val )";
	       $first = false;
	    } else {
	       $sql .= " OR ( parentID = $val )";
	    }
	 }
	 $sql .= " AND ( userID =  $_SESSION[userid] )
	          )";
//	 echo "<pre>$sql</pre>";
	 $result=$db->query($sql);
//	 echo $db->error;
	 HTTP::redirect_to_url("index.php?box=$boxID");

	 break;

      case 'Mark Read':
	 print("Marking as Read");
	 print_r($ids);

	 $first = true;
	 foreach ( $ids as $val ) {
	    if ( $first ) {
	       $sql = "UPDATE `{$toTable}` SET `readStatus` = '1' WHERE (
		  ( parentID = $val )";
	       $first = false;
	    } else {
	       $sql .= " OR ( parentID = $val )";
	    }
	 }
	 $sql .= " AND ( userID =  $_SESSION[userid] )
	    )";
//	 echo "<pre>$sql</pre>";
	 $result=$db->query($sql);
//	 echo $db->error;
	 HTTP::redirect_to_url("index.php?box=$boxID");

	 break;

      case 'Move to Inbox':
//	 print("Moving to inbox - need to build query");

//	 print_r($ids);

	 $first = true;
	 foreach ( $ids as $val ) {
	    if ( $first ) {
	       $sql = "UPDATE `{$toTable}` SET `folderTypeID` = '1', deletedStatus = '0' WHERE (
		  ( parentID = $val )";
	       $first = false;
	    } else {
	       $sql .= " OR ( parentID = $val )";
	    }
	 }
	 $sql .= " AND ( userID =  $_SESSION[userid] )
	    )";
//	 echo "<pre>$sql</pre>";
	 $result=$db->query($sql);
//	 echo $db->error;
	 HTTP::redirect_to_url("index.php?box=$boxID");

	 break;
   }

   print("<br/><a href=\"index.php\">Napkins</a>");

   $cnt = ob_get_contents();
   ob_clean();
   /* Save Content */
   $doc->DOMChangeTemplate("content",$cnt);
   $doc->WriteHTML();

?>
