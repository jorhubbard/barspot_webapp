<?PHP
/*****************************************************************************
*
*  File:	  /admin/napkins/compose.php
*  Author:	  Jarrod B. Reuter
*  Created:	  2009.08.07
*  
*  Description:	  Ability to compose a new napkin and send it.
*
*****************************************************************************/

/* Config */
include('../../core.php');
include('napkins.php');

$box = 0;

/* Connecting to Database and grabbing tables */
$db = Preferences::getInstance('database')->get('connection');
$napkins = Preferences::getInstance('napkin')->get('napkinTable');
$folders = Preferences::getInstance('napkin')->get('napkinFolders');
$toTable = Preferences::getInstance('napkin')->get('napkinToTable');

$usertable = Preferences::getInstance('login')->get('SqlUserTable');

$thread = "";
$fromID = "";
$messageSub = "";
if ( isset($_GET['m']) ) {
//   $messageSub = $_GET['re'];
   $fromID = $_GET['id'];
   $thread = $_GET['m'];
} else {
   $messageSub = '';
    if (isset($_GET['id']))
        $fromID = $_GET['id'];
}

ob_start();

/* Put Code Here */
print ("<link href=\"css/email.css\" rel=\"stylesheet\" type=\"text/css\"/>");

if( !$login->is_logged_in()) { HTTP::redirect_to_url("../login/login.php"); }


print ("<table id=\"emailSystem\" summary=\"Napkins\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\n");
print ("<tbody>\n
      <tr><td id=\"folderList\">\n");

   /* Display folderlist, highlight $folderID, link to same folder */
   printFolders( $box, true );

print ("</td><td id=\"secondList\">\n");
print ("<table id=\"composer\" summary=\"Napkin\"><tbody>\n");

   if ( '' == $thread ) {
      printComposer();
   } else {
//      print("Here - sending thread<br/>\n");
      printComposer($thread, $fromID, $_GET['la']);
   }
/*
   if ( '' == $messageSub ) { 
      printComposer();
   } else {
      printComposer($messageSub, $fromID, $_GET['la']);
   }
   */

print (" </tbody> </table>\n
         </td></tr></tbody></table>\n");

/* Get Contents, append, then write page using template */
$cnt = ob_get_contents();
ob_clean();
/* Save Content */
$doc->DOMChangeTemplate("content",$cnt);
$doc->WriteHTML();

ob_end_flush();

?>
