<?PHP

/****************************************************************************
*
*  Function:	     printFolders
*  Author:	     Jarrod B. Reuter
*
*  Params:	     $b		 - Integer to match with folderTypeID from DB
*		     $linkBack	 - Bool to determine if we should linke to 
*				    the current mailbox or not
*
*****************************************************************************/

function printFolders ( $b, $linkBack ) {
//$b = $_GET['b'];

$db = Preferences::getInstance('database')->get('connection');
$folders = Preferences::getInstance('napkin')->get('napkinFolders');

$folderList = $db->query("SELECT *
                           FROM `{$folders}`
			                              WHERE folderName != 'userCreated'");

						      echo $db->error;

if ( (0 == $b) && (!$linkBack) ) {
   print("<div id=\"composeNapkin\">Send a Napkin</div>\n");
} else if ( (0 == $b) && ($linkBack) ) {
   print("<a href=\"compose.php#composer\"><div id=\"currentNapkin\">Send a Napkin</div></a>\n");
} else {
   print("<a href=\"compose.php#composer\"><div id=\"composeNapkin\">Send a Napkin</div></a>\n");
}

$retVal = "";

while ( $list = $folderList->fetch_array() ) {
   if ( ($list['folderTypeID'] == $b) && (!$linkBack) ) {
      print("<div id=\"currentFolder\">$list[folderName]</div>\n");
      $retVal = $list['folderName'];
   } else if ( ($list['folderTypeID'] == $b) && ($linkBack) ) {
      print("<a href=\"index.php?box=$list[folderTypeID]\"><div id=\"currentFolder\">$list[folderName]</div></a>\n");
      $retVal = $list['folderName'];
   } else {
      print("<a href=\"index.php?box=$list[folderTypeID]\"><div id=\"folderItem\">$list[folderName]</div></a>\n");
   }
}

   return $retVal;
}

function printSummary ( $message, $len = 100, $delim = "..." ) {

   $tagfree = strip_tags($message);
   $len = $len - mb_strlen($delim, 'UTF-8');	   // This allows returning 
						      // actual length string

   /* If the original message didn't have tags, no point in doing anything else */
   if ( mb_strlen($tagfree, 'UTF-8') == mb_strlen($message, 'UTF-8') ) {
      /* If message is small enough to fit, just return it */
      if ( mb_strlen($message) <= ( $len+mb_strlen($delim, 'UTF-8') ) ) {
	 print("returning full message");
	 return $message;
      } else {
	 print("returning chopped message");
	 return mb_substr($message, 0, $len, 'UTF-8').$delim;
//	 return substr($message, 0, strrpos(substr($message, 0, $len), ' ')).$delim;
      }
   }

   /* This procedure chops original message to $len but skips over markup when counting chars */
   /* Set up varialbes to make new string. */
   $i = 0;					   // character pos in $message
   $j = 0;					   // character pos in $tagfree
   $insideTag = false;
   $newstr = '';				   // string to be returned
   $buffer = '';
   while ( $j <= $len ) {			   // while we haven't hit 
						      // the end
      $newstr .= $message[$i];
      /* We need to add more here to allow < and > as text */
      if ( $message[$i] == '<' ){
	 $insideTag = true;
//	 print("Length: $len\nGoing in tag: ");
      } else if ( $message[$i] == '>' ) {
	 $insideTag = false;
	 ++$len;				   // we still want the >
      }
      if ( ($message[$i] != $tagfree[$j]) || ($insideTag===true) ) {	   // is it markup?
//	 print($message[$i]);
	 ++$len;				   // if so, add to total len
	 ++$i;					   // move pointer for char
      } else {	  
	 ++$i;					   // if not, still move ptr
	 ++$j;					   // also used one viewable ch
      }
   }

   print ("new length: $len");

   /* If the new string is small enough with markup, we return *//*
   print( "string length is: ".mb_strlen($newstr, 'UTF-8') );
   if ( mb_strlen($newstr, 'UTF-8') <= ( $len+mb_strlen($delim, 'UTF-8') ) ) {
      print("chopping here1");
      return mb_substr($message, 0, $len+mb_strlen($delim, 'UTF-8'), 'UTF-8');
   } else {*/
      /***********************************************************************
      *	 Otherwise, we chop it off at the new length 
      *	 Note  - this will not chop in middle of markup because we added to 
      *	       the length for each markup character 
      ************************************************************************/
      print ( "chopping here2\n");
      $newstr = mb_substr($message, 0, $len, 'UTF-8');
      print ("$newstr\n");
//      $retstr = $newstr;
//      $newstr = substr($message, 0, strrpos(substr($message, 0, $len), ' '));
//   }

//   print("New String is: $newstr - ".$len." characters long");

   $regex = "/(<\w+)(\s*[^>]*)(>)/";
//   $array = preg_split("/<\w+((\s+\w+(\s*(?:\"(.|\n)*?\"|\'(.|\n)*?\'|[^\'\">\s]+))?)+\s*|\s*)/",$message );

   /* Gram all begining tags */
   preg_match_all( $regex, $newstr, $array, PREG_SET_ORDER);

/*
   foreach ( $array as $match ) {
      
      print( "Tag: $match[1] ");
   }
   */

   $regex2 = "/(<\/\w+>)/";
   /* Grab all ending tags */
   preg_match_all( $regex2, $newstr, $array2, PREG_SET_ORDER);

/*
   foreach ( $array2 as $match ) {
      print( "Closing Tag: $match[1] ");
   }*/

   $i = 0;
   $j = 0;
   $asize = count($array);
   $bsize = count($array2);

   /* removes the all but the tag name, and make them lowercase */
   /*
   $a1 = array();
   while ( $i < $asize ) {
      $a1[] = strtolower(str_replace("<","",$array[$i][1]) );
      ++$i;
   }
   $asize = count($a1);
   */

   $a2 = array();
   while ( $j < $bsize ) {
      $a2[] = strtolower(str_replace("</","",str_replace(">","",$array2[$j][1]) ) );
      ++$j;
   }
   $bsize = count($a2);
//   print_r($a2);

//   print ("Size: $size");
   $exceptions = array("br");
//   $a3 = array_diff( $a1, $a2, $exceptions );

   while ( $i < $asize ) {
      $tmp1 = strtolower(str_replace("<","",$array[$i][1]) );
//      print($tmp1."\n");
//      $tmp2 = strtolower(str_replace("</","",$array2[$j][1]) );
//      $tmp2 = str_replace(">","",$tmp2);
//      $index = array_search( $tmp1, $a2 );
//      print ("Index: $index\n");
      $in2 = in_array($tmp1, $a2);
      if ( ( !$in2 ) && (!in_array($tmp1, $exceptions)) ) {
//	 $index = array_search( $tmp1, $a2 );
//	 $index = array_search( $tmp1, $a2 );
	 $a3[] = $tmp1;
	 /* Couldn't find a way to remove an item from the middle of the 
	    array in PHP */
//	 $a2[$index] = "";		   // effectively removes tag
      } else {
	 $index = array_search( $tmp1, $a2 );
	 $a2[$index] = "";                 // effectively removes tag
      }
      ++$i;
   }

//   $i = count($a3);
   while ( count($a3) > 0 ) {
      $tmp = array_pop($a3);
      $newstr .= "</".$tmp.">";
//      --$i;
   }
   /*
   if ( count( > 0 ) {
      print ("Broken tags: ");
      print_r ($a3);
   }
   */
   return $newstr.$delim; 

/*
   $i = 0;
   $csize = count($a3);
   while ( $i < $csize ) {
      
      ++$i;
   }
   */

   /*
   while ( ($i < $asize) && ($j < $bsize ) ) {
      $tmp1 = strtolower(str_replace("<","",$array[$i][1]) );
      $tmp2 = strtolower(str_replace("</","",$array2[$j][1]) );
      $tmp2 = str_replace(">","",$tmp2);
      if ( !in_array($tmp1,$exceptions) ) {
	 if ( $tmp1 != $tmp2 ) {
	    print("We have a problem, broken tag");
	 }
	 print("Open: $tmp1");
	 print("Close: $tmp2");
	 ++$i;
	 ++$j;
      } else {
	 ++$i;
      }
//      print("Open: $tmp1");
//      print("Close: $tmp2");
   }
*/

//   print_r($array);
}

function printMailboxButtons( $boxID ) {
   if ( (2 != $boxID) && (3 != $boxID) ){
      print ("<input type=\"submit\" name=\"submit\" value=\"Delete Selected\" />");
   }
   if ( 2 != $boxID ) {
      print ("<input type=\"submit\" name=\"submit\" value=\"Mark Unread\" />");
      print ("<input type=\"submit\" name=\"submit\" value=\"Mark Read\" />");
   }
   if ( $boxID >= 3 ) {
      print ("<input type=\"submit\" name=\"submit\" value=\"Move to Inbox\" />");
   }
   print ("<input type=\"hidden\" name=\"boxID\" value=\"$boxID\" />");
}

function printComposeButtons( $idAppend ) {
   print ("<input type=\"submit\" name=\"submit\" value=\"Send Napkin\" />");
//   print ("<input id=\"autoSave$idAppend\" type=\"submit\" name=\"submit\" value=\"Save Now\" disabled=\"true\"/>");
   print ("<input type=\"submit\" name=\"submit\" value=\"Discard\" />");
}

function printComposer ( $thread = '', $fromID = '', $lastAuthor = '' ) {
   print("<script type=\"text/javascript\">");

   //print("alert('testing javascript in composer code.');");
   print("
   var req;
   var message = '';
   var subject = '';
   var to = '';
   var t;
   var s;

   function evalRequest(url) {
      req = false;              // branch for native XMLHttpRequest object
      if (window.XMLHttpRequest){
         try {
	    req = new XMLHttpRequest();
         } catch (e) {
            req = false;
         }
      }else if (window.ActiveXObject){
         try {
	    req = new ActiveXObject(\"Msxml2.XMLHTTP\");
         } catch(e) {
	    try {
	       req = new ActiveXObject (\"Microsoft.XMLHTTP\");
	    } catch(e) {
	       req = false;
	    }
	 }
      }
      if (req){
	 req.onreadystatechange = function() {
	    if (req.readyState==4 && req.status==200) {
	       eval(req.responseText);
	    }
	 }
	 req.open(\"GET\", url, true);
	 req.send(null);
      }
   }

   function saveDraft() {
//      alert('auto saving now...');
      disableButton('autoSave1');
      disableButton('autoSave2');
   }

   function enableButton( id ) {
//      alert('enable called');
//      clearTimeout(t);      
      document.getElementById(id).disabled=false;
   }

   function disableButton( id ) {
      document.getElementById(id).disabled=\"true\";
   }

   function autoSave() {
//      alert('autoSave called');
//      message = document.getElementById('message').innerHTML;
//      subject = document.getElementById('subject').innerHTML;
//      to = document.getElementById('to').innerHTML;
//      alert('autosave called');
      if ( message != document.getElementById('message').innerHTML ) {
	 message = document.getElementById('message').innerHTML;
         clearTimeout(s);
         clearTimeout(t);
	 s = setTimeout(\"saveDraft();\",10000);
         enableButton();
      }
//      disableButton();
//      t = setTimeout(\"enableButton();\",3000);
   }

//   setTimeout(\"enableButton('autoSave1');\", 1000 );
//   setTimeout(\"enableButton('autoSave2');\", 1000 );
//   autoSave();
   ");

   print("</script>");

    $row = array();
    $data = "";
   if ( '' != $thread ) {
      /* Connecting to Database and grabbing tables */
      $db = Preferences::getInstance('database')->get('connection');
      $napkins = Preferences::getInstance('napkin')->get('napkinTable');
      $folders = Preferences::getInstance('napkin')->get('napkinFolders');
      $toTable = Preferences::getInstance('napkin')->get('napkinToTable');

      $usertable = Preferences::getInstance('login')->get('SqlUserTable');

      if ( $lastAuthor == $_SESSION['userid'] ) {
	 $sql = " SELECT * FROM `{$napkins}` AS N
	    JOIN `{$toTable}` AS T ON N.napkinID = T.napkinID
	    JOIN `{$usertable}` AS U ON T.userID = U.userID
	    WHERE ( T.userID = '$fromID' ) AND
	       ( N.fromUserID = '$lastAuthor' )
	    AND T.parentID = '$thread'
	    GROUP BY sentTimestamp DESC";
      } else {

      $sql = " SELECT * FROM `{$napkins}` AS N
		  JOIN `{$toTable}` AS T ON N.napkinID = T.napkinID
		  JOIN `{$usertable}` AS U ON N.fromUserID = U.userID
		  WHERE ( T.userID = {$_SESSION['userid']} OR N.fromUserID = {$_SESSION['userid']} )
		  AND ( T.userID = '$fromID' OR N.fromUserID = '$fromID' )
		  AND T.parentID = '$thread'
		  GROUP BY sentTimestamp DESC";
      }
      //echo "<pre>$sql</pre>";		  

      $result = $db->query($sql);
      $row = $result->fetch_array();
      $data = preg_replace("/[\n\r]/", '', $row['message']);
      $data = preg_replace("/[']/","\'",$data);

      $data = stripslashes( $data );
      $data = htmlspecialchars( $data, ENT_COMPAT, 'UTF-8', false);
      $data = html_entity_decode( $data, ENT_COMPAT, 'UTF-8');
   }

   print ("<tr><td><a name=\"composer\"></a>");
   print ("<form name=\"Napkin\" action=\"sendNapkin.php\" method=\"post\" onsubmit=\"return submitForm();\" onkeyup=\"autoSave();\">");

   printComposeButtons('1');


   if ( '' != $thread ) {
      print ("<input type=\"hidden\" name=\"napkinID\" value=\"$row[napkinID]\" />");
      print ("<input type=\"hidden\" name=\"parentID\" value=\"$row[parentID]\" />");
   }

    $to = "";
   if(!empty($row['username']))
   {
      $to = $row['username'];
   }
   else
   {
       if (isset($_GET['id']))
        $to = getUserFromID($_GET['id']);
   }


    $subject = "";
    if (isset($row['subject']))
        $subject = $row['subject'];
   
   print ("<p><div id=\"fieldName\">To: </div><div id=\"fieldInput\"><textarea id=\"to\" name=\"to\" cols=\"75\" rows=\"5\" maxlenght=\"255\">$to</textarea></div></p>");
   print ("<div id=\"spacer\"></div>");
   print ("<p><div id=\"fieldName\">Subject: </div><div id=\"fieldInput\"><input type=\"text\" id=\"subject\" name=\"subject\" value=\"$subject\" size=\"99\" maxlenght=\"255\" /></div></p>");
   print ("<div id=\"spacer\"></div>");

   print ("<script language=\"JavaScript\" type=\"text/javascript\" src=\"js/richtext.js\" />");
   
/******************************RTE********************************************* 
   print ("<script language=\"JavaScript\" type=\"text/javascript\">
   <!--
   function submitForm() {
      updateRTEs();
      return true;
   }
   initRTE(\"../editor/images/\", \"../editor/\", \"\");
   //-->
   </script>
   <noscript><p><b>Javascript must be enabled to use this form.</b></p></noscript>");

   print ("
   <script language=\"JavaScript\" type=\"text/javascript\">
   <!--

   //Usage: writeRichText(fieldname, html, width, height, buttons, readOnly)
   writeRichText('message', '$data', 700, 350, true, false);

   //-->
   </script>");
****************************end RTE*********************************************/

/****************************start textbox**************************************/
$data = strip_tags($data);
print("<textarea style='width:600px; height:350px;margin-left:100px;' name='message'>$data</textarea>");


/*******************************end textbox*************************************/
   printComposeButtons('2');

   print ("</form></td></tr>");
}
function getUserFromID($id)
{
   $db = Preferences::getInstance('database')->get('connection');
   //$user_tbl = Preferences::getInstance('napkin')->get('napkinTable');

   $sql = "SELECT username FROM user_tbl WHERE userId = '$id' LIMIT 1;";
   $result = $db->query($sql);
   $row = $result->fetch_array();

   return $row['username'];

}
?>
