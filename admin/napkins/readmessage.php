<?PHP

/*****************************************************************************
*
*  File:	  /admin/napkins/readmessage.php
*  Author:	  Jarrod B. Reuter
*  Created:	  2009.08.07
*
*  Description:	  Ability to read a message.  Should include the messageID 
*		  and where you linked from.
*  
*****************************************************************************/

/* Config */
include('../../core.php');
include('napkins.php');

/* Connecting to Database and grabbing tables */
$db = Preferences::getInstance('database')->get('connection');
$napkins = Preferences::getInstance('napkin')->get('napkinTable');
$folders = Preferences::getInstance('napkin')->get('napkinFolders');
$toTable = Preferences::getInstance('napkin')->get('napkinToTable');

$usertable = Preferences::getInstance('login')->get('SqlUserTable');

/* Setting up variables from URL */
$folderID   = $db->real_escape_string( strip_tags( $_GET['f'] ) );
$messageID  = $db->real_escape_string( strip_tags( $_GET['t'] ) );

ob_start();

?>

<?

/* Put Code Here */
print ("<link href=\"css/email.css\" rel=\"stylesheet\" type=\"text/css\"/>");

if( !$login->is_logged_in()) { HTTP::redirect_to_url("../login/login.php"); }

/* Run Queries */
if ( 2 == $folderID ) {
   $sql = "SELECT * FROM `{$napkins}` AS N
	    JOIN `{$toTable}` AS T ON N.napkinID = T.napkinID
	    JOIN `{$folders}` AS F ON T.folderTypeID = F.folderTypeID
	    JOIN `{$usertable}` AS U ON N.fromUserID = U.userID
	    WHERE (
	       (N.fromUserID = {$_SESSION['userid']} )
	       OR 
	       (T.userID = {$_SESSION['userid']} AND replyStatus = 1)
	       AND
	       (T.parentID = $messageID )
	       )";
	       /*
   $sql = "SELECT * FROM `{$napkins}` AS N
            JOIN `{$toTable}` AS T ON N.napkinID = T.napkinID
	             JOIN `{$folders}` AS F ON N.folderTypeID = F.folderTypeID
		              JOIN `{$usertable}` AS U ON N.fromUserID = U.userID
			               WHERE N.fromUserID = {$_SESSION['userid']} AND
				                N.napkinID = $messageID";
						*/
} else {
/*
   $sql = "SELECT * FROM `{$napkins}` AS N
			JOIN `{$toTable}` AS T ON N.napkinID = T.napkinID
			JOIN `{$folders}` AS F ON T.folderTypeID = F.folderTypeID
			JOIN `{$usertable}` AS U ON N.fromUserID = U.userID
			WHERE (T.userID = {$_SESSION['userid']} OR N.fromUserID = {$_SESSION['userid']} )AND
			   T.folderTypeID = $folderID AND
			   N.subject = ( SELECT subject FROM `{$napkins}` WHERE napkinID = $messageID)
			   ";
			   */
   $sql = "SELECT * FROM `{$napkins}` AS N
	       JOIN `{$toTable}` AS T ON N.napkinID = T.napkinID".
//	       JOIN `{$folders}` AS F ON T.folderTypeID = F.folderTypeID
	       " JOIN `{$usertable}` AS U ON N.fromUserID = U.userID
	       WHERE (T.userID = {$_SESSION['userid']} OR N.fromUserID = {$_SESSION['userid']} )AND
		  T.parentID = $messageID";
//   $markRead = "UPDATE `{$toTable}` SET `readStatus` = '1' WHERE `parentID` = ''";
		  
}

//echo "<pre>$sql</pre>";

   $result = $db->query($sql);

/*
$folderList = $db->query("SELECT *
                           FROM `{$folders}`
			                              WHERE folderName != 'userCreated'");
						      */

print ("<table id=\"emailSystem\" summary=\"Napkins\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\n");
   print ("<tbody>\n
            <tr><td id=\"folderList\">\n");

   /* Display folderlist, highlight $folderID, link to same folder */
   $folderName = printFolders( $folderID, true );

   print ("</td><td id=\"secondList\">\n");
   print ("<table id=\"conversation\" summary=\"Napkin\"><tbody>\n");

   $first = true;
while ( $row = $result->fetch_array() ) {
//   print ("<a href=\"index.php\">Back to $row[folderName]</a><br/>");
//   print ("<tr><td>");
   if ( $first ) {
      print ("<tr><td class=\"conversationHeader\">");
	 print ("<a href=\"index.php?box=$folderID\">Back to $folderName</a>");
      print ("<div class=\"conversationSubject\">$row[subject]</div>");
      print ("<div class=\"conversationFrom\">From $row[username]</div>");
      print ("<div class=\"conversationDateStamp\">Sent: $row[sentTimestamp]</div>");
      print ("</td></tr><tr><td>");
      $first = false;
      $markRead = "UPDATE `{$toTable}` SET `readStatus` = '1' WHERE `parentID` = '$row[parentID]' AND `userID` = '$_SESSION[userid]'";
      //echo "<pre>$markRead</pre>";
      $readResult = $db->query($markRead);
   } else {

      print ("<tr><td class=\"conversationHeaderRepeat\">");
      print ("<div class=\"conversationFrom\">From $row[username]</div>");
            print ("<div class=\"conversationDateStamp\">Sent: $row[sentTimestamp]</div>");
	          print ("</td></tr>");
      print ("<tr><td>");
   }


   /* Converting data from the editor to something the DOM will like */
   $message = stripslashes($row['message']);				   // Remove escapes i.e. \' = '
   /**************************************************************************
   *
   *  Apparently, htmlentities breaks UTF-8 encoding, also some items might 
   *  already be converted.  This is why we put the last param to false 
   *  (double encoding) 
   *
   *  It seems strange to encode something we know the DOM will encode, but 
   *  we have to make sure everything is encoded once (and only once) before
   *  we decode with html_entity_decode.  The editor encodes some items, but
   *  not others, then if we decode it breaks UTF-8 and the DOM complains
   *
   ***************************************************************************/
   $message = htmlspecialchars( $message, ENT_COMPAT, 'UTF-8', false);	   // Convert to special chars
//   $message = htmlentities( $message, ENT_COMPAT, 'UTF-8' );
   /**************************************************************************
   *
   *  We are stripping out any encoding done by the editor or by us.  The 
   *  DOM encodes for us, and if we encode twice any special characters break
   *
   *  We have to state ENT_COMPAT and UTF-8 even though they are defaults 
   *  because in some cases it breaks UTF if it's not stated.
   ***************************************************************************/
   $message = html_entity_decode( $message, ENT_COMPAT, 'UTF-8');
//   $message = str_replace("&","/&", $message);
//   $message = $db->real_escape_string( $row['message']);

//   $message = htmlentities( stripslashes( $row['message']) );
   print ("<div class=\"conversationMessage\">$message</div></td></tr>");

   if ( $row['fromUserID'] != $_SESSION['userid'] ) {
      $replyTo = $row['username'];
      $replyThread = $row['parentID'];
      $replySub = $row['subject'];
      $fromID  = $row['fromUserID'];
   }
   $lastAuthor = $row['fromUserID'];
//   print ("<tr><td class=\"conversationReply\"><a href=\"compose.php?id=$row[fromUserID]&re=$row[subject]#composer\">Reply to $row[username]</a></td></tr>");
}

   if ( '' != $replyTo ) {
      print ("<tr><td class=\"conversationReply\"><a href=\"compose.php?id=$fromID&m=$replyThread&la=$lastAuthor#composer\">Reply to $replyTo</a></td></tr>");
   } else {
//      print("<tr><td></td></tr>");
   }

   print (" </tbody> </table>\n

	    </td></tr></tbody></table>\n");


/* Get Contents, append, then write page using template */
$cnt = ob_get_contents();

//$cnt = html_entity_decode($cnt);
//$cnt = htmlentities($cnt);
ob_clean();

//$doc->substituteEntities = true;

/* Save Content */
$doc->DOMChangeTemplate("content",$cnt);
$doc->WriteHTML();

ob_end_flush();

?>
