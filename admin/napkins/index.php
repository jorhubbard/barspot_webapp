<?PHP

/*****************************************************************************
*
*  File:	  /admin/napkins/index.php
*  Author:	  Jarrod B. Reuter
*  Created:	  2009.08.06
*
*  Description:	  Basic email system.  Allows users to send and receive emails
*
*  Warnings:	  There is no encryption here, messages are stored as plain
*		  text, very limited system (no bcc, cc, etc).
*
*  Features:	  Send, sort, save, delete napkings
*
*****************************************************************************/

/* Config */
include('../../core.php');
include('napkins.php');

/* Connecting to Database and grabbing tables */
$db = Preferences::getInstance('database')->get('connection');
$napkins = Preferences::getInstance('napkin')->get('napkinTable');
$folders = Preferences::getInstance('napkin')->get('napkinFolders');
$toTable = Preferences::getInstance('napkin')->get('napkinToTable');

$usertable = Preferences::getInstance('login')->get('SqlUserTable');

/*
$result2 = $db->query("SELECT * FROM `{$usertable}` WHERE userID = {$_SESSION['userid']}");
*/

/*
mysql_connect($db->get('host'),$db->get('user'), $db->get('pass')); //connect to database (host, username, password)
mysql_select_db($db->get('database')); //selects database to use
*/

/* Setting up variables from URL */
$ref = "";
if (isset($_GET['ref']))
    $ref = $_GET['ref'];    // reffering page (see uses
						      // below)
/*****************************************************************************
*
*  uses	       -  confirmation when message sent.
*	       -  more...?
*****************************************************************************/


if ( !isset( $_GET['box'] ) ) {
   /* If no box is selected, default to inbox */
   $boxID = 1;
} else {
   /* Escapte string for safe use in queries */
   $boxID = $db->real_escape_string( strip_tags($_GET['box']) );
}

/* Setting up other variables */
$messageLimit = 80;

ob_start();

if( !$login->is_logged_in()) { HTTP::redirect_to_url("../login/login.php"); }


/* Run Queries */
if ( 4 == $boxID ) {
   print ("Do something cool here for user defined folders");

} else if ( 2 == $boxID ) {
   /*$result = $db->query("SELECT *
			   FROM `{$napkins}` AS N
			   JOIN `{$toTable}` AS T ON N.napkinID = T.napkinID
			   JOIN `{$folders}` AS F ON N.folderTypeID = F.folderTypeID
			   JOIN `{$usertable}` AS U on T.userID = U.userID 			   
			   WHERE N.fromUserID = {$_SESSION['userid']}");
   */
   $sql = "SELECT T.userID, username, fromUserID, sentTimestamp, replyStatus, MAX(N.napkinID) AS napkinID, parentID, subject, message, readStatus, COUNT(*) AS number
      FROM `{$napkins}` AS N
      JOIN `{$toTable}` AS T ON N.napkinID = T.napkinID
      JOIN `{$folders}` AS F ON T.folderTypeID = F.folderTypeID
      JOIN `{$usertable}` AS U on T.userID = U.userID
      WHERE (
	    ( N.fromUserID = {$_SESSION['userid']} )
	    OR
	    ( T.userID = {$_SESSION['userid']} AND T.replyStatus = 1 )
	    OR
	    ( T.userID = {$_SESSION['userid']} AND T.parentID != N.napkinID )
	 )
	 GROUP BY T.parentID
	 ORDER BY N.sentTimestamp DESC";
      
/*   $result = $db->query($sql);
   echo $db->error;*/
   //echo "<pre>$sql</pre>";
   $result = $db->query($sql);
   //echo $db->error;

   $numRows = 0;
   $pIDs = array();

   while ( $row = $result->fetch_array() ){
      if ( 0 == $numRows ) {
	 $pidSql = "SELECT *
	    FROM `{$napkins}` AS N
	    JOIN `{$toTable}` AS T ON N.napkinID = T.napkinID
	    JOIN `{$usertable}` AS U ON N.fromUserID = U.userID
	    WHERE N.fromUserID = {$_SESSION['userid']} AND parentID = '$row[parentID]'";
      } else {
	 $pidSql .= " OR N.fromUserID = {$_SESSION['userid']} AND parentID = '$row[parentID]'";
      }
      $numRows++;
   }

   $row2 = array();

   if ( $numRows > 0 ){
      $result->data_seek(0);
      //echo "<pre>$pidSql</pre>";
      $result2 = $db->query($pidSql);
      //echo $db->error;


      $row2 = array();
      while ( $row = $result2->fetch_array() ){
	 $row2[$row[parentID]]["readStatus"] = $row['readStatus'];
	 $row2[$row[parentID]]["message"] = $row['message'];
	 $row2[$row[parentID]]["from"] = $row['username'];
	 $row2[$row[parentID]]["sentTimestamp"] = $row['sentTimestamp'];
      }
   }

//   print_r($row2);

} else if ( 3 == $boxID ) {
   $sql = "SELECT T.userID, username, fromUserID, sentTimestamp, replyStatus, MAX(N.napkinID) AS napkinID, parentID, subject, message, deletedStatus, readStatus, COUNT(*) AS number
      FROM `{$napkins}` AS N
      JOIN `{$toTable}` AS T ON N.napkinID = T.napkinID
      JOIN `{$folders}` AS F ON T.folderTypeID = F.folderTypeID
      JOIN `{$usertable}` AS U ON N.fromUserID = U.userID
      WHERE (
	    ( T.userID = {$_SESSION['userid']} AND T.folderTypeID = '$boxID' )
	    OR (
	       ( 1 = ( SELECT deletedStatus FROM `{$toTable}` WHERE userID = {$_SESSION['userid']} AND parentID = T.parentID LIMIT 1 ) )
	       AND ( N.fromuserID = {$_SESSION['userid']} )
	       AND ( ( T.parentID != N.napkinID ) OR ( T.replyStatus = 1 ) )
	       )
	    )
      GROUP BY T.parentID
      ORDER BY N.napkinID DESC";
      /*
   $sql  = "SELECT T.userID, username, fromUserID, sentTimestamp, replyStatus, MAX(N.napkinID) AS napkinID, parentID, subject, message, COUNT(*) AS number
      FROM `{$napkins}` AS N
      JOIN `{$toTable}` AS T ON N.napkinID = T.napkinID
      JOIN `{$folders}` AS F ON T.folderTypeID = F.folderTypeID
      JOIN `{$usertable}` AS U ON N.fromUserID = U.userID
      WHERE (
	    (T.userID = {$_SESSION['userid']} AND F.folderTypeID = '$boxID' )
	    OR
	    (fromUserID = {$_SESSION['userid']} AND T.replyStatus = 1)
	    OR
	    (fromUserID = {$_SESSION['userid']} AND T.parentID != N.napkinID )
	    )
	    AND
	    ( T.deletedStatus = 1 )
	    
      GROUP BY N.subject
      ORDER BY N.sentTimestamp DESC";
      */
//      $result = $db->query($sql);
   //echo "<pre>$sql</pre>";
   $result = $db->query($sql);
   //echo $db->error;

   $numRows = 0;
   $pIDs = array();

   while ( $row = $result->fetch_array() ){
      if ( 0 == $numRows ) {
	 $pidSql = "SELECT *
	    FROM `{$napkins}` AS N
	    JOIN `{$toTable}` AS T ON N.napkinID = T.napkinID
	    JOIN `{$usertable}` AS U ON N.fromUserID = U.userID
	    WHERE T.userID = {$_SESSION['userid']} AND parentID = '$row[parentID]' AND T.folderTypeID = '$boxID'";
      } else {
	 $pidSql .= " OR T.userID = {$_SESSION['userid']} AND parentID = '$row[parentID]' AND T.folderTypeID = '$boxID'";
      }
      $numRows++;
   }

   $row2 = array();

   if ( $numRows > 0 ){
      $result->data_seek(0);
      //echo "<pre>$pidSql</pre>";
      $result2 = $db->query($pidSql);
      //echo $db->error;


      $row2 = array();
      while ( $row = $result2->fetch_array() ){
	 $row2[$row[parentID]]["readStatus"] = $row['readStatus'];
	 $row2[$row[parentID]]["message"] = $row['message'];
	 $row2[$row[parentID]]["from"] = $row['username'];
	 $row2[$row[parentID]]["sentTimestamp"] = $row['sentTimestamp'];
      }
   }
} else {
   /*
   $result = $db->query("SELECT * 
			FROM `{$napkins}` AS N
			JOIN `{$toTable}` AS T ON N.napkinID = T.napkinID
			JOIN `{$folders}` AS F ON N.folderTypeID = F.folderTypeID
			JOIN `{$usertable}` AS U ON N.fromUserID = U.userID
			WHERE T.userID = {$_SESSION['userid']} AND 
			      F.folderTypeID = $boxID");
   */
   /*
   $sql  = "SELECT T.userID, username, fromUserID, sentTimestamp, replyStatus, MAX(N.napkinID) AS napkinID, parentID, subject, message, deletedStatus, COUNT(*) AS number	
      FROM `{$napkins}` AS N
      JOIN `{$toTable}` AS T ON N.napkinID = T.napkinID
      JOIN `{$folders}` AS F ON T.folderTypeID = F.folderTypeID
      JOIN `{$usertable}` AS U ON N.fromUserID = U.userID
      WHERE ( 
	 (T.userID = {$_SESSION['userid']} AND T.folderTypeID = '$boxID' )
	 OR (T.parentID != N.napkinID AND N.fromuserID = {$_SESSION['userid']} AND 0 = (SELECT deletedStatus FROM `{$toTable}` WHERE userID = {$_SESSION['userid']} AND parentID = T.parentID LIMIT 1))
	 OR (T.replyStatus = 1 AND N.fromuserID = {$_SESSION['userid']} AND 0 = (SELECT deletedStatus FROM `{$toTable}` WHERE userID = {$_SESSION['userid']} AND parentID = T.parentID LIMIT 1))
	 )".*/
	 /*
	 (fromUserID = {$_SESSION['userid']} AND T.parentID != N.napkinID AND (SELECT deletedStatus FROM `{$toTable}` WHERE T.userID = {$_SESSION['userid']} AND napkinID = N.napkinID ) != 1 )
	 OR
	 ( fromUserID = {$_SESSION['userid']} AND T.replyStatus = 1 )".
	 */
	 /*
	    "(
	       fromUserID = {$_SESSION['userid']} AND
		  (
		     (T.parentID != N.napkinID)
		  )
		  AND
		  (
		     '0' = (SELECT deletedStatus FROM `{$toTable}` WHERE T.userID = {$_SESSION['userid']} AND parentID = T.parentID LIMIT 1)
		  ) 
	    ) 
	    OR
	    (
	       fromUserID = {$_SESSION['userid']} AND
		  (
		     (T.replyStatus = 1)
		  )
		  AND
		  (
		    '0' = (SELECT deletedStatus FROM `{$toTable}` WHERE T.userID = {$_SESSION['userid']} AND parentID = T.parentID LIMIT 1) != 1
																			                      )
	    )".
	 //AND N.replyStatus = 1) 
	 ")".
	 */
//	 ) AND N.napkinID = (SELECT MAX(napkinID) FROM `{$napkins}` WHERE subject = N.subject GROUP BY N.subject )
//      " GROUP BY N.subject ".
//      ORDER BY N.napkinID D";
/*      " GROUP BY T.parentID".
      " ORDER BY N.napkinID DESC";*/
      
      $sql = "SELECT T.userID, username, fromUserID, sentTimestamp, T.replyStatus, MAX(N.napkinID) AS napkinID, T.parentID, subject, message, T.deletedStatus, COUNT( * ) AS number
	 FROM `{$napkins}` AS N
	 JOIN `{$toTable}` AS T ON N.napkinID = T.napkinID
	 JOIN `{$folders}` AS F ON T.folderTypeID = F.folderTypeID
	 JOIN `{$usertable}` AS U ON N.fromUserID = U.userID
	 WHERE ( 
	    ( T.userID = {$_SESSION['userid']} AND T.folderTypeID = '$boxID' )
	    OR (
	       ( 0 = ( SELECT deletedStatus FROM `{$toTable}` WHERE userID = {$_SESSION['userid']} AND parentID = T.parentID LIMIT 1 ) )
	       AND ( N.fromuserID = {$_SESSION['userid']} ) 
	       AND ( ( T.parentID != N.napkinID ) OR ( T.replyStatus = 1 ) )
	       )
	 )
	 GROUP BY parentID
	 ORDER BY N.napkinID DESC
	 ";
	 

   //echo "<pre>$sql</pre>";
   $result = $db->query($sql);
   //echo $db->error;

   $numRows = 0;
   $pIDs = array();

   while ( $row = $result->fetch_array() ){
      if ( 0 == $numRows ) {
	 $pidSql = "SELECT * 
		     FROM `{$napkins}` AS N 
		     JOIN `{$toTable}` AS T ON N.napkinID = T.napkinID 
		     JOIN `{$usertable}` AS U ON N.fromUserID = U.userID
		     WHERE T.userID = {$_SESSION['userid']} AND folderTypeID = '$boxID' AND parentID = '$row[parentID]'";
      } else {
	 $pidSql .= " OR T.userID = {$_SESSION['userid']} AND folderTypeID = '$boxID' AND parentID = '$row[parentID]'";
      }
      $numRows++;
   }
   
   $row2 = array();

   if ( $numRows > 0 ){
      $result->data_seek(0);
      //echo "<pre>$pidSql</pre>";
      $result2 = $db->query($pidSql);
      //echo $db->error;

      //$row2 = array();
      while ( $row = $result2->fetch_array() ){
	 $row2[$row[parentID]]["readStatus"] = $row['readStatus'];
	 $row2[$row[parentID]]["message"] = $row['message'];
	 $row2[$row[parentID]]["from"] = $row['username'];
	 $row2[$row[parentID]]["sentTimestamp"] = $row['sentTimestamp'];
      }
   }

   //print_r($row2);
}


//   print ("logged in, coming from $ref box = $boxID<br/>");
   print ("<link href=\"css/email.css\" rel=\"stylesheet\" type=\"text/css\"/>");

   ?>
	 <style type="text/css">
/*	    #successMessage { width: 100%; }*/
	    .rc {display:block;}
	    .rc *{display:block; height:1px; overflow:hidden; font-size:.01em; background:#b20000;}
	    .rc1 {margin-left:3px; margin-right:3px; padding-left:1px; padding-right:1px; border-left:1px solid #870000; border-right:1px solid #870000; background:#9f0000;}
	    .rc2 {margin-left:1px;
	      margin-right:1px;
	        padding-right:1px;
		  padding-left:1px;
		    border-left:1px solid #6f0000;
		      border-right:1px solid #6f0000;
		        background:#a30000;}
	    .rc3 {margin-left:1px;
	      margin-right:1px;
	        border-left:1px solid #a30000;
		  border-right:1px solid #a30000;}
	    .rc4 {border-left:1px solid #870000;
	      border-right:1px solid #870000;}
	    .rc5 {border-left:1px solid #9f0000;
	      border-right:1px solid #9f0000;}
	    .rcfg {background:#b20000;}
	 </style>
   <?PHP
   /*
   print ("
   <div id=\"successMessage\">
   <b class=\"rc\"> <b class=\"rc1\"><b></b></b> <b class=\"rc2\"><b></b></b> <b class=\"rc3\"></b> <b class=\"rc4\"></b> <b class=\"rc5\"></b></b>

   <div class=\"rcfg\">&nbsp;</div>
   
   <b class=\"rc\">
     <b class=\"rc5\"></b>
       <b class=\"rc4\"></b>
         <b class=\"rc3\"></b>
	   <b class=\"rc2\"><b></b></b>
	     <b class=\"rc1\"><b></b></b></b>

   </div>");
*/
   print ("<div id=\"successMessage\"> </div>");
   if ( $ref == "napkinSent" ) {
      print ("<script type=\"text/javascript\">");
      print (" function clearMessage() {
//		  document.getElementById('successMessage').style.display = 'none';
		  document.getElementById('successMessage').innerHTML = '&nbsp;';
		  document.getElementById('successMessage').style.backgroundColor = 'transparent';
	       }");
      print ("document.getElementById('successMessage').style.backgroundColor = '#FFE4E9';");
      print ("document.getElementById('successMessage').innerHTML = 'Message Sent';");
      print ("setTimeout(\"clearMessage();\", 10000 );");
      print ("</script>");
   }

   print ("<form name=\"napkinList\" action=\"manageNapkins.php\" method=\"post\" onsubmit=\"return submitForm();\">");
   
//   printMailboxButtons( $boxID );


   print ("<table id=\"emailSystem\" summary=\"Napkins\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\n");
   print ("<tbody>\n
         <tr><td id=\"folderList\">\n");

   printFolders($boxID, false);

   print ("</td><td id=\"secondList\">\n");

   printMailboxButtons( $boxID );

   print ("<table id=\"emailList\" summary=\"Inbox\">\n");
   print (" <thead>\n
         <tr>\n");
   if ( 2 == $boxID ) {
      print ("
	    <th scope=\"col\" class=\"napkinCheckBoxes\"></th>\n
	    <th scope=\"col\" class=\"rounded-company\">Sent To/Date</th>\n
	    <th scope=\"col\" class=\"rounded-q1\">Subject/Summary</th>\n
	    </tr>\n
	    </thead>\n");
   } else {
      print ("
	    <th scope=\"col\" class=\"napkinCheckBoxes\"></th>\n
	    <th scope=\"col\" class=\"rounded-company\">Sender/Date</th>\n
	    <th scope=\"col\" class=\"rounded-q1\">Subject/Summary</th>\n
	    </tr>\n
	    </thead>\n");
   }
   print ("<tfoot>
         <tr>
	          <td colspan=\"3\" class=\"emailFooter\"><em>(c)Hot Bar Spot 2009</em></td>
			         </tr>
				    </tfoot>");
   print ("<tbody>\n");

   $index = 2;
   while ( $row = $result->fetch_array() ){
      if ( $index%2 == 0 ){
	 $index = 1;
      } else {
	 $index = 2;
      }
      $number = $row['number'];
      /*
      if ( ( $number < 2 ) && ( $row['fromUserID'] == $_SESSION['userid'] ) ) {
	 // don't print - this can't be handled by the query - Case of deleting a conversation
      } else {
      */
      if ( 1 == $row2[$row['parentID']]['readStatus'] || 2 == $boxID ) {
	 print("<tr class=\"emailAbstractRead\">\n");
      } else {
	 print("<tr class=\"emailAbstractUnread\">\n");
      }
	 print ("<td><input type=\"checkbox\" name=\"napkinIDs[]\" value=\"$row[parentID]\" /></td>");

	 /* Checking if you are sender to make links and names work better.  If you are sender doesn't mean you are in sentbox, could be archived. */
	 if ( $row['userID'] != $_SESSION['userid'] ) { // if you are sender of the message
	    if ( 2 == $boxID ) {
	       print ("<td><div class=\"emailTo\"><a href=\"/user.php?id=$row[userID]\">$row[username]</a></div>\n");
	    } else {
	       print ("<td><div class=\"emailTo\"><a href=\"/user.php?id=$row[userID]\">".$row2[$row[parentID]]['from']."</a></div>\n");
	    }
	 } else {	      // if message received
	    print ("<td><div class=\"emailFrom\"><a href=\"/user.php?id=$row[fromUserID]\">$row[username]</a></div>\n");
	 }
	 print ("<div class=\"emailTime\">".$row2[$row[parentID]]['sentTimestamp']."</div></td><td>\n");
//	 print ("<div class=\"emailTime\">$row[sentTimestamp]</div></td><td>\n");
//	 $number = $row['number'];
	 /* Not needed?
	 if ( ($row['replyStatus'] > 0) && ($row['userID'] == $_SESSION['userid']) ) {
	    $number = $row['number'] + 1;
	 } else {
	    $number = $row['number'];
	 }
	 */
	 if ( $number > 1 ) {
	    print("<div class=\"emailSubject\"><a href=\"readmessage.php?t=$row[parentID]&f=$boxID\">$row[subject] ($number)</a></div>");
	 } else {
	    print("<div class=\"emailSubject\"><a href=\"readmessage.php?t=$row[parentID]&f=$boxID\">$row[subject]</a></div>");
	 }
//	 $message = stripslashes($row['message']);
	 /* Converting data from the editor to something the DOM will like */
	 $message = stripslashes($row2[$row['parentID']]['message']);
	 //$message = stripslashes($row['message']);                               // Remove escapes i.e. \' = '
	 /*
	 $message = str_replace("&lt;", "&#60;", $message );
         $message = str_replace("&gt;", "&#62;", $message );
	 */
	 /**************************************************************************
	  *
	  *  Apparently, htmlentities breaks UTF-8 encoding, also some items might
	  *  already be converted.  This is why we put the last param to false
	  *  (double encoding)
	  *
	  *  It seems strange to encode something we know the DOM will encode, but
	  *  we have to make sure everything is encoded once (and only once) before
	  *  we decode with html_entity_decode.  The editor encodes some items, but
	  *  not others, then if we decode it breaks UTF-8 and the DOM complains
	  *
	  ***************************************************************************/
	 $message = htmlspecialchars( $message, ENT_COMPAT, 'UTF-8', false);     // Convert to special chars
	 /**************************************************************************
	  *
	  *  We are stripping out any encoding done by the editor or by us.  The
	  *  DOM encodes for us, and if we encode twice any special characters break
	  *
	  *  We have to state ENT_COMPAT and UTF-8 even though they are defaults
	  *  because in some cases it breaks UTF if it's not stated.
	  ***************************************************************************/
	 $message = html_entity_decode( $message, ENT_COMPAT, 'UTF-8');
/*
	 $message = str_replace("&lt;", "&#60;", $message );
	 $message = str_replace("&gt;", "&#62;", $message );
	 */

//	 $message = "<".$message;
	 $message = strip_tags($message);
//	 $message = printSummary($message, 10);

//	 print("<div class=\"emailSummary\">".strip_tags($message)."</div></td>\n");

	 if ( strlen($message) > $messageLimit ) {
	    print("<div class=\"emailSummary\">".substr($message, 0, strrpos(substr($message, 0, $messageLimit), ' ')) . "...</div></td>\n");
	 } else {
	    print("<div class=\"emailSummary\">$message</div></td>\n");
	 }
	 
      print("</tr>\n");
//      }
   }
   print ("</tbody> </table>\n
   
   </td></tr></tbody></table></form>\n");

$cnt = ob_get_contents();
ob_clean();
/* Save Content */
$doc->DOMChangeTemplate("content",$cnt);
$doc->WriteHTML();

ob_end_flush();

?>
