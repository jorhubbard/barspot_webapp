<?PHP

/*****************************************************************************
*
*  File:          /admin/napkins/transactions.php
*  Author:        Jarrod B. Reuter
*  Created:       2009.08.10
*
*  Description:   Functions to support Transactions in MySQL using PHP
*
*  Requirements:  Requires MySQL 4 or newer, table must be of type InnoDB
*
*  Features:      Allows database to rollback to a previous state if insert
*		  does not fully work.  Brilliant for multiple tables
*
*  Usage:	  @mysql_connect(...) or die (mysql_error() ...);
*		  @mysql_select_db("DBNAME") or die (mysql_error() ...);
*		  $sql = " INSERT INTO table (id, item, etc) 
*			   VALUES ( null, one, etc )";
*		  beginTrans();
*		  $result = @mysql_query($sql);
*		  if ( !$result ) {
*		     rollbackTrans();
*		     echo "Rollback completed";
*		     exit;
*		  } else {
*		     commitTrans();
*		     echo "Insertion successful";
*		  }
*
*****************************************************************************/

function beginTrans() {
   @mysql_query("BEGIN");
}

function commitTrans() {
   @mysql_query("COMMIT");
}

function rollbackTrans() {
   @mysql_query("ROLLBACK");
}

?>
