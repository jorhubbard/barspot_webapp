<?php

#####################################################
# originally gleaned from
# http://www.fedeblog.com.ar/files/view.php?i=Watermark/watermark.php
# Modified by Liem Bahneman (liem@bahneman.com) with some 
# path enhancements when called to handled subdirectories


#####################################################
# Okay to edit these

# what is the root of your files?
include '../../connect.php';

$basedir=$fullpath."/images/uploaded";
$watermarkimage="watermark/watermark.png";

#####################################################
# end user modifiable stuff...

$file=basename($_GET['i']);
$path=($_GET['p']);

$image = $basedir."/".$path."/".$file;
$watermark = $fullpath."/admin/gallery/".$watermarkimage;

$im = imagecreatefrompng($watermark);

$ext = substr($image, -3);

if (strtolower($ext) == "gif") {
   if (!$im2 = imagecreatefromgif($image)) {
      echo "Error opening $image!"; exit;
   }
} else if(strtolower($ext) == "jpg") {
   if (!$im2 = imagecreatefromjpeg($image)) {
      echo "Error opening $image!"; exit;
   }
} else if(strtolower($ext) == "png") {
   if (!$im2 = imagecreatefrompng($image)) {
      echo "Error opening $image!"; exit;
   }
} else {
   die;
}

//imagecopy($im2, $im, (imagesx($im2)/2)-(imagesx($im)/2), (imagesy($im2)/2)-(imagesy($im)/2), 0, 0, imagesx($im), imagesy($im));
imagecopy($im2, $im, 10, 10, 0, 0, imagesx($im), imagesy($im));
/*
if($_GET[repeat]) {
   $waterless = imagesx($im2) - imagesx($im);
   $rest = ceil($waterless/imagesx($im)/2);

   for($n=1; $n<=$rest; $n++) {
      imagecopy($im2, $im, ((imagesx($im2)/2)-(imagesx($im)/2))-(imagesx($im)*$n), (imagesy($im2)/2)-(imagesy($im)/2), 0, 0, imagesx($im), imagesy($im));
      imagecopy($im2, $im, ((imagesx($im2)/2)-(imagesx($im)/2))+(imagesx($im)*$n), (imagesy($im2)/2)-(imagesy($im)/2), 0, 0, imagesx($im), imagesy($im));
   }
}
*/
$last_modified = gmdate('D, d M Y H:i:s T', filemtime ($image));

header("Last-Modified: $last_modified");
header("Content-Type: image/jpeg");
imagejpeg($im2,NULL,95);
imagedestroy($im);
imagedestroy($im2);

?> 
