<?php
include '../connect.php';
include_once '../../core.php';
ob_start();

$numCols = 4;
$colWidth = 100/$numCols;

$sql = "
SELECT galleryCategoryName, 
g.galleryCategoryID as galleryID, 
galleryUserID,
id,
ext
FROM galleryCategories as g
LEFT JOIN images ON g.galleryCategoryID = images.galleryCategoryID 
WHERE galleryUserID = '".quote_smart($_GET[userID])."' 
GROUP BY g.galleryCategoryID 
ORDER BY galleryCategoryName ASC
";

$result = mysql_query($sql);

if($row = mysql_fetch_array($result));
{
   $trCount = 1;
   print("<table>");
   do
   {
      if($trCount == 1)
      {
	 print("<tr>");
      }

      print("<td width='$colWidth%'><div align='center'>$row[galleryCategoryName]<br />
      <a href='/admin/gallery/imageupload/photos.php?galleryID=$row[galleryID]&userID=$_GET[userID]'>
      <img src='/images/uploaded/thumbs/$row[galleryUserID]/$row[id]$row[ext]' /></a></div></td>");
      
      if($trCount == $numCols)
      {
	 print("</tr>");
	 $trCount = 1;
      }
      $trCount++;

   }while($row = mysql_fetch_array($result));
   
   if($trCount != $numCols)
   {
      print("</tr>");
   }
   
   print("</table>");
}

$cnt = ob_get_contents();
ob_clean();
/* Save Content */
$doc->DOMChangeTemplate("content",$cnt);
$doc->WriteHTML();
