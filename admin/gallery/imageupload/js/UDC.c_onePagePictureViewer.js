/*  -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

                  UDC.c_onePagePictureViewer.js
                          2002-02-17
    
                             -~|~-

    Copyright (c) Samuel Trygger. All Rights Reserved. You may use
    this code if and only if this entire copyright notice appears
    unchanged.

    Contact samuel@trygger.nu for all other uses.

                             -~|~-

    Description : Javascript object that creates a one page
                  picture viewer/slide show, where the left
                  and right sides of the each picture are
                  used for navigation. Check the example code
                  to see how it works
    Requires    : Nothing in particular. It is self contained.
    Tested with : Microsoft Internet Explorer 6.0.2600.0000     Win2000
                  Netscape Communicator 4.79                    Win2000
                  Netscape 6.2                                  Win2000
    Author      : Samuel Trygger
    Contact     : samuel@trygger.nu
    Home Page   : http://samuel.trygger.nu/devzone/javascript/

                             -~|~-

    -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-  */

/*  -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    Internal variables. Should NOT be touched.
    -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-  */
    var _CONST_OnePagePictureViewer_HLPR_CURRSTRING   = '?current=';
    var _CONST_OnePagePictureViewer_HLPR_VERSION      = 1.1;       // Version number.

/*  -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    Main object.
    -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-  */
    function c_onePagePictureViewer(x) {  //added variable to initialize current image to something other then 0 in array
        this._CurrentImage        = x;
        this._PageName            = '';
        this.imageDirectory       = '';
        this.length               = 0;
        this.name                 = '';
        this.showAltText          = 1;
        this.showImageCount       = 1;
        this.VERSION              = _CONST_OnePagePictureViewer_HLPR_VERSION;

        return this;
    }

/*  -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    Helper object.
    -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-  */
    function _OBJ_OnePagePictureViewer_HLPR_Picture(imgdir, src, w, h, alt) {
        this.picture = imgdir + src;
        this.width   = w;
        this.height  = h;
        this.alt     = (alt + '' != 'undefined') ? alt : src;

        return this;
    }

/*  -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    Helper function for adding pictures to the picture viewer.
    -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-  */
    function _OnePagePictureViewer_HLPR_Add(src, w, h, alt) {
        this[this.length] = new _OBJ_OnePagePictureViewer_HLPR_Picture(this.imageDirectory, src, w, h, alt);
        this.length++;
    }
    c_onePagePictureViewer.prototype.add = _OnePagePictureViewer_HLPR_Add;

/*  -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    Helper function for printing the picture viewer.
    -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-  */
    function _OnePagePictureViewer_HLPR_Write() {
        if (this.name == '') {
            // Error... "this.name" must be set to be able to write the A HREF tags.
            alert('You must set the value of "object.name" to the name of your instance of c_onePagePictureViewer() before calling "object.write()".');
            return;
        }
        if (this.length == 0) return;
        
        this._SetCurrent();
        this._SetPageName();
        /*this is a hackjob until I get the mod_rewrite stuff working */
	document.write('<img src="/admin/gallery/watermark/wm.php?p=/sized&i=' + this[this._CurrentImage].picture + '" alt="' + ((this.showAltText) ? this[this._CurrentImage].alt + ((this.showImageCount) ? ' [' + (parseInt(this._CurrentImage) + 1) + '/' + this.length + ']' : '') : '') + '" width="' + this[this._CurrentImage].width + '" height="' + this[this._CurrentImage].height + '" border="0" usemap="#MAP_OnePagePictureViewer_' + this.name + '" hidefocus="true" />');
        document.write('<map name="MAP_OnePagePictureViewer_' + this.name + '">');
        document.write('<area shape="rect" href="javascript:' + this.name + '.previous();" alt="' + ((this.showAltText) ? this[this._CurrentImage].alt  + ((this.showImageCount) ? ' [' + (parseInt(this._CurrentImage) + 1) + '/' + this.length + ']' : '') : '') + '" coords="0, 0, ' + (this[this._CurrentImage].width / 2) + ', ' + this[this._CurrentImage].height + '" hidefocus="true" />');
        document.write('<area shape="rect" href="javascript:' + this.name + '.next();" alt="' + ((this.showAltText) ? this[this._CurrentImage].alt  + ((this.showImageCount) ? ' [' + (parseInt(this._CurrentImage) + 1) + '/' + this.length + ']' : '') : '') + '" coords="' + (this[this._CurrentImage].width / 2) + ', 0, ' + this[this._CurrentImage].width + ', ' + this[this._CurrentImage].height + '" hidefocus="true" />');
        document.write('</map>');
    }
    c_onePagePictureViewer.prototype.write = _OnePagePictureViewer_HLPR_Write;

/*  -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    Helper function for extracting image index from the URL.
    -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-  */
    function _OnePagePictureViewer_HLPR_SetCurrent() {
        if (document.location.href.toString().lastIndexOf(_CONST_OnePagePictureViewer_HLPR_CURRSTRING) > -1)
            this._CurrentImage = document.location.href.toString().substr(document.location.href.toString().lastIndexOf(_CONST_OnePagePictureViewer_HLPR_CURRSTRING) + 9);
        if (this._CurrentImage >= this.length) this._CurrentImage = 0;
    }
    c_onePagePictureViewer.prototype._SetCurrent = _OnePagePictureViewer_HLPR_SetCurrent;

/*  -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    Helper function for extracting page name from the URL.
    -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-  */
    function _OnePagePictureViewer_HLPR_SetPageName() {
        if (document.location.href.toString().lastIndexOf(_CONST_OnePagePictureViewer_HLPR_CURRSTRING) > -1) {
            this._PageName = document.location.href.toString().substr(document.location.href.toString().lastIndexOf('/') + 1, document.location.href.toString().lastIndexOf(_CONST_OnePagePictureViewer_HLPR_CURRSTRING) - (document.location.href.toString().lastIndexOf('/') + 1));
        } else {
            this._PageName = document.location.href.toString().substr(document.location.href.toString().lastIndexOf('/') + 1);
        }
    }
    c_onePagePictureViewer.prototype._SetPageName = _OnePagePictureViewer_HLPR_SetPageName;

    function _OnePagePictureViewer_HLPR_Previous() {
        if (parseInt(this._CurrentImage) - 1 < 0) {
            this._CurrentImage = this.length - 1;
        } else {
            this._CurrentImage--;
        }
        document.location.replace(this._PageName + _CONST_OnePagePictureViewer_HLPR_CURRSTRING + this._CurrentImage);
    }
    c_onePagePictureViewer.prototype.previous = _OnePagePictureViewer_HLPR_Previous;
    
    function _OnePagePictureViewer_HLPR_Next() {
        if (parseInt(this._CurrentImage) + 1 >= this.length) {
            this._CurrentImage = 0;
        } else {
            this._CurrentImage++;
        }
        document.location.replace(this._PageName + _CONST_OnePagePictureViewer_HLPR_CURRSTRING + this._CurrentImage);
    }
    c_onePagePictureViewer.prototype.next = _OnePagePictureViewer_HLPR_Next;
