<?php
include '../connect.php';
include_once '../../core.php';
ob_start();

print("<a href='/admin/'>Back to Admin page</a><br /><br />");

function drawForm($barID,$categoryID,$category,$img,$desc)
{
   $string .="
      <form action='".$_SERVER['PHP_SELF']."' enctype=\"multipart/form-data\" method='POST'>
      <input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"102400000\"></input>
      ";
   if( ($_POST[submit] == "Delete") || ($_POST[confirm] == 'Yes') )
   {
      //do nothing
   }
   else
   {
      $string .="<input type='hidden' name='categoryID' value='$categoryID' />";
   }
   $string .="
      <input type='hidden' name='barID' value='$barID' />
      <table>
      <tr>
	 <td>Category Name:</td>
	 <td><input type='text' name='category' value='$category' size='25' maxlength='50' /></td>
      </tr>
      <tr>
	 <td>Category Image:</td>
	 <td><input type='file' size='25' name='image[]' />";
	 if(!empty($img))
	 {
	    $string .="<br /><input type='hidden' name='currentImage' value='$img' />
	       <img src='/images/uploaded/thumbs/$img' />";
	 }
	 $string .="
	 </td>
      </tr>
      <tr>
	 <td>Category Description:</td>
	 <td><textarea name='desc' rows='4' cols='30'>$desc</textarea></td>
      </tr>
      <tr>
	 <td> </td>
	 <td><input type='submit' name='submit' value='Save' /></td>
      </tr>
      </table>	 
      </form>";

   return $string;
}
function insertCategory($barID,$category,$image,$desc)
{

   $sql = "INSERT INTO menuCategory VALUES ('','$barID','$category','$desc','$image');";
   $result = mysql_query($sql)or die("Invalid query: " . mysql_error());

   if($result)
   {
      return true;
   }
   else
   {
      return false;
   }

}
function drawCategories($barID)
{
   $sql = "SELECT * FROM menuCategory WHERE barID = '$barID' ORDER BY menuCategoryName ASC";
   $result = mysql_query($sql)or die("invalid query: " . mysql_error());
   
   if($row = mysql_fetch_array($result))
   {
      $string .="
	 <table width='650px' border='1'>
	 <tr>
	    <th>Edit</th>
	    <th>Delete</th>
	    <th>Items</th>
	    <th>Image</th>
	    <th>Name</th>
	    <th>Description</th>
	 </tr>";
      do
      {
	 $string .="
	    <tr>
	       <td>
	       <form action='".$_SERVER['PHP_SELF']."' method='POST'>
	       <input type='hidden' name='barID' value='$_POST[barID]' />
	       <input type='hidden' name='categoryID' value='$row[menuCategoryID]' />
	       <input type='submit' name='submit' value='Edit' /></form>
	       </td>
	       <td>
	       <form action='".$_SERVER['PHP_SELF']."' method='POST'>
	       <input type='hidden' name='barID' value='$_POST[barID]' />
	       <input type='hidden' name='categoryID' value='$row[menuCategoryID]' />
	       <input type='hidden' name='image' value='$row[menuCategoryImg]' />
	       <input type='submit' name='submit' value='Delete' /></form>
	       </td>
	       <td>
	       <form action='menu.php' method='POST'>
	       <input type='hidden' name='barID' value='$_POST[barID]' />
	       <input type='hidden' name='categoryID' value='$row[menuCategoryID]' />
	       <input type='submit' name='submit' value='Add' /></form>
	       </td>
	       <td>";
	 if(!empty($row[menuCategoryImg]) )
	 {
	    $string .= "<img src='/images/uploaded/thumbs/$row[menuCategoryImg]' />";
	 }
	 else
	 {
	    $string .="None";
	 }
	 $string .="
	       </td>
	       <td>$row[menuCategoryName]</td>
	       <td>$row[menuCategoryDesc]</td>
	    </tr>
	    ";
      }while($row = mysql_fetch_array($result));
      $string .="
	 </table>";
   }
   return $string;
}
function updateCategory($categoryID,$name,$img,$desc)
{
   $sql = "UPDATE menuCategory SET 
      menuCategoryName = '$name', 
      menuCategoryImg = '$img', 
      menuCategoryDesc = '$desc' 
      WHERE menuCategoryID = '$categoryID' 
      LIMIT 1;
   ";

   $result = mysql_query($sql)or die("invalid mysql: " . mysql_error());

   if($result)
   {
      return true;
   }
   else
   {
      return false;
   }

}
function confirmDelete($barID,$categoryID,$image)
{
   $sql = "SELECT * FROM menuCategory WHERE menuCategoryID = '$categoryID' LIMIT 1;";
   $result = mysql_query($sql);
   $row = mysql_fetch_array($result);

   $string .="Are you sure you want to delete this special? 
      <form action='".$_SERVER['PHP_SELF']."' method='POST'>
      <input type='hidden' name='categoryID' value='$categoryID' />
      <input type='hidden' name='barID' value='$barID' />
      <input type='hidden' name='image' value='$image' />
      <input type='submit' name='confirm' value='Yes' />
      |
      <input type='submit' name='confirm' value='No' />
      </form>
      ";
   return $string;

}
function deleteCategory($barID,$categoryID,$image,$fullpath)
{
   $sql = "DELETE FROM menuCategory WHERE menuCategoryID = '$categoryID' LIMIT 1;";
   $result = mysql_query($sql);
   if($result)
   {
      $img  = $fullpath."/images/uploaded/images/$image";
      $img2 = $fullpath."/images/uploaded/sized/$image";
      $img3 = $fullpath."/images/uploaded/thumbs/$image";

      if (file_exists($img) && file_exists($img2) && file_exists($img3))
      {
	 @unlink($img);
	 @unlink($img2);
	 @unlink($img3);
      }
      return true;
   }
   else
   {
      return false;
   }
}

if($_SESSION[userid])
{
   if($_POST[barID])
   {
      
      //checks to see if we need to insert/update/delete records/*{{{*/
      //if( (in_array(1,$_SESSION['sessgrouplist']) ) //|| (($_SESSION[sessuserid] == $_POST[barID])
      if ( /*in_array(1,$_SESSION['groupList']) ||*/ $_SESSION['usertype'] == 2 )
      {
	 //print("okay, should be a go from here");
	 if(empty($_POST[categoryID]) && !empty($_POST[category]) ) //adds the record
	 {
	    //if we want to make more then category name required, I would put the error checking here for insert
	    $categoryID = "13";
	    $fileInputBoxName = 'image';
	    include '../../images/uploaded/uploadfile.php'; //allows us to upload files with error checking
	    
	    if(insertCategory($_POST[barID],$_POST[category],$imagename,$_POST[desc]) )//inserts the special  
	    {
	       print("Your item has been added.");
   
	    }
	    else
	    {
	       print("There was an error adding your item.  Please try again.  
	       If you have continued problems please contact our webmaster at 
	       <a href='mailto:webmaster@hotbarspot.com'>webmaster@hotbarspot.com</a>.");
	    }
	    $flag = 'new';
	 }/*}}}*/
	 elseif(!empty($_POST[categoryID]) && $_POST[submit] == 'Save') //edits the record/*{{{*/
	 {
	    if(!empty($_FILES[image]['name'][0])) //means that we have a file we need to upload.
	    {

	       $categoryID = '13';
	       $fileInputBoxName = 'image';
	       include '../../images/uploaded/uploadfile.php'; //allows us to upload files with error checking
	       
	       //now that we have an uploaded file...lets delete the old
	       
	       $img  = $fullpath."/images/uploaded/images/$_POST[currentImage]";
	       $img2 = $fullpath."/images/uploaded/sized/$_POST[currentImage]";
	       $img3 = $fullpath."/images/uploaded/thumbs/$_POST[currentImage]";

	       if (file_exists($img) && file_exists($img2) && file_exists($img3))
	       {
		  @unlink($img);
		  @unlink($img2);
		  @unlink($img3);
	       }
	    }
	    else
	    {
	       $imagename = $_POST[currentImage];
	    }

	    if(updateCategory($_POST[categoryID],$_POST[category],$imagename,$_POST[desc]) )
	    {
	       print("Your item has been updated.");
   
	    }
	    else
	    {
	       print("There was an error updating your item.  Please try again.  
	       If you have continued problems please contact our webmaster at 
	       <a href='mailto:webmaster@hotbarspot.com'>webmaster@hotbarspot.com</a>.");
	    }
	    $flag = 'new';
	 }/*}}}*/
	 elseif($_POST[submit] == 'Delete') //confirms deletion of the record/*{{{*/
	 {
	    print confirmDelete($_POST[barID],$_POST[categoryID],$_POST[image]);
	 }/*}}}*/
	 elseif($_POST[confirm] == 'Yes' && !empty($_POST[categoryID]) )
	 {
	    if(deleteCategory($_POST[barID],$_POST[categoryID],$_POST[image],$fullpath) )//deletes the special  
	    {
	       print("Your item has been deleted.");
   
	    }
	    else
	    {
	       print("There was an error deleting your item.  Please try again.  
	       If you have continued problems please contact our webmaster at 
	       <a href='mailto:webmaster@hotbarspot.com'>webmaster@hotbarspot.com</a>.");
	    }
	 }

      
	 if(!empty($_POST[categoryID]))
	 {
	    $sql = "SELECT * FROM menuCategory WHERE menuCategoryID = '$_POST[categoryID]' LIMIT 1;";
	    $result = mysql_query($sql);
	    $row = mysql_fetch_array($result);
	 }
	 if($flag == 'new')
	 {
	    print drawForm($_POST[barID],'','','','');
	 }
	 else
	 {
	    print drawForm($_POST[barID],$_POST[categoryID],$row[menuCategoryName], $row[menuCategoryImg], $row[menuCategoryDesc]);
	 }
	 print("<br /><br />");
	 print drawCategories($_POST[barID]);



      }//ends check for proper permissions
      else
      {
	 print("You do not have sufficient privileges to edit this page for the user that was selected.");
      }
      
   }//ends check for post
   else
   {
      print("You can not link directly to this page.  
      You will have to go through the proper links to 
      navigate to this page with the correct information.");      

   }
}//ends check for sessuser (logged in)
else
{
   print("You must be logged in to view this page.");
}

$cnt = ob_get_contents();
ob_clean();

/* Save Content */
$doc->DOMChangeTemplate("content",$cnt);
$doc->WriteHTML();
