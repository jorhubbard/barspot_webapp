<?php
include '../connect.php';
include_once '../../core.php';
ob_start();

function drawForm($barID,$itemID,$categoryID,$item,$price,$desc)
{
   $price = explode('.',$price);
   $dollars = $price[0];
   $cents   = $price[1];
    $string = "";
   $string .="
      <form action='".$_SERVER['PHP_SELF']."' method='POST'>
      ";
   if( ($_POST[submit] == "Delete") || ($_POST[confirm] == 'Yes') )
   {
      //do nothing
   }
   else
   {
      $string .="<input type='hidden' name='itemID' value='$itemID' />";
   }
   $string .="
      <input type='hidden' name='barID' value='$barID' />
      <table>
      <tr>
	 <td>Item Category:</td>
	 <td><select name='categoryID'>
	 ";
   $sql = "SELECT menuCategoryID, menuCategoryName FROM menuCategory 
      WHERE barID = '$barID' ORDER BY menuCategoryName ASC";
   $result = mysql_query($sql);
   while($row = mysql_fetch_array($result))
   {
      $string .="<option value='$row[menuCategoryID]' ";
      if($row[menuCategoryID] == $categoryID)
      {
	 $string .="selected='yes' ";
      }
      $string .=">$row[menuCategoryName]</option>";
   }

   $string .="
	 </select></td>
      </tr>
      <tr>
      <td>Item Name:</td>
      <td><input type='text' name='item' value=\"$item\" size='25' maxlength='50' /></td>
      </tr>
      <tr>
      <td>Item Description:</td>
      <td><textarea name='desc' rows='4' cols='30'>$desc</textarea></td>
      </tr>
      <tr>
      <td>Item Price:</td>
      <td>$<input type='text' name='dollars' value='$dollars' maxlength='3' size='3'/>.
      <input type='text' name='cents' value='$cents' maxlength='2' size='2' /></td>
      </tr>
      <tr>
      <td colspan='2' align='right'><input type='submit' name='submit' value='Save' /></td>
      </tr>
      </table>   
      </form>";

   return $string;

}
function insertItem($categoryID,$item,$price,$desc)
{

   $sql = "INSERT INTO menuItem VALUES ('','$item','$desc','$price','$categoryID');";
   $result = mysql_query($sql)or die("Invalid query: " . mysql_error());

   if($result)
   {
      return true;
   }
   else
   {
      return false;
   }

}
function drawCategories($barID)
{
   $sql = "SELECT
   menuItemID, 
   menuItemPrice,
   menuItemName,
   menuItemDesc,
   menuItem.menuCategoryID AS categoryID,
   menuCategoryName
   FROM menuItem LEFT JOIN menuCategory ON menuItem.menuCategoryID = menuCategory.menuCategoryID 
   WHERE barID = '$barID'
   ORDER BY menuCategoryName ASC, menuItemName ASC";
   $result = mysql_query($sql)or die("invalid query: " . mysql_error());
   
   if($row = mysql_fetch_array($result))
   {
      $string .="
	 <table border='1'>
	 <tr>
	    <th>Edit</th>
	    <th>Delete</th>
	    <th>Category Name</th>
	    <th>Name</th>
	    <th>Description</th>
	    <th>Price</th>
	 </tr>";
      do
      {
	 $string .="
	    <tr>
	       <td>
	       <form action='".$_SERVER['PHP_SELF']."' method='POST'>
	       <input type='hidden' name='barID' value='$_POST[barID]' />
	       <input type='hidden' name='categoryID' value='$row[categoryID]' />
	       <input type='hidden' name='itemID' value='$row[menuItemID]' />
	       <input type='submit' name='submit' value='Edit' /></form>
	       </td>
	       <td>
	       <form action='".$_SERVER['PHP_SELF']."' method='POST'>
	       <input type='hidden' name='barID' value='$_POST[barID]' />
	       <input type='hidden' name='itemID' value='$row[menuItemID]' />
	       <input type='submit' name='submit' value='Delete' /></form>
	       </td>
	       <td>$row[menuCategoryName]</td>
	       <td>$row[menuItemName]</td>
	       <td>$row[menuItemDesc]</td>
	       <td>$row[menuItemPrice]</td>
	    </tr>
	    ";
      }while($row = mysql_fetch_array($result));
      $string .="
	 </table>";
   }
   return $string;
}
function updateItem($itemID,$name,$price,$desc,$categoryID)
{
   $sql = "UPDATE menuItem SET 
      menuCategoryID = '$categoryID',
      menuItemName = '$name', 
      menuItemDesc = '$desc',
      menuItemPrice = '$price' 
      WHERE menuItemID = '$itemID' 
      LIMIT 1;
   ";

   $result = mysql_query($sql)or die("invalid mysql: " . mysql_error());

   if($result)
   {
      return true;
   }
   else
   {
      return false;
   }

}
function confirmDelete($barID,$itemID,$categoryID)
{

   $string .="Are you sure you want to delete this special? 
      <form action='".$_SERVER['PHP_SELF']."' method='POST'>
      <input type='hidden' name='itemID' value='$itemID' />
      <input type='hidden' name='barID' value='$barID' />
      <input type='hidden' name='categoryID' value='$categoryID' />
      <input type='submit' name='confirm' value='Yes' />
      |
      <input type='submit' name='confirm' value='No' />
      </form>
      ";
   return $string;

}
function deleteCategory($barID,$itemID)
{
   $sql = "DELETE FROM menuItem WHERE menuItemID = '$itemID' LIMIT 1;";
   $result = mysql_query($sql);
   if($result)
   {
      return true;
   }
   else
   {
      return false;
   }
}


if($_SESSION[userid])
{
   if($_POST[barID])
   {
      
      //checks to see if we need to insert/update/delete records/*{{{*/
     // if( (in_array(1,$_SESSION['sessgrouplist']) ) || (($_SESSION[sessuserid] == $_POST[barID] ) && in_array(13,$_SESSION[sessgrouplist]) ) )
      //if($_SESSION['admin'] <= 1)
      //if ( in_array(1,$_SESSION['groupList']) )
      if ( /*in_array(1,$_SESSION['groupList']) ||*/ $_SESSION['usertype'] == 2 )
      {
	 //print("okay, should be a go from here");
	 if(empty($_POST[itemID]) && !empty($_POST[item]) ) //adds the record
	 {
	    //if we want to make more then item name required, I would put the error checking here for insert
	    if(insertItem($_POST[categoryID],$_POST[item],"$_POST[dollars].$_POST[cents]",$_POST[desc]) )//inserts the special  
	    {
	       print("Your item has been added.");
   
	    }
	    else
	    {
	       print("There was an error adding your item.  Please try again.  
	       If you have continued problems please contact our webmaster at 
	       <a href='mailto:webmaster@hotbarspot.com'>webmaster@hotbarspot.com</a>.");
	    }
	    $flag = 'new';
	 }/*}}}*/
	 elseif(!empty($_POST[itemID]) && $_POST[submit] == 'Save') //edits the record/*{{{*/
	 {
	    if(!empty($_FILES[image]['name'][0])) //means that we have a file we need to upload.
	    {

	       $categoryID = '13';
	       $fileInputBoxName = 'image';
	       include '../../images/uploaded/uploadfile.php'; //allows us to upload files with error checking
	       
	       //now that we have an uploaded file...lets delete the old
	       
	       $img  = $fullpath."/images/uploaded/images/$_POST[currentImage]";
	       $img2 = $fullpath."/images/uploaded/sized/$_POST[currentImage]";
	       $img3 = $fullpath."/images/uploaded/thumbs/$_POST[currentImage]";

	       if (file_exists($img) && file_exists($img2) && file_exists($img3))
	       {
		  @unlink($img);
		  @unlink($img2);
		  @unlink($img3);
	       }
	    }
	    else
	    {
	       $imagename = $_POST[currentImage];
	    }

	    if(updateItem($_POST[itemID],$_POST[item],"$_POST[dollars].$_POST[cents]",$_POST[desc],$_POST[categoryID]) )
	    {
	       print("Your item has been updated.");
   
	    }
	    else
	    {
	       print("There was an error updating your item.  Please try again.  
	       If you have continued problems please contact our webmaster at 
	       <a href='mailto:webmaster@hotbarspot.com'>webmaster@hotbarspot.com</a>.");
	    }
	    $flag = 'new';
	    $_POST[itemID] = ''; //clears contents of itemID
	 }/*}}}*/
	 elseif($_POST[submit] == 'Delete') //confirms deletion of the record/*{{{*/
	 {
	    print confirmDelete($_POST[barID],$_POST[itemID],$_POST[categoryID]);
	 }/*}}}*/
	 elseif($_POST[confirm] == 'Yes' && !empty($_POST[itemID]) )
	 {
	    if(deleteCategory($_POST[barID],$_POST[itemID]) )//deletes the special  
	    {
	       print("Your item has been deleted.");
   
	    }
	    else
	    {
	       print("There was an error deleting your item.  Please try again.  
	       If you have continued problems please contact our webmaster at 
	       <a href='mailto:webmaster@hotbarspot.com'>webmaster@hotbarspot.com</a>.");
	    }
	 }

      
	 if(!empty($_POST[itemID]))
	 {
	    $sql = "SELECT * FROM menuItem WHERE menuItemID = '$_POST[itemID]' LIMIT 1;";
	    $result = mysql_query($sql);
	    $row = mysql_fetch_array($result);
	 }
	 print("
	    <form action='/admin/menu/menuCategories.php' name='menuCategories' method='POST'>
	    <input type='hidden' name='barID' value='$_POST[barID]' />
	    <a href='#' onclick='document.menuCategories.submit()'>Manage Menu Categories</a>
	    </form>");

	 if($flag == 'new')
	 {
	    print drawForm($_POST[barID],$_POST[itemID],$_POST[categoryID],'','','');
	 }
	 else
	 {
	    print drawForm($_POST[barID],$_POST[itemID],$_POST[categoryID],$row[menuItemName], $row[menuItemPrice], $row[menuItemDesc]);
	 }
	 print("<br /><br />");
	 print drawCategories($_POST[barID]);
	 print("<a href='#' onclick='document.menuCategories.submit()'>Manage Menu Categories</a>");


      }//ends check for proper permissions
      else
      {
	 print("You do not have sufficient privileges to edit this page for the user that was selected.");
      }
      
   }//ends check for post
   else
   {
      print("You can not link directly to this page.  
      You will have to go through the proper links to 
      navigate to this page with the correct information.");      

   }
}//ends check for sessuser (logged in)
else
{
   print("You must be logged in to view this page.");
}

$cnt = ob_get_contents();
ob_clean();

/* Save Content */
$doc->DOMChangeTemplate("content",$cnt);
$doc->WriteHTML();
