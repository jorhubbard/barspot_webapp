<?php
include '../connect.php';
include '../../core.php';
include '../functions.php';
ob_start();
/* Login */
//if(!$login->is_logged_in()){ HTTP::redirect_to_url("login.php");        }

//print("<link href=\"/css/schedule.css\" rel=\"stylesheet\" type=\"text/css\" />");
function saveForm($post)
{
   $sql = "DELETE FROM userSchedule WHERE barID = $post[barID] LIMIT 1;";
   $result = mysql_query($sql);

   $monday	  = $post[mon];
   $tuesday	  = $post[tue];
   $wednesday	  = $post[wed];
   $thursday	  = $post[thu];
   $friday	  = $post[fri];
   $saturday	  = $post[sat];
   $sunday	  = $post[sun];
   
   for($x=0;$x<96;$x++)
   {
      $mon .=($monday[$x] == 1 ? 1:0);
      $tue .=($tuesday[$x] == 1 ? 1:0);
      $wed .=($wednesday[$x] == 1 ? 1:0);
      $thu .=($thursday[$x] == 1 ? 1:0);
      $fri .=($friday[$x] == 1 ? 1:0);
      $sat .=($saturday[$x] == 1 ? 1:0);
      $sun .=($sunday[$x] == 1 ? 1:0);
   }

   $sql = "INSERT INTO userSchedule (barID,mon,tue,wed,thu,fri,sat,sun,timezoneID,updatedTimestamp) VALUES ('$post[barID]','$mon','$tue','$wed','$thu','$fri','$sat','$sun','1',NOW() );";
   //print("sql = $sql<br /><br />");
   $result = mysql_query($sql)or die("Invalid " . mysql_error());

   if($result)
   {
      $string = "Your record has been updated.<br />";
   }
   else
   {
      $string = "There was an error updating your record.  Please try again, if the problem persists, contact support@hotbarspot.com.<br />";
   }
   return $string;
}


function createTimeLabels()
{
   $minute = array('00','15','30','45');
   $i = 0; //index counter
   for($x=0;$x<=11;$x++)//x is the hour we are on in am here
   {
      if($x == 0)
      {
	 $hour = 12;
      }
      else
      {
	 $hour = $x;
      }
      foreach($minute as $min)
      {
	 $timeLabel[$i] = "<td>$hour:$min AM</td>";
	 $i++;
      }
   }
   for($x=0;$x<=11;$x++)//x is the hour we are on in am here
   {
      if($x == 0)
      {
	 $hour = 12;
      }
      else
      {
	 $hour = $x;
      }
      foreach($minute as $min)
      {
	 $timeLabel[$i] = "<td>$hour:$min PM</td>";
	 $i++;
      }
   }

   //print("Time label = ");
   //print_r($timeLabel);
   //print("<br /><br />");
   
   return $timeLabel;

}

function createDayBox($name,$checked,$divID)
{

   $numberBoxes = 96; //4 for each hour, 24 hours
   if(empty($_SESSION[divID]) )
   {
      $_SESSION[divID] = 1;
   }
   for($x=0;$x<$numberBoxes;$x++)
   {
      if($checked[$x] == 1)
      {
	 $daybox[$x] = "<td><div id='".$divID."d' class='selected' onmousedown=\"StartDragSelect(this,'$name',$x);\"></div>";
	 $daybox[$x] .="<input type='hidden' value='1' id='input$name$x' name='$name"."[".$x."]' ";
      }
      else
      {
	 $daybox[$x] = "<td><div id='".$divID."d' onmousedown=\"StartDragSelect(this,'$name',$x);\"></div>";
	 $daybox[$x] .="<input type='hidden' value='0' id='input$name$x' name='$name"."[".$x."]' ";
      }
      $daybox[$x] .= "/></td>";
      $divID = $divID+7;
   }

   return $daybox;
}

function retrieveSchedule($barID)
{
   $sql = "SELECT * FROM userSchedule WHERE barID = '".quote_smart($barID)."' LIMIT 1;";
   //print("sql = $sql <br /><br />");
   $result = mysql_query($sql)or die("invalid: " .mysql_error());
   $count = mysql_affected_rows();
   $row = mysql_fetch_array($result);
   //print("count = $count");
   if($count == 1)
   {
	 $sch[0] = $row[mon];
	 $sch[1] = $row[tue];
	 $sch[2] = $row[wed];
	 $sch[3] = $row[thu];
	 $sch[4] = $row[fri];
	 $sch[5] = $row[sat];
	 $sch[6] = $row[sun];

   }
   else
   {
      //load default schedule starting with monday at 0
      //			        0    1    2    3    4    5    6    7    8    9    10   11   12   13   14   15   16   17   18   19   20   21   22   23
      $sch[0]	  = str_replace(' ','','0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000');
      $sch[1]	  = str_replace(' ','','0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000');
      $sch[2]	  = str_replace(' ','','0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 1111 1111 1111 1111');
      $sch[3]	  = str_replace(' ','','1111 1111 1100 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 1111 1111 1111 1111');
      $sch[4]	  = str_replace(' ','','1111 1111 1100 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 1111 1111 1111 1111');
      $sch[5]	  = str_replace(' ','','1111 1111 1100 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 1111 1111 1111 1111');
      $sch[6]	  = str_replace(' ','','0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000');
   }

   return $sch;
}

function drawForm()
{
   $string = "
   <form action='manageSchedule.php?barID=$_GET[barID]' method='POST'>
   <input type='hidden' name='barID' value='$_GET[barID]'  />
   Please note, if you want to record on a particular day after midnight, that you will want to set the record time for the following day.  For example, Friday from 6PM - 2AM would involve selecting from 6PM - 12PM Friday, and from 12AM - 2AM Saturday.<br />
   <span id=\"show_enabled_container\">
   Hide 2:30AM - 4PM Hours: <input type=\"checkbox\" id=\"show_enabled\" onchange=\"toggleEnabled(!this.checked);\" />
   </span>
   <table class='schedule' cellspacing='1' cellpadding='0'>
   <tr>
      <th>Time</th>
      <th>Mon</th>
      <th>Tue</th>
      <th>Wed</th>
      <th>Thu</th>
      <th>Fri</th>
      <th>Sat</th>
      <th>Sun</th>
   </tr>
   ";

   $sch	       = retrieveSchedule($_GET[barID]);
   $timeLabels = createTimeLabels();
   $mon	       = createDayBox('mon',$sch[0],1);
   $tue	       = createDayBox('tue',$sch[1],2);
   $wed	       = createDayBox('wed',$sch[2],3);
   $thu	       = createDayBox('thu',$sch[3],4);
   $fri	       = createDayBox('fri',$sch[4],5);
   $sat	       = createDayBox('sat',$sch[5],6);
   $sun	       = createDayBox('sun',$sch[6],7);

   $numberBoxes = 96;

   for($x=0;$x<$numberBoxes;$x++)
   {
      if($x >= 10 && $x <= 63)
      {
	 $hiddenClass = 'not_enabled';
      } 
      else
      {
	 $hiddenClass = 'enabled';
      }
      $string .="
      <tr class='$hiddenClass'>
	 $timeLabels[$x]
	 $mon[$x]
	 $tue[$x]
	 $wed[$x]
	 $thu[$x]
	 $fri[$x]
	 $sat[$x]
	 $sun[$x]
      </tr>
      ";
   }

   $string .="
   </table>
   <input type='submit' name='submit' value='Save' />
   </form>";


   return $string;
}

function writeFile($guid,$fileText)
{
   $myFile = "conf/$guid.conf";
   $fh = fopen("$myFile", 'w');

   $stringData .= $fileText;
   $write = fwrite($fh, $stringData);
   fclose($fh);

   if($write)
   {
      return true;
   }
   else
   {
      return false;
   }
}

if($_POST)
{
   //print("post = <br />");
   //print_r($_POST);
   //print("<br /><br />");
   //do user validation here
   print saveForm($_POST);
   //print retrieveScheduleFileInfo();
   }

print drawForm();
print("
<script type='text/javascript' src='select.js'></script>
");
$cnt = ob_get_contents();
ob_clean();
/* Save Content */
$doc->DOMChangeTemplate("content",$cnt);
$doc->WriteHTML();
