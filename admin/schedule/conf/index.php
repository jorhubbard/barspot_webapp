<?
include '../../functions.php';
include '../../connect.php';

function confToUTC($barID)
{
   //$barID = barIDFromMac($mac);

   $conf = explode("\n",retrieveScheduleFileInfo($barID) );

   $x = 1;//total line count index
   $i = 0;//useable line count index
   $d = 0;//day count index (monday = 0)
   foreach($conf as $c)
   {
      //print("line $x: $c<br />");
      if($c[0] != '#' && $c[0] != ' ' && $c[0] != "\n" && $c[0] != '')
      {
	 if(substr($c,0,2) == 'tz')
	 {
	    $tz = explode(':',$c);
	    $tz = $tz[1];
	    //print("tz = $tz<br />");
	 }
	 else if(substr($c,0,3) == 'day')
	 {
	    $dayArray = explode(":",$c);
	    $day[$d] = str_replace(' ','',$dayArray[1]);
	    //print("day[$d] = $day[$d]<br />");
	    $d++;
	 }
	 //print("c = $c<br />");
	 $line[$i] = $c;
	 //print("l $i: = $line[$i]<br />");
	 $i++;

      }
      $x++;
   }

   //print("<br /><br />");
   //set timezone to that of device area
   date_default_timezone_set($tz);

   //echo date('l jS \of F Y h:i:s A');
   $hourDiff = explode(':',date('P') ); //gets the timezone hour:minute offset and separates on colon
   $hourDiff = $hourDiff[0]; //retrieves just the hour portion
   $blocks   = $hourDiff * 4; //calculates how many characters need to be moved based on 15 minute blocks
   //print("hour difference = $hourDiff, blocks = $blocks<br />");

   //if($blocks < 0 )
   //{
   /*
      $newDay[0] = substr($day[6],$blocks,abs($blocks)) . substr($day[0],0,(96 + $blocks) );
      $i = 1;
      for($x=6;$x>=1;$x--)
      {
	 $dayInt = $x - 1;
	 $newDay[$i] = substr($day[$dayInt],$blocks,abs($blocks)) . substr($day[$x],0,(96 + $blocks) );
	 $i++;
      }
      //print("<pre>");
      //print_r( $newDay );
      //print("</pre>");
      
   //}
   */
   $newDay[0] = substr($day[6],80,16) . substr($day[0],0,80);
   $newDay[1] = substr($day[0],80,16) . substr($day[1],0,80);
   $newDay[2] = substr($day[1],80,16) . substr($day[2],0,80);
   $newDay[3] = substr($day[2],80,16) . substr($day[3],0,80);
   $newDay[4] = substr($day[3],80,16) . substr($day[4],0,80);
   $newDay[5] = substr($day[4],80,16) . substr($day[5],0,80);
   $newDay[6] = substr($day[5],80,16) . substr($day[6],0,80);


   $string .="#Last Time schedule Modified: ".date('l jS \of F Y h:i:s A')."\n";
   $string .="#monday = first day\n";
   $string .="tz:$tz\n";
   $string .="#    ";
   for($x=0;$x<24;$x++)
   {
      if(strlen($x) > 1)
      {
	 $string .="$x  ";
      }
      else
      {
	 $string .="$x   ";
      }
   }
   $string .="\n";
   for($x=0;$x<7;$x++)
   {
      $i = $x+1; //day count (monday = day 1)
      $string .="day$i:$newDay[$x]\n";
   }

   return $string;
}

//print ("<pre>".retrieveScheduleFileInfo($barID)."</pre>");
$barID = barIDFromMac($_GET[id]);
print confToUTC($barID);
?>
