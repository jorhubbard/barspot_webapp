<?
include '../connect.php';
include ("../jpgraph/jpgraph.php");
include ("../jpgraph/jpgraph_bar.php");

$id = $_GET[id];
//query for information
$sql = "
SELECT MONTH( barLogDate ) AS month , 
YEAR( barLogDate ) AS year, 
COUNT( barLogBarID ) AS count
FROM barLogs
WHERE barLogBarID = '$id'
GROUP BY YEAR( barLogDate ) , MONTH( barLogDate )
ORDER BY year DESC ,
month DESC
LIMIT 12 ;
";
//print("sql = $sql<br />");
   $result = mysql_query($sql) or die("Invalid query: " . mysql_error());

if($row = mysql_fetch_array($result))
{
   $x = 0;
   do
   {
      $datay[$x]     = $row['count'];
      $xlabels[$x]   = $row['month']."/".substr($row['year'],2,2);
      $x++;
   }while($row = mysql_fetch_array($result));

}



// We need some data
//$datay=array(4,8,6);

// Setup the graph. 
$graph = new Graph(500,150,"auto");
$graph->SetScale("textlin");
$graph->img->SetMargin(25,15,25,25);

$graph->title->Set('Last 12 Months');
$graph->title->SetColor('darkred');

$graph->xaxis->SetTickLabels($xlabels); 
//$graph->xaxis->SetLabelAngle(45);
// Setup font for axis
$graph->xaxis->SetFont(FF_FONT1);
//$graph->xaxis->SetFont(FF_ARIAL,FS_NORMAL,11);
$graph->yaxis->SetFont(FF_FONT1);

// Create the bar pot
$bplot = new BarPlot($datay);
$bplot->SetWidth(0.6);

// Setup color for gradient fill style 
$bplot->SetFillGradient("darkred","lightred",GRAD_WIDE_MIDVER);

// Set color for the frame of each bar
$bplot->SetColor("red");
$graph->Add($bplot);

// Finally send the graph to the browser
$graph->Stroke();

?>
