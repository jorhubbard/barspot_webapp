<?php
include '../connect.php';
include_once '../../core.php';
ob_start();

function startToCurrent($id)
{
   $sql = "SELECT COUNT(barLogBarID) AS count FROM barLogs WHERE barLogBarID = '$id' GROUP BY barLogBarID;";
   $result = mysql_query($sql);
   $row = mysql_fetch_array($result);

   $count = $row['count'];

   $string = "Visits to page since membership: $count<br />";

   return $string;

}

function currentYear($id)
{
   $sql = "SELECT MONTH(barLogDate) AS month, YEAR(barLogDate) AS year, COUNT(barLogBarID) AS count 
   FROM barLogs WHERE barLogBarID = '$id' 
   GROUP BY MONTH(barLogDate) , YEAR(barLogDate)
   ORDER BY year DESC, month DESC LIMIT 12;";

   $result = mysql_query($sql);

   if($row = mysql_fetch_array($result))
   {
      $string = "
      <table>
      <tr>
	 <th>Date</th>
	 <th>Visits</th>
      </tr>
      ";
      do
      {
	 $string .="
	 <tr>
	    <th>$row[month]/".substr($row[year],2,2)."</th>
	    <th>$row[count]</th>
	 </tr>
	 ";

      }while($row = mysql_fetch_array($result));
      
      $string .="</table>";
   }

   return $string;

}


print startToCurrent($_GET[id]);
print currentYear($_GET[id]);

print("<br /><img src='yearStatsGraph.php?id=$_GET[id]' /><br />");

$cnt = ob_get_contents();
ob_clean();
/* Save Content */
$doc->DOMChangeTemplate("content",$cnt);
$doc->WriteHTML();
