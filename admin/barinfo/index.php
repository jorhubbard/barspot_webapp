<?php
include '../connect.php';
include '../functions.php';

function checkBarOwner($id)
{
   $sql = "SELECT userId FROM user_bars_tbl WHERE barID = '".quote_smart($id)."' LIMIT 1;";
   $result = mysql_query($sql);
   $row = mysql_fetch_array($result);

   if($_SESSION[userid] == $row[userId])
   {
      return true;
   }
   else
   {
      return false;
   }

}
function drawHours($id)
{
   $string = "
   <table border='1'>
   ";

   $days = array(0 => 'Sunday', 1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday');
   foreach($days as $key=>$day)
   {
      $string .="
      <tr>
	 <td>$day ".drawHourDropdown($key)."</td>
      </tr>
      ";
   }

   $string .= "
   </table>
   ";

   return $string;
}
function drawHourDropdown($dayID)
{
   $string = "

   ";
   return $string;
}
function drawChangeBarInfo($id)
{
   $sql = "SELECT * FROM bars WHERE barID = '".quote_smart($id)."' LIMIT 1;";
   $result = mysql_query($sql);
   $row = mysql_fetch_array($result);

   $string = "  




      <h2>Change info for $row[barName]:</h2>
      <form action='index.php?id=$id' method='POST'>
      <table>
      <tr>
      <td>
      <table>
      <tr>
      <td>*Bar Name:</td>
      <td>
	 <input type='hidden' name='barID' value='$id' />
	 <input type='text' name='barName' maxlength='150' size='25' value=\"".htmlentities($row[barName])."\" /></td>
      </tr>
      <tr>
      <td>*Bar Address:</td>
      <td><input type='text' name='barAddress' maxlength='100' size='25' value='".htmlentities($row[barAddress])."' /></td>
      </tr>
      <tr>
      <td>*Bar Zip:</td>
      <td><input type='text' name='barZip' maxlength='5' size='25' value='$row[barZip]' /></td>
      </tr>
      <tr>
      <td>*Bar Phone:</td>
      <td><input type='text' name='barPhone' maxlength='15' size='25' value='$row[barPhone]' /></td>
      </tr>
      <tr>
      <td>Bar Website:</td>
      <td><input type='text' name='barWebsite' maxlength='200' size='25' value='$row[barWebsite]' /></td>
      </tr>
      <tr>
      <td>Bar Latitude</td>
      <td><input id='lat' type='text' name='barLat' maxlength='20' size='25' value='$row[barLat]' readonly='yes' /></td>
      </tr>
      <tr>
      <td>Bar Longitude</td>
      <td><input id='lon' type='text' name='barLon' maxlength='20' size='25' value='$row[barLon]' readonly='yes' /></td>
      </tr>
      </table>
      </td>

      <td>
      <!-- the div where the map will be displayed -->

      <div id=\"map\" style=\"width:400px; height: 200px\"></div><br />Move the marker to your location if it is not already there.

      </td>

      </tr>
      <tr>
	 <td colspan='2'>".drawFeatures($id)."</td>
      </tr>
      <tr>
	 <td colspan='2'>Bar Description (max 500 characters):<br /><textarea name='description' rows='5' cols='100'>$row[barDescription]</textarea>

	 <!-- consider adding javascript check for number of characters to make more user friendly -->
      <tr>
	 <td colspan='2'><input type='submit' name='submit' value='Update' /></td>
      </tr>
      </table>

      </form>

<noscript><b>JavaScript must be enabled in order for you to use Google Maps.</b> 
However, it seems JavaScript is either disabled or not supported by your browser. 
To view Google Maps, enable JavaScript by changing your browser options, and then 
try again.
</noscript>

<script type=\"text/javascript\">
//<![CDATA[

if (GBrowserIsCompatible()) { 

   function createMarker(point,html) {
      var marker = new GMarker(point);
      return marker;
   }

   // Display the map, with some controls and set the initial location 
   var map = new GMap2(document.getElementById(\"map\"));
   map.addControl(new GSmallMapControl());
   map.addControl(new GMapTypeControl());
   map.setCenter(new GLatLng($row[barLat],$row[barLon]),17);

   var marker = new GMarker(new GLatLng($row[barLat],$row[barLon]), {draggable: true});
      map.addOverlay(marker);
   GEvent.addListener(marker, \"dragend\", function() {
      var point =marker.getPoint();
      map.panTo(point);
      document.getElementById(\"lat\").value = point.lat();
      document.getElementById(\"lon\").value = point.lng();
   });
}
else {
   alert(\"Sorry, the Google Maps API is not compatible with this browser\");
}
//]]>
</script>
      ";

   return $string;
}
//verify permissions first
//print("group list: <pre>");
//print_r($_SESSION);
//print("</pre><br />");
if(( checkBarOwner($_GET[id]) || (is_array($_SESSION[groupList]) && in_array('1',$_SESSION[groupList]) ) ) && isset($_GET[id]) )
{

   if(isset($_POST[barID]))
   {
      if(!empty($_POST[barID]))
      {
	 if(!empty($_POST[barName]) && !empty($_POST[barAddress]) && !empty($_POST[barZip]) && !empty($_POST[barPhone]) )
	 {
	    $sql = "UPDATE bars SET 
	    barName = '".quote_smart($_POST[barName])."',
	    barAddress = '".quote_smart($_POST[barAddress])."',
	    barZip = '".quote_smart($_POST[barZip])."',
	    barPhone = '".quote_smart($_POST[barPhone])."',
	    barWebsite = '".quote_smart($_POST[barWebsite])."',
	    barLat = '".quote_smart($_POST[barLat])."',
	    barLon = '".quote_smart($_POST[barLon])."',
	    barDescription = '".quote_smart($_POST[description])."'
	    WHERE barID = '".quote_smart($_POST[barID])."'
	    LIMIT 1;";
	    $result = mysql_query($sql);
	    if($result)
	    {
	       $string .="Your bar information has been updated.<br />";
	    }
	    else
	    {
	       $string .="There was an error updating your bar information, please try again.  If the problem persists, contact support@hotbarspot.com<br />";
	    }

	    $sql = "DELETE FROM bar_group_id WHERE barID = '".quote_smart($_POST[barID])."';";
	    $result = mysql_query($sql);

	    if(!empty($_POST[features]) )
	    {
	       foreach($_POST[features] as $feature)
	       {
		  //might revisit this and make into one query that inserts all to save database hits
		  $sql = "INSERT INTO bar_group_id VALUES ('','".quote_smart($_POST[barID])."','".quote_smart($feature)."');";
		  $result = mysql_query($sql);
	       }
	    }
	 }
	 else
	 {
	    $string .="<font color='#FF0000'>PLEASE FILL OUT ALL REQUIRED FIELDS</font>";
	 }
      }

   }

   //$_SESSION['gmapkey'] = 'ABQIAAAAkcabiflt0rJ41s0jlveBxRTo6iNWyThNaEjbcXjqDfeFwrcj_BTDUBu7ik8uoZEHIUl7dIYZIfGeew';
   
$javascript .="<script src=\"http://maps.google.com/maps?file=api&amp;v=2&amp;key=".$_SESSION['gmapkey']."\" type=\"text/javascript\"></script>\n";

   include '../../html/header.html.php';

   print("$string <br />");
   print drawChangeBarInfo($_GET[id])

   ?>







<?
}
else
{
   print("You do not have permissions to view this page.");
}


include '../../html/footer.html.php';

//$cnt = ob_get_contents();
//ob_clean();

/* Save Content */
//$doc->DOMChangeTemplate("content",$cnt);
//$doc->WriteHTML();