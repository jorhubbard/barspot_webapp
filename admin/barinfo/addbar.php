<?
include '../connect.php';

include '../../core.php';
ob_start();

function drawAddBar()
{
   if(!empty($_POST))
   {
      if(!empty($_POST['barName']) && !empty($_POST['barAddress']) && !empty($_POST['zipcode']) && !empty($_POST['phone']) )
      {
	 $sql = "INSERT INTO bars VALUES ('','".quote_smart($_POST['barName'])."','".quote_smart($_POST['barAddress'])."','".quote_smart($_POST['zipcode'])."','".quote_smart($_POST['phone'])."','".quote_smart($_POST['website'])."',NOW(),'','','2','0','');";
	 $result = mysql_query($sql);
	 $barID = mysql_insert_id();
	 if($result)
	 {
	    $sql = "INSERT INTO user_bars_tbl VALUES ('','$_SESSION[userid]','$barID');";
	    $result = mysql_query($sql)or die("invalid mysql: " . mysql_error());

	    if($result)
	    {
	       $string .="Your bar has been inserted and is now listed on your bar list.";
	    }
	    else
	    {
	       $string .="Your bar has been added, but there was a problem associating it with your account.  Please contact support@hotbarspot.com.";
	    }
	 }
	 else
	 {
	    $string .="There was an error adding your bar, please try again.  If the problem persists, contact support@hotbarspot.com<br />";
	 } 
      }
      else
      {
	 $string .="<font color='#FF0000'>PLEASE FILL OUT ALL REQUIRED FIELDS</font>";
      }
   }
   $string .="
   <form action='".$_SERVER['PHP_SELF']."' method='POST'>
   <center>
   <table width='350px'>
   <tr>
      <td>*Bar Name:</td>
      <td><input type='text' name='barName' maxlength='100' size='25' /></td>
   </tr>
   <tr>
      <td>*Bar Address:</td>
      <td><input type='text' name='barAddress' maxlength='50' size='25' /></td>
   </tr>
   <tr>
      <td>*Zip Code:</td>
      <td><input type='text' name='zipcode' maxlength='5' size='25' /></td>
   </tr>
   <tr>
      <td>*Bar Phone:</td>
      <td><input type='text' name='phone' maxlength='15' size='25' /></td>
   </tr>
   <tr>
      <td>Bar Website:</td>
      <td><input type='text' name='website' maxlength='150' size='25' /></td>
   </tr>
   <tr>
      <td></td>
      <td><input type='submit' name='submit' value='Add Bar' /></td>
   </tr>
   <tr>
      <td colspan='2'>*Denotes a required field<br /><br />**Please note that you will be able to manage any bars that are added, but they will not show up in our database until they have gone through our review board to verify that all of the information provided is acurate.</td>
   </tr>
   </table>
   </center>
   </form>
   ";

   return $string;

}

if($_SESSION['usertype'] == 2) //bars
{
   print("<h2>Please only add a bar your bar if the bar is not already listed in our database.</h2>");
   print drawAddBar();
}
else
{
   print("You must be logged in with a bar account to be able to add a bar to the database.  If you would like to suggest a new bar, be added as a regular user, please visit <a href='/review.php?barID=0'>our refer a bar page</a> and fill out the form there.");
}

$cnt = ob_get_contents();
ob_clean();
/* Save Content */
$doc->DOMChangeTemplate("content",$cnt);
$doc->WriteHTML();


?>
