<?
function barIDFromMac($mac)
{
   $sql = "SELECT barID FROM barCameras WHERE barCameraMac = '".quote_smart($mac)."' LIMIT 1;";

   $result = mysql_query($sql);
   if($row = mysql_fetch_array($result) )
   {
      return $row[barID];
   }
   else
   {
      die("The mac you specified was not found.");
   }

}

function retrieveScheduleFileInfo($id)
{

   $sql = "SELECT barID, mon, tue, wed, thu, fri, sat, sun, updatedTimestamp, timezone FROM userSchedule 
   AS s LEFT JOIN timezones AS t ON t.timezoneID = s.timezoneID
   WHERE barID = '".quote_smart($id)."' LIMIT 1;";
   //print("sql = $sql<br />");
   $result = mysql_query($sql)or die("invalid: " . mysql_error());
   if($row = mysql_fetch_array($result))
   {
      $string .="#\n";
      //$string .="GUID: $row[barID]\n";
      $string .="#Last time schedule modified: $row[updatedTimestamp]\n";
      $string .="tz:$row[timezone]\n";
      $string .="#    0    1    2    3    4    5    6    7    8    9    10   11   12   13   14   15   16   17   18   19   20   21   22   23   \n";
      $string .="day1:".addSpaces($row[mon])."\n";
      $string .="day2:".addSpaces($row[tue])."\n";
      $string .="day3:".addSpaces($row[wed])."\n";
      $string .="day4:".addSpaces($row[thu])."\n";
      $string .="day5:".addSpaces($row[fri])."\n";
      $string .="day6:".addSpaces($row[sat])."\n";
      $string .="day7:".addSpaces($row[sun]);

      $output = $string;
   }
   else
   {
      $output = 'The company you specified is not actively listed in our database.  Please contact support@hotbarspot.com if you are seeing this message.<br />';
   }
   return $output;
}
function addSpaces($day)
{
   $txt_array=array();

   // GET STRING LENGHT
   $len=strlen($day);
   // WANT TO ADD SPACE AFTER X character
   $number="4"; // here, 5 character

   //COUNT FOR LOOP BELOW
   $count=$len/$number;

   //LOOP
   $i="0";
   while($i<=$count) {
      if($i=="0") {$ib="0";} else {$ib=($i*$number);}
      $txt_array[$i]=substr($day,$ib,$number);
      $i++;}

      //GET FINAL TEXT
      $count_array=count($txt_array)-1; $i="0";
      while ($i<=$count_array) {
	 if ($i=="0") {$day=$txt_array[$i].' '; } else {$day .=''.$txt_array[$i].' ';}
	 $i++;}

	 //echo '<br><br><b>RESULT</b><br>'.$day.''; 
	 return $day;

}



function getBarImage($id)
{
   $sql = "
      SELECT galleryUserID, id, ext FROM `galleryCategories` AS gc
      LEFT JOIN images AS i on gc.galleryCategoryID = i.galleryCategoryID
      WHERE galleryBarID = '$id'
      LIMIT 1;
   ";

   $result = mysql_query($sql);
   if($row = mysql_fetch_array($result) )
   {

      //$path = "http://www.hotbarspot.com/images/uploaded/iphone/$row[galleryUserID]/";
      $path = "http://www.hotbarspot.com/images/uploaded/thumbs/$row[galleryUserID]/";
      $image = $path.$row[id].$row[ext];

      return $image;
   }

   return false;
}

function retrieveUserInfo($id)
{
   $sql = "
      SELECT u.userId, username, userFirst, userLast, userEmail FROM `user_tbl` AS u
      LEFT JOIN user_info_tbl AS ui ON ui.userId = u.userId
      WHERE u.userId = '$id'
      LIMIT 1;
   ";
   $result = mysql_query($sql);
   $row = mysql_fetch_array($result);
   return $row;

}

function checkPref($id,$field)
{
   $sql = "
      SELECT $field FROM userPreferences WHERE userID = '$id' LIMIT 1;
   ";
   $result = mysql_query($sql);

   if(mysql_affected_rows() == '1')
   {
      $row = mysql_fetch_array($result);
      if($row[0] == '1')
	 return true;
      else //check to see if they are a friend
      {
	 if(isFriend($id))
	    return true;
	 else
	    return false;
      }
   }
   else
   {
      return true;
   }


}
function getMinutes($time)
{
   $time = explode(':',$time);

   //print("TIME = $time[0] $time[1]");
   $minutes = ($time[0] * 60) + $time[1];

   //get time zone and subtract
   $sql = "
      SELECT timezoneValue FROM userSchedule AS us
      LEFT JOIN timezones AS t ON us.timezoneID = t.timezoneID
      WHERE barID = '".quote_smart($_GET[id])."'
      ";
   $result = mysql_query($sql);
   $row = mysql_fetch_array($result);
   $tzoffset = $row[timezoneValue] * 60;
   $minutes = $minutes + $tzoffset;
   return $minutes;
}
function isCurrentFeed($id,$cam,$timezoneID,$type)
{
   $ls = shell_exec("ls -lt --time-style=long-iso /var/www/video/clients/$id/$cam | grep .$type | head -1 | awk '{print $6\" \"$7\" \"$8}'");
   //$lines = explode("\n",$ls);
   //print("ls = $ls<br />");
   if(!empty($ls) )
   {
      $piece = explode(" ",$ls);
      $date = $piece[0]." ".$piece[1];
      //print("<script>alert(\"$date\");</script>");
      $sql = "SELECT timeZoneValue FROM timezones WHERE timezoneID = '$timezoneID' LIMIT 1;";
      //print("sql = $sql");
      $result = mysql_query($sql);
      $row = mysql_query($sql);

      if(!empty($date))
      {
	 $currentUTCtimestamp = time(void); //in seconds
	 $timeDifferential = $row[timezoneValue] * 60 * 60; //in seconds
	 $fileTimestamp = strtotime($date);

	 $diffSeconds = ($currentUTCtimestamp + $timeDifferential) - $fileTimestamp;
	 if($diffSeconds <= 300 && $diffSeconds >= -300)
	 {
	    //print("<script>alert(\"date: $date ".str_replace("\n",'',$ls)." true: $diffSeconds\");</script>");
	    return "http://www.hotbarspot.com/video/clients/$id/$cam/$piece[2]";
	 }
	 else
	 {
	    //print("<script>alert(\"false: $diffSeconds\");</script>");
	    return false;
	 }
      }
      else
      {
	 return false;
      }
   }
   else
   {  
      return false;
   }
   return false;

}
function drawCamLabels($id)
{
   $sql = "SELECT * FROM barCameras
      WHERE barID = '$id'
      AND barCameraActive = '1'
      AND barCameraLive = '1'
      ORDER BY barCameraPrimary DESC;
   ";
   $result = mysql_query($sql);

   if($row = mysql_fetch_array($result))
   {
      $string2 .= "
	 <form action='".$_SERVER['PHP_SELF']."?id=".$id."' method='post'>
	 <select name='camView' onchange='this.form.submit()'>
	 <option value='#'>LIVE FEED</option>";
      $firstTime = '0';
      do{
	 if($firstTime == '0'){
	    $camID = $row['barCameraID'];
	    $firstTime = '1';
	 }
	 if($_POST['camView'] == $row['barCameraID']){
	    $camID = $row['barCameraID'];
	    //$checked = 'selected="yes"';
	 }
	 else {
	    $checked = '';
	 }
	 $string2 .="\t<option value='$row[barCameraID]' $checked>$row[barCameraDescription]</option>\n";
      }while($row = mysql_fetch_array($result) );
      $string2 .="</select>
	 </form>";

   }
   $string[0] = $camID;
   $string[1] = $string2;

   return $string;
}

function getDevice(){
   $device = '';

   if( stristr($_SERVER['HTTP_USER_AGENT'],'ipad') ) {
      $device = "iPad";
   } else if( stristr($_SERVER['HTTP_USER_AGENT'],'iphone') || strstr($_SERVER['HTTP_USER_AGENT'],'iphone') ) {
      $device = "iPhone";
   } else if( stristr($_SERVER['HTTP_USER_AGENT'],'iphone') || strstr($_SERVER['HTTP_USER_AGENT'],'HotBarSpot') ) {
      $device = "iPhone";
   } else if( stristr($_SERVER['HTTP_USER_AGENT'],'blackberry') ) {
      $device = "Blackberry";
   } else if( stristr($_SERVER['HTTP_USER_AGENT'],'android') ) {
      $device = "Android";
   } else if( stristr($_SERVER['HTTP_USER_AGENT'],'iphone') || strstr($_SERVER['HTTP_USER_AGENT'],'Apache-HttpClient/UNAVAILABLE') ) {
      $device = "Android";
   } else {
      $device = 'Website';
   }
   

   if( $device ) {
      return $device; 
   } return false; {
      return false;
   }
}

function saveVisitorLog($streamStatus)
{
   $date = date("Y-m-d H:i:s");
   $barId = quote_smart($_GET['id']);
   $device = getDevice();
   $tempUID = "";
   if (isset($_SESSION['userid']))
       $tempUID = $_SESSION['userid'];
   $sql = "INSERT INTO barLogs (barLogID, visitingUserID, barLogBarID, barLogDate, barLogViewingStatus, barLogViewingType) VALUES ('','$tempUID','$barId','$date','$streamStatus','$device');";
   $result = mysql_query($sql)or die("invalid query: " . mysql_error());

   if($result)
   {
      return true;
   }
   else
   {
      return false;
   }

}
function is_virtual_tour_customer($id)
{

   $sql = "select * from barVirtualTours where barID = '$id' limit 1;";
   $result = mysql_query($sql);

   if(mysql_affected_rows() == 1)
   {
      return true;
   }
   else
   {
      return false;
   }

}

function claimBar()
{
   $sql = "
      SELECT user_tbl.userId AS id,
	     username
		FROM user_bars_tbl
		LEFT JOIN user_tbl ON user_bars_tbl.userId = user_tbl.userId
		WHERE barID = '".quote_smart($_GET[id])."' LIMIT 1;
   ";
   $result = mysql_query($sql);

   if(mysql_affected_rows() >= 1)
   {
      $row = mysql_fetch_array($result);
      $string = "This bar has been claimed by the user $row[username].
	 Please contact <a href='mailto:dispute@hotbarspot.com'>dispute@hotbarspot.com</a>
	 if this is not an authorized user for this account.";
   }
   else
   {
      $string = "<a href='".$_SERVER['PHP_SELF']."?id=$_GET[id]&action=claimBar'>Claim this bar as a bar that I own or manage.</a>";
   }

   return $string;
}


function getDaySchedule($id,$negative=false)  //negative has to do with the time zone subtraction so that the prior day is pulled.
{
   if($negative)
   {
      $day = date("N");
      $day = $day - 1;
      //$day = date($day);
      $dayArray = array(0=>'Sun','Mon','Tue','Wed','Thu','Fri','Sat'); //starting at 0 because date function references monday at 0 and does not utilize zero.
      $day = $dayArray[$day];
   }
   else
   {
      $day = date("D");
   }
   $sql = "SELECT $day, timezoneID FROM userSchedule WHERE barID = '$id' LIMIT 1;";
   $result = mysql_query($sql);

   if(mysql_affected_rows() == 1)
   {
      $row = mysql_fetch_array($result);
      $schedule = $row[0];
      $timezoneID = $row[timezoneID];

      $string[0] = $row;
      $string[1] = $schedule;
      $string[2] = $timezoneID;
      return $string;
   }
   else
   {
      return false;
   }
}
function isActiveCam($schedule)
{
   //print("schedule = $schedule");
   $time = date("G:i");

   $minutes = getMinutes($time); //calculates the how many minutes into the day we are
   $curTimeBlock = ceil($minutes / 15); //divides that by 15 and rounds up to know what segment to check the schedule against

   if($curTimeBlock < 0)
   {
      $curTimeBlock = 96 - abs($curTimeBlock);
   }
   $scheduleIndex = $curTimeBlock - 1;
   //print("\nminutes = $minutes<br />curTimeBlock = $curTimeBlock<br />schedule = $schedule<br />scheduleValue = $schedule[$scheduleIndex]");

   if($schedule[$scheduleIndex] == 1)
      return true;
   else
      return false;

}

function systemEmail($email,$subject,$message)
{
   $headers .= 'From: donotreply@hotbarspot.com' . "\r\n" .
      'X-Mailer: PHP/' . phpversion();
   $result = mail($email, stripslashes($subject), stripslashes($message),$headers);
   //print("mail($email,<br /> $subject,<br /> $message,<br /> $headers");
   return;
}

function drawFeatures($id=false)
{
   $sql = "SELECT groupID FROM bar_group_id WHERE barID = '$id';";
   $result = mysql_query($sql);
   $x = 0;
   while($row = mysql_fetch_array($result))
   {
      $feature[$x] = $row[groupID];
      $x++;
   }
   $string .="
      <table>
      ";
   $sql = "SELECT * FROM group_tbl WHERE groupTypeID = '5' ORDER BY groupName ASC";
   $result = mysql_query($sql);
   $firstRun = 0;
   $numCols = 4;
   while($row = mysql_fetch_array($result))
   {
      if($firstRun == 0)
      {
	 $string .="<tr>\n";
      }
      $string .="\t<td><input type='checkbox' name='features[]' value='$row[groupId]' ";
      if(is_array($feature) )
      {
	 if(in_array($row[groupId],$feature) )
	 {
	    $string .= "checked='yes'";
	 }
      }
      $string .=">$row[groupName]</input></td>\n";
      $firstRun++;
      if($firstRun == $numCols)
      {
	 $string .="</tr>\n";
	 $firstRun = 0;
      }
   }
   if($firstRun != 0) //closes row if it wasn't closed in loop
   {
      $string .="</tr>\n";
   }
   $string .="
      </table>
      ";
   return $string;
}

function has_current_feed($id, $number_of_seconds_before_feed_is_stale)
{
   $sql = "
      SELECT timestamp FROM `liveFeeds` 
      WHERE barID = '$id' 
      and timestamp >= (unix_timestamp() - $number_of_seconds_before_feed_is_stale)
      order by id desc limit 1
      ";

   $result = mysql_query($sql);
   if(mysql_affected_rows() == 1)
   {
      return true;
   }
   else
   {
      return false;
   }
}

function validateSendEmail()
{
   $to      = "$_SESSION[validateEmail]";
   $subject = 'Verify your email address with hotbarspot.com';
   $authTokn= crypt($_SESSION[validateUser],$_SESSION[validateEmail]);
   $message =
"
Thank you for registering for an account with hotbarspot.com. Before we can activate your account one last step must be taken to complete your registration.

Please note - you must complete this last step to become a registered member. You will only need to visit this URL once to activate your account.

To complete your registration, please visit this URL:
http://www.hotbarspot.com/admin/login/activate.php?e=".urlencode($_SESSION['validateUser'])."&authTokn=$authTokn

<a href=\"http://www.hotbarspot.com/admin/login/activate.php?e=".urlencode($_SESSION['validateUser'])."&authTokn=$authTokn\">America Online Users Please Visit Here to be Activated</a>

**** Does The Above URL Not Work? ****
If the above URL does not work by clicking on it, please copy the url and paste it into your web browser.  Do not add any spaces or extra characters to the link when you paste it in.

";
   $headers .= 'MIME-Version: 1.0' . "\r\n";
   //$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
   $headers .= 'From: donotreply@hotbarspot.com' . "\r\n" .
      'X-Mailer: PHP/' . phpversion();

   //print("<pre>mail($to, $subject, $message, $headers);<br /><br /></pre>");
   $result = mail($to, $subject, $message, $headers);
   if($result )
   {
      $string = "An email has been sent for verification.  Please check your mail and then log in.";
   }
   else
   {
      $string = "There seems to have been an issue sending your email.  Try again, and if the problem persists, contact support@hotbarspot.com.";
   }

   return $string;

}
function isFriend($id)
{
   $sql = "
      SELECT response FROM user_friends
      WHERE
      (
       (requestingID = '$id' AND receivingID = '$_SESSION[userid]')
       OR
       (requestingID = '$_SESSION[userid]' AND receivingID = '$id')
      )
      AND
      response = '1'
      ";
   //print("sql = $sql<br />");
   $result = mysql_query($sql);
   $row = mysql_fetch_array($result);
   if($row[0] == '1')
      return true;
   else
      return false;
}

function drawFromWhere()
{
   //$from = array('Search Engine','Facebook','Myspace','Twitter','From a friend','From a band','Saloon Poker','Woodstock Entertainment','Other');
   $sql = "SELECT * FROM fromWhere_tbl ORDER BY fromWhereOrder ASC;";
   $result= mysql_query($sql);
   $i=0;
   while($row = mysql_fetch_array($result))
   {
      $from[$i] = $row['fromWhereName'];
      $i++;
   }
   $string = "<select name='fromWhere'>\n";
   for($x=1;$x<count($from);$x++)
   {
      $i = $x - 1;
      $value = $from[$i];
      $string .="<option value='$x'>$value</option>\n";
   }
   $string .="</select>\n";

   return $string;

}
