<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jstevens
 * Date: 1/15/13
 * Time: 11:54 PM
 * To change this template use File | Settings | File Templates.
 */

require_once 'app_config.php';

define("SUCCESS_MESSAGE", "success");
define("ERROR_MESSAGE", "error");

session_start();


function pageStart($title, $javascript = NULL, $success_message = NULL, $error_message = NULL) {

}

function viewPage($title = NULL, $content = NULL, $javascript = NULL, $success_message = NULL, $error_message = NULL) {

    displayHead($title, $javascript);
    displayNavigation();

    if ($success_message != NULL || $error_message != NULL) {
        displayMessages($success_message, $error_message);
    }

    displayContent($content);
    displayFooter();
}

function displayHead($title = "", $javascript = "") {
    $searchValue = (isset($_POST['search']) && strlen($_POST['search']) > 0) ? $_POST['search'] : "Zip Code";
    echo <<<EOD
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>{$title}</title>
    <meta name="Keywords" content="" />
    <meta name="Description" content="" />
    <link href="/css/Template.css" rel="stylesheet" type="text/css" />
    <link href="/css/carousel.css" rel="stylesheet" type="text/css" />
    <link href="/css/screen.css" rel="stylesheet" type="text/css" media="screen" />
    <script src="/js/jquery-1.8.3.min.js"></script>
    {$javascript}
</head>
<body>

<!-- Google Analytics tracking code -->

<div id="wrapper">
    <div id="header">
        <form action='/index.php' method='post'>
            <div id="searchhint"><img src="/images/SearchHint.png" alt="Look for Bars, Cities or Zip Codes" /></div>
            <div id="search"><input type="text" value="{$searchValue}" size="20" name="search" alt="search" style="width: 160px; color: rgb(128, 128, 128); font-style: normal;" onfocus="if(this.value=='Zip Code')this.value=''"></div>
            <div id="searchbutton"><input type="image" src="/images/findbutton.png" alt="Submit" value="FIND!" style="font-family:Arial, Helvetica, sans-serif; font-weight:bold;" /></div>
            <div id="leftnavside"><img src="/images/LeftNavSide.png" /></div>
            <div id="rightnavside"><img src="/images/RightNavSide.png" /></div>
        </form>
    </div>
EOD;
}


function displayNavigation() {

    echo <<<EOD
<div id="menu">
    <ul>
        <li><img src="/images/LogoNav.png" width="163px" height="108px" usemap="#Map" style="margin-top:-26px; border:none; float:left;" />
            <map name="Map" id="Map"><area shape="rectangle" coords="150,30,0,70" href="/" alt="HOME" /></map></li>

EOD;

    if(isset($_SESSION['userid']) ) {
        echo "<li><a href='/admin/login/' title='Login'><div id='navspace'>OPTIONS</div></a></li>\n";
    }
    else {
        echo "<li><a href='/admin/login/login.php' title='Login'><div id='navspace'>LOGIN</div></a></li>\n";
    }

    echo <<<EOD
        <li><a href="/pages/info.php" title="Our Service"><div id="navspace">OUR SERVICE</div></a></li>
        <li><a href="/pages/order.php" title="Get A Cam"><div id="navspace">GET A CAM</div></a></li>
        <li><a href="/contact.php" title="Contact"><div id="navspace">CONTACT</div></a></li>
        <li><img src="/images/NavRepeater.png" style="background-repeat:repeat-x; border-left:1px solid #b6b6b6" height="47px" width="272px" /> </li>
    </ul>
</div>


EOD;
}

function displaySecondaryNavigation() {
    echo "<div id='SecondaryNav'></div>\n\n";
}

function displayContent($content) {
    echo "<div id='content'>\n";
    displaySecondaryNavigation();
    echo $content;
    echo "</div>\n\n";
}

function displayMessages($success_message = NULL, $error_message = NULL) {
    echo "<div id='messages'>\n";
    if (!is_null($success_message) && strlen($success_message) > 0) {
        displayMessage($success_message, SUCCESS_MESSAGE);
    }
    if (!is_null($error_message) && strlen($error_message) > 0) {
        displayMessage($error_message, ERROR_MESSAGE);
    }
    echo "</div>\n\n";
}

function displayMessage($message, $messageType) {
    echo "<div class='{$messageType}'>\n";
    echo "    <p>{$message}</p>\n";
    echo "</div>\n";
}

function displayFooter() {
    echo <<<EOD
        <div id="footer">
            <p>&copy; 2012 barspot.tv | All Rights Reserved.</p>
        </div>
    </div>
</body>
EOD;

}