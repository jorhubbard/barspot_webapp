<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jstevens
 * Date: 1/15/13
 * Time: 10:37 PM
 * To change this template use File | Settings | File Templates.
 */

require_once 'app_config.php';
require_once 'database_connection.php';

session_start();

function authorize_user($groups = NULL) {

    // no real reason to call the method but if nothing to authorize against just return
    if (is_null($groups) || empty($groups)) {
        return true;
    }

    // check for user id
    if (!isset($_SESSION['user_id']) || !strlen($_SESSION['user_id'] > 0)) {
        header("Location: " . SITE_ROOT . "pages/login.php?" .
                "error_msg=You must login to see this page.");
        exit();
    }

    $stmt = getConnection()->prepare(
        "SELECT u.userId" .
        " FROM user_tbl u, user_type_tbl ut" .
        " WHERE ut.userType = ?" .
        "  AND ut.userTypeId = u.userTypeId" .
        "  AND u.userId = " . mysql_real_escape_string($_SESSION['user_id'])
    );

    $group = "";
    if (!$stmt->bind_param("s", $group)) {
        handle_error("Something when wrong while trying to authorize you.", $stmt->error);
        exit();
    }

    foreach ($groups as $g) {
        global $group;
        $group = $g;
        if (!$stmt->execute()) {
            handle_error("Something went wrong while trying to authorize you.", $stmt->error);
            exit();
        }

        if ($stmt->num_rows > 0) {
            return true;
        }
    }

    handle_error("You are not authorized to view this page.");
    exit();
}