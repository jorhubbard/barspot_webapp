<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jstevens
 * Date: 1/15/13
 * Time: 9:47 PM
 * To change this template use File | Settings | File Templates.
 */

define("DEBUG_MODE", true);

define("SITE_ROOT", "/");
define("HOST_WWW_ROOT", "/Users/jstevens/Projects/barspot/webapp/barspot_webapp");

define("DB_HOST", "localhost");
define("DB_USERNAME", "hbs");
define("DB_PASSWORD", "1234hbs1234");
define("DB_NAME", "hbs_hbs");
define("DB_PORT", 3306);

define("GOOGLE_MAPS_KEY", "AIzaSyANffrVjVG3zaJKDPc_UJ2ojxEE7eUD17M");
define("DEFAULT_ZIP", 43201);

function debug_print($message) {
    if (DEBUG_MODE) {
        echo $message;
    }
}

function handle_error($user_error_message = "", $system_error_message = "") {
    session_start();
    $_SESSION['error_message'] = $user_error_message;
    $_SESSION['system_error_message'] = $system_error_message;
    header("Location: " . SITE_ROOT . "pages/error.php");
    exit();
}

function get_web_path($file_system_path) {
    return str_replace($_SERVER['DOCUMENT_ROOT'], '', $file_system_path);
}

