<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jstevens
 * Date: 1/15/13
 * Time: 10:07 PM
 * To change this template use File | Settings | File Templates.
 */

require_once 'app_config.php';

$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
if ($mysqli->connect_errno)
{
    handle_error("There was a problem connecting you to the database.", $mysqli->connect_error);
    exit();
}


function getConnection() {
    global $mysqli;
    return $mysqli;
}