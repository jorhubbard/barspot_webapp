<?
include '../../admin/connect.php';
include '../../admin/functions.php';

header("Content-type: text/xml");
print("<kml>");
print("\n\t<Document>");


$id = $_GET[id];
$demo = "/var/www/video/clients/$id/demo.m4v";
$streamStatus = 'None';

if(date("g") < 4) //5 is the number of hours of the timezone, this should be fixed when we support multiple time zones!!!!!!!!!
{
   $negative = true;
}
else
{
   $negative = false;
}


$getDaySchedule = getDaySchedule($id, $negative);
if(!empty($getDaySchedule) )
{

   $row = $getDaySchedule[0];
   $schedule = $getDaySchedule[1];
   $timezoneID = $getDaySchedule[2];
   //print("\n\t\t<schedule>");
   //print_r($getDaySchedule);
   //print("</schedule>");

   //if(isActiveCam($schedule) )
   if(has_current_feed($id,$number_of_seconds_before_feed_is_stale) )
   {
      $drawCamLabels = drawCamLabels($id);
      $camID = $drawCamLabels[0];
      $streamStatus = 'Streaming';
      if(!empty($camID))
      {

	 $isCurrentFeed = isCurrentFeed($id,$camID,$timezoneID,'mp4');
	 if(!empty($isCurrentFeed) )
	 {
	    print("\n\t\t<message>Current Feed: $isCurrentFeed</message>");
	    print("\n\t\t<video>".rtrim($isCurrentFeed)."</video>");
	 }
	 else
	 {
	    $streamStatus = 'Virtual Tour';
	    print("\n\t\t<message>Feed Not Current id: $id camID: $camID, timzoneid: $timezoneID </message>");
	    if(file_exists("/var/www/video/clients/$id/demo.m4v") )
	    {
	       print("\n\t\t<video>http://www.hotbarspot.com/video/clients/$id/demo.m4v</video>");
	    }
	    else
	    {
	       print("\n\t\t<video>http://www.hotbarspot.com/video/advideo.mp4</video>");
	    }
	 }
      }
      else
      {
	 $streamStatus = 'Virtual Tour';
	 print("\n\t\t<message>No Default Cam Found</message>");
	 if(file_exists("/var/www/video/clients/$id/demo.m4v") )
	 {
	    print("\n\t\t<video>http://www.hotbarspot.com/video/clients/$id/demo.m4v</video>");
	 }
	 else
	 {
	    print("\n\t\t<video>http://www.hotbarspot.com/video/advideo.mp4</video>");
	 }
      }
   }
   else
   {
      $streamStatus = 'Virutal Tour';
      print("\n\t\t<message>Cam not active Num seconds: $number_of_seconds_before_feed_is_stale</message>");
      if(file_exists("/var/www/video/clients/$id/demo.m4v") )
      {
	 print("\n\t\t<video>http://www.hotbarspot.com/video/clients/$id/demo.m4v</video>");
      }
      else
      {
	 print("\n\t\t<video>http://www.hotbarspot.com/video/advideo.mp4</video>");
      }
   }
}
else if(file_exists($demo) )
{
   $streamStatus = 'Virutal Tour';
   print("\n\t\t<message>Play Demo</message>");
   if(file_exists("/var/www/video/clients/$id/demo.m4v") )
   {
      print("\n\t\t<video>http://www.hotbarspot.com/video/clients/$id/demo.m4v</video>");
   }
   else
   {
      print("\n\t\t<video>http://www.hotbarspot.com/video/advideo.mp4</video>");
   }

}
else
{
   $streamStatus = 'None';
   print("\n\t\t<message>Bar not set up, playing ad video</message>");
   print("\n\t\t<video>http://www.hotbarspot.com/video/advideo.mp4</video>");
}


print("\n\t</Document>");
print("\n</kml>");

if(isset($id) && is_numeric($id) )
{
   $updateVisit = saveVisitorLog($streamStatus);
}


?>
