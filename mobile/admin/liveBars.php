<?
include '../../admin/connect.php';
include '../../admin/functions.php';
include 'functions.php';

$timestamp = time(void);
$timeFrame = $timestamp - $number_of_seconds_before_feed_is_stale;
header("Content-type: text/xml");


if(!empty($_GET[zip]) ) //assign zip directly
{
   $zip = $_GET[zip];
   $match = validateZip($zip);
   if($match == 0) //this is not a 5 digit numeric value
   {
      die("The zip you entered is not valid.");
   }
   $sql = "SELECT zipCodeLat, zipCodeLon FROM zipcode WHERE zipcode = '$zip' LIMIT 1;";
   $result = mysql_query($sql);
   if(mysql_affected_rows() == '1')
   {
      $row = mysql_fetch_array($result);
      $lat = $row[0];
      $lon = $row[1];
      $_GET[distance] = 10;
   }
   else
   {
      die("The zip you entered was not found.");
   }
}
else if(!empty($_GET[lat]) && !empty($_GET[lon]) ) //check lat and lon
{
   $lat = quote_smart($_GET[lat]);
   $lon = quote_smart($_GET[lon]);
   $order = 'distance';

}


//max allowable range for bars is 50 miles, min range is 1 miles
if(!empty($_GET[distance]) && ($_GET[distance] >= 1 && $_GET[distance] <=50) )
{
   $distance = quote_smart($_GET[distance]);
}
else
{
   $distance = 99999;
}

if($_GET[start] && is_numeric($_GET[start]) )
{
   $start = $_GET[start];
}
else
{
   $start = 0;
}

if($_GET[numResults] == -1)
{
   //return all results by setting to a really high number
   $numResults = '10000';
}
elseif($_GET[numResults] && is_numeric($_GET[numResults]) )
{
   $numResults = $_GET[numResults];
}
else
{
   $numResults = 250;
}

if(!empty($_GET[orderby]) && strtolower($_GET[orderby]) == 'name')
{
   $order = 'barName';
}
else
{
   $order = 'distance';
}

$kilometers = $distance * 1.609344;
//$rad = 8.047;  // radius of bounding circle in kilometers
$rad = $kilometers;  // radius of bounding circle in kilometers

$R = 6371;  // earth's radius, km

// first-cut bounding box (in degrees)
$maxLat = $lat + rad2deg($rad/$R);
$minLat = $lat - rad2deg($rad/$R);
// compensate for degrees longitude getting smaller with increasing latitude
$maxLon = $lon + rad2deg($rad/$R/cos(deg2rad($lat)));
$minLon = $lon - rad2deg($rad/$R/cos(deg2rad($lat)));

// convert origin of filter circle to radians
$radianlat = deg2rad($lat);
$radianlon = deg2rad($lon);



   $sql = "
   SELECT 
   barLat, 
   barLon, 
   barName, 
   CONCAT(barAddress, ' ',barZip) AS barAddress,
   barZip,
   bars.barID as barID,
   barPhone,
   barActive,
   if(bc.barID is not null,40,
	 (if(bvt.barID is not null,
	     30,
	     10))
     ) barLevel,
";
if(!empty($lat) && !empty($lon))
{
   $sql .="
   if(bc.barID is not null,40,
            (if(bvt.barID is not null,
	                 30,
			              10))
				           ) barLevel,
					      ROUND((3956 * (2 * ASIN(SQRT(
					         POWER(SIN((($lat - barLat) * 0.017453293)/2),2) +
						    COS($lat * 0.017453293) *
						       COS(barLat * 0.017453293) *
						          POWER(SIN((($lon - barLon)*0.017453293)/2),2))))),2) AS distance,

   ";
}
else
{
   $order = 'barName';
}
$sql .="
   poolTables,
   music,
   karaoke,
   darts,
   atm,
   smokingPatio,
   ub.barID AS owned

    FROM liveFeeds
    left join bars on bars.barID = liveFeeds.barID 
       
    LEFT JOIN barFeatures AS bf ON bars.barID = bf.barID

    #RIGHT JOIN user_bars_tbl AS ub ON bars.barID = ub.barID
    LEFT JOIN user_bars_tbl AS ub ON bars.barID = ub.barID
    LEFT JOIN barCameras bc on bars.barID = bc.barID
        LEFT JOIN barVirtualTours bvt on bars.barID = bvt.barID

    WHERE 

barActive = '1'
AND timestamp >= '$timeFrame'
AND ub.barID is not null 

AND barLat > $minLat 
AND barLat < $maxLat
And barLon > $minLon 
AND barLon < $maxLon
AND barActive = '1'
AND ub.barID is not null 
";
   $sql .="

    group by bars.barID 
    ";
if(!empty($lat) && !empty($lon))
{
   $sql .="
    HAVING distance <= '$distance'
   ";
}

$sql .= "
        ORDER BY $order ASC
	";

   $countResult = mysql_query($sql);
   $count = mysql_affected_rows();
   //print("lat = $lat<br />lon = $lon<br />sql = <pre>$sql</pre>");
   print drawKML($sql, $lat, $lon, $count);
?>
