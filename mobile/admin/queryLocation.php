<?
include '../../admin/connect.php';
include '../../admin/functions.php';
include 'functions.php';
//check authentication here
$result = authenticateUser($_GET[user],$_GET[pass]);
$result[0] = true;
header("Content-type: text/xml");

function validateZip($zip) {
   return preg_match('/^([0-9]{5})?$/i',$zip);
}

if($result[0] == 'true')
{
   if(!empty($_GET[zip]) ) //assign zip directly
   {
      $zip = $_GET[zip]; 
      $match = validateZip($zip);
      if($match == 0) //this is not a 5 digit numeric value
      {
	 die("The zip you entered is not valid.");
      }
      $sql = "SELECT zipCodeLat, zipCodeLon FROM zipcode WHERE zipcode = '$zip' LIMIT 1;"; 
      $result = mysql_query($sql);
      if(mysql_affected_rows() == '1')
      {
	 $row = mysql_fetch_array($result);
	 $lat = $row[0];
	 $lon = $row[1];
	 $_GET[distance] = 10;
      }
      else
      {
	 die("The zip you entered was not found.");
      }
   }
   else if(!empty($_GET[lat]) && !empty($_GET[lon]) ) //check lat and lon
   {
      $lat = quote_smart($_GET[lat]);
      $lon = quote_smart($_GET[lon]);

   }
   else if(!empty($_GET[jumpto]) ) //search section based on zip or city, st
   {
      //do search here for city state
      if(strripos($_GET[jumpto],','))
      {
	 $cityState = explode(',',urldecode($_GET[jumpto]) );
	 $sql = "
	 SELECT zipCodeLat, zipCodeLon FROM zipcode 
	 WHERE zipCodeCity like '".quote_smart(ltrim($cityState[0]) )."%'
	 AND zipCodeState like '".quote_smart(ltrim($cityState[1]) )."'
	 LIMIT 1;
	 ";

	 $result = mysql_query($sql);
	 
	 if(mysql_affected_rows() == '1')
	 {
	    $row = mysql_fetch_array($result);
	    $lat = $row[0];
	    $lon = $row[1];
	    $_GET[distance] = 10;
	 }
	 else
	 {
	    die("The zip/city,state you entered was not found.");
	 }
      }//end if separated by comma

	 
   }
   else //error that we do not have enough info
   {
      die("You must provide a lat/lon, search or zip code to retrieve the xml from here");
   }
   
   //max allowable range for bars is 50 miles, min range is 1 miles
   if(!empty($_GET[distance]) && ($_GET[distance] >= 1 && $_GET[distance] <=50) )
   {
      $distance = quote_smart($_GET[distance]);
   }
   else
   {
      $distance = 15;
   }

   if($_GET[start] && is_numeric($_GET[start]) )
   {
      $start = $_GET[start];
   }
   else
   {
      $start = 0;
   }
   
   if($_GET[numResults] == -1)
   {
      //return all results by setting to a really high number
      $numResults = '10000';
   }
   elseif($_GET[numResults] && is_numeric($_GET[numResults]) )
   {
      $numResults = $_GET[numResults];
   }
   else
   {
      $numResults = 250;
   }

   if(!empty($_GET[orderby]) && strtolower($_GET[orderby]) == 'name')
   {
      $order = 'barName';
   }
   else
   {
      $order = 'distance';
   }

   $kilometers = $distance * 1.609344;
   //$rad = 8.047;  // radius of bounding circle in kilometers
   $rad = $kilometers;  // radius of bounding circle in kilometers

   $R = 6371;  // earth's radius, km

   // first-cut bounding box (in degrees)
   $maxLat = $lat + rad2deg($rad/$R);
   $minLat = $lat - rad2deg($rad/$R);
   // compensate for degrees longitude getting smaller with increasing latitude
   $maxLon = $lon + rad2deg($rad/$R/cos(deg2rad($lat)));
   $minLon = $lon - rad2deg($rad/$R/cos(deg2rad($lat)));

   // convert origin of filter circle to radians
   $radianlat = deg2rad($lat);
   $radianlon = deg2rad($lon);


   //we have a zone now, so we need to process it and return the emil
   $sql = "
   SELECT 
   barLat, 
   barLon, 
   barName, 
   CONCAT(barAddress, ' ',barZip) AS barAddress,
   barZip,
   bars.barID as barID,
   barPhone,
   barActive,
   if(bc.barID is not null,40,
	 (if(bvt.barID is not null,
	     30,
	     10))
     ) barLevel,
   ROUND((3956 * (2 * ASIN(SQRT(
   POWER(SIN((($lat - barLat)*0.017453293)/2),2) +
   COS($lat * 0.017453293) *
   COS(barLat * 0.017453293) *
   POWER(SIN((($lon - barLon)*0.017453293)/2),2))))),2) AS distance,

   poolTables,
   music,
   karaoke,
   darts,
   atm,
   smokingPatio,
   ub.barID AS owned

    FROM bars 
       
    LEFT JOIN barFeatures AS bf ON bars.barID = bf.barID

    #RIGHT JOIN user_bars_tbl AS ub ON bars.barID = ub.barID
    LEFT JOIN user_bars_tbl AS ub ON bars.barID = ub.barID
    LEFT JOIN barCameras bc on bars.barID = bc.barID
        LEFT JOIN barVirtualTours bvt on bars.barID = bvt.barID

    WHERE #barZone = '$zone'

barLat > $minLat 
AND barLat < $maxLat
And barLon > $minLon 
AND barLon < $maxLon
AND barActive = '1'
AND ub.barID is not null 
    group by bars.barID HAVING distance <= '$distance'
    ORDER BY $order ASC
   ";

   $sql2 .= $sql."LIMIT $start, $numResults";
   $countResult = mysql_query($sql);
   $count = mysql_affected_rows();
   //print("lat = $lat<br />lon = $lon<br />sql = <pre>$sql</pre>");
   print drawKML($sql2, $lat, $lon, $count);
}
else
{
   print $result[1];
}
//end check authentication
?>
