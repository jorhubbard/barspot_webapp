<?
header("Content-type: text/xml");



?>
<kml>
<Document>
<Style id="style1">
<IconStyle>
<Icon>
<href>http://hotbarspot.com/images/hbsPin.gif</href>
</Icon>
</IconStyle>
</Style>

<Style id="style2">
<IconStyle>
<Icon>
<href>http://hotbarspot.com/images/grayPin.gif</href>
</Icon>
</IconStyle>
</Style>
<Style id="style3">

<IconStyle>
<Icon>
<href>http://hotbarspot.com/images/infoPin.gif</href>
</Icon>
</IconStyle>
</Style>
<jumpto>
<lat>39.969036</lat>

<lon>-83.011389</lon>
</jumpto>
<rowCount>6</rowCount>
<Placemark>
<name>Park Street Tavern</name>
<barFeatures>
<barFeatureID>249</barFeatureID>

<barFeatureID>248</barFeatureID>
<barFeatureID>270</barFeatureID>
<barFeatureID>247</barFeatureID>
<barFeatureID>18</barFeatureID>
<barFeatureID>269</barFeatureID>
<barFeatureID>243</barFeatureID>

<barFeatureID>16</barFeatureID>
<barFeatureID>19</barFeatureID>
<barFeatureID>245</barFeatureID>
</barFeatures>
<featuresList>&amp;lt;abbr title='Pool Tables'&amp;gt;&amp;lt;img align='right' style='padding-right:5px;' src='/images/pool.png' border='0' alt='Pool Tables' /&amp;gt;&amp;lt;/abbr&amp;gt;&amp;lt;abbr title='Live Music/DJ'&amp;gt;&amp;lt;img  align='right' style='padding-right:5px;' src='/images/music.png' border='0' alt='Live Music/DJ' /&amp;gt;&amp;lt;/abbr&amp;gt;&amp;lt;abbr title='ATM'&amp;gt;&amp;lt;img align='right' style='padding-right:5px;'  src='/images/atm.png' border='0' alt='ATM Available' /&amp;gt;&amp;lt;/abbr&amp;gt;&amp;lt;abbr title='Smoking Patio'&amp;gt;&amp;lt;img align='right' style='padding-right:5px;'  src='/images/patio.png' border='0' alt='Smoking Patio' /&amp;gt;&amp;lt;/abbr&amp;gt;</featuresList>

<barID>55421</barID>
<barLevel>40</barLevel>
<barLink>http://www.hotbarspot.com/review.php?id=55421</barLink>
<barAddress>501 Park Street 43215</barAddress>
<barPhone>614-221-4099</barPhone>
<description>

&lt;table&gt;&lt;tr&gt;&lt;td&gt;&lt;b&gt;&lt;big&gt;Park Street Tavern&lt;/big&gt;&lt;/b&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;501 Park Street 43215 &lt;br /&gt;614-221-4099&lt;br /&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;a href=&#039;review.php?id=55421&#039;&gt;&lt;font size=&#039;4&#039;&gt;&lt;strong&gt;View Bar&lt;/strong&gt;&lt;/font&gt;&lt;/a&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;

</description>
<barImage></barImage>
<listImage>http://hotbarspot.com/images/live.jpg</listImage>
<styleUrl>#style1</styleUrl>
<Point>
<coordinates>-83.0047454,39.9723676,0.000000</coordinates>
</Point>

<distance>0.42</distance>
<features>
<metaInfo>1</metaInfo>
<metaDescription>FEATURES:
Arcade Games
   ATM
Banquet Room(s)
   Credit Cards
   DJ / Live Music
   Free WIFI
   Megatouch
   Pool Table(s)
Shuffleboard Table(s)
   Smoking Patio


   SPECIALS:
Everyday: $3 Tube and New York Apple Shots!
Everyday: $3.50 24oz Bud and Bud Light 24oz Cans
Friday: $3 Bacardi Rum And Flavors
Saturday: $3 Bacardi Rum And Flavors
Weekdays: Happy Hour $2 domestics, $3 Well Drinks, $3.50 Premium Drafts


</metaDescription>
</features>    </Placemark>
<Placemark>
<name>Novak's on High</name>

<barFeatures>
<barFeatureID>247</barFeatureID>
<barFeatureID>243</barFeatureID>
<barFeatureID>16</barFeatureID>
<barFeatureID>245</barFeatureID>
</barFeatures>
<featuresList>&amp;lt;abbr title='Pool Tables'&amp;gt;&amp;lt;img align='right' style='padding-right:5px;' src='/images/pool.png' border='0' alt='Pool Tables' /&amp;gt;&amp;lt;/abbr&amp;gt;&amp;lt;abbr title='ATM'&amp;gt;&amp;lt;img align='right' style='padding-right:5px;'  src='/images/atm.png' border='0' alt='ATM Available' /&amp;gt;&amp;lt;/abbr&amp;gt;&amp;lt;abbr title='Smoking Patio'&amp;gt;&amp;lt;img align='right' style='padding-right:5px;'  src='/images/patio.png' border='0' alt='Smoking Patio' /&amp;gt;&amp;lt;/abbr&amp;gt;</featuresList>

<barID>55403</barID>
<barLevel>40</barLevel>
<barLink>http://www.hotbarspot.com/review.php?id=55403</barLink>
<barAddress>479 North High Street 43215</barAddress>
<barPhone>614-224-8821</barPhone>
<description>

&lt;table&gt;&lt;tr&gt;&lt;td&gt;&lt;b&gt;&lt;big&gt;Novak&#039;s on High&lt;/big&gt;&lt;/b&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;479 North High Street 43215 &lt;br /&gt;614-224-8821&lt;br /&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;a href=&#039;review.php?id=55403&#039;&gt;&lt;font size=&#039;4&#039;&gt;&lt;strong&gt;View Bar&lt;/strong&gt;&lt;/font&gt;&lt;/a&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;

</description>
<barImage>http://www.hotbarspot.com/images/uploaded/thumbs/303/186.JPG</barImage>
<listImage>http://hotbarspot.com/images/live.jpg</listImage>
<styleUrl>#style1</styleUrl>
<Point>
<coordinates>-83.0029310,39.9720850,0.000000</coordinates>
</Point>

<distance>0.49</distance>
<features>
<metaInfo>1</metaInfo>
<metaDescription>FEATURES:
Credit Cards
   Megatouch
Pool Table(s)
   Smoking Patio


   </metaDescription>
   </features>    </Placemark>
   <Placemark>
   <name>Bernard's Tavern</name>

   <barFeatures>
   <barFeatureID>247</barFeatureID>
   <barFeatureID>269</barFeatureID>
   <barFeatureID>478</barFeatureID>
   <barFeatureID>16</barFeatureID>
   <barFeatureID>19</barFeatureID>

   </barFeatures>
   <featuresList>&amp;lt;abbr title='Pool Tables'&amp;gt;&amp;lt;img align='right' style='padding-right:5px;' src='/images/pool.png' border='0' alt='Pool Tables' /&amp;gt;&amp;lt;/abbr&amp;gt;&amp;lt;abbr title='Karaoke'&amp;gt;&amp;lt;img align='right' style='padding-right:5px;'  src='/images/karaoke.png' border='0' alt='Karaoke' /&amp;gt;&amp;lt;/abbr&amp;gt;&amp;lt;abbr title='Smoking Patio'&amp;gt;&amp;lt;img align='right' style='padding-right:5px;'  src='/images/patio.png' border='0' alt='Smoking Patio' /&amp;gt;&amp;lt;/abbr&amp;gt;</featuresList>

   <barID>55428</barID>
   <barLevel>40</barLevel>
   <barLink>http://www.hotbarspot.com/review.php?id=55428</barLink>
   <barAddress>630 North High Street 43215</barAddress>
   <barPhone>(614) 223-9601</barPhone>
   <description>

   &lt;table&gt;&lt;tr&gt;&lt;td&gt;&lt;b&gt;&lt;big&gt;Bernard&#039;s Tavern&lt;/big&gt;&lt;/b&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;630 North High Street 43215 &lt;br /&gt;(614) 223-9601&lt;br /&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;a href=&#039;review.php?id=55428&#039;&gt;&lt;font size=&#039;4&#039;&gt;&lt;strong&gt;View Bar&lt;/strong&gt;&lt;/font&gt;&lt;/a&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;

   </description>
   <barImage></barImage>
   <listImage>http://hotbarspot.com/images/live.jpg</listImage>
   <styleUrl>#style1</styleUrl>
   <Point>
   <coordinates>-83.0027880,39.9753240,0.000000</coordinates>
   </Point>

   <distance>0.63</distance>
   <features>
   <metaInfo>1</metaInfo>
   <metaDescription>FEATURES:
   Credit Cards
   Free WIFI
   Karaoke (Wednesday)
   Pool Table(s)
Shuffleboard Table(s)


   </metaDescription>
   </features>    </Placemark>
   <Placemark>
   <name>Zeno's Victorian Village</name>

   <barFeatures>
   <barFeatureID>249</barFeatureID>
   <barFeatureID>248</barFeatureID>
   <barFeatureID>247</barFeatureID>
   <barFeatureID>17</barFeatureID>
   <barFeatureID>269</barFeatureID>

   <barFeatureID>478</barFeatureID>
   <barFeatureID>486</barFeatureID>
   <barFeatureID>243</barFeatureID>
   <barFeatureID>16</barFeatureID>
   </barFeatures>
   <featuresList>&amp;lt;abbr title='Pool Tables'&amp;gt;&amp;lt;img align='right' style='padding-right:5px;' src='/images/pool.png' border='0' alt='Pool Tables' /&amp;gt;&amp;lt;/abbr&amp;gt;&amp;lt;abbr title='Darts'&amp;gt;&amp;lt;img align='right' style='padding-right:5px;'  src='/images/darts.png' border='0' alt='Darts' /&amp;gt;&amp;lt;/abbr&amp;gt;&amp;lt;abbr title='ATM'&amp;gt;&amp;lt;img align='right' style='padding-right:5px;'  src='/images/atm.png' border='0' alt='ATM Available' /&amp;gt;&amp;lt;/abbr&amp;gt;</featuresList>

   <barID>306</barID>
   <barLevel>40</barLevel>
   <barLink>http://www.hotbarspot.com/review.php?id=306</barLink>
   <barAddress>384 W 3rd Ave Columbus, OH  43201</barAddress>
   <barPhone>(614) 294-5815</barPhone>
   <description>

   &lt;table&gt;&lt;tr&gt;&lt;td&gt;&lt;b&gt;&lt;big&gt;Zeno&#039;s Victorian Village&lt;/big&gt;&lt;/b&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;384 W 3rd Ave Columbus, OH  43201 &lt;br /&gt;(614) 294-5815&lt;br /&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;a href=&#039;review.php?id=306&#039;&gt;&lt;font size=&#039;4&#039;&gt;&lt;strong&gt;View Bar&lt;/strong&gt;&lt;/font&gt;&lt;/a&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;

   </description>
   <barImage>http://www.hotbarspot.com/images/uploaded/thumbs/219/119.JPG</barImage>
   <listImage>http://hotbarspot.com/images/live.jpg</listImage>
   <styleUrl>#style1</styleUrl>
   <Point>
   <coordinates>-83.015340,39.983924,0.000000</coordinates>
   </Point>

   <distance>1.05</distance>
   <features>
   <metaInfo>1</metaInfo>
   <metaDescription>FEATURES:
   Arcade Games
   ATM
   Credit Cards
Dart Board(s)
   Free WIFI
   Karaoke (Wednesday)
Live Music (Thursday)
   Megatouch
Pool Table(s)


   SPECIALS:
Everyday: Happy Hour 11am-7pm daily.  $1.75 well drinks and domestic bottles. Domestic Draft Beer- $1.50 12oz mug/$2.00 pint
Sunday: Dos Equis Mix and Match Bucket- 5 for $13
Monday: $2 Budweiser 16oz bottles
Tuesday: $2 Miller Lite 16oz bottles
Wednesday: $2 Coors Light 16oz bottles
Thursday: $2 Bud light 16oz bottles. Coors Light Bucket-6 for $9.50 from 11am-7pm
Friday: $2 Budweiser Select 16oz bottles. Miller Lite Bucket- 6 for $9.50 from 11am-7pm


</metaDescription>
</features>	 </Placemark>
<Placemark>
<name>Charlie Bear</name>

<barFeatures>
<barFeatureID>248</barFeatureID>
<barFeatureID>247</barFeatureID>
<barFeatureID>495</barFeatureID>
<barFeatureID>491</barFeatureID>
<barFeatureID>269</barFeatureID>

<barFeatureID>16</barFeatureID>
<barFeatureID>245</barFeatureID>
</barFeatures>
<featuresList>&amp;lt;abbr title='Pool Tables'&amp;gt;&amp;lt;img align='right' style='padding-right:5px;' src='/images/pool.png' border='0' alt='Pool Tables' /&amp;gt;&amp;lt;/abbr&amp;gt;&amp;lt;abbr title='ATM'&amp;gt;&amp;lt;img align='right' style='padding-right:5px;'  src='/images/atm.png' border='0' alt='ATM Available' /&amp;gt;&amp;lt;/abbr&amp;gt;&amp;lt;abbr title='Smoking Patio'&amp;gt;&amp;lt;img align='right' style='padding-right:5px;'  src='/images/patio.png' border='0' alt='Smoking Patio' /&amp;gt;&amp;lt;/abbr&amp;gt;</featuresList>

<barID>304</barID>
<barLevel>40</barLevel>
<barLink>http://www.hotbarspot.com/review.php?id=304</barLink>
<barAddress>1562 N High St Columbus, OH  43201</barAddress>
<barPhone>(614) 429-3341</barPhone>
<description>

&lt;table&gt;&lt;tr&gt;&lt;td&gt;&lt;b&gt;&lt;big&gt;Charlie Bear&lt;/big&gt;&lt;/b&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;1562 N High St Columbus, OH  43201 &lt;br /&gt;(614) 429-3341&lt;br /&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;a href=&#039;review.php?id=304&#039;&gt;&lt;font size=&#039;4&#039;&gt;&lt;strong&gt;View Bar&lt;/strong&gt;&lt;/font&gt;&lt;/a&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;

</description>
<barImage></barImage>
<listImage>http://hotbarspot.com/images/live.jpg</listImage>
<styleUrl>#style1</styleUrl>
<Point>
<coordinates>-83.00683736801147,39.99430091179494,0.000000</coordinates>
</Point>

<distance>1.76</distance>
<features>
<metaInfo>1</metaInfo>
<metaDescription>FEATURES:
ATM
   Credit Cards
   DJ (Saturday)
DJ (Tuesday)
   Free WIFI
Pool Table(s)
   Smoking Patio


   </metaDescription>
   </features>    </Placemark>
   <Placemark>
   <name>Buffalo Wild Wings</name>

   <barFeatures>
   <barFeatureID>249</barFeatureID>
   <barFeatureID>250</barFeatureID>
   <barFeatureID>247</barFeatureID>
   <barFeatureID>18</barFeatureID>
   <barFeatureID>269</barFeatureID>

   <barFeatureID>243</barFeatureID>
   <barFeatureID>252</barFeatureID>
   <barFeatureID>245</barFeatureID>
   </barFeatures>
   <featuresList>&amp;lt;abbr title='Live Music/DJ'&amp;gt;&amp;lt;img  align='right' style='padding-right:5px;' src='/images/music.png' border='0' alt='Live Music/DJ' /&amp;gt;&amp;lt;/abbr&amp;gt;&amp;lt;abbr title='Smoking Patio'&amp;gt;&amp;lt;img align='right' style='padding-right:5px;'  src='/images/patio.png' border='0' alt='Smoking Patio' /&amp;gt;&amp;lt;/abbr&amp;gt;</featuresList>

   <barID>55432</barID>
   <barLevel>40</barLevel>
   <barLink>http://www.hotbarspot.com/review.php?id=55432</barLink>
   <barAddress>1573 Marion Mount Gilead Rd 43302</barAddress>
   <barPhone>(740) 725-9464</barPhone>
   <description>

   &lt;table&gt;&lt;tr&gt;&lt;td&gt;&lt;b&gt;&lt;big&gt;Buffalo Wild Wings&lt;/big&gt;&lt;/b&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;1573 Marion Mount Gilead Rd 43302 &lt;br /&gt;(740) 725-9464&lt;br /&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;a href=&#039;review.php?id=55432&#039;&gt;&lt;font size=&#039;4&#039;&gt;&lt;strong&gt;View Bar&lt;/strong&gt;&lt;/font&gt;&lt;/a&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;

   </description>
   <barImage></barImage>
   <listImage>http://hotbarspot.com/images/live.jpg</listImage>
   <styleUrl>#style1</styleUrl>
   <Point>
   <coordinates>-83.0873160,40.5796110,0.000000</coordinates>
   </Point>

   <distance>42.35</distance>
   <features>
   <metaInfo>1</metaInfo>
   <metaDescription>FEATURES:
   Arcade Games
   Cornhole
   Credit Cards
   DJ / Live Music
   Free WIFI
   Megatouch
   NTN Trivia/Poker
   Smoking Patio


   </metaDescription>
   </features>    </Placemark>

   </Document>

   </kml>
