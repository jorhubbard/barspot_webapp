<?
include '../../admin/connect.php';
include 'functions.php';

header("Content-type: text/xml");
$string = "<kml>\n";
$string .= drawMasterFeatureList();
$string .="<barLevels>\n";
$string .="\t<levelID>10</levelID>\n";
$string .="\t<levelDescription>General Bar</levelDescription>\n";
$string .="\t<levelID>20</levelID>\n";
$string .="\t<levelDescription>User Submitted Information Available</levelDescription>\n";
$string .="\t<levelID>30</levelID>\n";
$string .="\t<levelDescription>Virtual Tour Customer</levelDescription>\n";
$string .="\t<levelID>40</levelID>\n";
$string .="\t<levelDescription>Live Feed Customer</levelDescription>\n";
$string .="</barLevels>\n";
$string .= drawFeaturedCities();
$string .= "</kml>\n";


print $string;



?>
