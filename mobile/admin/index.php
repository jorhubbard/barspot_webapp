<?
include 'functions.php';
include '../../admin/functions.php';
include '../../admin/connect.php';
$result = authenticateUser($_GET['user'],$_GET['pass']);
//$result[0] = 'true'; //forces true
//$result[1] = '1';

if($result[0] == 'true')
{
   header("Content-type: text/xml");


   $sql = "
      SELECT 
      barLat,
      barLon,
      barName,
      CONCAT(barAddress, ' ',barZip) AS barAddress,
      barZip,
      bars.barID as barID,
      barPhone,
      barActive,
      if(bc.barID is not null,40,
      	    (if(bvt.barID is not null,
      		30,
      		10))
      	 ) barLevel,
      ub.userId AS owned,
      f1.groupId AS poolTables,
      f2.groupId AS music,
      f3.groupId AS karaoke,
      f4.groupId as darts,
      f5.groupId as atm,
      f6.groupId as smokingPatio
      FROM bars
      LEFT JOIN userSubscribe ON userSubscribeBarID = bars.barID
      LEFT JOIN user_bars_tbl AS ub ON ub.barID = bars.barID
      LEFT JOIN bar_group_id as f1 ON bars.barID = f1.barID AND f1.groupId = 16
      LEFT JOIN bar_group_id as f2 ON bars.barID = f2.barID AND f2.groupId = 18
      LEFT JOIN bar_group_id as f3 ON bars.barID = f3.barID AND f3.groupId = 20
      LEFT JOIN bar_group_id as f4 ON bars.barID = f4.barID AND f4.groupId = 17
      LEFT JOIN bar_group_id as f5 ON bars.barID = f5.barID AND f5.groupId = 248
      LEFT JOIN bar_group_id as f6 ON bars.barID = f6.barID AND f6.groupId = 245
      LEFT JOIN barCameras bc on bars.barID = bc.barID
      LEFT JOIN barVirtualTours bvt on bars.barID = bvt.barID


      WHERE userSubscribe.userID = '$result[1]'
      #WHERE userSubscribe.userID = '1'

      ORDER BY barName ASC
      ";
//print ("sql = $sql");

//$barImage = array(1=>'http://hotbarspot.com/images/hbsPin.gif','http://hotbarspot.com/images/grayPin.gif','http://hotbarspot.com/images/infoPin.gif');
   print drawKML($sql);
}
else
{
   print $result[1];

}


?>
