<?
include 'functions.php';
include '../../admin/connect.php';
$result = authenticateUser($_GET['user'],$_GET['pass']);
//$result[0] = true; //forces validation of user to be correct
function xmlSpecials($barID)
{
   $sql = "SELECT specialDay, special FROM specials WHERE barID = '".quote_smart($barID)."';";
   $day = array('All','Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Weekends');
   
   $result = mysql_query($sql)or die("Invalid query: " . mysql_error());
   if($row = mysql_fetch_array($result))
   {
      $string .="\n\t\t\t<specials>";
      do
      {
	 $dayVal = $row[specialDay];
	 $string .="\n\t\t\t\t<special>";
	 $string .="\n\t\t\t\t\t<day>".htmlentities($day[$dayVal])."</day>";
	 $string .="\n\t\t\t\t\t<value>".htmlentities($row[special])."</value>";
	 $string .="\n\t\t\t\t</special>";
      }while($row = mysql_fetch_array($result));
      $string .="\n\t\t\t</specials>";

   }
   else
   {
      $string .="\n\t\t\t<specials>";
      $string .="\n\t\t\t\t<special>";
      $string .="\n\t\t\t\t\t<day>Specials</day>";
      $string .="\n\t\t\t\t\t<value>No Specials Listed</value>";
      $string .="\n\t\t\t\t</special>";
      $string .="\n\t\t\t</specials>";

   }
   return $string;
}
function xmlMenu($barID)
{
   $sql = "SELECT * FROM menuCategory LEFT JOIN menuItem ON menuCategory.menuCategoryID = menuItem.menuCategoryID WHERE barID = '".quote_smart($barID)."' ORDER BY menuCategoryName ASC";
   $result = mysql_query($sql) or die("mysql error: ".mysql_error() );
   $category = "";

   $string .="\n\t\t\t<menu>";
   while($row = mysql_fetch_array($result)) {
      if ( $row['menuCategoryName'] != $category ) {
	 $category = $row['menuCategoryName'];
	 $i = 0;
	 $string .="\n\t\t\t\t<menuCategory>".htmlentities($row[menuCategoryName])."</menuCategory>";
      }
      // list item inside category
      $i++;
      $string .="\n\t\t\t\t<menuItem>";
      $string .="\n\t\t\t\t\t<name>".htmlentities($row[menuItemName])."</name>";
      $string .="\n\t\t\t\t\t<desc>".htmlentities($row[menuItemDesc])."</desc>";
      if($row[menuItemPrice] == '0.00')
      {
	 $price = 'Visit for pricing';
      }
      else
      {
	 $price = $row[menuItemPrice];
      }
      $string .="\n\t\t\t\t\t<price>".htmlentities($price)."</price>";
      $string .="\n\t\t\t\t</menuItem>";
   }
   $string .="\n\t\t\t</menu>";
   return $string;
}
function xmlPictures($barID)
{
   //$barID = '55343'; //set static for testing purposes
   $sql = "
   SELECT galleryUserID, id, ext FROM `galleryCategories` AS g
   LEFT JOIN images AS i ON g.galleryCategoryID = i.galleryCategoryID
   WHERE galleryBarID = '$barID';
   ";
   $result = mysql_query($sql);

   if($row = mysql_fetch_array($result) )
   {
      //pictures go here
      do{
	 $picture    .="\n\t\t\t\t\t<picture>http://hotbarspot.com/images/uploaded/thumbs/$row[galleryUserID]/$row[id]$row[ext]</picture>";
	 $thumbnail  .="\n\t\t\t\t\t<thumbnail>http://hotbarspot.com/images/uploaded/thumbs/$row[galleryUserID]/$row[id]$row[ext]</thumbnail>";

      }while($row = mysql_fetch_array($result));
      $string .="\n\t\t\t<pix>";
      $string .= "\n\t\t\t\t<pictures>";
      $string .= $picture;
      $string .= "\n\t\t\t\t</pictures>";
      $string .= "\n\t\t\t\t<thumbnails>";
      $string .= $thumbnail;
      $string .= "\n\t\t\t\t</thumbnails>";
      $string .= "\n\t\t\t</pix>";
   }

   return $string;
}
function getVideo($id)
{
   $sql = "SELECT * from barCameras WHERE barID = '$id' ORDER BY barCameraPrimary DESC LIMIT 1;";
   $result = mysql_query($sql);
   $row = mysql_fetch_array($result);
   $ls = shell_exec("ls -t /var/www/video/clients/$id/$row[barCameraID]/ | grep .mp4 | head -1");
   $lines = explode("\n",$ls);
   
   $string = "http://www.hotbarspot.com/video/clients/$id/$row[barCameraID]/$lines[0]";
   //$string = "http://www.hotbarspot.com/video/clients/$id/demo.m4v";
   return $string;
}
function getFeatures($id)
{
   $sql = "
   SELECT 
   groupName
   FROM `bar_group_id` as b 
   LEFT JOIN group_tbl as g ON b.groupID = g.groupId
   WHERE b.barID = '".quote_smart($id)."'
   ORDER BY groupName ASC;";

   $result = mysql_query($sql);
   if($row = mysql_fetch_array($result) )
   {
      $string .="FEATURES:\n";
      do
      {
	 $string .="$row[groupName]\n";
      }while($row = mysql_fetch_array($result) );
      $string .="\n\n";
   }

   return $string;
}
//forces true auth for testing purposes
//$result[0] = 'true';
if($result[0] == 'true')
{
   header("Content-type: text/xml");

   $sql = "
      SELECT barID, barName, barAddress, barZip, barPhone, barWebsite, barLat, barLon
      FROM bars WHERE barID ='".quote_smart($_GET[barID])."';
      ";

   $result = mysql_query($sql);

   if($row = mysql_fetch_array($result))
   {
      print("<kml>");
      print("\n\t<Document>");
      print("\n\t\t<bar>");
      print("\n\t\t\t<barID>$row[barID]</barID>");
      //print("\n\t\t\t<barImage>http://hotbarspot.com/images/red.jpg</barImage>\n");
      print("\n\t\t\t<barName>".htmlentities($row[barName])."</barName>");
      print("\n\t\t\t<barAddress>".htmlentities($row[barAddress])."</barAddress>");
      print("\n\t\t\t<barZip>".htmlentities($row[barZip])."</barZip>");
      print("\n\t\t\t<barPhone>".htmlentities($row[barPhone])."</barPhone>");
      print("\n\t\t\t<barWebsite>".htmlentities($row[barWebsite])."</barWebsite>");
      print("\n\t\t\t<barLat>".htmlentities($row[barLat])."</barLat>");
      print("\n\t\t\t<barLon>".htmlentities($row[barLon])."</barLon>");
      print("\n\t\t\t<barFeed>".htmlentities($row[barFeed])."</barFeed>");
      print xmlPictures($_GET[barID]);
      //print("\n\t\t\t<video>http://hotbarspot.com/video/clients/highlights/out.mp4</video>");
      print("\n\t\t\t<video>".getVideo($_GET[barID])."</video>");
      print xmlSpecials($_GET[barID]);
      print xmlMenu($_GET[barID]);
      print("\n\t\t\t<features>");
      print("\n\t\t\t\t<metaInfo>1</metaInfo>");
      print("\n\t\t\t\t<metaDescription>".getFeatures($_GET[barID])."</metaDescription>");
      print("\n\t\t\t</features>");
      print("\n\t\t</bar>");
      print("\n\t</Document>");
      print("\n</kml>");
   }
   else
   {
      print("bar not found");
   }

}
else
{
   print $result[1];

}


?>
