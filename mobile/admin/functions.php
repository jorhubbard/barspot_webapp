<?
function getAuthInfo($user)
{
   if (strripos($user,'@')) //have @ sign, ussimg email address
   {
      $sql = "SELECT userId, username, userpassword FROM user_tbl WHERE userEmail = '".quote_smart($user)."' LIMIT 1;";
   }
   else //no @ sign, assuming username
   {
      $sql = "SELECT userId, username, userpassword FROM user_tbl WHERE username = '".quote_smart($user)."' LIMIT 1;";
   }
   $result = mysql_query($sql);
   $row = mysql_fetch_array($result);
   $passpieces = explode('$',$row[userpassword]);
   //$salt = $passpieces[0].$passpieces[1];
   $salt = substr($row[userpassword],0,11);
   
   return $salt;

}
function authenticateUser($user,$pass)
{
   
   if (strripos($user,'@')) //have @ sign, ussimg email address
   {
      $sql = "SELECT userId, username, userpassword FROM user_tbl WHERE userEmail = '".quote_smart($user)."' LIMIT 1;";
   }
   else //no @ sign, assuming username
   {
      $sql = "SELECT userId, username, userpassword FROM user_tbl WHERE username = '".quote_smart($user)."' LIMIT 1;";
   }
   //print ("sql = $sql<br/>");
   $result = mysql_query($sql)or die("Invalid myqsl: " . mysql_error());

   if(mysql_affected_rows() == 1) //found record
   {
      $row = mysql_fetch_array($result);
      //$pass = crypt(crypt($pass,$row[userpassword]),$_SERVER['REMOTE_ADDR']); //turn plain text pass into hash against ip (authenticate plaintext for testing 
      
      $passpiece = explode('$',$row[userpassword]);
      if(crypt($passpiece[3],crypt($_SERVER['REMOTE_ADDR'],'cj') ) == $pass)
      {
	 return array('true',$row[userId]); //true
      }
      else
      {
	 //print crypt($row[userpassword],$_SERVER['REMOTE_ADDR']);
	 //print("<br />$pass<br />");
	 $error = array('false','The password you provided did not match what is on file for the user specified.');
	 return $error;
      }
      
   }
   else //no record found, invalid username
   {
      $error = array('false','The username provided was not found.');
      return $error;
   }

}


function getFeatures($id)
{
   $sql = "
      SELECT 
      groupName
      FROM `bar_group_id` as b 
      LEFT JOIN group_tbl as g ON b.groupID = g.groupId
      WHERE b.barID = '".quote_smart($id)."'
      ORDER BY groupName ASC;";

   $result = mysql_query($sql);
   if($row = mysql_fetch_array($result) )
   {
      $string .="FEATURES:\n";
      do
      {
	 $string .="$row[groupName]\n";
      }while($row = mysql_fetch_array($result) );
      $string .="\n\n";
   }
   if(empty($string) )
   {
      $features = getUserFeatures($id);
      if(!empty($features))
      {
	 $featureList = implode(',',$features);
	 $sql = "SELECT groupName FROM group_tbl WHERE groupID IN ($featureList);";
	 //print("sql = $sql");
	 $result = mysql_query($sql);
	 if($row = mysql_fetch_array($result) )
	 {
	    $string .="FEATURES:\n";
	    do
	    {
	       $string .="$row[groupName]\n";
	    }while($row = mysql_fetch_array($result) );
	    $string .="\n\n";
	 }
      }
   }
	 
   return $string;
}
function getSpecials($id)
{
   $days = array(0=>'Everyday',1=>'Sunday',2=>'Monday',3=>'Tuesday',4=>'Wednesday',5=>'Thursday',6=>'Friday',7=>'Saturday',8=>'Weekdays');
   $sql = "SELECT specialDay, special FROM specials WHERE barID = '".quote_smart($id)."' ORDER BY specialDay ASC";
   $result = mysql_query($sql);
   if($row=mysql_fetch_array($result) )
   {
      $string = "SPECIALS:\n";
      do
      {	 
	 $dayIndex = $row[specialDay];
	 $string .="$days[$dayIndex]: $row[special]\n";
	 $string = str_replace('&','and', $string);
      }while($row = mysql_fetch_array($result) );
      $string .="\n\n";
   }
   return $string;
}
function getUserFeatures($bid)
{
   $sql = "
      SELECT  featureID,
	      (count(featureID) / (
				   SELECT
				   count(distinct(reviewID) )
				   FROM barUserFeatures
				   LEFT JOIN barReviews ON reviewID = barReviewID
				   WHERE barID = '$bid'
				   group by barID) 
	      ) * 100 AS percentage
		 FROM `barUserFeatures` 
		 LEFT JOIN barReviews ON reviewID = barReviewID
		 WHERE barID = '$bid'
		 group by featureID;
   ";

   $result = mysql_query($sql);

   if($row = mysql_fetch_array($result) )
   {
      do
      {
	 if($row[percentage] >= '70')
	 {
	    $feature[] = $row[featureID];
	 }
      }while($row = mysql_fetch_array($result) );
   }
   else
   {
      $feature[] = '0';
   }
   return $feature;

}
function drawMasterFeatureList()
{
   $sql = "SELECT groupId, groupName
   FROM `group_tbl`
   WHERE groupTypeId = 5;";
   $result = mysql_query($sql);

   if($row = mysql_fetch_array($result) )
   {
      $featureList = "\t\t<masterFeatureList>\n";
      do{
	 $featureList .= "\t\t\t<featureItem>\n";
	 $featureList .= "\t\t\t\t<featureID>".$row['groupId']."</featureID>\n";
	 $featureList .= "\t\t\t\t<featureName>".$row['groupName']."</featureName>\n";
	 $featureList .= "\t\t\t</featureItem>\n";
      }while($row = mysql_fetch_array($result)) ;
      $featureList .= "\t\t</masterFeatureList>\n";

   }

   return $featureList;
}
function drawBarFeatureList($barID)
{
   $sql = "select groupID from bar_group_id where barID = '$barID';";
   $result = mysql_query($sql);
   if($row = mysql_fetch_array($result) )
   {
      $featureList = "\t\t\t<barFeatures>\n"; //initialize featureList
      do {
	 $featureList .="\t\t\t\t<barFeatureID>".$row['groupID']."</barFeatureID>\n";
      }while($row = mysql_fetch_array($result) );
      $featureList .= "\t\t\t</barFeatures>\n";
   }

   return $featureList;
}
function drawFeaturedCities()
{
   $sql = "select * from streamingCities order by streamingCityName;";
   $result = mysql_query($sql);

   if($row = mysql_fetch_array($result) )
   {
      $featuredCities.="<featuredCities>\n";
      do {
	 $featuredCities .="\t<city>\n";
	 $featuredCities .="\t\t<cityID>".$row['streamingCityID']."</cityID>\n";
	 $featuredCities .="\t\t<name>".$row['streamingCityName']."</name>\n";
	 $featuredCities .="\t\t<state>".$row['streamingCityState']."</state>\n";
	 $featuredCities .="\t\t<lat>".$row['streamingCityLat']."</lat>\n";
	 $featuredCities .="\t\t<lon>".$row['streamingCityLon']."</lon>\n";
	 if($row['streamingCityName'] == 'Columbus')
	 {
	    $default = '1';
	 }
	 else
	 {
	    $default = '0';
	 }
	 $featuredCities .="\t\t<default>$default</default>\n";
	 $featuredCities .="\t</city>\n";
      }while($row = mysql_fetch_array($result) );
      $featuredCities.="</featuredCities>\n";

   }

   return $featuredCities;

}
function drawKML($sql,$lat=false,$lon=false,$count=false)
{
   $listImage = array(30=>'http://hotbarspot.com/images/red.jpg',10=>'http://hotbarspot.com/images/gray.jpg',20=>'http://hotbarspot.com/images/black.jpg',40=>'http://hotbarspot.com/images/live.jpg');
   $result = mysql_query($sql)or die("Invalid query: " . mysql_error());

   if($row = mysql_fetch_array($result))
   {
      $string  = "<kml>\n";
      $string .= "\t<Document>\n";
      //$string .= drawMasterFeatureList();
      //$string .= "\t\t<name>HBS Bars</name>\n";
      //$string .= "\t\t<description></description>\n";
      $string .= "\t\t<Style id=\"style1\">\n";
      $string .= "\t\t\t<IconStyle>\n";
      $string .= "\t\t\t\t<Icon>\n";
      $string .= "\t\t\t\t\t<href>http://hotbarspot.com/images/hbsPin.gif</href>\n";
      $string .= "\t\t\t\t</Icon>\n";
      $string .= "\t\t\t</IconStyle>\n";
      $string .= "\t\t</Style>\n";
      $string .= "\t\t<Style id=\"style2\">\n";
      $string .= "\t\t\t<IconStyle>\n";
      $string .= "\t\t\t\t<Icon>\n";
      $string .= "\t\t\t\t\t<href>http://hotbarspot.com/images/grayPin.gif</href>\n";
      $string .= "\t\t\t\t</Icon>\n";
      $string .= "\t\t\t</IconStyle>\n";
      $string .= "\t\t</Style>\n";
      $string .= "\t\t<Style id=\"style3\">\n";
      $string .= "\t\t\t<IconStyle>\n";
      $string .= "\t\t\t\t<Icon>\n";
      $string .= "\t\t\t\t\t<href>http://hotbarspot.com/images/infoPin.gif</href>\n";
      $string .= "\t\t\t\t</Icon>\n";
      $string .= "\t\t\t</IconStyle>\n";
      $string .= "\t\t</Style>\n";
      $string .= "\t\t<jumpto>\n";
      $string .= "\t\t\t<lat>$lat</lat>\n";
      $string .= "\t\t\t<lon>$lon</lon>\n";
      $string .= "\t\t</jumpto>\n";
      $string .= "\t\t<rowCount>$count</rowCount>\n"; 

      do{
	    $features = ''; //clears the variable
	    $marker =''; //clears the variable
	    $barLevel = $row['barLevel'];
	    
	    $feature = getUserFeatures($row[barID]); //pulls user contributed variables.
	    if(!empty($row[poolTables]) || (is_array($feature) && in_array('16',$feature) ) )
	    {
	       $features .= "<abbr title='Pool Tables'><img align='right' style='padding-right:5px;' src='/images/pool.png' border='0' alt='Pool Tables' /></abbr>";
	       $marker = '3';
	    }
	    if(!empty($row[music]) || (is_array($feature) && in_array('18',$feature) ) )
	    {
	       $features .= "<abbr title='Live Music/DJ'><img  align='right' style='padding-right:5px;' src='/images/music.png' border='0' alt='Live Music/DJ' /></abbr>";
	       $marker = '3';
	    }
	    if(!empty($row[karaoke]) || (is_array($feature) && in_array('20',$feature) ) )
	    {
	       $features .= "<abbr title='Karaoke'><img align='right' style='padding-right:5px;'  src='/images/karaoke.png' border='0' alt='Karaoke' /></abbr>";
	       $marker = '3';
	    }
	    if(!empty($row[darts]) || (is_array($feature) && in_array('17',$feature) ) )
	    {
	       $features .= "<abbr title='Darts'><img align='right' style='padding-right:5px;'  src='/images/darts.png' border='0' alt='Darts' /></abbr>";
	       $marker = '3';
	    }
	    if(!empty($row[atm]) || (is_array($feature) && in_array('248',$feature) ) )
	    {
	       $features .= "<abbr title='ATM'><img align='right' style='padding-right:5px;'  src='/images/atm.png' border='0' alt='ATM Available' /></abbr>";
	       $marker = '3';
	    }
	    if(!empty($row[smokingPatio]) || (is_array($feature) && in_array('245',$feature) ) )
	    {
	       $features .= "<abbr title='Smoking Patio'><img align='right' style='padding-right:5px;'  src='/images/patio.png' border='0' alt='Smoking Patio' /></abbr>";
	       $marker = '3';
	    }

	    if(!empty($row[owned]) )
	    {
	       $marker = '1';
	    }
	    else if($marker == '3')
	    {
	       //do nothing
	    }
	    else
	    {
	       $marker = '2';
	    }
	    $string .="\t\t<Placemark>\n";
	    $string .="\t\t\t<name>".htmlspecialchars(htmlentities($row[barName]) )."</name>\n";
	    $string .= drawBarFeatureList($row['barID']);
	    $string .="\t\t\t<featuresList>".htmlspecialchars(htmlentities($features) )."</featuresList>\n";
	    $string .="\t\t\t<barID>$row[barID]</barID>\n";
	    $string .="\t\t\t<barLevel>$barLevel</barLevel>\n";
	    $string .="\t\t\t<barLink>http://www.hotbarspot.com/review.php?id=$row[barID]</barLink>\n";
	    $string .="\t\t\t<barAddress>$row[barAddress]</barAddress>\n";
	    $string .="\t\t\t<barPhone>$row[barPhone]</barPhone>\n";
	    $string .="\t\t\t<description>\n";
	    $string .="\t\t\t\t".htmlspecialchars("<table><tr><td><b><big>".htmlentities($row[barName])."</big></b></td></tr><tr><td>$row[barAddress] <br />$row[barPhone]<br /></td></tr><tr><td><a href='review.php?id=$row[barID]'><font size='4'><strong>View Bar</strong></font></a></td></tr></table>\n",ENT_QUOTES);
	    $string .="\t\t\t</description>\n";
	    $string .="\t\t\t<barImage>".getBarImage($row[barID])."</barImage>\n";
	    $string .="\t\t\t<listImage>$listImage[$barLevel]</listImage>\n";
	    $string .="\t\t\t<styleUrl>#style$marker</styleUrl>\n";
	    $string .="\t\t\t<Point>\n";
	    $string .="\t\t\t\t<coordinates>$row[barLon],$row[barLat],0.000000</coordinates>\n";
	    $string .="\t\t\t</Point>\n";
	    $string .="\t\t\t<distance>$row[distance]</distance>\n"; //returned in miles
	    $string .="\t\t\t<features>\n";
	    $string .="\t\t\t\t<metaInfo>1</metaInfo>\n";
	    $metaDesc = getFeatures($row[barID]).getSpecials($row[barID]);
	    if(empty($metaDesc) )
	    {
	       $metaDesc = "We currently do not have any information about this bar.  Please help us update our database with your favorite bars information by logging in with your user account at www.hotbarspot.com and reviewing your favorite spots.";
	    }
	    $string .="\t\t\t\t<metaDescription>$metaDesc</metaDescription>\n";
	    $string .="\t\t\t</features>";
	    $string .="\t\t</Placemark>\n";

      }while($row = mysql_fetch_array($result));
      $string .="\n\t</Document>\n</kml>";
   }
   else
   {
      $string = "<kml>\n\t<error>No results were found</error>\n</kml>";
   }


   return $string;

}

?>
