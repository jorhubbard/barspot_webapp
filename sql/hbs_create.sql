SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hbs_hbs`
--

-- --------------------------------------------------------

--
-- Table structure for table `barCameras`
--

CREATE TABLE IF NOT EXISTS `barCameras` (
  `barCameraID` int(11) NOT NULL AUTO_INCREMENT,
  `barCameraMac` varchar(20) NOT NULL,
  `barID` int(11) NOT NULL,
  `barCameraDescription` varchar(50) NOT NULL,
  `barCameraPrimary` set('0','1') NOT NULL DEFAULT '0' COMMENT 'denotes camera to display by default',
  `barCameraActive` set('0','1') NOT NULL DEFAULT '1' COMMENT 'this is administratively active',
  `barCameraLive` set('0','1') NOT NULL DEFAULT '0' COMMENT 'this is if bar wants camera enabled or not',
  `barCameraDateAdded` date NOT NULL,
  PRIMARY KEY (`barCameraID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2;


-- --------------------------------------------------------

--
-- Stand-in structure for view `barfeatures`
--
CREATE TABLE IF NOT EXISTS `barFeatures` (
`barID` int(11)
,`poolTables` int(11)
,`music` int(11)
,`karaoke` int(11)
,`darts` int(11)
,`atm` int(11)
,`smokingPatio` int(11)
);

-- --------------------------------------------------------

--
-- Table structure for table `barlogs`
--

CREATE TABLE IF NOT EXISTS `barLogs` (
  `barLogID` int(11) NOT NULL AUTO_INCREMENT,
  `visitingUserID` int(11) NOT NULL,
  `barLogBarID` int(11) NOT NULL,
  `barLogDate` datetime NOT NULL,
  `barLogViewingStatus` enum('Streaming','Virtual Tour','None') COLLATE utf8_unicode_ci DEFAULT NULL,
  `barLogViewingType` enum('Android','iPad','iPhone','Blackberry','Website') COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`barLogID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `barreviews`
--

CREATE TABLE IF NOT EXISTS `barReviews` (
  `barReviewID` int(11) NOT NULL AUTO_INCREMENT,
  `barID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `reviewOverall` int(11) NOT NULL,
  `reviewDrinkValue` int(11) NOT NULL,
  `reviewCrowd` int(11) NOT NULL,
  `reviewAge` int(11) NOT NULL,
  `reviewGenderRatio` int(11) NOT NULL,
  `reviewEntertainment` int(11) NOT NULL,
  `reviewWriteup` text NOT NULL,
  `reviewTimestamp` int(11) NOT NULL,
  PRIMARY KEY (`barReviewID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bars`
--

CREATE TABLE IF NOT EXISTS `bars` (
  `barID` int(11) NOT NULL AUTO_INCREMENT,
  `barName` varchar(150) NOT NULL,
  `barAddress` varchar(100) NOT NULL,
  `barZip` varchar(5) NOT NULL,
  `barPhone` varchar(15) DEFAULT NULL,
  `barWebsite` varchar(200) DEFAULT NULL,
  `barIndexTimestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `barLat` varchar(20) NOT NULL,
  `barLon` varchar(20) NOT NULL,
  `barActive` set('0','1','2') NOT NULL DEFAULT '2',
  `barDoNotCall` int(1) NOT NULL DEFAULT '0',
  `barDescription` varchar(500) NOT NULL,
  `barZone` int(11) DEFAULT NULL,
  PRIMARY KEY (`barID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5;

-- --------------------------------------------------------

--
-- Table structure for table `baruserfeatures`
--

CREATE TABLE IF NOT EXISTS `barUserFeatures` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `reviewID` int(11) NOT NULL,
  `featureID` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `barvirtualtours`
--

CREATE TABLE IF NOT EXISTS `barVirtualTours` (
  `barVirtualTourID` int(11) NOT NULL AUTO_INCREMENT,
  `barID` int(11) NOT NULL,
  PRIMARY KEY (`barVirtualTourID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2;


-- --------------------------------------------------------

--
-- Table structure for table `bar_group_id`
--

CREATE TABLE IF NOT EXISTS `bar_group_id` (
  `barGroupID` int(11) NOT NULL AUTO_INCREMENT,
  `barID` int(11) NOT NULL,
  `groupID` int(11) NOT NULL,
  PRIMARY KEY (`barGroupID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11;

-- --------------------------------------------------------

--
-- Table structure for table `calendar_events`
--

CREATE TABLE IF NOT EXISTS `calendar_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_date` date NOT NULL,
  `event_time` varchar(25) NOT NULL,
  `event_title` varchar(30) NOT NULL,
  `event_text` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `bar_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fromWhere_tbl`
--

CREATE TABLE IF NOT EXISTS `fromWhere_tbl` (
  `fromWhereID` int(11) NOT NULL AUTO_INCREMENT,
  `fromWhereName` varchar(50) NOT NULL,
  `fromWhereOrder` int(11) NOT NULL,
  PRIMARY KEY (`fromWhereID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- Table structure for table `gallerycategories`
--

CREATE TABLE IF NOT EXISTS `galleryCategories` (
  `galleryCategoryID` int(11) NOT NULL AUTO_INCREMENT,
  `galleryUserID` int(11) NOT NULL,
  `galleryCategoryName` varchar(25) NOT NULL DEFAULT '',
  `galleryCategoryProfileImage` varchar(50) NOT NULL,
  `galleryBarID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`galleryCategoryID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `group_tbl`
--

CREATE TABLE IF NOT EXISTS `group_tbl` (
  `groupId` int(11) NOT NULL AUTO_INCREMENT,
  `groupName` varchar(30) NOT NULL,
  `userParentId` int(11) NOT NULL,
  `groupTypeId` int(11) NOT NULL,
  `barId` int(11) NOT NULL,
  PRIMARY KEY (`groupId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=496 ;

-- --------------------------------------------------------

--
-- Table structure for table `group_type_tbl`
--

CREATE TABLE IF NOT EXISTS `group_type_tbl` (
  `groupTypeId` int(11) NOT NULL AUTO_INCREMENT,
  `groupType` varchar(25) NOT NULL,
  PRIMARY KEY (`groupTypeId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL DEFAULT '',
  `ext` varchar(40) NOT NULL DEFAULT '',
  `galleryCategoryID` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `livefeeds`
--

CREATE TABLE IF NOT EXISTS `liveFeeds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `barID` int(7) NOT NULL,
  `timestamp` int(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `menucategory`
--

CREATE TABLE IF NOT EXISTS `menuCategory` (
  `menuCategoryID` int(11) NOT NULL AUTO_INCREMENT,
  `barID` int(11) NOT NULL,
  `menuCategoryName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `menuCategoryDesc` text COLLATE utf8_unicode_ci NOT NULL,
  `menuCategoryImg` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`menuCategoryID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menuitem`
--

CREATE TABLE IF NOT EXISTS `menuItem` (
  `menuItemID` int(11) NOT NULL AUTO_INCREMENT,
  `menuItemName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `menuItemDesc` text COLLATE utf8_unicode_ci NOT NULL,
  `menuItemPrice` decimal(5,2) NOT NULL,
  `menuCategoryID` int(11) NOT NULL,
  PRIMARY KEY (`menuItemID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `napkins`
--

CREATE TABLE IF NOT EXISTS `napkins` (
  `napkinID` int(11) NOT NULL AUTO_INCREMENT,
  `modifiedTimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `createdTimestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sentTimestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `fromUserID` int(11) NOT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `message` longtext,
  PRIMARY KEY (`napkinID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `napkinsfoldertype`
--

CREATE TABLE IF NOT EXISTS `napkinsFolderType` (
  `folderTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `folderName` varchar(60) NOT NULL,
  PRIMARY KEY (`folderTypeID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `napkinstotable`
--

CREATE TABLE IF NOT EXISTS `napkinsToTable` (
  `toID` int(11) NOT NULL AUTO_INCREMENT,
  `napkinID` int(11) NOT NULL,
  `parentID` int(11) NOT NULL,
  `folderTypeID` int(11) NOT NULL DEFAULT '1',
  `flagged` tinyint(1) NOT NULL DEFAULT '0',
  `userID` int(11) NOT NULL,
  `replyStatus` tinyint(1) NOT NULL DEFAULT '0',
  `readStatus` tinyint(1) NOT NULL DEFAULT '0',
  `deletedStatus` tinyint(1) NOT NULL DEFAULT '0',
  `deletedTimestamp` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`toID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `phone_type_tbl`
--

CREATE TABLE IF NOT EXISTS `phone_type_tbl` (
  `phoneTypeId` int(11) NOT NULL AUTO_INCREMENT,
  `phoneType` varchar(15) NOT NULL,
  PRIMARY KEY (`phoneTypeId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `specials`
--

CREATE TABLE IF NOT EXISTS `specials` (
  `specialID` int(11) NOT NULL AUTO_INCREMENT,
  `barID` int(11) NOT NULL,
  `specialDay` set('0','1','2','3','4','5','6','7','8') NOT NULL COMMENT '0 is every day, 1 starts on sunday',
  `special` varchar(160) NOT NULL,
  PRIMARY KEY (`specialID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `status_tbl`
--

CREATE TABLE IF NOT EXISTS `status_tbl` (
  `statusID` int(11) NOT NULL AUTO_INCREMENT,
  `statusUserID` int(11) NOT NULL,
  `statusDatestamp` datetime NOT NULL,
  `statusValue` varchar(500) NOT NULL,
  PRIMARY KEY (`statusID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `streamingcities`
--

CREATE TABLE IF NOT EXISTS `streamingCities` (
  `streamingCityID` int(11) NOT NULL AUTO_INCREMENT,
  `streamingCityName` varchar(50) NOT NULL,
  `streamingCityState` char(2) NOT NULL,
  `streamingCityLat` varchar(20) NOT NULL,
  `streamingCityLon` varchar(20) NOT NULL,
  PRIMARY KEY (`streamingCityID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5;

-- --------------------------------------------------------

--
-- Table structure for table `timezones`
--

CREATE TABLE IF NOT EXISTS `timezones` (
  `timezoneID` int(11) NOT NULL AUTO_INCREMENT,
  `timezone` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `timezoneValue` int(11) NOT NULL DEFAULT '-5',
  PRIMARY KEY (`timezoneID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `userpreferences`
--

CREATE TABLE IF NOT EXISTS `userPreferences` (
  `preferenceID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `companyEmails` set('0','1') NOT NULL,
  `publicWallView` set('0','1') NOT NULL DEFAULT '1',
  `emailWallPosts` set('0','1') NOT NULL DEFAULT '1',
  `emailNapkins` set('0','1') NOT NULL DEFAULT '1',
  `emailFriendRequests` set('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`preferenceID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `userprofilepic`
--

CREATE TABLE IF NOT EXISTS `userProfilePic` (
  `userProfilePicID` int(11) NOT NULL AUTO_INCREMENT,
  `userProfilePicImage` varchar(25) NOT NULL,
  `userProfileUserID` int(11) NOT NULL,
  PRIMARY KEY (`userProfilePicID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `userschedule`
--

CREATE TABLE IF NOT EXISTS `userSchedule` (
  `userScheduleID` int(11) NOT NULL AUTO_INCREMENT,
  `barID` int(11) NOT NULL,
  `mon` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `tue` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `wed` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `thu` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `fri` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `sat` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `sun` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `timezoneID` int(11) NOT NULL DEFAULT '1',
  `updatedTimestamp` datetime NOT NULL,
  PRIMARY KEY (`userScheduleID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `usersubscribe`
--

CREATE TABLE IF NOT EXISTS `userSubscribe` (
  `userSubscribeID` int(11) NOT NULL AUTO_INCREMENT,
  `userSubscribeBarID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  PRIMARY KEY (`userSubscribeID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_address_tbl`
--

CREATE TABLE IF NOT EXISTS `user_address_tbl` (
  `userAddressId` int(11) NOT NULL AUTO_INCREMENT,
  `userAddressTypeId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `userAddress1` varchar(60) NOT NULL,
  `userAddress2` varchar(60) NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(2) NOT NULL,
  `userZip` varchar(5) NOT NULL,
  PRIMARY KEY (`userAddressId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_bars_tbl`
--

CREATE TABLE IF NOT EXISTS `user_bars_tbl` (
  `userBarsId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `barID` int(11) NOT NULL,
  PRIMARY KEY (`userBarsId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_friends`
--

CREATE TABLE IF NOT EXISTS `user_friends` (
  `userFriendID` int(11) NOT NULL AUTO_INCREMENT,
  `requestingID` int(11) NOT NULL,
  `receivingID` int(11) NOT NULL,
  `response` set('0','1','2') NOT NULL COMMENT '0 is no, 1 is yes, 2 is pending',
  `message` text NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`userFriendID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_group_id`
--

-- CREATE TABLE IF NOT EXISTS `user_group_id` (
--   `userGroupId` int(11) NOT NULL AUTO_INCREMENT,
--   `userId` int(11) NOT NULL,
--   `groupId` int(11) NOT NULL,
--   PRIMARY KEY (`userGroupId`)
-- ) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_info_tbl`
--

CREATE TABLE IF NOT EXISTS `user_info_tbl` (
  `userInfoId` int(11) NOT NULL AUTO_INCREMENT,
  `userFirst` varchar(25) NOT NULL,
  `userLast` varchar(25) NOT NULL,
  `userId` int(11) NOT NULL,
  `userDOB` varchar(10) NOT NULL,
  `userCompany` varchar(40) NOT NULL,
  `userWebsite` text NOT NULL,
  PRIMARY KEY (`userInfoId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_phone_tbl`
--

CREATE TABLE IF NOT EXISTS `user_phone_tbl` (
  `userPhoneId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `userPhoneAreaCode` text NOT NULL,
  `userPhoneExchange` text NOT NULL,
  `userPhoneNumber` text NOT NULL,
  `userPhoneExtension` text NOT NULL,
  `phoneTypeId` int(11) NOT NULL,
  PRIMARY KEY (`userPhoneId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_tbl`
--

CREATE TABLE IF NOT EXISTS `user_tbl` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `userpassword` varchar(100) NOT NULL,
  `userEmail` varchar(100) NOT NULL,
  `userParentId` int(11) NOT NULL,
  `userTypeId` int(11) NOT NULL,
  `userActive` int(11) NOT NULL DEFAULT '2' COMMENT '0 is deleted, 1 is active, 2 is not activated, 3 is mobile allowed but account not confirmed',
  `userFailedLogins` int(11) NOT NULL,
  `userFrom` int(11) NOT NULL,
  `userCreatedTimestsamp` int(11) NOT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_type_tbl`
--

CREATE TABLE IF NOT EXISTS `user_type_tbl` (
  `userTypeId` int(11) NOT NULL AUTO_INCREMENT,
  `userType` varchar(25) NOT NULL,
  PRIMARY KEY (`userTypeId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_wall`
--

CREATE TABLE IF NOT EXISTS `user_wall` (
  `user_wall_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_wall_userID` int(11) NOT NULL,
  `user_wall_sendingID` int(11) NOT NULL,
  `user_wall_note` varchar(400) NOT NULL,
  `user_wall_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_wall_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `zipcode`
--

CREATE TABLE IF NOT EXISTS `zipcode` (
  `zipCode` varchar(5) NOT NULL DEFAULT '0',
  `zipCodeLat` decimal(12,6) NOT NULL,
  `zipCodeLon` decimal(12,6) NOT NULL,
  `zipCodeCity` varchar(50) NOT NULL,
  `zipCodeState` char(2) NOT NULL,
  PRIMARY KEY (`zipCode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
