-- First update script
--  by Jonathan Stevens

-- First part
--  this will create a way to introduce schema updates to the db as time goes on.
--  for each sql script created add a current schema value, which is formatted as 'yearMonthDay'
--  when a new script is created add that value to the db_schema_versions data base and set it as the 'CURRENT_SCHEMA' value on the properties table.
--  Then for each script there can be a check at the top to see if it needs to be ran on the database. This should make db updates less painful. :P

SET @current_schema := '20130107';

-- Make a table to be used for global key, value properties
CREATE TABLE IF NOT EXISTS properties (
  name VARCHAR(20) NOT NULL PRIMARY KEY,
  value VARCHAR(20)
);

-- set blank initial value
INSERT IGNORE INTO properties VALUES ('CURRENT_SCHEMA', '');


-- create a table to contain schema versions applied to db
CREATE TABLE IF NOT EXISTS db_schema_versions (
  schema_key VARCHAR (8) PRIMARY KEY,
  created_timestamp TIMESTAMP
);

INSERT INTO db_schema_versions (schema_key, created_timestamp) VALUES (@current_schema, NOW());

UPDATE properties SET value = @current_schema WHERE name = 'CURRENT_SCHEMA';



-- Second Part
--  Add a new admin user type, need a space allow creation of admin specific areas

INSERT INTO user_type_tbl VALUES (null, "Admin");



-- Drop Map zones - no longer used
DROP TABLE IF EXISTS mapZones;