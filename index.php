<?php

require_once 'scripts/app_config.php';

session_start();

$searchValue = array('zip' => 'null', 'city' => 'null', 'state' => 'null');
$error_message = "";
$success_message = "";

// Check for search value
if (isset($_POST['search']) && strlen($_POST['search']) > 0) {
    $searchString = mysql_real_escape_string( $_POST['search'] );
    $matches = array();
    if (preg_match('/(\d{5}(?:-\d{4})?)/', $searchString, $matches) > 0) {
        $searchValue['zip'] = $matches[1];
        $_SESSION['search'] = $searchValue;
    }
    else if (preg_match('/([^,]+),\s*(\w{2,})/', $searchString, $matches) > 0) {
        $searchValue['city'] = "'" . $matches[1] . "'";
        $searchValue['state'] = "'" . $matches[2] . "'";
        $_SESSION['search'] = $searchValue;
    }
    else {
        $error_message = "Sorry could not evaluate search. Please enter a valid zip code or valid city, state.";
        unset($_SESSION['search']);
    }
}
else if (isset($_SESSION['search'])) {
    $searchValue = $_SESSION['search'];
}
else {  // default
    $searchValue['zip'] = 43215;
}


$gmapkey = GOOGLE_MAPS_KEY;
$javascript = <<<EOD
<script src="/js/jquery-1.8.3.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key={$gmapkey}&libraries=places&v=3.exp&sensor=false" type="text/javascript"></script>
<script src="/js/barListItem.js" type="text/javascript"></script>
<script src="/js/barspotMap.js" type="text/javascript"></script>
<script src="/js/googleGeoSearch.js" type="text/javascript"></script>
<script>

$(document).ready(translateGeoLocation({$searchValue['zip']},{$searchValue['city']},{$searchValue['state']}));

</script>
EOD;



$content = <<<EOD
<div id="colOne">
    <div id="map"></div>
</div>

<div id="colTwo">
    <div id="barList"></div>
</div>
EOD;



// add view
include 'html/index.html.php';
