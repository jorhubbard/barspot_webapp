<?php
include '../admin/connect.php';
include '../html/header.html.php';
?>

<div id="apparel">

<center><img src="/images/apparelheader.png" alt="Show your support for your favorite spot" /></center>
</div>


<center>
<div id="apparelItems">

<div class="apparelItem">

<img src='/images/BlackShirtFront.png' />
<img src='/images/BlackShirtBack.png' />

<h2>HBS Black T: $9.95</h2>
<!--<form target='paypal' action='https://www.paypal.com/cgi-bin/webscr' method='post'>-->
<form target='paypal' action='https://www.paypal.com/cgi-bin/webscr' method='post'>
<input type='hidden' name='cmd' value='_s-xclick' />
<input type='hidden' name='hosted_button_id' value='967708' />
<table>
<tr>
	<td><input type='hidden' name='on0' value='Size' />Size</td>
</tr>
<tr>
	<td><select name='os0'>
		<option value='Small'>Small $9.95</option>
		<option value='Medium'>Medium $9.95</option>
		<option value='Large'>Large $9.95</option>
		<option value='X-Large' selected='yes'>X-Large $9.95</option>
		<option value='XX-Large'>XX-Large $12.95</option>
		<option value='XXX-Large'>XXX-Large $12.95</option>
	</select> </td>
</tr>
</table>
<input type='hidden' name='currency_code' value='USD' />
<input type='image' src='https://www.paypal.com/en_US/i/btn/btn_cart_LG.gif' border='0' name='submit' alt='' />
<img alt='' border='0' src='https://www.paypal.com/en_US/i/scr/pixel.gif' width='1' height='1' />
</form>
</div>




<div class="apparelItem">

<img src='/images/WhiteShirtFront.png' />
<img src='/images/WhiteShirtBack.png' />

<h2>HBS White T: $9.95</h2>

<form target='paypal' action='https://www.paypal.com/cgi-bin/webscr' method='post'>
<input type='hidden' name='cmd' value='_s-xclick' />
<input type='hidden' name='hosted_button_id' value='968099' />
<table>
<tr>
	<td><input type='hidden' name='on0' value='Size' />Size</td>
</tr>
<tr>
	<td><select name='os0'>
		<option value='Small'>Small $9.95</option>
		<option value='Medium'>Medium $9.95</option>
		<option value='Large'>Large $9.95</option>
		<option value='X-Large' selected='yes'>X-Large $9.95</option>
		<option value='XX-Large'>XX-Large $12.95</option>
		<option value='XXX-Large'>XXX-Large $12.95</option>
	</select></td>
</tr>
</table>
<input type='hidden' name='currency_code' value='USD' />
<input type='image' src='https://www.paypal.com/en_US/i/btn/btn_cart_LG.gif' border='0' name='submit' alt='' />
<img alt='' border='0' src='https://www.paypal.com/en_US/i/scr/pixel.gif' width='1' height='1' />
</form>
</div>


</div>	<!-- END APPAREL ITEMS -->
</center>


<?php include '../html/footer.html.php'; ?>
