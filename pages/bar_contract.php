<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
xmlns:css="http://macVmlSchemaUri" xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta name=Title content="">
<meta name=Keywords content="">
<meta http-equiv=Content-Type content="text/html; charset=macintosh">
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 2008">
<meta name=Originator content="Microsoft Word 2008">
<link rel=File-List href="hotbarspot_web_contract_files/filelist.xml">
<!--[if gte mso 9]><xml>
<o:DocumentProperties>
<o:Author>Ben</o:Author>
<o:Template>Normal.dotm</o:Template>
<o:LastAuthor>Bill Gates</o:LastAuthor>
<o:Revision>2</o:Revision>
<o:TotalTime>17</o:TotalTime>
<o:LastPrinted>2008-04-23T22:39:00Z</o:LastPrinted>
<o:Created>2010-09-02T06:45:00Z</o:Created>
<o:LastSaved>2010-09-02T06:45:00Z</o:LastSaved>
<o:Pages>1</o:Pages>
<o:Words>963</o:Words>
<o:Characters>5491</o:Characters>
<o:Lines>45</o:Lines>
<o:Paragraphs>10</o:Paragraphs>
<o:CharactersWithSpaces>6743</o:CharactersWithSpaces>
<o:Version>12.0</o:Version>
</o:DocumentProperties>
<o:OfficeDocumentSettings>
<o:PixelsPerInch>96</o:PixelsPerInch>
<o:TargetScreenSize>800x600</o:TargetScreenSize>
</o:OfficeDocumentSettings>
</xml><![endif]--><!--[if gte mso 9]><xml>
<w:WordDocument>
<w:TrackMoves>false</w:TrackMoves>
<w:TrackFormatting/>
<w:HyphenationZone>0</w:HyphenationZone>
<w:DoNotHyphenateCaps/>
<w:DrawingGridHorizontalSpacing>0 pt</w:DrawingGridHorizontalSpacing>
<w:DrawingGridVerticalSpacing>0 pt</w:DrawingGridVerticalSpacing>
<w:DisplayHorizontalDrawingGridEvery>0</w:DisplayHorizontalDrawingGridEvery>
<w:DisplayVerticalDrawingGridEvery>0</w:DisplayVerticalDrawingGridEvery>
<w:UseMarginsForDrawingGridOrigin/>
<w:ValidateAgainstSchemas/>
<w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
<w:IgnoreMixedContent>false</w:IgnoreMixedContent>
<w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
<w:DrawingGridHorizontalOrigin>0 pt</w:DrawingGridHorizontalOrigin>
<w:DrawingGridVerticalOrigin>0 pt</w:DrawingGridVerticalOrigin>
<w:DoNotShadeFormData/>
<w:Compatibility>
<w:NoTabHangIndent/>
<w:FootnoteLayoutLikeWW8/>
<w:ShapeLayoutLikeWW8/>
<w:AlignTablesRowByRow/>
<w:ForgetLastTabAlignment/>
<w:DoNotUseHTMLParagraphAutoSpacing/>
<w:LayoutRawTableWidth/>
<w:LayoutTableRowsApart/>
<w:UseWord97LineBreakingRules/>
<w:UseNormalStyleForList/>
<w:DontUseIndentAsNumberingTabStop/>
<w:FELineBreak11/>
<w:WW11IndentRules/>
<w:DontAutofitConstrainedTables/>
<w:AutofitLikeWW11/>
<w:UnderlineTabInNumList/>
<w:HangulWidthLikeWW11/>
</w:Compatibility>
</w:WordDocument>
</xml><![endif]--><!--[if gte mso 9]><xml>
<w:LatentStyles DefLockedState="false" LatentStyleCount="276">
</w:LatentStyles>
</xml><![endif]-->
<style>
<!--
/* Font Definitions */
@font-face
{font-family:Arial;
   panose-1:2 11 6 4 2 2 2 2 2 4;
   mso-font-charset:0;
   mso-generic-font-family:auto;
   mso-font-pitch:variable;
   mso-font-signature:3 0 0 0 1 0;}
   @font-face
{font-family:"Courier New";
   panose-1:2 7 3 9 2 2 5 2 4 4;
   mso-font-charset:77;
   mso-generic-font-family:modern;
   mso-font-format:other;
   mso-font-pitch:fixed;
   mso-font-signature:3 0 0 0 1 0;}
   /* Style Definitions */
   p.MsoNormal, li.MsoNormal, div.MsoNormal
{mso-style-parent:"";
margin:0in;
       margin-bottom:.0001pt;
       mso-pagination:widow-orphan;
       font-size:12.0pt;
       mso-bidi-font-size:10.0pt;
       font-family:"Times New Roman";
       mso-fareast-font-family:"Times New Roman";
       mso-bidi-font-family:"Times New Roman";}
       p.level11, li.level11, div.level11
{mso-style-name:_level11;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:.25in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.level21, li.level21, div.level21
{mso-style-name:_level21;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:.5in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.level31, li.level31, div.level31
{mso-style-name:_level31;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:.75in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.level41, li.level41, div.level41
{mso-style-name:_level41;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:1.0in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.level51, li.level51, div.level51
{mso-style-name:_level51;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:1.25in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.level61, li.level61, div.level61
{mso-style-name:_level61;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:1.5in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.level71, li.level71, div.level71
{mso-style-name:_level71;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:1.75in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.level81, li.level81, div.level81
{mso-style-name:_level81;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:2.0in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.level91, li.level91, div.level91
{mso-style-name:_level91;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:2.25in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.levsl11, li.levsl11, div.levsl11
{mso-style-name:_levsl11;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:.25in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.levsl21, li.levsl21, div.levsl21
{mso-style-name:_levsl21;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:.5in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.levsl31, li.levsl31, div.levsl31
{mso-style-name:_levsl31;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:.75in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.levsl41, li.levsl41, div.levsl41
{mso-style-name:_levsl41;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:1.0in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.levsl51, li.levsl51, div.levsl51
{mso-style-name:_levsl51;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:1.25in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.levsl61, li.levsl61, div.levsl61
{mso-style-name:_levsl61;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:1.5in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.levsl71, li.levsl71, div.levsl71
{mso-style-name:_levsl71;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:1.75in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.levsl81, li.levsl81, div.levsl81
{mso-style-name:_levsl81;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:2.0in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.levsl91, li.levsl91, div.levsl91
{mso-style-name:_levsl91;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:2.25in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.levnl11, li.levnl11, div.levnl11
{mso-style-name:_levnl11;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:.25in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.levnl21, li.levnl21, div.levnl21
{mso-style-name:_levnl21;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:.5in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.levnl31, li.levnl31, div.levnl31
{mso-style-name:_levnl31;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:.75in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.levnl41, li.levnl41, div.levnl41
{mso-style-name:_levnl41;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:1.0in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.levnl51, li.levnl51, div.levnl51
{mso-style-name:_levnl51;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:1.25in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.levnl61, li.levnl61, div.levnl61
{mso-style-name:_levnl61;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:1.5in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.levnl71, li.levnl71, div.levnl71
{mso-style-name:_levnl71;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:1.75in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.levnl81, li.levnl81, div.levnl81
{mso-style-name:_levnl81;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:2.0in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.levnl91, li.levnl91, div.levnl91
{mso-style-name:_levnl91;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:2.25in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   span.DefaultPara
{mso-style-name:"Default Para";
   mso-ansi-font-size:10.0pt;}
   p.level1, li.level1, div.level1
{mso-style-name:_level1;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:.25in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:.25in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.level2, li.level2, div.level2
{mso-style-name:_level2;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:.5in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:.5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.level3, li.level3, div.level3
{mso-style-name:_level3;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:.75in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:.75in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.level4, li.level4, div.level4
{mso-style-name:_level4;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:1.0in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.level5, li.level5, div.level5
{mso-style-name:_level5;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:1.25in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:1.25in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.level6, li.level6, div.level6
{mso-style-name:_level6;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:1.5in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.level7, li.level7, div.level7
{mso-style-name:_level7;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:1.75in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:1.75in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.level8, li.level8, div.level8
{mso-style-name:_level8;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:2.0in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.level9, li.level9, div.level9
{mso-style-name:_level9;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:2.25in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:2.25in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.levsl1, li.levsl1, div.levsl1
{mso-style-name:_levsl1;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:.25in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:.25in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.levsl2, li.levsl2, div.levsl2
{mso-style-name:_levsl2;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:.5in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:.5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.levsl3, li.levsl3, div.levsl3
{mso-style-name:_levsl3;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:.75in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:.75in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.levsl4, li.levsl4, div.levsl4
{mso-style-name:_levsl4;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:1.0in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.levsl5, li.levsl5, div.levsl5
{mso-style-name:_levsl5;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:1.25in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:1.25in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.levsl6, li.levsl6, div.levsl6
{mso-style-name:_levsl6;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:1.5in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.levsl7, li.levsl7, div.levsl7
{mso-style-name:_levsl7;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:1.75in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:1.75in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.levsl8, li.levsl8, div.levsl8
{mso-style-name:_levsl8;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:2.0in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.levsl9, li.levsl9, div.levsl9
{mso-style-name:_levsl9;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:2.25in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:2.25in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.levnl1, li.levnl1, div.levnl1
{mso-style-name:_levnl1;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:.25in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:.25in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.levnl2, li.levnl2, div.levnl2
{mso-style-name:_levnl2;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:.5in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:.5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.levnl3, li.levnl3, div.levnl3
{mso-style-name:_levnl3;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:.75in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:.75in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.levnl4, li.levnl4, div.levnl4
{mso-style-name:_levnl4;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:1.0in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.levnl5, li.levnl5, div.levnl5
{mso-style-name:_levnl5;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:1.25in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:1.25in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.levnl6, li.levnl6, div.levnl6
{mso-style-name:_levnl6;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:1.5in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.levnl7, li.levnl7, div.levnl7
{mso-style-name:_levnl7;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:1.75in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:1.75in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.levnl8, li.levnl8, div.levnl8
{mso-style-name:_levnl8;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:2.0in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.levnl9, li.levnl9, div.levnl9
{mso-style-name:_levnl9;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:2.25in;
   margin-bottom:.0001pt;
   text-indent:-.25in;
   mso-pagination:none;
   tab-stops:2.25in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in right 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   span.DefaultPara0
{mso-style-name:"Default Para";
   mso-ansi-font-size:10.0pt;}
   p.26, li.26, div.26
{mso-style-name:_26;
margin:0in;
       margin-bottom:.0001pt;
       mso-pagination:none;
       font-size:12.0pt;
       mso-bidi-font-size:10.0pt;
       font-family:"Times New Roman";
       mso-fareast-font-family:"Times New Roman";
       mso-bidi-font-family:"Times New Roman";}
       p.25, li.25, div.25
{mso-style-name:_25;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:1.0in;
   margin-bottom:.0001pt;
   text-indent:-.5in;
   mso-pagination:none;
   tab-stops:1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.24, li.24, div.24
{mso-style-name:_24;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:1.5in;
   margin-bottom:.0001pt;
   mso-pagination:none;
   tab-stops:1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.23, li.23, div.23
{mso-style-name:_23;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:2.0in;
   margin-bottom:.0001pt;
   mso-pagination:none;
   tab-stops:2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.22, li.22, div.22
{mso-style-name:_22;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:2.5in;
   margin-bottom:.0001pt;
   mso-pagination:none;
   tab-stops:2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.21, li.21, div.21
{mso-style-name:_21;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:3.0in;
   margin-bottom:.0001pt;
   mso-pagination:none;
   tab-stops:3.0in 3.5in 4.0in 4.5in 5.0in 5.5in 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.20, li.20, div.20
{mso-style-name:_20;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:3.5in;
   margin-bottom:.0001pt;
   mso-pagination:none;
   tab-stops:3.5in 4.0in 4.5in 5.0in 5.5in 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.19, li.19, div.19
{mso-style-name:_19;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:4.0in;
   margin-bottom:.0001pt;
   mso-pagination:none;
   tab-stops:4.0in 4.5in 5.0in 5.5in 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.18, li.18, div.18
{mso-style-name:_18;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:4.5in;
   margin-bottom:.0001pt;
   mso-pagination:none;
   tab-stops:4.5in 5.0in 5.5in 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.17, li.17, div.17
{mso-style-name:_17;
margin:0in;
       margin-bottom:.0001pt;
       mso-pagination:none;
       tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in 6.0in;
       font-size:12.0pt;
       mso-bidi-font-size:10.0pt;
       font-family:"Times New Roman";
       mso-fareast-font-family:"Times New Roman";
       mso-bidi-font-family:"Times New Roman";}
       p.16, li.16, div.16
{mso-style-name:_16;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:1.0in;
   margin-bottom:.0001pt;
   text-indent:-.5in;
   mso-pagination:none;
   tab-stops:1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.15, li.15, div.15
{mso-style-name:_15;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:1.5in;
   margin-bottom:.0001pt;
   mso-pagination:none;
   tab-stops:1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.14, li.14, div.14
{mso-style-name:_14;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:2.0in;
   margin-bottom:.0001pt;
   mso-pagination:none;
   tab-stops:2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.13, li.13, div.13
{mso-style-name:_13;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:2.5in;
   margin-bottom:.0001pt;
   mso-pagination:none;
   tab-stops:2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.12, li.12, div.12
{mso-style-name:_12;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:3.0in;
   margin-bottom:.0001pt;
   mso-pagination:none;
   tab-stops:3.0in 3.5in 4.0in 4.5in 5.0in 5.5in 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.11, li.11, div.11
{mso-style-name:_11;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:3.5in;
   margin-bottom:.0001pt;
   mso-pagination:none;
   tab-stops:3.5in 4.0in 4.5in 5.0in 5.5in 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.10, li.10, div.10
{mso-style-name:_10;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:4.0in;
   margin-bottom:.0001pt;
   mso-pagination:none;
   tab-stops:4.0in 4.5in 5.0in 5.5in 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.9, li.9, div.9
{mso-style-name:_9;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:4.5in;
   margin-bottom:.0001pt;
   mso-pagination:none;
   tab-stops:4.5in 5.0in 5.5in 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.8, li.8, div.8
{mso-style-name:_8;
margin:0in;
       margin-bottom:.0001pt;
       mso-pagination:none;
       tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in 6.0in;
       font-size:12.0pt;
       mso-bidi-font-size:10.0pt;
       font-family:"Times New Roman";
       mso-fareast-font-family:"Times New Roman";
       mso-bidi-font-family:"Times New Roman";}
       p.7, li.7, div.7
{mso-style-name:_7;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:1.0in;
   margin-bottom:.0001pt;
   text-indent:-.5in;
   mso-pagination:none;
   tab-stops:1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.6, li.6, div.6
{mso-style-name:_6;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:1.5in;
   margin-bottom:.0001pt;
   mso-pagination:none;
   tab-stops:1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.5, li.5, div.5
{mso-style-name:_5;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:2.0in;
   margin-bottom:.0001pt;
   mso-pagination:none;
   tab-stops:2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.4, li.4, div.4
{mso-style-name:_4;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:2.5in;
   margin-bottom:.0001pt;
   mso-pagination:none;
   tab-stops:2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.3, li.3, div.3
{mso-style-name:_3;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:3.0in;
   margin-bottom:.0001pt;
   mso-pagination:none;
   tab-stops:3.0in 3.5in 4.0in 4.5in 5.0in 5.5in 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.2, li.2, div.2
{mso-style-name:_2;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:3.5in;
   margin-bottom:.0001pt;
   mso-pagination:none;
   tab-stops:3.5in 4.0in 4.5in 5.0in 5.5in 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.1, li.1, div.1
{mso-style-name:_1;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:4.0in;
   margin-bottom:.0001pt;
   mso-pagination:none;
   tab-stops:4.0in 4.5in 5.0in 5.5in 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.a, li.a, div.a
{mso-style-name:_;
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:4.5in;
   margin-bottom:.0001pt;
   mso-pagination:none;
   tab-stops:4.5in 5.0in 5.5in 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   p.WPNormal, li.WPNormal, div.WPNormal
{mso-style-name:WP_Normal;
margin:0in;
       margin-bottom:.0001pt;
       mso-pagination:none;
       tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in 6.0in;
       font-size:12.0pt;
       mso-bidi-font-size:10.0pt;
       font-family:"Times New Roman";
       mso-fareast-font-family:"Times New Roman";
       mso-bidi-font-family:"Times New Roman";}
       p.DefinitionT, li.DefinitionT, div.DefinitionT
{mso-style-name:"Definition T";
margin:0in;
       margin-bottom:.0001pt;
       mso-pagination:none;
       tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in 6.0in;
       font-size:12.0pt;
       mso-bidi-font-size:10.0pt;
       font-family:"Times New Roman";
       mso-fareast-font-family:"Times New Roman";
       mso-bidi-font-family:"Times New Roman";}
       p.DefinitionL, li.DefinitionL, div.DefinitionL
{mso-style-name:"Definition L";
   margin-top:0in;
   margin-right:0in;
   margin-bottom:0in;
   margin-left:.25in;
   margin-bottom:.0001pt;
   mso-pagination:none;
   tab-stops:.25in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in 6.0in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   span.Definition
{mso-style-name:Definition;
   font-style:italic;
   mso-bidi-font-style:normal;}
   p.H1, li.H1, div.H1
{mso-style-name:H1;
margin:0in;
       margin-bottom:.0001pt;
       mso-pagination:none;
       tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in 6.0in;
       font-size:24.0pt;
       mso-bidi-font-size:10.0pt;
       font-family:"Times New Roman";
       mso-fareast-font-family:"Times New Roman";
       mso-bidi-font-family:"Times New Roman";
       font-weight:bold;
       mso-bidi-font-weight:normal;}
       p.H2, li.H2, div.H2
{mso-style-name:H2;
margin:0in;
       margin-bottom:.0001pt;
       mso-pagination:none;
       tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in 6.0in;
       font-size:18.0pt;
       mso-bidi-font-size:10.0pt;
       font-family:"Times New Roman";
       mso-fareast-font-family:"Times New Roman";
       mso-bidi-font-family:"Times New Roman";
       font-weight:bold;
       mso-bidi-font-weight:normal;}
       p.H3, li.H3, div.H3
{mso-style-name:H3;
margin:0in;
       margin-bottom:.0001pt;
       mso-pagination:none;
       tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in 6.0in;
       font-size:14.0pt;
       mso-bidi-font-size:10.0pt;
       font-family:"Times New Roman";
       mso-fareast-font-family:"Times New Roman";
       mso-bidi-font-family:"Times New Roman";
       font-weight:bold;
       mso-bidi-font-weight:normal;}
       p.H4, li.H4, div.H4
{mso-style-name:H4;
margin:0in;
       margin-bottom:.0001pt;
       mso-pagination:none;
       tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in 6.0in;
       font-size:12.0pt;
       mso-bidi-font-size:10.0pt;
       font-family:"Times New Roman";
       mso-fareast-font-family:"Times New Roman";
       mso-bidi-font-family:"Times New Roman";
       font-weight:bold;
       mso-bidi-font-weight:normal;}
       p.H5, li.H5, div.H5
{mso-style-name:H5;
margin:0in;
       margin-bottom:.0001pt;
       mso-pagination:none;
       tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in 6.0in;
       font-size:10.0pt;
       font-family:"Times New Roman";
       mso-fareast-font-family:"Times New Roman";
       mso-bidi-font-family:"Times New Roman";
       font-weight:bold;
       mso-bidi-font-weight:normal;}
       p.H6, li.H6, div.H6
{mso-style-name:H6;
margin:0in;
       margin-bottom:.0001pt;
       mso-pagination:none;
       tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in 6.0in;
       font-size:8.0pt;
       mso-bidi-font-size:10.0pt;
       font-family:"Times New Roman";
       mso-fareast-font-family:"Times New Roman";
       mso-bidi-font-family:"Times New Roman";
       font-weight:bold;
       mso-bidi-font-weight:normal;}
       p.Address, li.Address, div.Address
{mso-style-name:Address;
margin:0in;
       margin-bottom:.0001pt;
       mso-pagination:none;
       tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in 6.0in;
       font-size:12.0pt;
       mso-bidi-font-size:10.0pt;
       font-family:"Times New Roman";
       mso-fareast-font-family:"Times New Roman";
       mso-bidi-font-family:"Times New Roman";
       font-style:italic;
       mso-bidi-font-style:normal;}
       p.Blockquote, li.Blockquote, div.Blockquote
{mso-style-name:Blockquote;
   margin-top:0in;
   margin-right:.25in;
   margin-bottom:0in;
   margin-left:.25in;
   margin-bottom:.0001pt;
   mso-pagination:none;
   tab-stops:.25in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in 5.75in;
   font-size:12.0pt;
   mso-bidi-font-size:10.0pt;
   font-family:"Times New Roman";
   mso-fareast-font-family:"Times New Roman";
   mso-bidi-font-family:"Times New Roman";}
   span.CITE
{mso-style-name:CITE;
   font-style:italic;
   mso-bidi-font-style:normal;}
   span.CODE
{mso-style-name:CODE;
   mso-ansi-font-size:10.0pt;
   font-family:"Courier New";
   mso-ascii-font-family:"Courier New";
   mso-hansi-font-family:"Courier New";}
   span.WPEmphasis
{mso-style-name:WP_Emphasis;
   font-style:italic;
   mso-bidi-font-style:normal;}
   span.WPHyperlink
{mso-style-name:WP_Hyperlink;
color:blue;
      text-decoration:underline;
      text-underline:single;}
      span.FollowedHype
{mso-style-name:FollowedHype;
color:purple;
      text-decoration:underline;
      text-underline:single;}
      span.Keyboard
{mso-style-name:Keyboard;
   mso-ansi-font-size:10.0pt;
   font-family:"Courier New";
   mso-ascii-font-family:"Courier New";
   mso-hansi-font-family:"Courier New";
   font-weight:bold;
   mso-bidi-font-weight:normal;}
   p.Preformatted, li.Preformatted, div.Preformatted
{mso-style-name:Preformatted;
margin:0in;
       margin-bottom:.0001pt;
       mso-pagination:none;
       tab-stops:0in 47.95pt 95.9pt 143.75pt 191.7pt 239.7pt 287.7pt 335.65pt 383.6pt 431.45pt 6.0in;
       font-size:10.0pt;
       font-family:"Times New Roman";
       mso-ascii-font-family:"Courier New";
       mso-fareast-font-family:"Times New Roman";
       mso-hansi-font-family:"Courier New";
       mso-bidi-font-family:"Times New Roman";}
       p.zBottomof, li.zBottomof, div.zBottomof
{mso-style-name:"zBottom of ";
margin:0in;
       margin-bottom:.0001pt;
       text-align:center;
       mso-pagination:none;
       tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in 6.0in;
border:none;
       mso-border-top-alt:double black 0in;
padding:0in;
	mso-padding-alt:0in 0in 0in 0in;
	font-size:8.0pt;
	mso-bidi-font-size:10.0pt;
	font-family:"Times New Roman";
	mso-ascii-font-family:Arial;
	mso-fareast-font-family:"Times New Roman";
	mso-hansi-font-family:Arial;
	mso-bidi-font-family:"Times New Roman";}
	p.zTopofFor, li.zTopofFor, div.zTopofFor
{mso-style-name:"zTop of For";
margin:0in;
       margin-bottom:.0001pt;
       text-align:center;
       mso-pagination:none;
       tab-stops:0in .5in 1.0in 1.5in 2.0in 2.5in 3.0in 3.5in 4.0in 4.5in 5.0in 5.5in 6.0in;
border:none;
       mso-border-bottom-alt:double black 0in;
padding:0in;
	mso-padding-alt:0in 0in 0in 0in;
	font-size:8.0pt;
	mso-bidi-font-size:10.0pt;
	font-family:"Times New Roman";
	mso-ascii-font-family:Arial;
	mso-fareast-font-family:"Times New Roman";
	mso-hansi-font-family:Arial;
	mso-bidi-font-family:"Times New Roman";}
	span.Sample
{mso-style-name:Sample;
   font-family:"Courier New";
   mso-ascii-font-family:"Courier New";
   mso-hansi-font-family:"Courier New";}
   span.WPStrong
{mso-style-name:WP_Strong;
   font-weight:bold;
   mso-bidi-font-weight:normal;}
   span.Typewriter
{mso-style-name:Typewriter;
   mso-ansi-font-size:10.0pt;
   font-family:"Courier New";
   mso-ascii-font-family:"Courier New";
   mso-hansi-font-family:"Courier New";}
   span.Variable
{mso-style-name:Variable;
   font-style:italic;
   mso-bidi-font-style:normal;}
   span.HTMLMarkup
{mso-style-name:"HTML Markup";
color:red;
display:none;
	mso-hide:all;}
	span.Comment
{mso-style-name:Comment;
display:none;
	mso-hide:all;}
	/* Page Definitions */
	@page
{mso-page-border-surround-header:no;
   mso-page-border-surround-footer:no;
   mso-facing-pages:yes;}
   @page Section1
{size:8.5in 11.0in;
margin:1.0in 1.25in 1.0in 1.25in;
       mso-header-margin:1.0in;
       mso-footer-margin:1.0in;
       mso-paper-source:0;}
       div.Section1
{page:Section1;}
-->
</style>
<!--[if gte mso 10]>
<style>
/* Style Definitions */
table.MsoNormalTable
{mso-style-name:"Table Normal";
   mso-tstyle-rowband-size:0;
   mso-tstyle-colband-size:0;
   mso-style-noshow:yes;
   mso-style-parent:"";
   mso-padding-alt:0in 5.4pt 0in 5.4pt;
   mso-para-margin:0in;
   mso-para-margin-bottom:.0001pt;
   mso-pagination:widow-orphan;
   font-size:10.0pt;
   font-family:"Times New Roman";}
   </style>
   <![endif]--><!--[if gte mso 9]><xml>
   <o:shapedefaults v:ext="edit" spidmax="2050"/>
   </xml><![endif]--><!--[if gte mso 9]><xml>
   <o:shapelayout v:ext="edit">
   <o:idmap v:ext="edit" data="1"/>
   </o:shapelayout></xml><![endif]-->
   </head>

   <body lang=EN-US style='tab-interval:.5in'>

   <div class=Section1>

   <p class=MsoNormal align=center style='text-align:center'><!--[if supportFields]><span
   style='mso-element:field-begin'></span><span style="mso-spacerun:
   yes">&nbsp;</span>SEQ CHAPTER \h \r 1<![endif]--><!--[if supportFields]><span
   style='mso-element:field-end'></span><![endif]--><b style='mso-bidi-font-weight:
   normal'>hotbarspot.com Service Contract</b><u><o:p></o:p></u></p>

   <p class=MsoNormal align=center style='text-align:center'><u><o:p><span
   style='text-decoration:none'>&nbsp;</span></o:p></u></p>

   <p class=MsoNormal>In consideration of the payment of a damage deposit and
   timely payment of monthly subscription fees by the Customer, HH&amp;A LLC
   provides certain specialized marketing efforts for restaurants and bars, which
   efforts include the installation and operation of cameras using specialized
   hardware and software to transmit data and images to the public at large via
   the World Wide Web (as follows, the “Service”). <u><o:p></o:p></u></p>

   <p class=MsoNormal><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u></p>

   <p class=MsoNormal><u>Subscriptions</u>. Subscription fees are locked at the
   signup price for 12 months with the first payment being included in the deposit
   and remaining payments once per month starting one month after the opening of
   the website for the public and are due on the 1<sup>st</sup> of each month.
   Several varying levels of the Service may be offered now or in the future by
   HH&amp;A LLC, for which differing amounts of a subscription fee may apply; in
   such case, these varying levels and amounts will be communicated to the
   Customer upon prior written notice.<span style="mso-spacerun: yes">&nbsp;&nbsp;

   </span></p>

   <p class=MsoNormal><o:p>&nbsp;</o:p></p>

   <p class=MsoNormal>If payment is received by the end of business (5pm EST) the 10<sup>th</sup>
   day of the month, no late fees will apply. For all payments received by
   HH&amp;A LLC after 5pm EST on the 10<sup>th</sup> <span style="mso-spacerun:
   yes">&nbsp;</span>day of the month and after, a late fee of<span
   style="mso-spacerun: yes">&nbsp; </span>$5 will apply. If charges and fees are
   not paid in a timely manner, the Service may be terminated by HH&amp;A LLC at
   any time. </p>

   <p class=MsoNormal><o:p>&nbsp;</o:p></p>

   <p class=MsoNormal>The amount of monthly subscription fees and/or late fee may
   be increased or decreased by HH&amp;A LLC upon two months’ prior written notice
   to the Customer, or by notification windows displayed on the website in the
   same two month timeframe.<span style="mso-spacerun: yes">&nbsp; </span></p>

   <p class=MsoNormal><o:p>&nbsp;</o:p></p>

   <p class=MsoNormal><u>Cancellations</u>. The Service may be cancelled at any
   time and for any reason by the Customer upon written notice to HH&amp;A LLC. No
   pro-rated amounts for prepaid but unused partial months of a subscription will
   be refunded to the Customer upon cancellation by the Customer.<span
   style="mso-spacerun: yes">&nbsp; </span>If the customer is still under
   contract, there will be a prorated cancellation fee.<span style="mso-spacerun:
   yes">&nbsp; </span>If the customer cancels within the first 6 months, the
   charge is $200.<span style="mso-spacerun: yes">&nbsp; </span>For every month
   after the six month, the amount of this cancellation fee is reduced by $25.</p>

   <p class=MsoNormal><o:p>&nbsp;</o:p></p>

   <p class=MsoNormal>The Service may be cancelled at any time and for any reason
   by HH&amp;A LLC upon one months’ prior notice to the Customer. In the event of
   cancellation by HH&amp;A LLC any subscription fees prepaid by the Customer
   beyond the calendar month in which notice of cancellation is given by HH&amp;A
   LLC will be refunded to the Customer.<span style="mso-spacerun:
   yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></p>

   <p class=MsoNormal><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u></p>

   <p class=MsoNormal><u>Equipment</u>. Any and all equipment, including software,
   licensed to the Customer by HH&amp;A LLC in connection with the provision of
   the Service (the “Equipment”) will remain at all times the property of HH&amp;A
   LLC. Upon payment of the damage deposit by the Customer, HH&amp;A LLC grants to
   the Customer a license to install and use the Equipment. The amount of this
   damage deposit is subject to change and the current amount is on the
   hotbarspot.com website at hotbarspot.com/admin/cart/.<span style="mso-spacerun:
   yes">&nbsp; </span>This damage deposit must be paid in full before provision of
   the Service will begin. </p>

   <p class=MsoNormal>All Equipment must be returned to HH&amp;A LLC in an intact,
   orderly, and functional condition within 30 days of the date of cancellation of
   the Service. Upon verification by HH&amp;A LLC that returned Equipment remains
   in an intact, orderly, and functional condition, the damage deposit will be
   returned within 10 days of the date on which HH&amp;A LLC receives returned
   Equipment back from the Customer.<span style="mso-spacerun: yes">&nbsp;
   </span>If shipping is involved to return the equipment, this is at the cost of
   the Customer. </p>

   <p class=MsoNormal><o:p>&nbsp;</o:p></p>

   <p class=MsoNormal><u>Installation and Maintenance</u>. HH&amp;A LLC provides
   self install kits that include a small computer, usb camera, and a 100 foot Ethernet
   cable.<span style="mso-spacerun: yes">&nbsp; </span>If requested, HH&amp;A
   sales representatives will help find a qualified, licensed and bonded installer
   in the area at the sole cost of the Customer.<span style="mso-spacerun:
   yes">&nbsp; </span>HH&amp;A LLC will repair or replace non-functioning
   Equipment without unreasonable delay. HH&amp;A LLC reserves the right to charge
   additional amounts to the Customer for damage to Equipment which is due to the
   Customer or for which the Customer must answer (<i style='mso-bidi-font-style:
	 normal'>i.e.</i> damage to Equipment done by patrons of the Customer’s
	 restaurant or bar).<span style="mso-spacerun: yes">&nbsp;&nbsp;&nbsp; </span><u><o:p></o:p></u></p>

   <p class=MsoNormal><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u></p>

   <p class=MsoNormal><u>Exclusivity of Use</u>. All Equipment supplied by
   HH&amp;A for provision of the Service may only be used exclusively for
   provision of the Service by HH&amp;A. Use by the Customer of any of the
   Equipment for any other purpose, directly or indirectly, whatsoever shall
   render any obligations of HH&amp;A under this Contract voidable by HH&amp;A, and
   may give rise to cancellation by HH&amp;A LLC of the Service.<span
   style="mso-spacerun: yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></p>

   <p class=MsoNormal><o:p>&nbsp;</o:p></p>

   <p class=MsoNormal><u>Privacy/Content</u>. It shall be the exclusive
   responsibility of the Customer to post notice to its patrons that Equipment
   exists on the premises of the Customer for the transmission of video and/or
   audio data. </p>

   <p class=MsoNormal><o:p>&nbsp;</o:p></p>

   <p class=MsoNormal>HH&amp;A LLC will undertake its reasonable best efforts to
   ensure that no viruses, malware, or other corruption or unauthorized access to
   video and/or audio data occurs via the provision of its Service over the World
   Wide Web. HH&amp;A LLC will also undertake its reasonable best efforts to
   ensure that no video and/or audio data other than the video and/or audio data
   of its Customers is transmitted, and that no corruption, distortion, or other
   falsification of said video and/or audio data occurs. </p>

   <p class=MsoNormal><o:p>&nbsp;</o:p></p>

   <p class=MsoNormal>HH&amp;A LLC assumes no responsibility for any possible
   claims by any party predicated upon the transmission of video and/or audio data
   from the premises of a Customer or the content of said transmission, including
   but not limited to privacy, copyright, trademark, indecency, or any other legal
   grounds. The Customer agrees to indemnify and hold harmless HH&amp;A LLC from
   any claims by any party brought against HH&amp;A LLC on the basis of the
   transmission of video and/or audio data from the premises of a Customer or the
   content of said transmission.<span style="mso-spacerun:
   yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
   </span></p>

   <p class=MsoNormal><o:p>&nbsp;</o:p></p>

   <p class=MsoNormal><o:p>&nbsp;</o:p></p>

   <p class=MsoNormal><u>Computer Technical Support</u>. HH&amp;A LLC is not
   responsible for any technical support for any computer hardware or software
   belonging to the Customer. As an exception to the foregoing, HH&amp;A will
   undertake technical support and repair damage to the computer system of a
   Customer to the extent that such technical support or repair is required as a
   direct and proximate result of the Equipment of HH&amp;A LLC licensed to the
   Customer. </p>

   <p class=MsoNormal><o:p>&nbsp;</o:p></p>

   <p class=MsoNormal><o:p>&nbsp;</o:p></p>

   <p class=MsoNormal>In deciding whether technical support or repairs of damage
   to the computer system of a Customer is a direct and proximate result of the
   Equipment of HH&amp;A LLC, the decision of HH&amp;A LLC controls exclusively.
   In making this decision, HH&amp;A LLC will use its just discretion and good
   faith, and will consider the interests of the Customer. </p>

   <p class=MsoNormal><o:p>&nbsp;</o:p></p>

   <p class=MsoNormal><u>Good Faith and Fair Dealing</u>. Both HH&amp;A LLC and
   the Customer agree to use good faith and fair dealing with one another in the
   performance of their obligations in this Contract.</p>

   <p class=MsoNormal><o:p>&nbsp;</o:p></p>

   </div>

   </body>

   </html>

