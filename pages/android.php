<?php
include '../admin/connect.php';
include_once '../core.php';

ob_start();

?>
<div id="contactus">


<div id="contactSubheader">
<div style='width:300px;'>
<p>If you're looking to sign up, advertise, or leave a 
suggestion...then fill out the form below. Our team 
is working around the clock to answer your 
messages as quickly as possible.</p>
</div>
<img src="/images/contactheader.png" alt="Get in touch... we're listening." />
</div>


<div id="contactForm">



<table width='100%' cellspacing='10'>
<tr>
   <td colspan='2' align='center'>
   <?
   print $submitOutput;
   ?>
   </td>
</tr>
<tr>
   <td width='45%' valign='top'>
   <h2>Bar Owners</h2>
   <center><strong>Want to be apart of our community?</strong></center>
   <br />
   Fill out the form below and start
   showing the world what's happening in your fine establishment. Hotbarspot
   also offers listings with virtual tours, as well as links to your bar's web
   site at a reduced cost!

   <?
   $list = array('Virutal Tour','Fixed Point Camera','Selling Food Online','Website Design / Hosting','Search Engine Optimization');
   print drawForm($list);
   ?>
   </td>
   <td valign='top'>
   <h2>Advertisers</h2>
   <center><strong>Getting the word out has never been so easy here at HotBarSpot!</strong></center>
   <br />
   Choose from options that let you advertise nationally, locally or a combination
   of both. You also have the ability to select different demographics based
   on which bars you advertise in! For more details fill out the form below.

   <?
   $list = array('National Advertising','Local Advertising','Focused Demograhic Advertising');
   print drawForm($list);
   ?>
   </td>
</tr>
</table>
</div>	<!-- End contactForm -->
</div>  <!-- end contactus -->

<?

$cnt = ob_get_contents();
ob_clean();
/* Save Content */
$doc->DOMChangeTemplate("content",$cnt);
$doc->WriteHTML();
