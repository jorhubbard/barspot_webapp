<?php
require_once("core.php");
//$doc = new dom("login/template.html"); // Load Theme
ob_start();
require_once("core/modules/class.calendar.php");
include 'admin/connect.php';
include 'admin/functions.php';

print("
<table>
<tr>
   <td>
   <h2>Bar Patrons</h2>
   Looking for a reason to join?  Here are just a few reasons below.
   <br/><br />
   <ul>
      <li>See all bars in your area</li>
      <li>See LIVE VIDEO FEEDS of bars in your area</li>
      <li>Access to bar specials, menus and events</li>
      <li>Text options available so that you can know what is going on the second the bar announces it</li>
      <li>Subscribe to your favorite places</li>
      <li>Access to live feeds and specials via your iPhone! (search hotbarspot)</li>
   </ul>
   </td>
   <td>
   <h2>Bar Owners</h2>
   Looking for an innovative new way to advertise your bar and gain new business?  Hot Bar Spot is your ticket to reaching your target demographic.  Below are just some of the ways we are here to help you.
   <br /><br />
   <ul>
      <li>Post your menus, specials and events for free!</li>
      <li>Upload photos of your bar for people to get an idea of the atmosphere you provide</li>
      <li>See statistics on how many people have visited your specific bar page</li>
      <li>Have access to email subscribers to your bar about events or promotions</li>
   </ul>
   </td>
</tr>
</table>

");

$cnt = ob_get_contents();
ob_clean();

/* Save Content */
$doc->DOMChangeTemplate("content",$cnt);
$doc->WriteHTML();
