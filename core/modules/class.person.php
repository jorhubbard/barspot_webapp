<?php

abstract class Person_Definition 
{

}

class person extends Person_Definition
{
	public $id;
	public $firstName;
	public $lastName;
	protected $email;
	public function __construct()
	{ 
		//$this->id = md5(microtime());
	}
	
	public function getName()
	{
		return $this->firstName." ".$this->lastName;
	}
	
	public function setEmail( Email_Address_Definition $email)
	{
		$this->email = $email;
	}
	
	public function getEmail()
	{
		return $this->email->getEmail();
	}
	
	public function getEmailLink()
	{
		return $this->email->getEmailLink();
	}	
}

?>