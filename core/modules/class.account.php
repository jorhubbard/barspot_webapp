<?php

abstract class Account_Definition
{
	abstract public function addDepartment(Department_Definition $person);
}

class Account extends Account_Definition 
{
	private $id;
	public $title;
	protected $departments;
	
	public function __construct($title){
		$this->title=$title;
		$this->departments = array();
	}
	
	public function addDepartment(Department_Definition $department){
		array_push($this->departments,$department);		
	}
}


?>