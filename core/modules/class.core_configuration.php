<?php

class Core_Configuration {

        private static $config;

        public function __construct($initFile){ }

        private static function constructRegistry(){
                foreach(self::$config as $section => $section_array){
                        $reg = Preferences::getInstance($section);
                        foreach ($section_array as $field => $value){
                                $reg->set($field, $value);
                        }
                }
        }

        public static function getRegistry($initFile)
        {
                if (is_file($initFile)){
                        self::$config = parse_ini_file($initFile,TRUE);
                        self::constructRegistry();
                }else{
                        echo "Error Processing Configuration File";
                }
        }
}



