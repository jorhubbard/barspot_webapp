<?php
/**
 * @Author: Dusten Smith
 * Description: First Test with a composite pattern for use in creating a heirarchy.
 * 	This class can be used for building a heiracial category tree based on the Nested
 * principal and the adjacent model.
 *
 * I specified both models for this class because each has their strengths and weaknesses
 *
 * Nested Model:
 * 		Strength:	The ability to grown infinitly. More reliable inheritance.
 *		Weakness:	Pain in the arsh to manage in a database or client application.
 *					Larger SQL statements to add remove or adjust the objects around.
 *
 * Adjacent Model:
 *		Strength:	Easier to manage for a client application or in a database.
 *		Weakness:	Doesn't grow very well. Hard to build multiple levels.
 */
 
/**
 * Category Definition
 * Description: This defines the important parts of a category type.
 *
 */
abstract class category_def {
	abstract function set_parent( category_def $cat_parent );
	abstract function set_name( $name );
	abstract function set_left( $left );
	abstract function set_right( $right );
	abstract function get_id();
	abstract function get_name();
	abstract function get_parent();
	abstract function get_left();
	abstract function get_right();
}

/**
 * Category Class
 */ 
class category extends category_def {
	protected $cat_id;
	protected $cat_name;
	protected $cat_parent;
	protected $cat_left;
	protected $cat_right;
	
	public function __construct( $cat_id, $cat_name ) {
		$this->cat_id = $cat_id;
		$this->cat_name = $cat_name;
	}
	
	public function set_parent( category_def $cat_parent ) {
		$this->cat_parent = $cat_parent;
	}

	public function set_name( $name ){
		$this->cat_name = $name;
	}
	
	public function set_left( $left ){
		$this->cat_left = $left;
	}
	
	public function set_right( $right ){
		$this->cat_right = $right;
	}		
	
	public function get_name(){
		return $this->cat_name;
	}
	
	public function get_id() {
		return $this->cat_id;
	}
	
	public function get_parent() {
		return $this->cat_parent;
	}
	
	public function get_left() {
		return $this->cat_left;
	}
	
	public function get_right() {
		return $this->cat_right;
	}
	
}