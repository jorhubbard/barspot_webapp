<?php

class HTTP {

        // redirect url
        static public function redirect_to_url($relative_url="index.php"){
                header('Location:'.self::baseurl().$relative_url);
        }


        // Base URL
        static protected function baseurl() {
                // BUILD HTTP URL PORT
                (strpos($_SERVER['HTTP_HOST' ], ":") != false)?$port = '':
                        (($_SERVER['SERVER_PORT'] == '80')?$port = '':
                                (($_SERVER['SERVER_PORT'] == '8080')?$port = '':$port=':'.$_SERVER['SERVER_PORT']));
                $proto = 'http://';

                // BUILD HTTPS URL PORT
                if ($_SERVER['HTTPS'] == 'on') {
                  $proto = 'https://';
                  if ($port == ':443'){ $port = ''; }
                }

                if ((dirname($_SERVER['PHP_SELF'])=='\\' || dirname($_SERVER['PHP_SELF'])=='/')){
                  // Hosted at root.
                  return($proto.$_SERVER['HTTP_HOST'].$port.'/');
                } else {
                  // Hosted in sub-directory.
                  return($proto.$_SERVER['HTTP_HOST'].$port.dirname($_SERVER['PHP_SELF']).'/');
                }
        }

}
