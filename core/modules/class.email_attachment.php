<?php

/**
 * Email Attachment Definition
 */
abstract class Email_Attachment_Definition 
{
	abstract public function getFile();
    abstract public function getFilename();

}


/**
 * Email Attachment
 */
class Email_Attachment extends Email_Attachment_Definition
{
    public $objFile;
	public $filename;
	public $data;

    public function __construct( File_Definition $objFile, $fileRename=NULL){
      $this->objFile = $objFile;
        if ($fileRename == NULL){
            $this->filename = $objFile->getFilename();
        }else{
            $this->filename = $fileRename;
        }
    }

    public function getFile(){
       return $this->objFile;
    }

    public function getFilename() {
      return $this->filename;
    }
}
