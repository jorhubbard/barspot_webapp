<?php

abstract class Command {
	abstract function execute( CommandContext $context );
}

class CommandContext {
	private $parms = array();
	private $error = "";

	function __construct() {
		$this->parms = $_REQUEST;
	}

	function addParm( $key, $value) {
		$this->parms[$key] = $value;
	}

	function get( $key ) {
		return $this->parms[$key];
	}

	function setError( $error ) {
		$this->error = $error;
	}

	function getError() {
		return $this->error;
	}
}

class CommandFactory {
	private static $dir = "commands";

	static function getCommand( $action='Default' ) {
		if ( preg_match('/\W/', $action )) {
			throw new Excetion("Illegal Characters in Action");
		}

		$class = strtolower($action)."Command";
		if (!class_exists($class,false)) { // Is the class already defined
			// Find the class.
			$file = DIRMODULES.self::$dir.DIRECTORY_SEPARATOR."{$class}.php";
			if ( !file_exists( $file )){
				throw new Exception("Could Not File '{$file}'");
			}	
			require_once( $file );
		}
		$cmd = new $class();
		return $cmd;
	}
}

class CommandController {
	private $context;
	function __construct() {
		$this->context = new CommandContext();
	}

	function getContext() {
		return $this->context;
	}

	function process() {
		$cmd = CommandFactory::getCommand( $this->context->get('action') );
		if (!$cmd->execute( $this->context )) {
			 // Success
		}else{
			throw new Exception("Command Failer: Unable to Execute Commmand");
		}
	}
}


class CommandResolver {
	private static $base_cmd;
	private static $default_cmd;

	public function __construct() 
	{
		if (!self::$base_cmd) {
			self::$base_cmd = new ReflectionClass( "Command" );
			self::$default_cmd = new DefaultCommand();
		}
	}

	public function getCommand( Request $request )
	{
		$cmd = $request->getProperty('cmd');
		$sep = DIRECTORY_SEPARATOR;
		if (!$cmd){
			return self::$default_cmd;
		}

		$cmd = str_replace( array('.', $sep), "", $cmd);
		$filename = "Command{$sep}";
		
	}
}
