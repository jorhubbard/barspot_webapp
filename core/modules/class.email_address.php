<?php

/**
 * Email Address Definition
 */
abstract class Email_Address_Definition 
{
	abstract public function getEmail();
	abstract public function getEmailLink();
	//abstract public static function isValid($textEmail);
}

/**
 * Email Address
 */
class Email_Address extends Email_Address_Definition 
{
	protected $emailAddress;
	
	public function __construct($textEmail){
		if (self::isValid($textEmail)){
			$this->emailAddress = $textEmail;
		}else{
			throw new specialException("204","EmailAddress Error: {$textEmail} is not a valid email address.",1);
		}
	}
	
	public function __toString(){
		return $this->emailAddress;
	}
	
	public function getEmail(){
		return $this->emailAddress;
	}
	
	public function getEmailLink(){
		return "mailto:{$this->emailAddress}";
	}
	
    /**
     * isValid($textEmail)
     * Description: Check to see if the email address is a valid email in structure.
     * Returns:
     *      True - if the email is valid.
     *      False - if the email is invalid.
     */
    public static function isValid($textEmail) {
	    $filter = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/';
		if(preg_match($filter, $textEmail)) {
		  return true;		  // Valid email address
		}else{
		  return false;		  // Invalid email address
		}
	}
}