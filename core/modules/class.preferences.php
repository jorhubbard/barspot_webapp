<?php

class Preferences {
	protected static $_instance = array();  
	protected $array = array();	

    /** 
     * Private constructor 
     */  
    private function __construct() {}  

    /** 
     * Get instance 
     */  
    public static function getInstance($registryName)
    {  
         if(!isset(self::$_instance[$registryName]))
         {  
             self::$_instance[$registryName] = new self();  
         }  
         return self::$_instance[$registryName];  
    }        

	public function get($key){
		return $this->array[strtolower($key)];	
	}

	public function set($key, $value){
		$key = strtolower($key);
		if (!isset($this->array[$key]))	{
			$this->array[$key] = $value;
		}
	}
}
