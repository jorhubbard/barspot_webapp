<?php

/**
 * Binder_db
 *	Get categories from database based on nested list model.
 */						
class catagory_binder_db extends catagory_binder {
	protected $mysql;
	
	public function __construct(mysqli $mysql){
		$this->mysql = $mysql;	
		$this->retrieve_categories();
	}

	public function retrieve_categories() {
		$this->categories = array();
		$sql = "SELECT 	node.category_id as CID, 
						parent.category_id as PID, 
						parent.name as NAME,
						parent.lft as CLEFT,
						parent.rgt as CRIGHT
				  FROM 	category_tbl as node,
					   	category_tbl as parent
				 WHERE 	node.lft BETWEEN parent.lft AND parent.rgt
				   AND 	node.rgt = node.lft + 1
				 ORDER BY node.category_id, parent.lft";
		$r = $this->mysql->query($sql);

		if ($r){
			$pids = array();
            $cid = NULL;
			while ($mr = $r->fetch_array()){
				if ($cid != $mr['CID']){
					// Set Current Category
					$cid = $mr['CID'];
					// Remove Parent
					$prnt = "";				
				}
				// Set Category Name
				$cat = $mr['NAME'];
				
				if (!in_array($mr['PID'],$pids)){	// If not used
					// If category has not been created, the create it.
					if (!isset($this->categories[$mr['PID']])){	
						// Create Category
						$this->add_category( new category($mr['PID'], $cat));
						$this->categories[$mr['PID']]->set_left($mr['CLEFT']);
						$this->categories[$mr['PID']]->set_right($mr['CRIGHT']);
						if ($prnt != ""){
							// Set Parent Category if There is a parent category
							$this->categories[$mr['PID']]->set_parent($this->categories[$prnt]);
						}							
					}
					$pids[] = $mr['PID'];
				}
				
				// Set Parent for next record.
				if ($cid == $mr['CID']){
					$prnt = $mr['PID'];
				}
			}
		}
	}
	
	public function addCategoryNextTo( $cid, $newCatName) {
  	    /* EXAMPLE TRANSACTION
		$query = "INSERT INTO trans (id,item,quantity)
		values (null,'Baseball',4)";
		$this->begin(); // transaction begins
		$result = @mysql_query($query);
		if($result)
		{
			$this->commit(); // transaction is committed
			echo "your insertion was successful";
		}
		else
		{
			$this->rollback(); // transaction rolls back
			echo "you rolled back";
			exit;
		}
		*/
		$sql = "Call cat_addNode( {$cid} , '{$newCatName}');";
		$this->mysql->query($sql);
		
		if ($this->mysql->error == ""){
			$success = TRUE;
		}else{
			$success = FALSE;	
		}
		
	return $success;
	}
	
	public function addSubCategory( $cid, $newCatName) {
		$sql = "Call cat_addNodeWOChild( {$cid} , '{$newCatName}');";
		$this->mysql->query($sql);
		
		if ($this->mysql->error == ""){
			$success = TRUE;
		}else{
			$success = FALSE;	
		}
		
	return $success;
	}
	
	public function deleteCategory( $cid) {
		$sql = "Call cat_deleteNode( {$cid} );";
		$this->mysql->query($sql);
		
		if ($this->mysql->error == ""){
			$success = TRUE;
		}else{
			$success = FALSE;	
		}
		
	return $success;
	}	
	
}// end class

?>