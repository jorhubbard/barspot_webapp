<?php	
/**
 * @Author: Dusten Smith 		Start Date: 8-20-2007								  
 * @version: 2.0 																	  
 * Description:                                                                       
 *		Build and Send and e-mail. 													  
 *																					  
 * Features:																		  
 *       - Multiple Recipients														  
 *       - Attachments																  
 *       - Text / Html Messages														  
 *
 * Objects:
 *		Email
 *		Email_Address_Definition
 *		Email_Address
 *		Email_Attachment_Definition
 *		Email_Attachment 
 *
 * Requirements:
 *      class.file.php   
 *          File_Definition
 *          File
 */
 
/**
 * Email
 */
class Email 
{
	private $mailSubject;
	private $mailMessage;
	private $mailFrom;								// Who email is from
	private $mailTo;								// Array of email addresses
	private $mailToText;							// Comma delimited list of emails.
	private $mailPriority;
	private $randomHash;							// Random key for e-mails
	private $attachments;							// Attachment Array
	
	/**
     * Construct
     */
	public function __construct( Email_Address_Definition $fromEmail) 
	{
		$this->mailFrom = $fromEmail;				// Validate From Email
		$this->mailPriority = "Normal";				// Set Email Priority
		
		// Generate random key
		$this->randomHash = @md5(time());

	}

	/** 
	 * Get Emails for Headers
	 */
	private function getRecipients() 
	{
		// Send to is not blank
		if (count($this->mailTo) > 0){
			// Strip Array to Text String
            
			$this->mailToText = implode(", ",$this->mailTo);
		}else{
			throw new specialException("200", "Email Error: No Recipients!", 1);
		}
	}
	
	/**
	 * createHeaders()
	 * Description:
	 *   This builds the text headers to the emails
	 */
	private function createHeaders() 
	{
		$mailHeaders  = "MIME-Version: 1.0\r\n"
		              . "Content-Type: multipart/mixed;"
					  . "boundary=\"PHP-mixed-{$this->randomHash}\"\r\n"
					  . "From: {$this->mailFrom}\r\n"
					  . "Cc: \r\n"
					  . "Bcc: \r\n"
					  . "Reply-To: {$this->mailFrom}\r\n"
					  . "X-Priority: 1\r\n"
					  . "X-MSMail-Priority: {$this->mailPriority}\r\n"
					  . "X-Mailer: \r\n";
	return $mailHeaders;
	}

	/**
     * createMessageBody($type)
     * Description: Build Text Message
     * Parms:
     *   $type  - "text" or "html" message type
	 */
	private function createMessageBody($type=NULL)
	{
		switch($type){
			case "text": 
				$messageType = "text/plain";
				$this->mailMessage = strip_tags($this->mailMessage);
			break;
			case "html":
				$messageType = "text/html";
			break;
			default:
				$messageType = "text/plain";
				$this->mailMessage = strip_tags($this->mailMessage);
			break;
		}
		$message = "--PHP-alt-{$this->randomHash}\r\n"
                 . "Content-Type: {$messageType}; charset=\"iso-8859-1\"\r\n"
		         . "Content-Transfer-Encoding: base64\r\n\r\n";		// base64 or 7bit
		$message .= chunk_split(base64_encode($this->mailMessage))."\r\n\r\n";
	return $message;		
	}
	
	/**
     * createAttachment()
	 * Build Attachment Message
	 */
	private function createAttachment()
	{
		$message = "";
		foreach($this->attachments as $attachment){
            $aFile = $attachment->getFile();
             $message .=  "--PHP-mixed-{$this->randomHash}\r\n"
			          . "Content-Type: {$aFile->getDocType()};"
				      . " name=\"{$attachment->getFilename()}\"\r\n"
			          . "Content-Transfer-Encoding: base64\r\n"
			          . "Content-Disposition: attachment\r\n"
			          . "Filename=\"{$attachment->getFilename()}\"\r\n\r\n"
                      . chunk_split(base64_encode(file_get_contents($aFile)));
		$x++;
		}
	return $message;
	}
	
	/**
	 * Create Email Message Body
	 */
	private function createMessage()
	{
		$message = "--PHP-mixed-{$this->randomHash}\r\n"
		         . "Content-Type: multipart/alternative;"
			     . " boundary=\"PHP-alt-{$this->randomHash}\"\r\n\r\n";
        // Display both formates for email's that support html or text only.
		$message .= $this->createMessageBody("text");
		$message .= $this->createMessageBody("html");
		$message .= "--PHP-alt-{$this->randomHash}--\r\n\r\n";	// End alt message tag
        // Add attachements if any
		if (count($this->attachments) > 0){		
			$message .= $this->createAttachment();
		}
        // End Message
		$message .= "--PHP-mixed-{$this->randomHash}--\r\n\r\n";// End mixed tag

	return $message; 
	}

	/**
	 * Send To
	 */
	public function sendTo( Email_Address_Definition $sendTo) 
	{
        if (count($this->mailTo) == 0) { $this->mailTo = array(); }
        array_push($this->mailTo, $sendTo);
	}

	/**
	 * Set Subject
	 */
	public function setSubject($subject)	
	{ 
		$this->mailSubject = strip_tags($subject);	
	}

	/**
     * setMessage( String $message)
	 * Set Message
	 */
	public function setMessage($message) 
	{ 
		$this->mailMessage = $message; 
	}

	/** 
     * attachFile( File_Definition $objFile, $overrideName)
     * Description:
	 *    Attach a file to the email
     * Parms:
     *    File_Definition $objFile - Attach a file object.
     *    $overrideName - Rename file to attach to email (Use when filename is temperary. 
	 */
	public function attachFile( File_Definition $objFile, $overrideName=NULL)
	{
        if (count($this->attachments) == 0) { $this->attachments = array(); }
        array_push($this->attachments, new Email_Attachment($objFile,$overrideName));
	}

	/**
	 * sendMail()
	 * Description: This function will finialize the email out bound.
	 */
	public function sendMail()
	{
        // Get Recipients
        $this->getRecipients();
		// Set up the headers
		$mailHeaders = $this->createHeaders();
		// Set the message
		$mailMessage = $this->createMessage();

		try {
			// Send the email
			$mailSent = @mail($this->mailToText, $this->mailSubject, $mailMessage, $mailHeaders);
		}catch(exception $e) {
			throw new specialException("202","Email Error: Unable to send email.",1);
		}

	return $mailSent;
	}
}

/* Example Source Code */
/*
// Create Email Addresses
$fromEmail = new Email_Address("kc8ruk@gmail.com");
$toEmail   = new Email_Address("distorted@gmail.com");

// Create Email
$em = new Email($fromEmail);
  // Recipients
  $em->sendTo($toEmail);
  $em->sendTo(new Email_Address("test@test.com"));

  // Message
  $em->setSubject("Test");
  $em->setMessage("<b>Test Message</b><br/>\r\nThis is a test message");

  // Attach Files
  $em->attachFile(new File("./class.dom.php"));
  $em->attachFile(new File("./class.ftp.php"), "ftp.php");

// Send Mail
try{
	$em->sendMail();
	echo "Email Sent Successfully!";
}catch(exception $e){
	echo "Email Failed to Send: {$e}";
}
*/