<?php

class EditeventCommand extends Command {
	function execute( CommandContext $context ){
		/* Commands */
		$event_id	=	$context->get('event_id');
		$event_title = 	$context->get('event_title');
		$event_date = 	$context->get('event_date');
		$event_time = 	$context->get('event_time');
		$event_text = 	$context->get('event_text');
		$event_user = 	$context->get('event_user');
	        $bar_id     =   $context->get('bar_id');		
		$mysql = Preferences::getInstance('database')->get('connection');
	

		try {
			//$event_title = $mysql->real_escape_string($event_title);
			//$event_time = $mysql->real_escape_string($event_time);
			//$event_text = $mysql->real_escape_string($event_text);
			

			$mysql->query("UPDATE `calendar_events` 
			                  SET `event_date` = \"{$event_date}\",
							      `event_time` = \"{$event_time}\",
							      `event_title` = \"{$event_title}\",
							      `event_text` = \"{$event_text}\",
								  `user_id` = \"{$event_user}\",
								  `bar_id` = \"{$bar_id}\"
							WHERE `id` = '{$event_id}' ");
			echo $mysql->error;
		}catch(specialException $e){
			echo "<span class=\"error\">Edit Event Failed[{$e->getCode()}]: {$e->getMessage()}</span>";
		}
	}
}
