<?php

class AddEventCommand extends Command {
	function execute( CommandContext $context ){
		/* Commands */
		$event_title = 	$context->get('event_title');
		$event_date = 	$context->get('event_date');
		$event_time = 	$context->get('event_time');
		$event_text = 	$context->get('event_text');
		$event_user = 	$context->get('event_user');
	        $bar_id	    =   $context->get('barID');		
		$mysql = Preferences::getInstance('database')->get('connection');
	
		try {
			//$event_title = $mysql->real_escape_string($event_title);
			//$event_time = $mysql->real_escape_string($event_time);
			//$event_text = $mysql->real_escape_string($event_text);
			
			$mysql->query("INSERT INTO `calendar_events` VALUES(null,
										  \"{$event_date}\",
										  \"{$event_time}\",
										  \"{$event_title}\",
										  \"{$event_text}\",
										  \"{$event_user}\",
										  \"{$bar_id}\"
										  )");
			echo $mysql->error;
		}catch(specialException $e){
			echo "<span class=\"error\">Add Event Failed[{$e->getCode()}]: {$e->getMessage()}</span>";
		}
	}
}
