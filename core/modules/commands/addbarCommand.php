<?php
$msg = "";
$err = FALSE;
if (isset($_POST['password']) && ($_POST['password'] == $_POST['c_password'])){
    $pass 		= crypt($_POST['password']);
}else{
    $err = TRUE;
    $msg .= "Passwords Do Not Match<br/>";
}
if (isset($_POST['username']) && $_POST['username'] != ""){
    // Validate The user is not already taken.
    $r = $mysql->query("SELECT * FROM `user_tbl` WHERE `username` = '{$_POST['username']}'");
    if ($r->num_rows > 0 ){
        $err = TRUE;
        $msg .= "Username Already Taken<br/>";
    }
}else{
    $err = TRUE;
    $msg .= "Please Enter a Username";
}
$username 	= $mysql->real_escape_string(strip_tags($_POST['username']));
$firstname	= $mysql->real_escape_string(strip_tags($_POST['firstname']));
$lastname	= $mysql->real_escape_string(strip_tags($_POST['lastname']));
$phone_area	= $mysql->real_escape_string(strip_tags($_POST['phone_area']));
$phone_exchange = $mysql->real_escape_string(strip_tags($_POST['phone_exchange']));
$phone_number	= $mysql->real_escape_string(strip_tags($_POST['phone_number']));
$email 		= $mysql->real_escape_string(strip_tags($_POST['email']));
$dob		= $mysql->real_escape_string(strip_tags($_POST['dob']));
$street1	= $mysql->real_escape_string(strip_tags($_POST['addr1']));
$street2	= $mysql->real_escape_string(strip_tags($_POST['addr2']));
$city		= $mysql->real_escape_string(strip_tags($_POST['city']));
$state		= $mysql->real_escape_string(strip_tags($_POST['state']));
$zip		= $mysql->real_escape_string(strip_tags($_POST['zip']));
$phone_type	= $_POST['phone_type'];
$barName	= $mysql->real_escape_string(strip_tags($_POST['barName']));
$barAddress	= $mysql->real_escape_string(strip_tags($_POST['barAddress']));
$barPhone	= $mysql->real_escape_string(strip_tags($_POST['barPhone']));
$barZip		= $mysql->real_escape_string(strip_tags($_POST['barZip']));

if ($err == FALSE){
    // Setup User
    $r = $mysql->query("INSERT INTO `{$site_cfg['login']['SqlUserTable']}`
                   VALUES (null,'{$username}',
                              '{$pass}',
                              '{$email}',
                              '0','4','0','0')");
    echo $mysql->error;
    if ($r){
        $uid = $mysql->insert_id;
        // Setup User Info
        $mysql->query("INSERT INTO `user_info_tbl` VALUES (null,'{$firstname}',
                        '{$lastname}',{$uid},'{$dob}','','')");
        echo $mysql->error;

        // Setup User Phone
        $mysql->query("INSERT INTO `user_phone_tbl` VALUES (null,{$uid},
                        '{$phone_area}','{$phone_exchange}','{$phone_number}','',{$phone_type})");
        echo $mysql->error;

        // Setup User Group
        $mysql->query("INSERT INTO `group_tbl` VALUES (null, '{$username}', {$uid}, 4, 0)");
        echo $mysql->error;

        // Bar Address
        if (isset($_POST['barID'])){
            $barID = $_POST['barID'];
                // Update Bar
            $mysql->query("UPDATE `bars`
                      SET `barName`='{$barName}',
                          `barAddress`='{$barAddress}',
                          `barZip` = '{$barZip}',
                                                  `barPhone` = '{$barPhone}',
                                                  `barWebsite` = '{$barWebsite}'
                    WHERE `barID` = {$barID}");
            echo $mysql->error;
        }else{
                // Setup Bar
            $mysql->query("INSERT INTO `bars` VALUES (null,'{$barName}','{$barAddress}','{$barZip}',
                        '{$barPhone}','{$barWebsite}',CURRENT_TIMESTAMP(),'0','0','1')");
            echo $mysql->error;
            $barID = $mysql->insert_id;
        }
        echo "<pre>";
        print_r ($_POST);
        echo "</pre>";
        // Setup Bar Group
        $mysql->query("INSERT INTO `group_tbl` VALUES (null,'{$barName}',{$uid},3,{$barID})");
        echo $mysql->error;
        die;
    //			HTTP::redirect_to_url("signup_thankyou.php");
    }
}