<?php

class LoginCommand extends Command {
	function execute( CommandContext $context ){
		$user = $context->get('username');
		$pass = $context->get('pass');
		$rememberme = $context->get('rememberme');
		$redirect = $context->get('redirect');
		
		try {
			$login = new login();
			$login->validate_login($user, $pass, $rememberme);
			if (is_file($redirect)){
				HTTP::redirect_to_url($redirect);
			}else{
				echo "Login: Invalid Redirect Path";
			}
		}catch(specialException $e){
			echo "<span class=\"error\">Login Failed[{$e->getCode()}]: {$e->getMessage()}</span>";
		}
	}
}
