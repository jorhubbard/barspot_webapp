<?PHP

// User API
class catagory_binder {
	protected $categories;
	
	public function __construct() {	}

	public function add_category( category_def $category ) {
		$this->categories[$category->get_id()] = $category;
	}
	
	public function getCategoryNameById( $id ){
		return $this->categories[$id]->get_name();
	}
	
	public function getCategoryObjectById( $id ){
		return $this->categories[$id];
	}
	
	/** 
	 * Get Categories 
	 * Description: Returns an array of categories and id's, this is useful when used in a 
	 * dropdrown list or just for output.
	 */
	public function get_categories(){
		$ctemp= array();
		foreach ($this->categories as $cat){
			// Output Id
			$output = "";

			// Get All Parents
			$catp = $cat->get_parent();
			$ancesters = array();
			while ($catp != NULL){
					$ancesters[] = $catp->get_name();
					$catp = $catp->get_parent();
			}
			$a = implode(" > ",array_reverse($ancesters));
			if ($a != ""){ $a .= " > ";	}
			// Output Name			
			$output .= $a;
			$output .= $cat->get_name();
		$ctemp[] = array($cat->get_id(), $output);
		}
	return $ctemp;
	}
	
	public function retrieve_parents( $id ){
		$parents = array();
			$catp = $this->categories[$id]->get_parent();
			$ancesters = array();
			while ($catp != NULL){
					$parents[] = array($catp->get_id(), $catp->get_name());;
					$catp = $catp->get_parent();
			}
			$parents = array_reverse($parents);
	return $parents;
	}
	
	
	public function retrieve_nodes( $parent_id ){
		$leaves = array();
		foreach ($this->categories as $cat){
			if ($cat->get_parent() == $parent_id){			
				$leaves[] = array($cat->get_id(), $cat->get_name());	
			}
		}
		
		if (count($leaves) > 0){
			// Sorting Visually
			foreach ($leaves as $key => $row) {
				$ctemp[$key] = $row[1];
			}
			$ctemp_lowercase = array_map('strtolower', $ctemp);	// For case insensitive sorting
			array_multisort($ctemp_lowercase, SORT_ASC, SORT_REGULAR, $leaves);
		}		
	return $leaves;
	}
	
	public function fetch_array(){
		return $this->categories;
	}
}







/* Usage Example */
/*
$binder = new binder_db($mysql);

//$binder->get_categories();

echo $binder->getCategoryNameById(7);

echo "<pre>";
print_r($binder->getCategoryObjectById(7));
echo "</pre>";
*/
?>
