<?PHP
/**
 * @author Dusten Smith
 */
class login {
   protected $username;
   protected $password;
   protected $hash;	
   protected $cookie_name;
   private $mysqli;
   private $usertable;

   public function __construct(){
      $this->mysqli = Preferences::getInstance('database')->get('connection');
      $this->usertable = Preferences::getInstance('login')->get('SqlUserTable');
      $this->cookie_name = "hbs";

      if (isset($_GET['logout']) && $_GET['logout'] == 1) {
	 $this->logout();
      }
   }

   // Validate Login
   public function validate_login($username, $password, $setcookie=NULL)
   {	
      $this->username = strtolower($username);
      $this->password = $password;
      // Search for users
      if (strripos($this->username,'@')) //have @ sign, ussimg email address
      {
	 $field = 'userEmail';
      }
      else //using username, not email address
      {
	 $field = 'username';
      }
      $results = $this->mysqli->query("SELECT * 
	    FROM `{$this->usertable}` 
	    WHERE `$field`='{$this->username}'");
      echo $this->mysqli->error;
      // Get user data to process password
      if ($results && $results->num_rows > 0) {	// Check for results
	 $mr = $results->fetch_array();

	 $this->hash = md5($mr['userId'].$mr['userpassword'].$mr['userTypeId']).$mr['userId'];
	 
	 // Check Password
	 if (crypt($this->password,$mr['userpassword'])==$mr['userpassword'])
	 {
	    if($mr['userActive'] == '1')
	    {
	       $_SESSION['pass'] = TRUE;
	       $_SESSION['userid'] = $mr['userId'];
	       $_SESSION['username'] = $mr['username'];
	       $_SESSION['usertype'] = $mr['userTypeId'];
	       // Set Cookie for 2 weeks
	       if (isset($setcookie)){
		  $time = @time() + (86400*14);
		  setcookie($this->cookie_name,$this->hash,$time);
	       }

	       /**************************************************
		*   CJ's block of crappy code
		*   that puts the groups into a session variable
		**************************************************/
	       /*$sql = "SELECT groupId FROM user_group_id WHERE userId = '".$_SESSION['userid']."';";
	       $result = mysql_query($sql)or die("Invalid query: " . mysql_error());
	       $x = 1;
	       $groupArray[0] = 0;
	       while($row = mysql_fetch_array($result))
	       {	  
		  $groupArray[$x] = $row[groupId];
		  $x++;
	       }
	       $_SESSION['groupList'] = $groupArray;*/
	       /********end crappy block*************************/
	    }
	    else
	    {
	       if($mr['userActive'] == '2')
	       {
		  $_SESSION['validateUser']  = $mr['username'];
		  $_SESSION['validateEmail'] = $mr['userEmail']; //sets a session variable so that we know the user already authenticated and we can use this to send the email
		  throw new specialException(102, "Account has not had email verified yet.  Please check your inbox and follow the link to verify your account. <a href='/admin/login/sendAuthEmail.php'>Click here to resend the email to user $mr[username] at $mr[userEmail].</a>");
		  //need to add ability to change listed email account from this section
	       }
	       if($mr['userActive'] == '3')
	       {
		  $_SESSION['validateUser'] = $mr['username'];
		  $_SESSION['validateEmail']= $mr['userEmail'];
		  throw new specialException(104,"Account was added from a mobile device and has not had the email verified yet.  Please check your inbox and follow the link to verify your account. <a href='/admin/login/sendAuthEmail.php'>Click here to resend the email to user $mr[username] at $mr[userEmail].</a>");
	       }
	       else
	       {
		  throw new specialException(103, "Account has been deleted or de-activated.  If this has been done in error, please contact support@hotbarspot.com.");
	       }
	    }
	 }else{
	    // Invalid password
	    throw new specialException(101, "Invalid Password!");
	 }
      }else{					
	 // Unable to find username
	 throw new specialException(100, "Invalid Username!");
      }

   }

   // Logout
   public function logout(){
      session_unset();
      session_destroy();
      // Clear Cookie
      setcookie($this->cookie_name,"");
   }

   // is Logged In
   public function is_logged_in(){
      if($_SESSION['pass']){ 
	 return TRUE;
      }else if ((isset($_COOKIE[$this->cookie_name]) && $_COOKIE[$this->cookie_name] != "")){
	 if ($this->validate_cookie()) {
	    return TRUE;
	 }else{
	    return FALSE;
	 }
      }else{
	 return FALSE;
      }
   }

   // Check Cookie
   protected function validate_cookie()
   {   global $site_cfg;
      $id = substr($_COOKIE[$this->cookie_name],32,strlen($_COOKIE[$this->cookie_name])-32);
      $hash = substr($_COOKIE[$this->cookie_name],0,32);

      // Search for users
      $results = $this->mysqli->query("SELECT * 
	    FROM `{$this->usertable}` 
	    WHERE `userId`='{$id}'");

      // Get user data to process password
      if ($results && $results->num_rows > 0) {	// Check for results
	 $mr = $results->fetch_array();
	 $this->hash = md5($mr['userId'].$mr['userpassword'].$mr['userTypeId']).$mr['userId'];
	 if ($this->hash === $_COOKIE[$this->cookie_name]){
	    $_SESSION['pass'] = true;
	    $_SESSION['userid'] = $mr['userId'];
	    $_SESSION['usertype'] = $mr['userTypeId']; 	// PERMISSION LEVEL 0 - Admin
	    return TRUE;
	 }else{
	    return FALSE;
	 }


      }
   }

}

?>
