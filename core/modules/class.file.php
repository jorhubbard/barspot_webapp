<?PHP
/**
 * File Object
 * @author:     Dusten Smith
 * @date:       2/13/2009 (Friday the 13th)
 * Description:
 *
 */

/**
 * File Definition
 */
abstract class File_Definition
{
    abstract public function getDirectory();
    abstract public function getFilename();
    abstract public function getDocType();
    abstract public function getExtension();
	abstract public function getData();
}

/**
 * File 
 */
class File extends File_Definition
{
    protected $fullFilename;
    protected $path;
    protected $doctype;
    protected $extension;
    protected $filename;
  
    /**
    * __construct($fullFilename)
    * Description:
    *   When the file path is validated then break the file into its individual components.
    */
  
    public function __construct($fullFilename){
        if (is_file($fullFilename)){
            $this->fullFilename = $fullFilename;
            // Seperate Path and Filename
            $this->path = substr($fullFilename,0,strrpos($fullFilename,"/")+1);
            $this->filename = substr($fullFilename,strrpos($fullFilename,"/")+1);
            $this->doctype = @exec("file -i -b {$this->path}"); // LINUX ONLY COMMAND
            $this->extension = substr($fullFilename,strrpos($fullFilename,".")+1);
        }else{
            throw new specialException("400", "Invalid File Name", 1);
        }
    }
    
    public function __toString() {
        return $this->fullFilename;
    }
    
    public function getDirectory() {
        return $this->path;
    }

    public function getFilename() {
        return $this->filename;
    }

    public function getDocType() {
        return $this->doctype;
    }

    public function getExtension() {
        return $this->extension;
    }
	public function getData() {
		return file_get_contents($this->fullFilename);
	}
}


// New File Doesn't Exist
class File_Raw extends File
{
	private $data;
	
	public function __construct($filename, $doctype, $data){
		// File not Exist
		$this->fullFilename = $filename;
		// Seperate Path and Filename
		$this->path = "/";
		$this->filename = $filename;
		$this->doctype = $doctype;
		$this->extension = substr($filename,strrpos($filename,".")+1);
		$this->data = $data;
	}
	
	public function getData() {
		return $this->data;
	}
}