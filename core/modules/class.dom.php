<?php

/**
 * Author: Dusten Smith
 * Date: 12/28/2007
 * Requirements:
 *		PHP 5
 * Description:
 *		PHP site display functions based on DOM (Document Object Model)
 * This is a great way to seperate design from logic. PHPDOM allows you
 * to access individual objects within the loaded template. 
 *		This allows you to do the majority of the site design in CSS and 
 * to display content into the site.
 *
 * References:
 * 		http://us.php.net/manual/en/ref.dom.php
 *
 * Fixes:
 * ID		Date		Description
 * ------	--------	-----------------------------------------------
 *
 */

class dom
{
  /* Vars */
  private $DOMdoc;
  private $DOMload;

  /* Functions */
  public function __construct($template)
  {
    $this->DOMdoc = new DOMDocument();
    $this->DOMload = @$this->DOMdoc->loadHTMLFile($template);
  }

  /************************************
   * MODIFY DOM OBJECT
   * Date: 12/17/2007
   * This function is used to modify an
   * object on the themed template.
   ************************************/
  public function DOMChangeTemplate($objName,$objValue)
  {
    // Rewrite some special chars
    $objValue = str_replace("&","&#38;",$objValue); // Save yourself down the road.

    // Modify Object
    $obj = $this->DOMdoc->createDocumentFragment();
    $obj->appendXML(utf8_encode($objValue) );
    $this->DOMdoc->getElementById($objName)->appendChild($obj);
  }
	
  /************************************
   * WriteHTML
   * This function will output the final Template
   ************************************/
  public function WriteHTML()
  {
    echo $this->DOMdoc->saveHTML();
  }

}


?>
