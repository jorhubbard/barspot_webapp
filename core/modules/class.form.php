<?PHP
/**************************************************************************************
 * Author: Dusten Smith 		Start Date: 8-20-2007								  *
 * PHP VERSION 5																	  *
 * Version: 2.0 																	  *
 * Description:                                                                       *
 *		Form class allows the construction of forms behind the scenes. You can load	  *
 * forms from a configuration file, or create a form on the fly.					  *
 *																					  *
 **************************************************************************************/
/**************************************************************************************
   Change Log
   ID			Date			Description
   --------		---------		----------------------------------------------------- 
   001		    2008-03-31		Added check to take values into htmlspecialchars() to 
   								prevent utf issues
   -			2008-05-05		Updated Naming Conventions
 **************************************************************************************/
class form 
{
	private $formAttribute;
	private $fields;

// Construct
	public function __construct()
	{
		$this->formAttribute = array();
		$this->fields = array();
	}

// Private Members	

// Public Members
	/* Set Form Attribute */
	public function setAttr($attributeName, $value)
	{
		$this->formAttribute[$attributeName] = $value;	
	}
	
	/* Create Form Element */
	public function createField($fieldType,$fieldId,$value=NULL)
	{
		$this->fields[$fieldId] = new formField($fieldType,$fieldId);
		if ($value != NULL){
			$this->setFieldAttr($fieldId,"value", $value);
		}			
	}
	
	/* Set Form Element Attribute */
	public function setFieldAttr($fieldId, $attributeName, $value)
	{ 
		$this->fields[$fieldId]->setAttr($attributeName,$value); 
	}
	
	/* Set Values From an Array */
	public function setValues($values)
	{
		foreach($values as $fieldId => $value){
			$value = htmlspecialchars($value);							// Lookout for special chars
			if (isset($this->fields[$fieldId])){
				$fieldType = $this->fields[$fieldId]->getFieldType(); 	// Get Field Type
				
				if ($fieldType == "select"){
					// If FieldType is of type `select`, set the default value.
					$this->fields[$fieldId]->setDefault($value);
				}else{
					// Set Value Attribute for a Field
					$this->setFieldAttr($fieldId,"value",$value);
				}
			}
		}	
	}

	/* Load Form Template */	
	public function loadFormTemplate($formFile)
	{
		if (is_file($formFile)){
			$fp = fopen($formFile,"r");								// Open File for Read
			while(!feof($fp)){
				$line = fgets($fp);

				// Get a line that is not blank or start with a #
				// # is reserved for comments
				if ($line != "" || substr($line,0,1) != "#"){
					$line = explode("|",trim($line));			// split line based on pipe char
					switch($line[0]){
						case 0:
							$numFields=(count($line)-1)/2;
							for($i=0;$i<$numFields;++$i){
								$arg1 = $line[1+(2*$i)];
								$arg2 = $line[2+(2*$i)];
								$this->setAttr($arg1,$arg2);
							}
						break;
						case 1:
							// Create item
							$this->createField($line[1],$line[2]);
							// Load Attributes
							$numFields = (count($line)-3)/2;
							for($i=0;$i<$numFields;++$i){
								$arg1 = $line[3+(2*$i)];
								$arg2 = $line[4+(2*$i)];
								$this->setFieldAttr($line[2],$arg1,$arg2);
							}
						break;
						default:
							echo "<b>form:</b> Error building field. Unknown line ID \"{$line[0]}\"";
						break;
					}
				}
			}
		fclose($fp);
		}else{ 
			echo "<b>form:</b> Invalid Path or Filename: \"{$ff}\""; 
		}
	}	
	
	/* Debug Functions */
	public function printAttr()
	{	
		echo "<pre>";print_r($this->formAttribute);echo "</pre>";	
	}
	
	public function printFields()
	{	
		echo "<pre>";print_r($this->fields);echo "</pre>";	
	}	
	
	/* Start Form */
	public function startForm()
	{
		// Print all Attributes for <form> tag
		foreach($this->formAttribute as $attribute => $value){
			$f .= " {$attribute}=\"{$value}\"";
		}
		$form = "\r\n<form{$f}>\r\n";
		// Print all hidden fields
		// ...
		//
	return $form;
	}
	
	/* Print Form Element */
	public function dspField($id)
	{ 
		return $this->fields[$id]->dspField(); 
	}
	
	/* End Form */
	public function endForm()
	{ 
		return "\r\n</form>\r\n"; 
	}
}


/* Form Element Class */
class formField 
{
	private $fieldAttribute;		// Attribute Array
	private $defaultValue;			// Set Default Value
	
// Construct
	public function __construct($fieldType, $fieldId) 
	{
		$this->fieldAttribute = array();
		$this->setAttr("type",$fieldType);
		$this->setAttr("id",str_replace("[]","",$fieldId));
		$this->setAttr("name",$fieldId);
	}

// Private Members
	/* Build Input Field */
	private function buildInput()
	{
		$fieldAttributeList = NULL;					// Reset Field Attributes
		foreach($this->fieldAttribute as $attribute => $value){
			$fieldAttributeList .= " {$attribute}=\"{$value}\"";
		}
	return "<input{$fieldAttributeList} />";
	}

	/* Build Select Field */
	private function buildSelect()
	{
		$fieldAttributeList = NULL;					// Reset Field Attributes
		// Build Field
		foreach($this->fieldAttribute as $attribute => $value){
			if ($attribute != "value" && $attribute != "type"){
				$fieldAttributeList .= " {$attribute}=\"{$value}\"";
			}else{
				$values = $value;
			}
		}
		
		// Set values
		$options = NULL;
		foreach ($values as $value){
			// Build Option
			if (is_array($value)){
				// Set Default value
				(isset($this->defaultValue) && $this->defaultValue == $value[0])
					?$default=" selected=\"selected\"":$default = NULL;
				$finalValueId = $value[0];
				$finalValueDisplay = $value[1];
			}else{
				// Set Default value
				(isset($this->defaultValue) && $this->defaultValue == $value)
					?$default=" selected=\"selected\"":$default = NULL;
				$finalValueId = $value;
				$finalValueDisplay = $value;
			}
			$options .= "\t<option value=\"{$finalValueId}\" {$default}>{$finalValueDisplay}</option>\r\n";
		}
	return "\r\n<select{$fieldAttributeList} >\r\n{$options}</select>\r\n";
	}

	/* Build Text Area */
	private function buildTextarea()
	{
		$fieldAttributeList = NULL;					// Reset Field Attributes
		foreach($this->fieldAttribute as $attribute => $value){
			if ($attribute != "value" && $attribute != "type"){
				$fieldAttributeList .= " {$attribute}=\"{$value}\"";
			}else{
				if ($attribute == "value"){			// 20080623 - Added: `type` was showing in the text area.
					$values = $value;
				}
			}
		}
	return "<textarea{$fieldAttributeList}>{$values}</textarea>";
	}
	
	private function buildHyperlink()
	{
		$fieldAttributeList = NULL;					// Reset Field Attributes
		foreach($this->fieldAttribute as $attribute => $value){
			if ($attribute != "value" && $attribute != "type"){
				$fieldAttributeList .= " {$attribute}=\"{$value}\"";
			}else{
				$values = $value;
			}
		}
	return "<a{$fieldAttributeList}>{$values}</a>";
	}
// Public Members
	/* Debug */
	public function printAttr()
	{ 
		echo "<pre>";print_r($this->fieldAttribute);echo "</pre>";	
	}

	/* Set Default Value */
	public function setDefault($value)
	{		
		$this->defaultValue = $value; 
	}

	/* Set Element Attribute */
	public function setAttr($attributeName, $value)
	{ 
		// utf8_encode and htmlspecialchars functions doesn't work on arrays
		if (!is_array($value)){
			$this->fieldAttribute[$attributeName] = utf8_encode(htmlspecialchars($value));	//001
		}else{
			$this->fieldAttribute[$attributeName] = $value;
		}
	}
	
	/* Get Element Type */
	public function getFieldType()
	{
		return $this->fieldAttribute["type"];	
	}
	
	public function dspField()
	{
		switch($this->fieldAttribute["type"]){
			case "checkbox":
			case "file":
			case "hidden":
			case "password":
			case "radio":
			case "reset":
			case "submit":
			case "image":
			case "text":
				$f = $this->buildInput();
			break;
			case "select":
				$f = $this->buildSelect();
			break;
			case "textarea":
				$f = $this->buildTextArea();
			break;
			case "a":
				$f = $this->buildHyperlink();
			break;
			default:
				echo "Error[dspField]: Invalid Field Type \"{$this->fieldAttribute['type']}\"";
			break;
		}
	return $f;
	}
}


/* Example Source Code */
/*
// Form
$colors = array("red","yellow","blue");
$form = new form();
	$form->loadFormTemplate("../../forms/frm_login.txt");
	$form->createField("select","color",$colors);

// Output	
echo $form->startForm();
	echo $form->dspField("username")."<br/>\r\n";
	echo $form->dspField("password")."<br/>\r\n";
	echo $form->dspField("color")."<br/>\r\n";
	echo $form->dspField("submit")."<br/>\r\n";
echo $form->endForm();

*/
?>
