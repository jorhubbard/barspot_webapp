<?php

abstract class Department_Definition 
{
	abstract public function addPerson(Person_Definition $person);
}

class Department extends Department_Definition 
{
	private $id;
	public $title;
	protected $people;
	
	public function __construct($title){
		$this->title=$title;
		$this->people = array();
	}
	
	public function addPerson(Person_Definition $person){
		array_push($this->people,$person);		
	}
}


?>