<?php 

class calendar_info	{	
	/* COMMON VARIABLES */
		public $curdate;					// Static Current Date
		public $day;						// Day   (01)
		public $month;						// Month (07)
		public $year;						// Year  (2007)
		public $first_day;					// Set Time to the First Day of the Month
		public $title;						// Month Full(January)
		public $day_of_week;				// First Day Falls On A (Sun)
		public $days_in_month;				// Number of Days in Month

	/* CONSTRUCT */
		public function calendar_info()
		{
			$this->set_time_curr(); 		// Set Current Date
		}

	/* FUNCTIONS */
		/* GET EXTENDED CALENDAR INFORMATION */
		public function get_cal_info()
		{
			// GO TO THE FIRST DAY OF THE MONTH
				$this->first_day = mktime(0,0,0,$this->month, 1, $this->year); 
			// GET MONTH BY NAME
				$this->title = date('F', $this->first_day); 
			// Day of Week the first day starts on.
				$this->day_of_week = date('D', $this->first_day); 
				$this->days_in_month = cal_days_in_month(0, $this->month, $this->year);
		}

		/* SET CURRENT TIME */
		public function set_time_curr()
		{
			// TODAY
				$this->curdate =time();
					$this->day = date('d', $this->curdate); 
					$this->month = date('m', $this->curdate);
					$this->year = date('Y', $this->curdate); 
			// GET CALENDAR INFORMATION
				$this->get_cal_info();
		}

		/* SET NEW TIME */
		public function set_time_new($new_month,$new_year)
		{
			// TODAY
				$this->month = $new_month;
				$this->year = $new_year;
			// GET CALENDAR INFORMATION
				$this->get_cal_info();
		}

		/* SET NEW DAY */
		public function set_day($new_day)
		{
			$this->day = $new_day;
		}
} // END CALENDAR_INFO CLASS

// CALENDAR CLASS
class calendar extends calendar_info {
		public $displayControls;
	/* CONSTRUCT */
		public function __construct()
		{
			/* ??? IS THIS THE BEST PLACE FOR THIS ??? */ 
			// SET NEW DAY
				if (isset($_GET['new_day'])) { $new_day=$_GET['new_day']; }
			// SET NEW MONTH
				if (isset($_POST['new_month'])) { 
					$new_month=$_POST['new_month']; 
				}else if (isset($_GET['new_month'])){
					$new_month=$_GET['new_month']; 
				}
			// SET YEAR
				if (isset($_POST['new_year'])) { 
					$new_year=$_POST['new_year']; 
				}else if (isset($_GET['new_year'])){
					$new_year=$_GET['new_year']; 
				}
			/* ??? IS THIS THE BEST PLACE FOR THIS ??? */ 
			// SET NEW TIME
				if (isset($new_year) && isset($new_month)) {
					$this->set_time_new($new_month,$new_year);
				}else{
					$this->set_time_curr(); // CALENDAR CLASS
				}
			// SET NEW DAY
				if (isset($new_day)){
					$this->set_day($new_day);
				}
		}
	/* FUNCTIONS */
		/* DISPLAY CHANGE DATE FORM */
		public function display_changedate_form()
		{
			echo "<form action=\"{$_SERVER['PHP_SELF']}?display=$_GET[display]&id=$_GET[id]\" method=\"POST\">";
			echo "<table width=\"100%\"><tr><td>";
				// CHANGE MONTH
					echo "<select name=\"new_month\" onChange=\"submit()\">";
						for ($x=1; $x<=12;$x++)
						{
							// GET MONTH
							$mm = date("F",mktime(0,0,0,$x+1,0,0));
							echo $mm;
							// ADD LEADING ZERO TO VAR
							($x < 10)?$x="0".$x:$x=$x;				
							// SELECT CURRENT MONTH
							($x == $this->month) ? $def = "SELECTED=\"SELECTED\"" : $def="";	
							echo "<option value=\"$x\" {$def}>{$mm}</option>\n"; // DISPLAY OPTION
						}
					echo "</select> ";
			echo "</td><td align=\"right\">";
				// CHANGE YEAR
					echo "<select name=\"new_year\" onChange=\"submit()\">";
						for ($x = $this->year-5; $x<=$this->year+5;$x++)
						{
							($x == $this->year) ? $def = "SELECTED=\"SELECTED\"" : $def="";
							echo "<option value=\"{$x}\" {$def}>{$x}</option>";
						}
				echo "</select>";
			echo "</td></tr></table>";
			echo "</form>";
		}

		/* DISPLAY CALENDAR */
		public function display_calendar()
		{
			// How many blank days
				switch($this->day_of_week){ 
					case "Sun": $blank = 0; break; 	case "Mon": $blank = 1; break; 	
					case "Tue": $blank = 2; break; 	case "Wed": $blank = 3; break; 	
					case "Thu": $blank = 4; break; 	case "Fri": $blank = 5; break; 
					case "Sat": $blank = 6; break; 
				}
			//Table Headers
				echo "<table id=\"calendar\">";
			/* TITLE 1 NO IMAGES*/ 
				echo "<tr><th colspan=\"13\" class=\"calh\">{$this->title} {$this->year}</th></tr>";
			/* TITLE 2 IMAGES */
				echo "<tr>	
							<td class=\"calh\"><B>S</B></td>
							<td class=\"calh\"><B>M</B></td>
							<td class=\"calh\"><B>T</B></td>
							<td class=\"calh\"><B>W</B></td>
							<td class=\"calh\"><B>T</B></td>
							<td class=\"calh\"><B>F</B></td>
							<td class=\"calh\"><B>S</B></td>
					  </tr>";
				echo "<tr align='right'>";

			//Beginning Blank Days
				$day_count = 1;
				while($blank > 0) { 
					echo "<td class=\"_blanks\"></td>"; 
					$blank=$blank-1; 
					$day_count++;
				} 
				$day_num = 1;	// Day 1

			//Display Day's
				while( $day_num <= $this->days_in_month ) { 
					//SET CELL TYPES
					if ($day_num == date("j") && $this->month == date("n") && $this->year == date("Y"))	{
						echo "<td class=\"_current\">
						<a href='".$_SERVER['PHP_SELF']."?display=$_GET[display]&id=$_GET[id]&new_day=$day_num&new_month=".$this->month."&new_year=".$this->year."'>$day_num</a></td>";
					}else if ($day_num == $this->day){
						echo "<td class=\"_today\">
						<a href='".$_SERVER['PHP_SELF']."?display=$_GET[display]&id=$_GET[id]&new_day=$day_num&new_month=".$this->month."&new_year=".$this->year."'>$day_num</a></td>";
					}else{
						// Normal Day
						echo "<td class=\"_normal\">
						<a href='".$_SERVER['PHP_SELF']."?display=$_GET[display]&id=$_GET[id]&new_day=$day_num&new_month=".$this->month."&new_year=".$this->year."'>$day_num</a></td>";
				
					}
					$day_num++; 
					$day_count++;
					
					//New row at end of the week
					if ($day_count > 7 && $day_num <= $this->days_in_month) {
						echo "</tr><tr align='right'>";
						$day_count = 1;
					}
				} 
			//Finish Blank Days
				while( $day_count >1 && $day_count <=7 ) { 
					echo "<td class=\"_blanks\"></td>"; 
					$day_count++;
				}
				echo "</tr>";
				
			// Display Change Date Form
				if ($this->displayControls == TRUE){
				echo "<tr><td colspan=\"13\" class=\"calh\">";
					$this->display_changedate_form();
				echo "</td></tr>";
				}
				echo "</table>";
		}
		
		// Display Controls
		public function setCalendarControls($state = FALSE){
			$this->displayControls = $state;
		}
		
}// END CALENDAR CLASS


/* DATABASE CONNECTION FOR EVENTS */
	class mysql_events
	{
		/* COMMON VARIABLES */
			private $event_tbl;		// Table
			private $user_id;		// USER ID
			private $mysql;
			
			// CONSTRUCT
			public function __construct($mysql) { 
				$this->mysql = $mysql;
			}
			// SET USER FOR EVENTS
			public function set_user($user_id) { $this->user_id = $user_id;	}
			// SET CONNECTION
			public function set_table($table)	{
				$this->event_tbl=$table;
			}
			// GET MONTH EVENTS
			public function get_months_events($month,$year)	{
				$event_date = $year."-".$month."-";
				// CONNECT TO DATABASE
				$sql = "SELECT * FROM `".$this->event_tbl."` 
						WHERE `event_date` LIKE '".$event_date."%'
						AND   `bar_id`='".$this->user_id."'
						ORDER BY `event_date`";

				$r = $this->mysql->query($sql) or die();
				$nr=0; // NUM OF RECORDS
				while ($mr = $r->fetch_array())	{
					$monthly_events[$nr]['event_date'] = $mr[1];
					$monthly_events[$nr]['event_time'] = $mr[2];
					$monthly_events[$nr]['event_title'] = $mr[3];
					$monthly_events[$nr]['event_text'] = $mr[4];
					$monthly_events[$nr]['user_id'] = $mr[5];
					$nr++;
				}
			return $monthly_events;
			}
			// GET DAILY EVENTS
			public function get_daily_events($myday, $month, $year){
				$event_date = $year."-".$month."-".$myday;
				//CONNECT
					$sql = "SELECT * FROM `".$this->event_tbl."` 
							WHERE `event_date` = '".$event_date."'
							AND   `bar_id`='".$this->user_id."' 
							ORDER BY `event_time`";
							//print("sql = $sql");
				$r = $this->mysql->query($sql) or die("Error getting month events: ".mysql_error());
				$nr=0; // NUM OF RECORDS
				while ($mr = $r->fetch_array()) {
					$todays_events[$nr]['event_date'] = $mr[1];
					$todays_events[$nr]['event_time'] = $mr[2];
					$todays_events[$nr]['event_title'] = $mr[3];
					$todays_events[$nr]['event_text'] = $mr[4];
					$todays_events[$nr]['user_id'] = $mr[5];
					$nr++;
				}
			return $todays_events;
			}
	}// END MYSQLCAL CLASS
	
class events_cal extends mysql_events {
/* DATABASE CLASS */		
		public function __construct($mysql)
		{
			parent::__construct($mysql);
		}
		// DISPLAY MONTH EVENTS
		public function display_months_events($month,$year,$title) {
			echo "<table id=\"event_month\" cellspacing=\"0\" cellpadding=\"0\">
			         <tr><th colspan=\"2\">".$title." ".$year." Events</th>
			         </tr>";
			if ($monthly_events = $this->get_months_events($month,$year)) {
				foreach($monthly_events as $me){
                                  $day = substr($me['event_date'],8,2);
					echo "<tr><td class='caldate' width='50' height='20'>".str_replace($year."-","",$me['event_date'])."</td>";
					    echo "<td class='calevent'><a href='".$_SERVER['PHP_SELF']."?display=$_GET[display]&id=$_GET[id]&new_day=$day&new_month=".$month."&new_year=".$year."'>".$me['event_title']."</a></td></tr>";
				}
			}else{
				echo "<tr><td colspan=\"2\"><B>No Events This Month</B></td></tr>";
			}
			echo "</table>";
		}
		
		// DISPLAY Daily EVENTS
		public function display_daily_events($day,$month,$year,$title) {
			echo "<table id=\"event_day\">
			         <tr><th colspan=\"2\">{$title} {$day}, {$year} Events</th>
			         </tr>";
			if ($todays_events = $this->get_daily_events($day,$month,$year)){
				foreach($todays_events as $te){
					echo "<tr><td id=\"event_day_time\">{$te['event_time']}</td>";
					    echo "<td id=\"event_day_event\">{$te['event_title']}</td></tr>";
					echo "<tr><td></td><td id=\"event_day_body\">{$te['event_text']}</td></tr>";
				}
			}else{
				echo "<tr><td colspan=\"2\"><B>No Events Today</B></td></tr>";
			}
			echo "</table>";
		}
	}// END MYSQL CLASS CLASS
	
?> 
