<?php

/**
 * Extend Person to allow for a title
 */
class Department_Person extends Person
{
	public $title;
	public function __construct(){
		parent::__construct();
	}
}
