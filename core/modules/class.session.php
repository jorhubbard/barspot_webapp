<?php

abstract class Session_Definition
{
	abstract public function attachUser(Person_Definition $user);
//	abstract private function writeSession();
}

class Session extends Session_Definition
{
	private $sessionId;
	private $permissions;
	private $account;
	private $department;
	public $user;
	
	public function __construct($sessionId=NULL){
		session_start();
		// Try to restore session
			if (isset($sessionId)){
				$this->restoreSession($sessionId);
				$this->sessionId = $sessionId;	
			}else{
				$this->sessionId = session_id();
			}
		$_SESSION['sessionId'] = $this->sessionId;
	}
	
	
	public function attachUser(Person_Definition $user){
	    if (isset($_SESSION['user_id'])){
			if ($user->id == $_SESSION['user_id']){
				$this->user = $user;			
			}else{
				echo "Attempt to move different user in";
			}
		}else{
			$_SESSION['user_id'] = $user->id;
			$this->user = $user;			
		}
	}
	
}

?>