<?PHP
/**
 * Class specialException
 * Create Date: 8/4/2008
 *
 *
 * Requires a config.ini.php entry for error and database
 */

/**
 * specialException class
 * Description:
 *		The specialException class will record errors of high severity into a database
 * or email to a technician.
 */
class specialException extends Exception { 
	protected $severity;
	protected $cdate;
	protected $ctime;
	
	public function __construct($code, $message, $severity=NULL)
	{
		// Time Stamp
		$this->cdate = @date("Y-m-d");
		$this->ctime = @date("H:i:s");
		
		// Define Severity Levels
		$this->severity = $severity;	
		
		// Call the parent's constructor to get the normal exception behavior.
		parent::__construct($message, $code);	
		
		// Execute Logging
		$this->attackTheIssue();
	}

	/**
	 * attachTheIssue()
	 * Description: This function will take the severity and determine if the issue is high enough priority
	 * to record the issue in a database.
	 */
	protected function attackTheIssue()
	{
		// How sever is the problem.
		switch($this->severity){
			case 0:	// Low
				// Do Not Log
			break;
			case 1:	// Normal
				// Database Log Only
				$this->writeDatabaseLog();
			break;
			case 2:	// High
				// Database and Email
				$this->writeDatabaseLog();
			break;
			default: 
				// Database Log Only
				$this->writeDatabaseLog();
			break;
		}
		
		// $this->getExceptionMessage();
	}
	
	/**
	 * Format error message to be logged
	 */
	protected function getExceptionMessage()
	{
		$msg = "\n\n{$this->getCode()} : {$this->getMessage()}\n";
		$msg .="Time of error: {$this->cdate} {$this->ctime}\n";
		$msg .="{$this->getTraceAsString()}\n";
	return $msg;
	}
	
	/**
	 * writeDatabaseLog()
	 * Description:
	 * 		Writes an entry into a database specified in the config.ini.php file.
	 *
	 */
	protected function writeDatabaseLog()
	{
		// Global Site Config Variable From Site Configuration
		global $site_cfg;
		try {
			// Connect to the Database
			$mysqli = new mysqli($site_cfg['database']['host'], 
							 	 $site_cfg['database']['user'], 
								 $site_cfg['database']['pass'], 
								 $site_cfg['database']['database']); 

			// Insert Query		
			$message = $mysqli->real_escape_string($this->getMessage());
			$trace = $mysqli->real_escape_string($this->getTraceAsString());			
			$mysqli->query("INSERT INTO `{$site_cfg['errorlog']['dbtable']}`
								VALUES ('',
										'{$this->cdate}',
										'{$this->ctime}',
										'{$this->getCode()}',
										{$this->severity},
										'{$message}',
										'{$trace}')");
			// $mysqli->error;
			// Close Database
			$mysqli->close();
		} catch(Exception $e) {
			// Oops can't connect to the database.
			echo __CLASS__." Unable to write to database.";
		}
	}	

	/* Also note that the methods in the parent class (exception) are marked final -- no 
	overloading them. */
}


/**
 * Extend specialException 
 */
class fileException extends specialException {
	public function __construct($message, $code, $severity=NULL)
	{
		parent::__construct($message, $code, $severity);
		/* Specify Special Code for File Exception */
	}
}
?>