<?
$string = ''; //initialize string
$sql = "select barLat, barLon from bars where barID = '$barID';";
$result = mysql_query($sql);
$row = mysql_fetch_array($result);

$barLat = $row['barLat'];
$barLon = $row['barLon'];

if(isset($barLat) && isset($barLon) )
{

   $string .= "<div id=\"barMap\">
   <h1>MAP</h1>
   <!-- map goes here -->
   <div id=\"barMapSpace\" style=\"width:300px; height: 200px\"></div>
   </div><!-- end bar map-->

   <script type=\"text/javascript\">
   //<![CDATA[

   if (GBrowserIsCompatible()) {

      function createMarker(point,html) {
	 var marker = new GMarker(point);
	 return marker;
      }

      // Display the map, with some controls and set the initial location
      var map = new GMap2(document.getElementById(\"barMapSpace\"));
      map.addControl(new GSmallMapControl());
      map.addControl(new GMapTypeControl());
      map.setCenter(new GLatLng($barLat,$barLon),17);

      var marker = new GMarker(new GLatLng($barLat,$barLon), {draggable: true});
      map.addOverlay(marker);
      GEvent.addListener(marker, \"dragend\", function() {
	    var point =marker.getPoint();
	    map.panTo(point);
	    document.getElementById(\"lat\").value = point.lat();
	    document.getElementById(\"lon\").value = point.lng();
	    });
   }
   else {
      alert(\"Sorry, the Google Maps API is not compatible with this browser\");
   }
   //]]>
   </script>


   ";
}
print $string;

?>
